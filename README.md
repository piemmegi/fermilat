# Fermi-LAT analysis tools

Scripts and tools developed to perform several types of analysis with the Fermi-LAT data.

Toolkit developed by [piemmegi](mailto:monti.guarnieri.p@gmail.com), University of Trieste.

Last update of this guide: 2024/11/12

## Structure of the repository

This repository is structured as follows:
- **Bin**. This folder is a temporary storage for files deemed not worthy of being saved (think of it as a sort-of "scratch" folder)
- **Data**. In this folder it is possible to find all the weekly LAT data files, both the photon and the spacecraft ones. Note that the files are not saved in this repository, for adherence to the Git principles, but they should be placed here, in order for the scripts to work.
- **Documentation**. This folder contains a set of useful guides for using the *fermitools*, the SLAC computing system (*s3df*) and the Flare Advocate scripts.
- **Functions**. As the name suggests, in this folder there are a few files which define a set of functions that are common to several scripts and may be of general interest. Specifically, they are:
	- [`MyFunctions.py`](Functions/MyFunctions.py), which is cloned from the [tbtools](https://gitlab.com/piemmegi/tbtools/-/tree/master) repository and contains general-purpose functions needed to create and show histograms, etc.
	- [`AstroFunctions.py`](Funcionts/AstroFunctions.py), a set of functions written specifically for this repository and for interfacing with the `Fermitools` and `Fermipy`.
- **Images**. As the name suggests, in this folder all the images that will be produced during the analysis will be saved and stored. As above, note that this folder is stored as empty in the repository, for adherence to the Gitlab principles, but it will be filled soon.
- **Outputs**. This folder is the opposite of **Bin**: it is used to store long-term pictures and temporary files which should not be deleted.
- **Scripts**. In this folder all the analysis scripts will be located, with an internal sub-division in folders (each corresponding to a general theme, or a project idea).
- **Tempfiles**. As the name suggests, in this folder all the temporary files produced during the analysis will be stored. As above, note that this folder is stored as empty in the repository, for adherence to the Git principles, but it will be filled during the work. The difference between **Bin** and **Tempfiles** is that the former should contain mainly temporary scripts or guide files, while the latter is for the temporary files created by the scripts (e.g., the *fermitools* `.par` files)

## Known bugs

- In some scripts we use functions from `gammapy`. However, from `astropy=6.0.0` onwards, some compatibility between the modules is lost. To solve this problem it is enough to downgrade `astropy` to the version 5.3.4.
