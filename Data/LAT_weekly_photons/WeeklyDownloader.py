# ==========================================================================================
#                                          HEADER
# ==========================================================================================
# Lo scopo di questo script è scaricare in serie molteplici weekly photon files. 
# Se manca un argomento tra wBEGIN e wEND, lo script scarica tutti i file.
#
# Sintassi di call:
#   python3 WeeklyDownloader.py wBEGIN wEND
#
# Esempio:
#   python3 PrWeeklyDownloader.py 100 200
#
# ==========================================================================================
#                                  PREAMBOLO (load moduli)
# ==========================================================================================
import sys
import subprocess

# ==========================================================================================
#                                  CODIFICA DEI FILE DATI
# ==========================================================================================
# Definisco l'intervallo da analizzare
try:
    wBEGIN = int(sys.argv[1])
    wEND = int(sys.argv[2])
except:
    wBEGIN = None
    wEND = None

print(f"")
print(f"*"*75)
print(f"Download dei weekly photon files da {wBEGIN} a {wEND}...")
print(f"N.B. in caso di estremi fissati a {None}, tutti i weekly files saranno scaricati!")
print(f"*"*75 + '\n')

# Definisco il path dove si trovano i file
prename = "https://heasarc.gsfc.nasa.gov/FTP/fermi/data/lat/weekly/photon/"

if (wBEGIN is not None) & (wEND is not None):
    for week in range(wBEGIN,wEND+1):
        postname = f'lat_photon_weekly_w{week:03d}_p305_v001.fits'
        name = prename + postname
        subprocess.run(['wget','-m','-P','.','-nH','--cut-dirs=4','-np','-e','robots=off',name])
else:
    subprocess.run(['wget','-m','-P','.','-nH','--cut-dirs=4','-np','-e','robots=off',prename])
    
print(f"Job done! \n")