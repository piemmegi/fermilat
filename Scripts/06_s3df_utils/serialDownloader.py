'''
# ==========================================================================================
#                                       HEADER
# ==========================================================================================
The aim of this script is to download serially the HE files produced as output of the FA pipeline.

-------------------------------------------------------------------------------------------
Call syntax:
  	python3 serialDownloader.py [DOM_START] [DOM_STOP]

where DOM is the day of mission to be downloaded (e.g., 06014) and all the files in [START,STOP]
are downloaded
'''

# Header
import subprocess
import sys

# ==========================================================================================
#                                       SCRIPT
# ==========================================================================================
# Determine what file to download
try:
	startFile = int(sys.argv[1])
	stopFile = int(sys.argv[2])
except IndexError:
	print(f"*"*75)
	print(f"startFile and/or stopFile not defined")
	print(f"*"*75)
	sys.exit(1)

# Call multiple times the downloader file
print(f"Attempting to download all files between {startFile} and {stopFile}")
for i in range(startFile,stopFile+1):
	subprocess.run(['python3','fitsDownloader.py',f'{i}'])
print(f"Job done!")