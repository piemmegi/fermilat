'''
# ==========================================================================================
#                                       HEADER
# ==========================================================================================
The aim of this script is to call serially the fitsViewer.py script

-------------------------------------------------------------------------------------------
Call syntax:
  	python3 serialViewer.py [DOM_START] [DOM_STOP] [ROI_RA] [ROI_DEC] [ROI_RADIUS]

where all days from DOM_START to DOM_STOP will be opened, ROI_RA and ROI_DEC are the RA/DEC
coordinates of the point to be investigated, ROI_RADIUS is the maximum distance accepted from the source
'''

# Header
import subprocess
import sys

# ==========================================================================================
#                                       SCRIPT
# ==========================================================================================
# Determine what file to download
try:
	startFile = int(sys.argv[1])
	stopFile = int(sys.argv[2])
	ROI_RA = sys.argv[3]
	ROI_DEC = sys.argv[4]
	ROI_RADIUS = sys.argv[5]
except IndexError:
	print(f"*"*75)
	print(f"Arguments not all well defined!")
	print(f"*"*75)
	sys.exit(1)

# Call multiple times the downloader file
print(f"Attempting to view the content of all the files between {startFile} and {stopFile}")
for i in range(startFile,stopFile+1):
	subprocess.run(['python3','fitsViewer.py',f'{i}',f"{ROI_RA}",f"{ROI_DEC}",f"{ROI_RADIUS}"])
print(f"Job done!")