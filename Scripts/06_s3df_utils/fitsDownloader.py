'''
# ==========================================================================================
#                                       HEADER
# ==========================================================================================
The aim of this script is to download the HE files produced as output of the FA pipeline.

-------------------------------------------------------------------------------------------
Call syntax:
  	python3 fitsDownloader.py [DOM]

where DOM is the day of mission to be downloaded (e.g., 06014)
'''

# Header
import subprocess
import sys

# ==========================================================================================
#                                       SCRIPT
# ==========================================================================================
# Determine what file to download
try:
	numFile = int(sys.argv[1])
except IndexError:
	print(f"*"*75)
	print(f"DOM to be downloaded not defined")
	print(f"*"*75)
	sys.exit(1)

# Download the file file from the NFS farm
cmd = ["scp",f"pietromg@rhel6-64p:/nfs/farm/g/glast/u20/skyWatchers/reprocess/HE/06000/{numFile:05d}.he.fits",
	   "/sdf/home/p/pietromg/pmg-home/FlareAdvocate/datadir"]
print(f"Running command: {cmd}")
subprocess.run(cmd)