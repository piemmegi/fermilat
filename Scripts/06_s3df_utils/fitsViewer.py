'''
# ==========================================================================================
#                                       HEADER
# ==========================================================================================
The aim of this script is to view the HE photons recorded in a single day nearby a point in the sky

-------------------------------------------------------------------------------------------
Call syntax:
    python3 fitsViewer.py [DOM] [ROI_RA] [ROI_DEC] [ROI_RADIUS]

where DOM is the day of mission to be opened, ROI_RA and ROI_DEC are the RA/DEC coordinates of
the point to be investigated, ROI_RADIUS is the maximum distance accepted from the source
'''

# Header
import sys
sys.path.append("/sdf/home/p/pietromg/pmg-home/FlareAdvocate/")
import numpy as np
from astropy.io import fits
from AstroFunctions import angdist

# ==========================================================================================
#                                       SCRIPT
# ==========================================================================================
# Read DOM from argument
print(f"*"*75)
try:
    dom = int(sys.argv[1])
except IndexError:
    print(f"*"*75)
    print(f"DOM to be analyzed not defined")
    print(f"*"*75)
    sys.exit(1)

filename = f"/sdf/home/p/pietromg/pmg-home/FlareAdvocate/datadir/{dom:05d}.he.fits"

# Open the file
with fits.open(filename) as f:
	data = f[1].data

# Parse energy, time and position of the photons
E = data.field("ENERGY")
RA = data.field("RA")
DEC = data.field("DEC")
TIME = data.field("TIME")

# Read the position around which you want to know if there are photons
try:
    ROI_RA = float(sys.argv[2])
    ROI_DEC = float(sys.argv[3])
    ROI_RADIUS = float(sys.argv[4])
except IndexError:
    print(f"*"*75)
    print(f"ROI_RA, ROI_DEC and/or ROI_RADIUS not defined")
    print(f"*"*75)
    sys.exit(1)

print(f"Opening data from DOM {dom}")
print(f"Searching for HE photons around RA = {ROI_RA:.2f}, DEC = {ROI_DEC:.2f}, radius = {ROI_RADIUS:.2f}")

# Compute angular distance between all photons and the given point
distances = angdist(ROI_DEC,ROI_RA,DEC,RA,ifdeg=True)

# Select events within acceptance
cond = (distances <= ROI_RADIUS)
numEvents = np.sum(cond)
RA_CUT = RA[cond]
DEC_CUT = DEC[cond]
E_CUT = E[cond]
TIME_CUT = TIME[cond]
distances_cut = distances[cond]

print(f"{numEvents} events found:")
for i in range(numEvents):
	print(f"\tRA = {RA_CUT[i]:.2f}°, DEC = {DEC_CUT[i]:.2f}°, E = {E_CUT[i]/1000:.2f} GeV, TIME = {TIME_CUT[i]:.2f} s, dist = {distances_cut[i]:.2f}°")

print(f"")
print(f"*"*75)