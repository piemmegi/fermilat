'''
# ==========================================================================================
#                                       HEADER
# ==========================================================================================
The aim of this script is to extract the data of the 4FHL period and call:
- gtselect/gtmktime
- gtbin (in CMAP mode)
- gtltcube/gtexpmap

-------------------------------------------------------------------------------------------
Call syntax:
  	python3 01a_DataExtractor.py

'''
# ==========================================================================================
#                                 PREAMBLE (module load)
# ==========================================================================================
# Import of the Python Standard Library modules required for the analysis
import glob, os, subprocess, sys			# To act on the operative system
import json									# To read and write JSON files
import matplotlib as mpl					# For plotting
import matplotlib.pyplot as plt				# For plotting
import numpy as np							# For numerical analysis
import pandas as pd							# For reading and writing csv files
import warnings								# To deactivate the warnings
import time									# For script timing
import logging								# For logging purposes
import datetime								# For timestamp printing
import copy									# To copy objects

# Import of the astropy/gammapy/fermipy (& fermitools) modules necessary for astrophysics
import astropy.units as u					# Celestial units
from astropy.io import fits
from gammapy.maps import Map, WcsNDMap		# To work with sky maps
from astropy.coordinates import SkyCoord 	# To work with sky frames

# Import of personal functions from custom modules
sys.path.append("../../Functions")
from AstroFunctions import gtselect, gtmktime, gtbin_CMAP, gtltcube, gtexpcube2

# Set the warnings of a few functions
np.seterr(divide='ignore', invalid='ignore')	# To avoid 0/0 errors
mpl.rcParams['backend'] = 'agg'					# Non-interactive backend for plotting
warnings.filterwarnings("ignore", category=DeprecationWarning) # To avoid deprecation warnings

# =========================================================================================
#                                      COMMON DEFINITIONS                                      
# =========================================================================================
# Define the dimension for the fonts in the plots, the figures dimensions and so on
textfont = 20			# Fontsize for labels and legends
dimfig = (12,7)			# Figure dimensions (A4-like)
dimfigbig = (16,12)		# Figure dimensions (a bit larger than A4-like)
dimfiglong = (16,25)	# Figure dimensions (a bit larger and much longer than A4-like)
filetype = '.pdf'		# File format to save the pictures
filedpi = 520			# Resolution used when saving pictures in .png/.jpeg
nptsfit = 10000			# Number of points used in plotting the fits

# ==========================================================================================
#                                  STATUS FILE DATA
# ==========================================================================================
# Open the status file
statusfile = '../statusfile.json'
with open(statusfile) as json_file:
	statusdata = json.load(json_file)

# Extract all the relevant variables from the file
datadir = statusdata['datadir']
imgdir = statusdata['imgdir']
tempdir = statusdata['tempdir']
dataSCdir = statusdata['dataSCdir']
dataPHdir = statusdata['dataPHdir']
catalog_file_4FGL = statusdata['catalog_file_4FGL']
catalog_file_3FHL = statusdata['catalog_file_3FHL']
catalog_file_3FHL_reg = statusdata['catalog_file_3FHL_reg']
catalog_file_3FHL_regAssoc = statusdata['catalog_file_3FHL_regAssoc']

# Define the name for the subfolders (in imgdir and tempdir) where we will save
# the temporary images and files
pretempdir = '04_4FHL'
imgdir = os.path.join(imgdir,pretempdir)
tempdir = os.path.join(tempdir,pretempdir)

# Setting of the logging module
logfile = "debug.log"
logging.basicConfig(format='[%(asctime)s] %(message)s',level=logging.INFO,
					handlers=[logging.FileHandler(logfile,mode='w'),
					logging.StreamHandler()])
logging.info("*"*75)
logging.info(f"Beginning the execution of {__file__}")
logging.info(f"")

# ==========================================================================================
#                                 CONFIG FILE DATA
# ==========================================================================================
# Open the configuration file made for the 1st level trigger and extract the variables
# necessary for the analysis
configfile = os.path.join(tempdir,f'configfile_trignum_0.json')
with open(configfile) as json_file:
	configdata = json.load(json_file)

# Extract all the relevant variables from the file
tstart            = configdata['tstart']             # gtselect
tstop             = configdata['tstop']              # gtselect 
ROI_ra            = configdata['ROI_ra']             # gtselect
ROI_dec           = configdata['ROI_dec']            # gtselect
ROI_rad           = configdata['ROI_rad']            # gtselect
Emin              = configdata['Emin']               # gtselect
Emax              = configdata['Emax']               # gtselect
zmax              = configdata['zmax']               # gtselect
evclass           = configdata['evclass']            # gtselect
evtype            = configdata['evtype']             # gtselect  
roicut            = configdata['roicut']             # gtmktime
filtercond        = configdata['filtercond']         # gtmktime
dcostheta         = configdata['dcostheta']          # gtltcube
binsz             = configdata['binsz']              # gtexpmap
chatter           = configdata['chatter']            # All fermitools
gtmode            = configdata['gtmode']             # All fermitools
PHlistname        = configdata['PHlistname']         # File management
SClistname        = configdata['SClistname']         # File management
SCfilename        = configdata['SCfilename']         # File management
  
CMAP_algorithm    = configdata['CMAP_algorithm']     # Flux maps
CMAP_binsz        = configdata['CMAP_binsz']         # Flux maps
CMAP_nxpix        = configdata['CMAP_nxpix']         # Flux maps
CMAP_nypix        = configdata['CMAP_nypix']         # Flux maps
CMAP_coordsys     = configdata['CMAP_coordsys']      # Flux maps
CMAP_xref         = configdata['CMAP_xref']          # Flux maps
CMAP_yref         = configdata['CMAP_yref']          # Flux maps
CMAP_axisrot      = configdata['CMAP_axisrot']       # Flux maps
CMAP_proj         = configdata['CMAP_proj']          # Flux maps
CMAP_indef        = configdata['CMAP_indef']         # Flux maps
expmap_evtype     = configdata['expmap_evtype']      # gtexpcube2
expmap_irfs       = configdata['expmap_irfs']        # gtexpcube2
expcubes_cmap     = configdata['expcubes_cmap']      # gtexpcube2
expcubes_ebinalg  = configdata['expcubes_ebinalg']   # gtexpcube2
expcubes_enumbins = configdata['expcubes_enumbins']  # gtexpcube2
expcubes_irfs     = configdata['expcubes_irfs']      # gtexpcube2

# ==========================================================================================
#                                 TEMPORARY FILE NAMES
# ==========================================================================================
# Define the names of the .fits, .txt, .xml and .npz files which will be produced in the execution
data_PH_after_gtselect   = os.path.join(tempdir,f"PH_after_gtselect.fits")
data_PH_after_gtmktime   = os.path.join(tempdir,f"PH_after_gtmktime.fits")
data_PH_after_gtbin      = os.path.join(tempdir,f"PH_after_gtbin_{CMAP_proj}_{int(100*CMAP_binsz):02d}.fits")
data_PH_after_gtltcube   = os.path.join(tempdir,f"PH_after_gtltcube.fits")
data_PH_after_gtexpcube2 = os.path.join(tempdir,f"PH_after_gtexpcube2_{CMAP_proj}_{int(100*CMAP_binsz):02d}.fits")

# ==========================================================================================
#                                   DATA EXTRACTION
# ==========================================================================================
# Call gtselect to extract the data in the given ROI, energy band and temporal window
ifDoAnyways = False
if ifDoAnyways:
	logging.info(f"Calling gtselect...")
	gtselect(infile=PHlistname,outfile=data_PH_after_gtselect,
			 ROI_ra=ROI_ra,ROI_dec=ROI_dec,ROI_rad=ROI_rad,Emin=Emin,Emax=Emax,
			 zmax=zmax,tstart=tstart,tstop=tstop,evclass=evclass,evtype=evtype,
			 chatter=chatter,gtmode=gtmode)
	logging.info(f"Done!")
	logging.info(f"")
else:
	logging.info(f"Skipping gtselect...")
	logging.info(f"")

# Call gtmktime to adjust the GTIs following the chosen ROI and other conditions
ifDoAnyways = False
if ifDoAnyways:
	logging.info(f"Calling gtmktime...")
	gtmktime(infile=data_PH_after_gtselect,outfile=data_PH_after_gtmktime,SCfile=SCfilename,
			 ROI_ra=ROI_ra,ROI_dec=ROI_dec,roicut=roicut,filtercond=filtercond,
			 chatter=chatter,gtmode=gtmode)
	logging.info(f"Done!")
	logging.info(f"")
else:
	logging.info(f"Skipping gtmktime...")
	logging.info(f"")

# ==========================================================================================
#                        COUNT MAP PRODUCTION AND EXPOSURE CORRECTION
# ==========================================================================================
# Call gtbin to produce a count map of the sky
ifDoAnyways = True
if ifDoAnyways:
	logging.info(f"Calling gtbin...")
	gtbin_CMAP(infile=data_PH_after_gtmktime,outfile=data_PH_after_gtbin,SCfile=SCfilename,
			   algorithm=CMAP_algorithm,nxpix=CMAP_nxpix,nypix=CMAP_nypix,binsz=CMAP_binsz,
			   coordsys=CMAP_coordsys,xref=CMAP_xref,yref=CMAP_yref,axisrot=CMAP_axisrot,proj=CMAP_proj,
			   chatter=chatter,gtmode=gtmode)
	logging.info(f"Done!")
	logging.info(f"")
else:
	logging.info(f"Skipping gtbin...")
	logging.info(f"")

# Call gtltcube to produce a map of the LAT livetime
ifDoAnyways = False
if ifDoAnyways:
	logging.info(f"Calling gtltcube...")
	gtltcube(infile=data_PH_after_gtmktime,outfile=data_PH_after_gtltcube,SCfile=SCfilename,
			 zmax=zmax,dcostheta=dcostheta,binsz=binsz,chatter=chatter,gtmode=gtmode)
	logging.info(f"Done!")
	logging.info(f"")
else:
	logging.info(f"Skipping gtltcube...")
	logging.info(f"")

# Call gtexpcube2 to produce an exposure map of the LAT
ifDoAnyways = True
if ifDoAnyways:
	logging.info(f"Calling gtexpcube2...")
	gtexpcube2(infile=data_PH_after_gtltcube,outfile=data_PH_after_gtexpcube2,cmap=data_PH_after_gtbin,
				irfs=expcubes_irfs,evtype=expmap_evtype,nxpix=CMAP_nxpix,nypix=CMAP_nypix,binsz=CMAP_binsz,
				coordsys=CMAP_coordsys,xref=CMAP_xref,yref=CMAP_yref,axisrot=CMAP_axisrot,
				proj=CMAP_proj,ebinalg=expcubes_ebinalg,Emin=Emin,Emax=Emax,enumbins=expcubes_enumbins,
				chatter=chatter,gtmode=gtmode)
	logging.info(f"Done!")
	logging.info(f"")
else:
	logging.info(f"Skipping gtexpcube2...")
	logging.info(f"")

# ==========================================================================================
# Final printouts
logging.info(f"")
logging.info(f"Completed the execution of {__file__}")
logging.info("*"*75)