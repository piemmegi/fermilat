'''
# ==========================================================================================
#                                       HEADER
# ==========================================================================================
The aim of this script is to analyze the 4FHL dataset to determine a list of seeds of VHE
sources, which will be later feed as input to the likelihood analysis stage. In this case,
we simply parse the 3FHL catalog.

-------------------------------------------------------------------------------------------
Call syntax:
  	python3 04d_seeder_3FHL.py

'''
# ==========================================================================================
#                                 PREAMBLE (module load)
# ==========================================================================================
# Import of the Python Standard Library modules required for the analysis
import glob, os, subprocess, sys			# To act on the operative system
import json									# To read and write JSON files
import matplotlib as mpl					# For plotting
import matplotlib.pyplot as plt				# For plotting
import numpy as np							# For numerical analysis
import pandas as pd							# For reading and writing csv files
import warnings								# To deactivate the warnings
import time									# For script timing
import logging								# For logging purposes
import datetime								# For timestamp printing
import copy									# To copy objects

# Import of the astropy/gammapy/fermipy (& fermitools) modules necessary for astrophysics
import astropy.units as u					# Celestial units
from astropy.io import fits                 # To open fits files
from gammapy.maps import Map, WcsNDMap		# To work with sky maps
from astropy.coordinates import SkyCoord 	# To work with sky frames

# Imports specific to this script 
from sklearn.cluster import HDBSCAN, OPTICS # Clustering algorithms

# Import of personal functions from custom modules
sys.path.append("../../Functions")
#from MyFunctions import ...
from AstroFunctions import regExtractor, radec2gal

# Set the warnings of a few functions
np.seterr(divide='ignore', invalid='ignore')	# To avoid 0/0 errors
mpl.rcParams['backend'] = 'agg'					# Non-interactive backend for plotting
warnings.filterwarnings("ignore", category=DeprecationWarning) # To avoid deprecation warnings

# =========================================================================================
#                                      COMMON DEFINITIONS                                      
# =========================================================================================
# Define the dimension for the fonts in the plots, the figures dimensions and so on
textfont = 20			# Fontsize for labels and legends
dimfig = (12,7)			# Figure dimensions (A4-like)
dimfigbig = (16,12)		# Figure dimensions (a bit larger than A4-like)
dimfiglong = (16,25)	# Figure dimensions (a bit larger and much longer than A4-like)
filetype = '.pdf'		# File format to save the pictures
filedpi = 520			# Resolution used when saving pictures in .png/.jpeg
nptsfit = 10000			# Number of points used in plotting the fits

# ==========================================================================================
#                                  STATUS FILE DATA
# ==========================================================================================
# Open the status file
statusfile = '../statusfile.json'
with open(statusfile) as json_file:
	statusdata = json.load(json_file)

# Extract all the relevant variables from the file
datadir = statusdata['datadir']
imgdir = statusdata['imgdir']
tempdir = statusdata['tempdir']
dataSCdir = statusdata['dataSCdir']
dataPHdir = statusdata['dataPHdir']
catalog_file_4FGL = statusdata['catalog_file_4FGL']
catalog_file_3FHL = statusdata['catalog_file_3FHL']
catalog_file_3FHL_reg = statusdata['catalog_file_3FHL_reg']
catalog_file_3FHL_regAssoc = statusdata['catalog_file_3FHL_regAssoc']

# Define the name for the subfolders (in imgdir and tempdir) where we will save
# the temporary images and files
pretempdir = '04_4FHL'
imgdir = os.path.join(imgdir,pretempdir)
tempdir = os.path.join(tempdir,pretempdir)

# Setting of the logging module
logfile = "debug.log"
logging.basicConfig(format='[%(asctime)s] %(message)s',level=logging.INFO,
					handlers=[logging.FileHandler(logfile,mode='w'),
					logging.StreamHandler()])
logging.info("*"*75)
logging.info(f"Beginning the execution of {__file__}")
logging.info(f"")

# ==========================================================================================
#                                 DATA EXTRACTION
# ==========================================================================================
# Open the catalog file and parse all the entires
_, source_coords = regExtractor(catalog_file_3FHL_reg, catalog_file_3FHL_regAssoc, 
								ifWholeCatalog = True, fManageExtended = 1, verbosity = 0)

numSources = np.shape(source_coords)[1]
logging.info(f"{numSources} sources parsed from the 3FHL catalog")

# Convert the source coordinates from RA/DEC to galactic
source_ra = source_coords[0,:]
source_dec = source_coords[1,:]
source_blong, source_blat = radec2gal(source_ra, source_dec)

# ==========================================================================================
#                                 OUTPUT STAGE
# ==========================================================================================
# Prepare the output file, depending on the method chosen
outfilename = os.path.join(tempdir,"04_seedlists",f'seedlist_3FHL.npz')

logging.info(f"\nSaving the cluster data in file {outfilename}")
np.savez_compressed(outfilename, source_blat = source_blat,
								 source_blong = source_blong)
logging.info("Saving completed!")

# ==========================================================================================
# Final printouts
logging.info(f"")
logging.info(f"Completed the execution of {__file__}")
logging.info("*"*75)