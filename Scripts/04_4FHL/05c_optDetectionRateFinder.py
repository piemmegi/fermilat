'''
# ==========================================================================================
#                                       HEADER
# ==========================================================================================
The aim of this script is to provide an alternative to the 05c_optConfigFinder script.
In other words, in this case we find the optimal configuration of the pgwave2D parameters
as the one that maximises the detection sensitivity. This means that, for each configuration,
we must find the source flux at which we have TPR > a given threshold (e.g. 0.9). Then, we
minimize this flux.

-------------------------------------------------------------------------------------------
Call syntax:
  	python3 05c_optDetectionRateFinder.py [catalog]

Arguments:
- catalog: "4FGL" or "3FHL", the model used for the analysis

'''
# ==========================================================================================
#                                 PREAMBLE (module load)
# ==========================================================================================
# Import of the Python Standard Library modules required for the analysis
import glob, os, subprocess, sys            # To act on the operative system
import json                                 # To read and write JSON files
import matplotlib as mpl                    # For plotting
import matplotlib.pyplot as plt             # For plotting
import numpy as np                          # For numerical analysis
import warnings                             # To deactivate the warnings
import time                                 # For script timing
import logging                              # For logging purposes
import datetime                             # For timestamp printing
import copy                                 # To copy objects
from lmfit import Model                     # To do advanced fitting

# Import of personal functions from custom modules
sys.path.append("../../Functions")
from MyFunctions import histoplotter1D, histoplotter2D, truehisto1D, truebins
from AstroFunctions import angdist, duplicateRemoval

# Set the warnings of a few functions
np.seterr(divide='ignore', invalid='ignore')    # To avoid 0/0 errors
mpl.rcParams['backend'] = 'agg'                 # Non-interactive backend for plotting
warnings.filterwarnings("ignore", category=DeprecationWarning) # To avoid deprecation warnings

# =========================================================================================
#                                      COMMON DEFINITIONS                                      
# =========================================================================================
# Define the dimension for the fonts in the plots, the figures dimensions and so on
textfont = 20           # Fontsize for labels and legends
dimfig = (12,7)         # Figure dimensions (A4-like)
dimfigbig = (16,12)     # Figure dimensions (a bit larger than A4-like)
dimfiglong = (16,25)    # Figure dimensions (a bit larger and much longer than A4-like)
filetype = '.pdf'       # File format to save the pictures
filedpi = 520           # Resolution used when saving pictures in .png/.jpeg
nptsfit = 10000         # Number of points used in plotting the fits

# ==========================================================================================
#                                  STATUS FILE DATA
# ==========================================================================================
# Open the status file
statusfile = '../statusfile.json'
with open(statusfile) as json_file:
    statusdata = json.load(json_file)

# Extract all the relevant variables from the file
datadir = statusdata['datadir']
imgdir = statusdata['imgdir']
tempdir = statusdata['tempdir']
dataSCdir = statusdata['dataSCdir']
dataPHdir = statusdata['dataPHdir']
catalog_file_4FGL = statusdata['catalog_file_4FGL']
catalog_file_3FHL = statusdata['catalog_file_3FHL']
catalog_file_3FHL_reg = statusdata['catalog_file_3FHL_reg']
catalog_file_3FHL_regAssoc = statusdata['catalog_file_3FHL_regAssoc']

# Define the name for the subfolders (in imgdir and tempdir) where we will save
# the temporary images and files
pretempdir = '04_4FHL'
imgdir = os.path.join(imgdir,pretempdir)
tempdir = os.path.join(tempdir,pretempdir)

# Setting of the logging module
logfile = "debug.log"
logging.basicConfig(format='[%(asctime)s] %(message)s',level=logging.INFO,
                    handlers=[logging.FileHandler(logfile,mode='w'),
                    logging.StreamHandler()])
logging.info("*"*75)
logging.info(f"Beginning the execution of {__file__}")
logging.info(f"")

# ==========================================================================================
#                                  CUSTOM FUNCTIONS
# ==========================================================================================
# Define a general-purpose hyperbolic tangent function and the corresponding fitter
def detectionRateFun(x,a,b,c,d):
    return( c*np.tanh(a*(x-b)) + d)

def fitter_detectionRate(xvec,yvec,yerr = None,fitcut_low = None,fitcut_up = None,p0=None):
    # Extract from the data the region to be fitted. If one of the two fit region boundaries
    # isn't defined, then ignore it.
    if (fitcut_low is not None) & (fitcut_up is not None):
        cond = (xvec >= fitcut_low) & (xvec <= fitcut_up)
    elif (fitcut_low is not None) & (fitcut_up is None):
        cond = (xvec >= fitcut_low)
    elif (fitcut_low is None) & (fitcut_up is not None):
        cond = (xvec <= fitcut_up)
    else:
        cond = (xvec == xvec)
    
    xfit = xvec[cond]
    yfit = yvec[cond]
    
    # Define the starting points for the fit.
    start_a = 1e11
    start_b = xfit[yfit >= (np.min(yfit) + np.max(yfit))/2][0]
    start_c = (np.max(yfit)-np.min(yfit))/2
    start_d = np.min(yfit)+start_c

    # Perform fit and extract parameters. Use weights if necessary
    gmodel = Model(detectionRateFun)
    
    pesi = 1/yerr[cond]
    weightcond = (yfit > 0)
    xfit = xfit[weightcond]
    yfit = yfit[weightcond]
    pesi = pesi[weightcond]
    result = gmodel.fit(yfit, x=xfit, a=start_a, b=start_b, c=start_c, d=start_d, weights = pesi, scale_covar = False)

    # Extract the parameters
    a,b,c,d = result.params['a'], result.params['b'], result.params['c'], result.params['d']

    # Build thoretical x and y vector
    nptsfit = 10000
    xth = np.linspace(np.nanmin(xvec),np.nanmax(xvec),nptsfit)
    yth = detectionRateFun(xth,a,b,c,d)
    
    # Return fit results
    return(result,xth,yth)

# ==========================================================================================
#                                 INPUT PARSING
# ==========================================================================================
# Parse the command-line to know which model to use for the simulations
try:
    modelcatalog = sys.argv[1]
    if (modelcatalog != "3FHL") & (modelcatalog != "4FGL"):
        modelcatalog = "4FGL"
        logging.info(f"modelcatalog variable vas badly set from command line. Defaulting to {modelcatalog}...")    
except:
    modelcatalog = "4FGL"
    logging.info(f"modelcatalog variable vas badly set from command line. Defaulting to {modelcatalog}...")

logging.info(f"Input check: modelcatalog = {modelcatalog}")

# ==========================================================================================
#                               QUADRANT ITERATION
# ==========================================================================================
# Determine the quadrants to look at
quadrantInds = list(range(44))
numQuadrants = len(quadrantInds)
logging.info(f"Quadrants to analyze: {quadrantInds}")

quadrantFile = "./SkyQuadrants.dat"
ROI_lat_min, ROI_lat_max, _, _ = np.loadtxt(quadrantFile, skiprows = 1, unpack = True, dtype = np.float64, delimiter = "\t")


# Define what combinations to examine here. Note: we assume that all the quadrants have
# the same number of points (= configurations of pgwave2D tested).
fermipy_simdir = f'05_{modelcatalog}_simCMAPs'
pgwave_simdir = f'05_{modelcatalog}_simPgwaveOut'
filename_pgwave = os.path.join(tempdir, pgwave_simdir, f"Q{quadrantInds[0]:02d}",'*')
numCombinations = len(glob.glob(filename_pgwave))

# Define the binning vector of the fluxes, which we will use to determine the algorithm
# sensitivity in different configurations
if modelcatalog == "3FHL":
    # Simply approach: regular spacing in the available interval (in log scale)
    exp0 = -12
    exp1 = -10.3
    nBinsPerDecade = 5
    expdiff = int(np.abs(exp0-exp1-1))
    fluxBins = np.logspace(exp0,exp1,expdiff*nBinsPerDecade)
else:
    # Use a packed log spacing at low fluxes, then a less packed log spacing, then a single bin
    fluxBins = np.hstack((np.logspace(-12.5,-10,19),np.logspace(-10,-9,6)[1:-1])) # approx starting from 5e-13

numBinsInFlux = np.shape(fluxBins)[0]-1
tfluxBins = truebins(fluxBins,iflog=True)

# Iterate on all the quadrants. The aim is to compute, for each one and for each hyperparameter
# configuration, the amount of correctly or wrongly associated sources (so we need the output
# from the fermipy simulation)
mindist_thr = 0.3      # Min dist in deg between a pg source and a real one, for the association
egal_cutoff = 10       # Consider egal any ROI with absolute latitude of the center above this value

all_ifegals = np.zeros((numQuadrants,),dtype=bool)
all_scala = np.zeros((numQuadrants,numCombinations))
all_otpix = np.zeros_like(all_scala)
all_histoFluxRealSources = np.zeros((numQuadrants,numBinsInFlux),dtype=np.float64)
all_histoFluxPgMatchingSources = np.zeros((numQuadrants,numCombinations,numBinsInFlux),dtype=np.float64)
all_numPgSources = np.zeros((numQuadrants,numCombinations),dtype=np.float64)
all_numPgUnmatched = np.zeros_like(all_numPgSources)

for i,quadrantInd in enumerate(quadrantInds):
    # ======================================================================================
    #                                    PRELIMINARY
    # ======================================================================================
    # Are we gal or egal?
    ROI_glat_center = (ROI_lat_min + ROI_lat_max)/2
    all_ifegals[i] = (np.abs(ROI_glat_center)[i] >= egal_cutoff)

    # ======================================================================================
    #                               LOAD TRUE SOURCES DATA
    # ======================================================================================
    # Load the data from the fermipy simulation: extract the list of simulated sources (for this
    # quadrant)
    printstr = 'egal' if all_ifegals[i] else 'gal'

    logging.info("*"*25)
    logging.info(f"Working on quadrant {quadrantInd} (type: {printstr})")
    logging.info("*"*25)

    filename_fermipy = os.path.join(tempdir,fermipy_simdir,f'pgSIM_fermisim_Q{quadrantInd:02d}.npz')
    with np.load(filename_fermipy) as f:
        realSources_blat = f['all_blat']
        realSources_blong = f['all_blong']
        realSources_flux = f['allflux']

    numRealSources = np.shape(realSources_flux)[0]

    # Compute a histogram of the real sources, using as reference the binning vector in flux
    # defined above
    histoFluxRealSources, binsFluxRealSources = truehisto1D(realSources_flux,bins=fluxBins)
    all_histoFluxRealSources[i,:] = histoFluxRealSources

    # ======================================================================================
    #                            ITERATE ON PGWAVE CONFIGS
    # ======================================================================================
    # Iterate on all the configurations tested with pgwave2D
    filename_pgwave = os.path.join(tempdir, pgwave_simdir, f"Q{quadrantInd:02d}",'*')
    allfiles = glob.glob(filename_pgwave)
    allfiles.sort()

    for j,file in enumerate(allfiles):
        # ==================================================================================
        #                               LOAD PG SOURCES DATA
        # ==================================================================================
        # Parse the name of the file to compute the values of the parametrs. Remember that the
        # names are such as "/path/.../pgSIM_scala_2.0_otpix_2.npz"
        all_scala[i,j] = float(file.split('/')[-1].split('_')[2])
        all_otpix[i,j] = float(file.split('/')[-1].split('_')[4].split('.')[0]) # Remove trailing ".npz"
    
        # Open the file and extract the sources coordinates
        with np.load(file) as f:
            pgSources_blat = f['pg_b']
            pgSources_blong = f['pg_l']

        all_numPgSources[i,j] = np.shape(pgSources_blat)[0]
        
        # ==================================================================================
        #                                  MATCH THE SOURCES
        # ==================================================================================
        # Associate the sources
        fluxPgMatching = []     # Real fluxes of the associated pg sources
        indsPgMatching = []     # Which pgwave sources were associated to some real source

        for k in range(numRealSources):
            # Compute the distance from the k-th real source to all the pgwave ones and see if the closest
            # is under threshold. If so, we have a match and we should save the flux of this source, so
            # we know that it can be found!
            thisdist = angdist(realSources_blat[k],realSources_blong[k],pgSources_blat,pgSources_blong,ifdeg=True)
            if np.min(thisdist) <= mindist_thr:
                fluxPgMatching.append(realSources_flux[k])
                indsPgMatching.append(np.argmin(thisdist)) # thisdist has dim = (numPgSources,)

        fluxPgMatching = np.array(fluxPgMatching)

        # Determine how many pgwave sources were never associated to the real ones
        all_numPgUnmatched[i,j] = all_numPgSources[i,j] - len(set(indsPgMatching))
        
        # Now compute the same histogram as above: fluxes of the sources, but this time only
        # the associated ones
        histoFluxPgMatchingSources, _ = truehisto1D(fluxPgMatching,bins=fluxBins)

        # Save the outputs
        all_histoFluxPgMatchingSources[i,j,:] = histoFluxPgMatchingSources       
        
    logging.info("Done!")
    logging.info("")

# ==========================================================================================
#                               GAL/EGAL SEPARATION
# ==========================================================================================
# Separate the parameters (scala, otpix, histograms) in galactic and extragalatic
all_gal_scala = all_scala[all_ifegals == False]
all_gal_otpix = all_otpix[all_ifegals == False]

all_egal_scala = all_scala[all_ifegals]
all_egal_otpix = all_otpix[all_ifegals]

all_gal_histoFluxRealSources = all_histoFluxRealSources[all_ifegals == False,:]
all_gal_histoFluxPgMatchingSources = all_histoFluxPgMatchingSources[all_ifegals == False,:,:]

all_egal_histoFluxRealSources = all_histoFluxRealSources[all_ifegals,:]
all_egal_histoFluxPgMatchingSources = all_histoFluxPgMatchingSources[all_ifegals,:,:]

all_gal_PgUnmatched = all_numPgUnmatched[all_ifegals == False,:]
all_gal_PgSources = all_numPgSources[all_ifegals == False,:]
all_egal_PgUnmatched = all_numPgUnmatched[all_ifegals,:]
all_egal_PgSources = all_numPgSources[all_ifegals,:]

# ==========================================================================================
#                               REDUCTIONS
# ==========================================================================================
# Compute the configurations available. These can be taken from any row in the "all_scala"
# and "all_otpix" matrixes, since these should be equal
all_gal_scala = all_gal_scala.mean(axis=0)
all_gal_otpix = all_gal_otpix.mean(axis=0)
all_egal_scala = all_egal_scala.mean(axis=0)
all_egal_otpix = all_egal_otpix.mean(axis=0)

all_gal_PgUnmatched = all_gal_PgUnmatched.sum(axis=0)
all_gal_PgSources = all_gal_PgSources.sum(axis=0)
all_egal_PgUnmatched = all_egal_PgUnmatched.sum(axis=0)
all_egal_PgSources = all_egal_PgSources.sum(axis=0)

all_gal_FPR = all_gal_PgUnmatched / all_gal_PgSources
all_egal_FPR = all_egal_PgUnmatched / all_egal_PgSources

logging.info(f"Configurations tested for gal ROIs:")
logging.info(f"\t scala: {np.unique(all_gal_scala)}")
logging.info(f"\t otpix: {np.unique(all_gal_otpix)}")
logging.info(f"")
logging.info(f"Configurations tested for egal ROIs:")
logging.info(f"\t scala: {np.unique(all_egal_scala)}")
logging.info(f"\t otpix: {np.unique(all_egal_otpix)}")
logging.info(f"")

# Now sum the histograms of the associated sources, keeping the flux dependence
all_gal_histoFluxRealSources = all_gal_histoFluxRealSources.sum(axis=0)             # New dim = (numBinsInFlux,)
all_gal_histoFluxPgMatchingSources = all_gal_histoFluxPgMatchingSources.sum(axis=0) # New dim = (numCombinations,numBinsInFlux)

all_egal_histoFluxRealSources = all_egal_histoFluxRealSources.sum(axis=0)             # New dim = (numBinsInFlux,)
all_egal_histoFluxPgMatchingSources = all_egal_histoFluxPgMatchingSources.sum(axis=0) # New dim = (numCombinations,numBinsInFlux)

# Associate Poissonian errors to all the histograms
all_gal_histoErrRealSources = np.sqrt(all_gal_histoFluxRealSources)
all_gal_histoErrPgMatchingSources = np.sqrt(all_gal_histoFluxPgMatchingSources)

all_egal_histoErrRealSources = np.sqrt(all_egal_histoFluxRealSources)
all_egal_histoErrPgMatchingSources = np.sqrt(all_egal_histoFluxPgMatchingSources)


# ==========================================================================================
#                               OPTIMAL CONFIG FINDER
# ==========================================================================================
# Iterate on all the combinations and compute the pgwave2D sensitivity as the flex point
# of a tanh function fitter on the TPR curve. Start with gal
thrTPR = 0.8

all_gal_f0 = np.zeros((numCombinations,),dtype=np.float64)
all_gal_f0err = np.zeros_like(all_gal_f0)
all_gal_TPR = np.zeros_like(all_gal_f0)
logging.info("Looking for the optimal parameters config...")

for i in range(numCombinations):
    # Compute the TPR for this config
    thisTPR = all_gal_histoFluxPgMatchingSources[i,:] / all_gal_histoFluxRealSources
    thisTPRerr = thisTPR * np.sqrt( (all_gal_histoErrRealSources / all_gal_histoFluxRealSources)**2 + \
                                    (all_gal_histoErrPgMatchingSources[i,:] / all_gal_histoFluxPgMatchingSources[i,:])**2 )

    # Perform the fit
    cutcond = (thisTPR > 0)
    result, xth, yth = fitter_detectionRate(tfluxBins[cutcond],thisTPR[cutcond],thisTPRerr[cutcond])

    # Extract f0 and the flux at given threshold
    all_gal_f0[i], all_gal_f0err[i] = result.params['b'].value, result.params['b'].stderr
    try:
        all_gal_TPR[i] = xth[yth >= thrTPR][0]
    except:
        all_gal_TPR[i] = np.nan

logging.info("Found the optimal point for gal quadrants!")

# Repeat for egal
all_egal_f0 = np.zeros((numCombinations,),dtype=np.float64)
all_egal_f0err = np.zeros_like(all_egal_f0)
all_egal_TPR = np.zeros_like(all_egal_f0)

for i in range(numCombinations):
    # Compute the TPR for this config
    thisTPR = all_egal_histoFluxPgMatchingSources[i,:] / all_egal_histoFluxRealSources
    thisTPRerr = thisTPR * np.sqrt( (all_egal_histoErrRealSources / all_egal_histoFluxRealSources)**2 + \
                                    (all_egal_histoErrPgMatchingSources[i,:] / all_egal_histoFluxPgMatchingSources[i,:])**2 )

    # Perform the fit
    cutcond = (thisTPR > 0)
    result, xth, yth = fitter_detectionRate(tfluxBins[cutcond],thisTPR[cutcond],thisTPRerr[cutcond])

    # Extract f0
    all_egal_f0[i], all_egal_f0err[i] = result.params['b'].value, result.params['b'].stderr
    try:
        all_egal_TPR[i] = xth[yth >= thrTPR][0]
    except:
        all_egal_TPR[i] = np.nan

logging.info("Found the optimal point for egal quadrants!")
logging.info("")

# Find the optimal configurations
#priority = 'f0'
priority = 'FPR'

if priority == 'f0':
    # Minimize the f0
    best_ind_gal = np.argmin(all_gal_f0)
    best_ind_egal = np.argmin(all_egal_f0)
else:
    # Cut on the flux at fixed TPR, then minimize the FPR
    thrTPR_gal = 5e-11
    thrTPR_egal = 1.75e-11
    
    allinds = np.array(list(range(numCombinations)))
    cutcond = (all_gal_TPR <= thrTPR_gal) & (np.isnan(all_gal_TPR) == False)
    indreduced = np.argmin(all_gal_FPR[cutcond])
    best_ind_gal = allinds[cutcond][indreduced]

    cutcond = (all_egal_TPR <= thrTPR_egal) & (np.isnan(all_egal_TPR) == False)
    indreduced = np.argmin(all_egal_FPR[cutcond])
    best_ind_egal = allinds[cutcond][indreduced]

# Extract the corresponding values of scala and otpix
best_gal_scala = all_egal_scala[best_ind_gal]
best_gal_otpix = all_egal_otpix[best_ind_gal]

best_egal_scala = all_egal_scala[best_ind_egal]
best_egal_otpix = all_egal_otpix[best_ind_egal]

best_gal_f0 = all_gal_f0[best_ind_gal]
best_gal_f0err = all_gal_f0err[best_ind_gal]
best_egal_f0 = all_egal_f0[best_ind_egal]
best_egal_f0err = all_egal_f0err[best_ind_egal]

best_gal_TPR = all_gal_TPR[best_ind_gal] 
best_egal_TPR = all_egal_TPR[best_ind_egal]
best_gal_FPR = all_gal_FPR[best_ind_gal]
best_egal_FPR = all_egal_FPR[best_ind_egal]

logging.info("Optimal configuration:")
logging.info(f"\t gal: scala = {best_gal_scala}, otpix = {best_gal_otpix}")
logging.info(f"\t egal: scala = {best_egal_scala}, otpix = {best_egal_otpix}")
logging.info("f0 values for the optimal configuration:")
logging.info(f"\t gal: {best_gal_f0:.2e} +/- {best_gal_f0err:.2e}") 
logging.info(f"\t egal: {best_egal_f0:.2e} +/- {best_egal_f0err:.2e}")
logging.info("") 
logging.info(f"Flux at fixed detection rate ({thrTPR}) values for the optimal configuration:")
logging.info(f"\t gal: {best_gal_TPR:.2e}") 
logging.info(f"\t egal: {best_egal_TPR:.2e}")
logging.info("")
logging.info("FPR values for the optimal configuration:")
logging.info(f"\t gal: {best_gal_FPR:.2f}") 
logging.info(f"\t egal: {best_egal_FPR:.2f}")
logging.info("") 

# Save the results
outfile = os.path.join(tempdir, pgwave_simdir, 'pgwave2D_bestconfig.npz')

np.savez_compressed(outfile, best_gal_scala = best_gal_scala,
                             best_gal_otpix = best_gal_otpix,
                             best_egal_scala = best_egal_scala,
                             best_egal_otpix = best_egal_otpix)

logging.info("Saving the best configuration in the file:")
logging.info(outfile)
logging.info("")

# ==========================================================================================
#                               PLOTTING / 2D
# ==========================================================================================
# Let's show how the sensitivity (f0) changes as a function of scala and otpix in gal and egal
# To do so, first reshape the f0 matrix in 2D
binvecScalas = np.unique(all_gal_scala)
binvecOtpix = np.unique(all_gal_otpix)
numScalas = np.shape(binvecScalas)[0]
numOtpix = np.shape(binvecOtpix)[0]

# Adjust the binning vectors to allow theit use with histoplotter2D
dx = (binvecScalas[1] - binvecScalas[0])
binvecScalas[0] = binvecScalas[0] - dx/2
binvecScalas[-1] = binvecScalas[-1] + dx/2

dy = (binvecOtpix[1] - binvecOtpix[0])
binvecOtpix[0] = binvecOtpix[0] - dy/2
binvecOtpix[-1] = binvecOtpix[-1] + dy/2

# Define variables useful for 2D plotting
slope = None
iflog = False
ifcbarfont = True

# ==========================================================================================
# Plot the FPR map
cmap = 'gnuplot2'
new_gal_FPR = np.reshape(all_gal_FPR,(numScalas,numOtpix))

logging.info("Plotting the FPR heatmaps in 2D...")
figname = os.path.join(imgdir,'05_optDetectionRateFinder',f'pgwave_{modelcatalog}_paramscan_FPR_gal.pdf')

fig,ax = plt.subplots(figsize = dimfig)
histoplotter2D(ax,binvecScalas,binvecOtpix,new_gal_FPR,cmap,slope,'scala','otpix',textfont,iflog,
                ifcbarfont = ifcbarfont, cbarlabel = 'FPR')
ax.set_title("Galactic ROIs",fontsize=1.5*textfont,pad=20)
fig.set_tight_layout('tight')
fig.savefig(figname)
plt.close(fig)

# Plot the FPR map
new_egal_FPR = np.reshape(all_egal_FPR,(numScalas,numOtpix))

figname = os.path.join(imgdir,'05_optDetectionRateFinder',f'pgwave_{modelcatalog}_paramscan_FPR_egal.pdf')

fig,ax = plt.subplots(figsize = dimfig)
histoplotter2D(ax,binvecScalas,binvecOtpix,new_egal_FPR,cmap,slope,'scala','otpix',textfont,iflog,
                ifcbarfont = ifcbarfont, cbarlabel = 'FPR')
ax.set_title("Extra-galactic ROIs",fontsize=1.5*textfont,pad=20)
fig.set_tight_layout('tight')
fig.savefig(figname)
plt.close(fig)

logging.info("Done!")

# ==========================================================================================
# Plot the detection rate map
cmap = 'gnuplot2_r'
new_gal_f0 = np.reshape(all_gal_f0,(numScalas,numOtpix))

logging.info("Plotting the f0 heatmaps in 2D...")
figname = os.path.join(imgdir,'05_optDetectionRateFinder',f'pgwave_{modelcatalog}_paramscan_f0_gal.pdf')

fig,ax = plt.subplots(figsize = dimfig)
histoplotter2D(ax,binvecScalas,binvecOtpix,new_gal_f0,cmap,slope,'scala','otpix',textfont,iflog,
                ifcbarfont = ifcbarfont, cbarlabel = 'f0 [photons / (cm2 s)]')
ax.set_title("Galactic ROIs",fontsize=1.5*textfont,pad=20)
fig.set_tight_layout('tight')
fig.savefig(figname)
plt.close(fig)

# Repeat but for egal
new_egal_f0 = np.reshape(all_egal_f0,(numScalas,numOtpix))

figname = os.path.join(imgdir,'05_optDetectionRateFinder',f'pgwave_{modelcatalog}_paramscan_f0_egal.pdf')

fig,ax = plt.subplots(figsize = dimfig)
histoplotter2D(ax,binvecScalas,binvecOtpix,new_egal_f0,cmap,slope,'scala','otpix',textfont,iflog,
                ifcbarfont = ifcbarfont, cbarlabel = 'f0 [photons / (cm2 s)]')
ax.set_title("Extra-galactic ROIs",fontsize=1.5*textfont,pad=20)

fig.set_tight_layout('tight')
fig.savefig(figname)
plt.close(fig)
logging.info("Done!")
logging.info("")

# ==========================================================================================
# Plot the detection rate map
cmap = 'gnuplot2_r'
new_gal_TPR = np.reshape(all_gal_TPR,(numScalas,numOtpix))

logging.info("Plotting the f0 heatmaps in 2D...")
figname = os.path.join(imgdir,'05_optDetectionRateFinder',f'pgwave_{modelcatalog}_paramscan_flux08_gal.pdf')

fig,ax = plt.subplots(figsize = dimfig)
histoplotter2D(ax,binvecScalas,binvecOtpix,new_gal_TPR,cmap,slope,'scala','otpix',textfont,iflog,
                ifcbarfont = ifcbarfont, cbarlabel = f'Flux @ TPR = {thrTPR} [photons / (cm2 s)]')
ax.set_title("Galactic ROIs",fontsize=1.5*textfont,pad=20)
fig.set_tight_layout('tight')
fig.savefig(figname)
plt.close(fig)

# Repeat but for egal
new_egal_TPR = np.reshape(all_egal_TPR,(numScalas,numOtpix))

figname = os.path.join(imgdir,'05_optDetectionRateFinder',f'pgwave_{modelcatalog}_paramscan_flux08_egal.pdf')

fig,ax = plt.subplots(figsize = dimfig)
histoplotter2D(ax,binvecScalas,binvecOtpix,new_egal_TPR,cmap,slope,'scala','otpix',textfont,iflog,
                ifcbarfont = ifcbarfont, cbarlabel = f'Flux @ TPR = {thrTPR} [photons / (cm2 s)]')
ax.set_title("Extra-galactic ROIs",fontsize=1.5*textfont,pad=20)

fig.set_tight_layout('tight')
fig.savefig(figname)
plt.close(fig)
logging.info("Done!")
logging.info("")

# ==========================================================================================
#                          PLOTTING / 1D HISTOGRAMS
# ==========================================================================================
# Show the distribution of the simulated and matched sources
logging.info("Plotting the detection counts histograms in 1D...")
figname = os.path.join(imgdir,'05_optDetectionRateFinder',f'pgwave_{modelcatalog}_bestconfig_fluxDistribution.pdf')

fig,ax = plt.subplots(figsize = dimfig)
histoplotter1D(ax,tfluxBins,all_gal_histoFluxPgMatchingSources[best_ind_gal,:],leglabel='Matched sources (gal)',hcolor='cyan')
histoplotter1D(ax,tfluxBins,all_gal_histoFluxRealSources,leglabel='Simulated sources (gal)',hcolor='darkorange')
histoplotter1D(ax,tfluxBins,all_egal_histoFluxPgMatchingSources[best_ind_egal,:],leglabel='Matched sources (egal)',hcolor='mediumblue')
histoplotter1D(ax,tfluxBins,all_egal_histoFluxRealSources,leglabel='Simulated sources (egal)',hcolor='red')
ax.legend(loc='upper left',framealpha=1,fontsize=0.75*textfont)
ax.set_xlabel('Source flux [photons / (cm2 s)]',fontsize=textfont)
ax.set_ylabel('Counts',fontsize=textfont)
ax.set_xscale('log')
ax.grid(linewidth=0.5)
ax.tick_params(labelsize = textfont)
fig.set_tight_layout('tight')
fig.savefig(figname)
plt.close(fig)

logging.info("Done!")
logging.info("")

# ==========================================================================================
#                          PLOTTING / 1D CURVE
# ==========================================================================================
# Re-find the flux at which we get a certain value for the detection rat
bestTPR_gal = all_gal_histoFluxPgMatchingSources[best_ind_gal,:] / all_gal_histoFluxRealSources
bestTPR_galerr = bestTPR_gal * np.sqrt( (all_gal_histoErrRealSources / all_gal_histoFluxRealSources)**2 + \
                                        (all_gal_histoErrPgMatchingSources[best_ind_gal,:] / all_gal_histoFluxPgMatchingSources[best_ind_gal,:])**2 )

result_gal, xth_gal, yth_gal = fitter_detectionRate(tfluxBins,bestTPR_gal,bestTPR_galerr)

bestTPR_egal = all_egal_histoFluxPgMatchingSources[best_ind_egal,:] / all_egal_histoFluxRealSources
bestTPR_egalerr = bestTPR_egal * np.sqrt( (all_egal_histoErrRealSources / all_egal_histoFluxRealSources)**2 + \
                                          (all_egal_histoErrPgMatchingSources[best_ind_egal,:] / all_egal_histoFluxPgMatchingSources[best_ind_egal,:])**2 )

result_egal, xth_egal, yth_egal = fitter_detectionRate(tfluxBins,bestTPR_egal,bestTPR_egalerr)

# Show how the detection rate scales with the flux in both gal and egal, with the best
# combination of parameters.
logging.info(f"Plotting the optimal detection rate curve ...")
figname = os.path.join(imgdir,'05_optDetectionRateFinder',f'pgwave_{modelcatalog}_bestconfig_fluxVsDetectionRate.pdf')

fig,ax = plt.subplots(figsize = dimfig)
ax.errorbar(tfluxBins,bestTPR_egal,bestTPR_egalerr,color='mediumblue',linestyle='',capsize=5,
            marker='.',markersize=10,label=f'Extra-galactic, data (scala = {best_egal_scala}, otpix = {best_egal_otpix})')
ax.errorbar(tfluxBins,bestTPR_gal,bestTPR_galerr,color='red',linestyle='',capsize=5,
            marker='.',markersize=10,label=f'Galactic, data (scala = {best_gal_scala}, otpix = {best_gal_otpix})')
ax.plot(xth_gal,yth_gal,color='red',linestyle='-',linewidth=1,label=f'Galactic, tanh fit')
ax.plot(xth_egal,yth_egal,color='mediumblue',linestyle='-',linewidth=1,label=f'Extra-galactic, tanh fit')
ax.plot(xth_gal,xth_gal*0+thrTPR,color='black',linestyle='--',linewidth=2.5,label=f'Threshold: TPR = {thrTPR}')
ax.set_xlabel(f"Source flux [photons / cm2 / s]",fontsize = textfont)
ax.set_ylabel(f"Detection rate [%]",fontsize = textfont)
ax.set_ylim([0,1.2])
ax.set_xscale('log')
ax.legend(loc='upper left',framealpha=1,fontsize=textfont*0.7)
ax.grid(linewidth=0.5)
ax.tick_params(labelsize = textfont)
fig.set_tight_layout('tight')
fig.savefig(figname)
plt.close(fig)

logging.info("Done!")

# ==========================================================================================
# Final printouts
logging.info(f"")
logging.info(f"Completed the execution of {__file__}")
logging.info("*"*75)