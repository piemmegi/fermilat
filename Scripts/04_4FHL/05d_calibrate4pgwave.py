'''
# ==========================================================================================
#                                       HEADER
# ==========================================================================================
The aim of this script is to use all the files generated by the 05a_simulate4pgwave simulation,
in the optimal configuration (as found by 05c_optDetectionRateFinder), to measure the general tagging
precision (TPR/FPR) of pgwave2D and calibrate its response (from counts to fluxes)

-------------------------------------------------------------------------------------------
Call syntax:
  	python3 05d_calibrate4pgwave [config] [catalog]

Arguments:
- config: str, either "egal" or "all", the quadrants to use for the optimization
- catalog: "4FGL" or "3FHL", the model used for the analysis

'''
# ==========================================================================================
#                                 PREAMBLE (module load)
# ==========================================================================================
# Import of the Python Standard Library modules required for the analysis
import glob, os, subprocess, sys			# To act on the operative system
import json									# To read and write JSON files
import matplotlib as mpl					# For plotting
import matplotlib.pyplot as plt				# For plotting
import numpy as np							# For numerical analysis
import warnings								# To deactivate the warnings
import time									# For script timing
import logging								# For logging purposes
import datetime								# For timestamp printing
import copy									# To copy objects

import yaml                                 # For editing yaml files (necessary for fermipy)
from fermipy.gtanalysis import GTAnalysis   # To run fermipy utilities
from astropy.io import fits                 # To open fits files
from gammapy.maps import WcsNDMap           # To plot maps

# Import of personal functions from custom modules
sys.path.append("../../Functions")
from MyFunctions import fitter_linear, truehisto1D, histoplotter1D
from MyFunctions import histoproj, truehisto2D, histoplotter2D, wmean
from AstroFunctions import angdist

# Set the warnings of a few functions
np.seterr(divide='ignore', invalid='ignore')	# To avoid 0/0 errors
mpl.rcParams['backend'] = 'agg'					# Non-interactive backend for plotting
warnings.filterwarnings("ignore", category=DeprecationWarning) # To avoid deprecation warnings

# =========================================================================================
#                                      COMMON DEFINITIONS                                      
# =========================================================================================
# Define the dimension for the fonts in the plots, the figures dimensions and so on
textfont = 20			# Fontsize for labels and legends
dimfig = (12,7)			# Figure dimensions (A4-like)
dimfigbig = (16,12)		# Figure dimensions (a bit larger than A4-like)
dimfiglong = (16,25)	# Figure dimensions (a bit larger and much longer than A4-like)
filetype = '.pdf'		# File format to save the pictures
filedpi = 520			# Resolution used when saving pictures in .png/.jpeg
nptsfit = 10000			# Number of points used in plotting the fits

# ==========================================================================================
#                                  STATUS FILE DATA
# ==========================================================================================
# Open the status file
statusfile = '../statusfile.json'
with open(statusfile) as json_file:
	statusdata = json.load(json_file)

# Extract all the relevant variables from the file
datadir = statusdata['datadir']
imgdir = statusdata['imgdir']
tempdir = statusdata['tempdir']
dataSCdir = statusdata['dataSCdir']
dataPHdir = statusdata['dataPHdir']
catalog_file_4FGL = statusdata['catalog_file_4FGL']
catalog_file_3FHL = statusdata['catalog_file_3FHL']
catalog_file_3FHL_reg = statusdata['catalog_file_3FHL_reg']
catalog_file_3FHL_regAssoc = statusdata['catalog_file_3FHL_regAssoc']

# Define the name for the subfolders (in imgdir and tempdir) where we will save
# the temporary images and files
pretempdir = '04_4FHL'
imgdir = os.path.join(imgdir,pretempdir)
tempdir = os.path.join(tempdir,pretempdir)

# Setting of the logging module
logfile = "debug.log"
logging.basicConfig(format='[%(asctime)s] %(message)s',level=logging.INFO,
					handlers=[logging.FileHandler(logfile,mode='w'),
					logging.StreamHandler()])
logging.info("*"*75)
logging.info(f"Beginning the execution of {__file__}")
logging.info(f"")

# ==========================================================================================
#                                 INPUT PARSING
# ==========================================================================================
# Parse the command-line to know which model to use for the simulations
try:
    modelcatalog = sys.argv[1]
    if (modelcatalog != "3FHL") & (modelcatalog != "4FGL"):
        modelcatalog = "4FGL"
        logging.info(f"modelcatalog variable vas badly set from command line. Defaulting to {modelcatalog}...")    
except:
    modelcatalog = "4FGL"
    logging.info(f"modelcatalog variable vas badly set from command line. Defaulting to {modelcatalog}...")

# Parse the command line to know which quadrants to use
try:
    quadrantCut = sys.argv[2]
    if (quadrantCut != "all") & (quadrantCut != "egal"):
        quadrantCut = "egal"
        logging.info(f"quadrantCut variable vas badly set from command line. Defaulting to {modelcatalog}...")    
except:
    quadrantCut = "egal"
    logging.info(f"quadrantCut variable vas badly set from command line. Defaulting to {modelcatalog}...")    

logging.info(f"Input check: modelcatalog = {modelcatalog}; quadrantCut = {quadrantCut}")

# ==========================================================================================
#                             OPTIMAL CONFIGURATION
# ==========================================================================================
# Determine the optimal configuration for pgwave2D
pgwave_simdir = f'05_{modelcatalog}_simPgwaveOut'
optfilename = os.path.join(tempdir, pgwave_simdir, 'pgwave2D_bestconfig.npz')

best_type = 'gal' # 'egal'
best_gal_scala = np.load(optfilename)[f"best_gal_scala"]
best_gal_otpix = np.load(optfilename)[f"best_gal_otpix"]
best_egal_scala = np.load(optfilename)[f"best_egal_scala"]
best_egal_otpix = np.load(optfilename)[f"best_egal_otpix"]

# ==========================================================================================
#                               QUADRANT LOADING
# ==========================================================================================
# Determine the quadrants to look at
quadrantInds = list(range(44))
if quadrantCut == "egal":
    for num in range(16,27):
        quadrantInds.remove(num)
numQuadrants = len(quadrantInds)
logging.info(f"Quadrants to analyze: {quadrantInds}")

quadrantFile = "./SkyQuadrants.dat"
egal_cutoff = 10
ROI_lat_min, ROI_lat_max, _, _ = np.loadtxt(quadrantFile, skiprows = 1, unpack = True, dtype = np.float64, delimiter = "\t")

# ==========================================================================================
#                               QUADRANT ITERATION
# ==========================================================================================
# Iterate on all the quadrants. For each of them, we will extract the data corresponding
# to the best pgwave2D configuration (flux/counts and positions of all real and localized sources)
mindist_thr = 0.3 # [deg] Minimum distance for the matching

simdir = f'05_{modelcatalog}_simCMAPs'
all_matchedRealFlux = np.zeros((0,))
all_matchedPgCounts = np.zeros((0,))
all_matchedDistances = np.zeros((0,))
all_matchedPosErr = np.zeros((0,))
all_matchedSNR = np.zeros((0,))
all_matchedKsignif = np.zeros((0,))

for j,quadrantInd in enumerate(quadrantInds):
    # ======================================================================================
    #                                    PRELIMINARY
    # ======================================================================================
    # Are we gal or egal?
    ROI_glat_center = (ROI_lat_min[quadrantInd] + ROI_lat_max[quadrantInd])/2
    ifegals = (np.abs(ROI_glat_center) >= egal_cutoff)
    if ifegals:
        best_scala = best_egal_scala
        best_otpix = best_egal_otpix
    else:
        best_scala = best_gal_scala
        best_otpix = best_gal_otpix

    # ======================================================================================
    #                               LOAD TRUE SOURCES DATA
    # ======================================================================================
    # Load the data from the fermipy simulation: extract the list of simulated sources (for this
    # quadrant)
    printstr = 'egal' if ifegals else 'gal'

    logging.info("*"*25)
    logging.info(f"Working on quadrant {quadrantInd} (type: {printstr})")
    logging.info("*"*25)
    
    filename_fermipy = os.path.join(tempdir,simdir,f'pgSIM_fermisim_Q{quadrantInd:02d}.npz')
    filename_pgwave = os.path.join(tempdir, pgwave_simdir,f"Q{quadrantInd:02d}", 
                                   f"pgSIM_scala_{best_scala}_otpix_{int(best_otpix)}.npz")

    with np.load(filename_fermipy) as f:
        realSources_blat  = f['all_blat']
        realSources_blong = f['all_blong']
        realSources_flux  = f['allflux']

    with np.load(filename_pgwave) as f:
        pgSources_blat     = f['pg_b']
        pgSources_blong    = f['pg_l']
        pgSources_counts   = f['pg_counts']
        pgSources_pos_err  = f['pg_pos_err']
        pgSources_snr      = f['pg_snr']
        pgSources_k_signif = f['pg_k_signif']

    # ======================================================================================
    #                                 SOURCE MATCHING
    # ======================================================================================
    # Iterate on all the real sources and match them
    indsRealMatching = []
    indsPgMatching = []
    numRealSources = np.shape(realSources_flux)[0]

    for i in range(numRealSources):
        # Compute the distance from this source to all the pgwave ones and see if the closest
        # is under threshold. If so, we have a match
        thisdist = angdist(realSources_blat[i],realSources_blong[i],pgSources_blat,pgSources_blong,ifdeg=True)
        if np.min(thisdist) <= mindist_thr:
            indsRealMatching.append(i)
            indsPgMatching.append(np.argmin(thisdist))

    # Extract the matched sources fluxes and the other parameters
    matchedRealFlux = realSources_flux[indsRealMatching]
    matchedPgCounts = pgSources_counts[indsPgMatching]
    matchedDistance = angdist(realSources_blat[indsRealMatching],realSources_blong[indsRealMatching],
                              pgSources_blat[indsPgMatching],pgSources_blong[indsPgMatching],ifdeg=True)
    matchedPosErr = pgSources_pos_err[indsPgMatching]
    matchedSNR = pgSources_snr[indsPgMatching]
    matchedKsignif = pgSources_k_signif[indsPgMatching]

    # Stack the outputs of this iteration step
    all_matchedRealFlux = np.hstack((all_matchedRealFlux,matchedRealFlux))
    all_matchedPgCounts = np.hstack((all_matchedPgCounts,matchedPgCounts))
    all_matchedDistances = np.hstack((all_matchedDistances,matchedDistance))
    all_matchedPosErr = np.hstack((all_matchedPosErr,matchedPosErr))
    all_matchedSNR = np.hstack((all_matchedSNR,matchedSNR))
    all_matchedKsignif = np.hstack((all_matchedKsignif,matchedKsignif))

    logging.info("Quadrant completed!")
    logging.info("")

# Compute the errors associated to the counts
all_matchedPgErrs = np.sqrt(all_matchedPgCounts)
logging.info("All quadrants done!")
logging.info("*"*50)

# ==========================================================================================
#                                OUTLIARS TAGGING
# ==========================================================================================
# Define a procedure to locate outliars in the dataset
thrK = 25

logging.info(f"Tagging outliars as: (pgwave2D K-significance < {thrK})")
cond_outliars = (all_matchedKsignif < thrK)
cond_true = (cond_outliars == False)

logging.info(f"Tagging as outliars: {cond_outliars.sum()} / {cond_outliars.shape[0]} sources")

# ==========================================================================================
#                            CALIBRATIONS (COUNTS TO FLUX, POS_ERR)
# ==========================================================================================
# Computing the correlation between the real fluxes and the pgwave counts
r = np.corrcoef(all_matchedRealFlux[cond_true],all_matchedPgCounts[cond_true])[0,1]
logging.info(f"Correlation between real flux and pgwave counts: {r:.3f}")
logging.info(f"")

# Fit the flux vs counts relationship with a straight line
result, xth, yth = fitter_linear(all_matchedRealFlux[cond_true],all_matchedPgCounts[cond_true],
                                 all_matchedPgErrs[cond_true], ifq=False)

# Save the scaling coefficients and print them
m = result.params['m'].value
m_err = result.params['m'].stderr
logging.info("Fitting: pgwave2D counts = m*flux")
logging.info("Fit parameter(s):")
logging.info(f"\t m = {m:.2e} +/- {m_err:.2e} counts / (photons / cm2 s)")
logging.info(f"")

# Compute the error associated to the localization
stdDistance = np.std(all_matchedDistances)

logging.info(f"Std of (true position - pgwave position): {stdDistance:.5f} deg")
logging.info(f"")

# ==========================================================================================
#                              HISTO 1D: COUNTS, FLUXES
# ==========================================================================================
# Compute the distribution of fluxes and pgwave2D counts for the matched sources
fluxbins = np.hstack((np.logspace(-12.5,-10,19),np.logspace(-10,-9,6)[1:-1])) # approx starting from 5e-13
histoFlux, binsFlux = truehisto1D(all_matchedRealFlux,bins=fluxbins)

countsbins = np.logspace(0,4,50)
histoCounts, binsCounts = truehisto1D(all_matchedPgCounts,bins=countsbins)

# Normalize the histograms
histoFlux = histoFlux / np.sum(histoFlux)
histoCounts = histoCounts / np.sum(histoCounts)

# Show the histograms in a single plot
logging.info(f"Plotting the histos1D (flux and pgwave2D counts distibutions)...")
figname = os.path.join(imgdir,'05_optDetectionRateFinder',f'pgwave_{modelcatalog}_calibrate_{quadrantCut}_histo_counts.pdf')

color1 = 'mediumblue'
color2 = 'red'

fig,ax1 = plt.subplots(figsize = dimfig)
histoplotter1D(ax1,binsFlux,histoFlux,'Flux [photons/cm2/s]','Normalized counts','Data','best',textfont,
               hcolor = color1)
ax1.set_xlabel('Flux [photons/cm2/s]',color=color1,fontsize=textfont)
ax1.set_xticklabels(ax1.get_xticklabels(),color=color1)
ax1.tick_params(color=color1)
ax1.get_legend().remove()
ax1.set_xscale('log')

ax2 = ax1.twiny()
histoplotter1D(ax2,binsCounts,histoCounts,'pgwave counts','Counts','Data','best',textfont,hcolor=color2)
ax2.set_xlabel('pgwave2D counts',color=color2,fontsize=textfont,labelpad=10)
ax2.set_xticklabels(ax2.get_xticklabels(),color=color2)
ax2.tick_params(color=color2)
ax2.get_legend().remove()
ax2.set_xscale('log')

fig.set_tight_layout('tight')
fig.savefig(figname)
plt.close(fig)
logging.info("Done!")

# ==========================================================================================
#                         SCATTERPLOT 1D: COUNTS vs K-SIGNIFICANCE
# ==========================================================================================
# Define the colors for the plotting
color_true = 'mediumblue'
color_outliars = 'darkorange'

# Plot the data
logging.info(f"Plotting the scatter plot (pgwave2D K-significance vs pgwave2D counts)...")
figname = os.path.join(imgdir,'05_optDetectionRateFinder',f'pgwave_{modelcatalog}_calibrate_{quadrantCut}_scatter_K_vs_Counts.pdf')

fig,ax = plt.subplots(figsize = dimfig)
ax.plot(all_matchedKsignif[cond_true],all_matchedPgCounts[cond_true],color=color_true,
        linestyle='', marker='.',markersize=10, label = f'{modelcatalog} sources ({quadrantCut} quadrants)')
ax.plot(all_matchedKsignif[cond_outliars],all_matchedPgCounts[cond_outliars],color=color_outliars,
        linestyle='', marker='.',markersize=10, label = f'Outliars')

ax.legend(loc = 'upper right', fontsize = textfont, framealpha = 1)
ax.set_xlabel(f"pgwave2D K-significance",fontsize = textfont)
ax.set_ylabel(f"pgwave2D counts",fontsize = textfont)
ax.grid(linewidth=0.5)
ax.tick_params(labelsize = textfont)
    
fig.set_tight_layout('tight')
fig.savefig(figname,dpi=filedpi)
plt.close(fig)
logging.info("Done!")

# ==========================================================================================
#                         SCATTERPLOT 1D: COUNTS vs SNR
# ==========================================================================================
# Plot the data
logging.info(f"Plotting the scatter plot (SNR vs pgwave2D counts)...")
figname = os.path.join(imgdir,'05_optDetectionRateFinder',f'pgwave_{modelcatalog}_calibrate_{quadrantCut}_scatter_SNR_vs_Counts.pdf')

fig,ax = plt.subplots(figsize = dimfig)
ax.plot(all_matchedSNR[cond_true],all_matchedPgCounts[cond_true],color=color_true,
        linestyle='', marker='.',markersize=10, label = f'{modelcatalog} sources ({quadrantCut} quadrants)')
ax.plot(all_matchedSNR[cond_outliars],all_matchedPgCounts[cond_outliars],color=color_outliars,
        linestyle='', marker='.',markersize=10, label = f'Outliars')
ax.legend(loc = 'upper left', fontsize = textfont, framealpha = 1)
ax.set_xlabel(f"pgwave2D SNR",fontsize = textfont)
ax.set_ylabel(f"pgwave2D counts",fontsize = textfont)
ax.grid(linewidth=0.5)
ax.tick_params(labelsize = textfont)
    
fig.set_tight_layout('tight')
fig.savefig(figname,dpi=filedpi)
plt.close(fig)
logging.info("Done!")

# ==========================================================================================
#                         SCATTERPLOT 1D: COUNTS vs FLUX
# ==========================================================================================
# Plot the data
logging.info(f"Plotting the scatter plot (fluxes vs pgwave2D counts)...")
figname = os.path.join(imgdir,'05_optDetectionRateFinder',f'pgwave_{modelcatalog}_calibrate_{quadrantCut}_scatter_flux_vs_Counts.pdf')

fig,ax = plt.subplots(figsize = dimfig)
ax.plot(all_matchedRealFlux[cond_true],all_matchedPgCounts[cond_true],color=color_true,
        linestyle='', marker='.',markersize=10, label = f'{modelcatalog} sources ({quadrantCut} quadrants)')
ax.plot(all_matchedRealFlux[cond_outliars],all_matchedPgCounts[cond_outliars],color=color_outliars,
        linestyle='', marker='.',markersize=10, label = f'Outliars')
ax.plot(xth,yth,color='red',linestyle='--',linewidth=2.5,label=f"Linear fit (no outliars)")
ax.legend(loc = 'lower right', fontsize = textfont, framealpha = 1)
ax.set_xlabel(f"Source flux [photons / cm2 / s]",fontsize = textfont)
ax.set_ylabel(f"pgwave2D counts",fontsize = textfont)
ax.grid(linewidth=0.5)
ax.tick_params(labelsize = textfont)
    
fig.set_tight_layout('tight')
fig.savefig(figname,dpi=filedpi)
plt.close(fig)
logging.info("Done!")

# ==========================================================================================
#                         SCATTERPLOT 1D: COUNTS vs ANGULAR ERROR
# ==========================================================================================
# Plot the data
logging.info(f"Plotting the scatter plot (angular error vs pgwave2D counts)...")
figname = os.path.join(imgdir,'05_optDetectionRateFinder',f'pgwave_{modelcatalog}_calibrate_{quadrantCut}_scatter_locerr_vs_Counts.pdf')

fig,ax = plt.subplots(figsize = dimfig)
ax.plot(all_matchedDistances[cond_true],all_matchedPgCounts[cond_true],color=color_true,
        linestyle='', marker='.',markersize=10, label = f'{modelcatalog} sources ({quadrantCut} quadrants)')
ax.plot(all_matchedDistances[cond_outliars],all_matchedPgCounts[cond_outliars],color=color_outliars,
        linestyle='', marker='.',markersize=10, label = f'Outliars')
ax.legend(loc = 'upper left', fontsize = textfont, framealpha = 1)
ax.set_xlabel(f"Localization error [deg]",fontsize = textfont)
ax.set_ylabel(f"pgwave2D counts",fontsize = textfont)
#ax.set_ylim([0,3000])
ax.grid(linewidth=0.5)
ax.tick_params(labelsize = textfont)

fig.set_tight_layout('tight')
fig.savefig(figname,dpi=filedpi)
plt.close(fig)
logging.info("Done!")

# ==========================================================================================
#                         SCATTERPLOT 1D: POS ERR vs ANGULAR ERROR
# ==========================================================================================
# Plot the data
logging.info(f"Plotting the scatter plot (angular error vs pgwave2D counts)...")
figname = os.path.join(imgdir,'05_optDetectionRateFinder',f'pgwave_{modelcatalog}_calibrate_{quadrantCut}_scatter_locerr_vs_poserr.pdf')
fig,ax = plt.subplots(figsize = dimfig)
ax.plot(all_matchedDistances[cond_outliars],all_matchedPosErr[cond_outliars],color=color_outliars,
        linestyle='', marker='.',markersize=10, label = f'Outliars')
ax.plot(all_matchedDistances[cond_true],all_matchedPosErr[cond_true],color=color_true,
        linestyle='', marker='.',markersize=10, label = f'{modelcatalog} sources ({quadrantCut} quadrants)')
ax.legend(loc = 'lower right', fontsize = textfont, framealpha = 1)
ax.set_xlabel(f"Localization error [deg]",fontsize = textfont)
ax.set_ylabel(f"pgwave2D pos_err [deg]",fontsize = textfont)
#ax.set_ylim([0,3000])
ax.grid(linewidth=0.5)
ax.tick_params(labelsize = textfont)

fig.set_tight_layout('tight')
fig.savefig(figname,dpi=filedpi)
plt.close(fig)
logging.info("Done!")

# ==========================================================================================
#                         SCATTERPLOT 1D: POS ERR vs ANGULAR ERROR
# ==========================================================================================
# Saving the calibration coefficients
outfilename = os.path.join(tempdir, pgwave_simdir, 'pgwave2D_calcoeffs.npz')
logging.info(f"Saving the outputs in:")
logging.info(outfilename)
np.savez_compressed(outfilename, m = m,
                                 m_err = m_err,
                                 stdDistance = stdDistance)

# ==========================================================================================
# Final printouts
logging.info(f"")
logging.info(f"Completed the execution of {__file__}")
logging.info("*"*75)