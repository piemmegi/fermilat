'''
# ==========================================================================================
#                                       HEADER
# ==========================================================================================
The aim of this script is to create a flux map of a region of the full sky view, for a given energy
band and time window, with a relatively fine granularity. In a sense, this is sort-of-like ds9.

-------------------------------------------------------------------------------------------
Call syntax:
  	python3 03_ds9.py

'''
# ==========================================================================================
#                                 PREAMBLE (module load)
# ==========================================================================================
# Import of the Python Standard Library modules required for the analysis
import glob, os, subprocess, sys             # To act on the operative system
import json                                  # To read and write JSON files
import matplotlib as mpl                     # For plotting
import matplotlib.pyplot as plt              # For plotting
import numpy as np                           # For numerical analysis
import pandas as pd                          # For reading and writing csv files
import warnings                              # To deactivate the warnings
import time                                  # For script timing
import logging                               # For logging purposes
import datetime                              # For timestamp printing
import copy                                  # To copy objects

# Import of the astropy/gammapy/fermipy (& fermitools) modules necessary for astrophysics
import astropy.units as u                    # Celestial units
from astropy.io import fits
from gammapy.maps import Map, WcsNDMap       # To work with sky maps
from astropy.coordinates import SkyCoord     # To work with sky frames

# Import of personal functions from custom modules
sys.path.append("../../Functions")
from AstroFunctions import gtselect, gtmktime, gtbin_CMAP, gtltcube, gtexpcube2
from AstroFunctions import regExtractor

# Set the warnings of a few functions
np.seterr(divide='ignore', invalid='ignore') # To avoid 0/0 errors
mpl.rcParams['backend'] = 'agg'              # Non-interactive backend for plotting
warnings.filterwarnings("ignore", category=DeprecationWarning) # To avoid deprecation warnings

# =========================================================================================
#                                      COMMON DEFINITIONS                                      
# =========================================================================================
# Define the dimension for the fonts in the plots, the figures dimensions and so on
textfont = 20			# Fontsize for labels and legends
dimfig = (12,7)			# Figure dimensions (A4-like)
dimfigbig = (16,12)		# Figure dimensions (a bit larger than A4-like)
dimfiglong = (16,25)	# Figure dimensions (a bit larger and much longer than A4-like)
filetype = '.pdf'		# File format to save the pictures
filedpi = 520			# Resolution used when saving pictures in .png/.jpeg
nptsfit = 10000			# Number of points used in plotting the fits

# ==========================================================================================
#                                  STATUS FILE DATA
# ==========================================================================================
# Open the status file
statusfile = '../statusfile.json'
with open(statusfile) as json_file:
	statusdata = json.load(json_file)

# Extract all the relevant variables from the file
datadir = statusdata['datadir']
imgdir = statusdata['imgdir']
tempdir = statusdata['tempdir']
dataSCdir = statusdata['dataSCdir']
dataPHdir = statusdata['dataPHdir']
catalog_file_4FGL = statusdata['catalog_file_4FGL']
catalog_file_3FHL = statusdata['catalog_file_3FHL']
catalog_file_3FHL_reg = statusdata['catalog_file_3FHL_reg']
catalog_file_3FHL_regAssoc = statusdata['catalog_file_3FHL_regAssoc']

# Define the name for the subfolders (in imgdir and tempdir) where we will save
# the temporary images and files
pretempdir = '04_4FHL'
imgdir = os.path.join(imgdir,pretempdir)
tempdir = os.path.join(tempdir,pretempdir)

# Setting of the logging module
logfile = "debug.log"
logging.basicConfig(format='[%(asctime)s] %(message)s',level=logging.INFO,
					handlers=[logging.FileHandler(logfile,mode='w'),
					logging.StreamHandler()])
logging.info("*"*75)
logging.info(f"Beginning the execution of {__file__}")
logging.info(f"")

# ==========================================================================================
#                                 CONFIG FILE DATA
# ==========================================================================================
# Open the configuration file made for the 1st level trigger and extract the variables
# necessary for the analysis
configfile = os.path.join(tempdir,f'configfile_trignum_0.json')
with open(configfile) as json_file:
	configdata = json.load(json_file)

# ==========================================================================================
#                                 TEMPORARY FILE NAMES
# ==========================================================================================
# Define the names of the .fits, .txt, .xml and .npz files to be opened in the execution
projtype = 'CAR'
binsz = '10'

data_PH_after_gtmktime   = os.path.join(tempdir,f"PH_after_gtmktime.fits")
data_PH_after_gtbin      = os.path.join(tempdir,f"PH_after_gtbin_{projtype}_{binsz}.fits")
data_PH_after_gtltcube   = os.path.join(tempdir,f"PH_after_gtltcube.fits")
data_PH_after_gtexpcube2 = os.path.join(tempdir,f"PH_after_gtexpcube2.fits")

# ==========================================================================================
#		                       COUNT MAP EXTRACTION AND SMOOTHING
# ==========================================================================================
# Do we want to show count maps or flux maps (exposure corrected)?
ifexpmap = False

# Open the output files from the previous steps and compute the count or flux map
countmap_skyview = WcsNDMap.read(data_PH_after_gtbin)

if ifexpmap:
	expmap_skyview = WcsNDMap.read(data_PH_after_gtexpcube2).sum_over_axes(keepdims=False)
	fluxmap_data = countmap_skyview.data / expmap_skyview.data

	# Remove non numerical values from the flux map (e.g., the output of 0/0 calculations)
	badinds = (np.isinf(fluxmap_data)) | (np.isnan(fluxmap_data))
	fluxmap_data[badinds] = 0

	# Associate the flux map to a proper gammapy object 
	fluxmap_skyview = copy.deepcopy(countmap_skyview)
	fluxmap_skyview.data = fluxmap_data

# Choose whether to smooth the count maps, for visual clarity
LAT_PSF = 0.5			# LAT is 0.15° at 10 GeV, 0.1° at higher energies
ifsmooth = True
if ifexpmap:
	showmap = fluxmap_skyview
	if ifsmooth:
		showmap = showmap.smooth(LAT_PSF,kernel='gauss')
else:
	showmap = countmap_skyview
	if ifsmooth:
		showmap = showmap.smooth(LAT_PSF,kernel='gauss')	    

# ==========================================================================================
#                                       REGION FILE PARSING
# ==========================================================================================
# Define the center of the region to be extracted from the count map
cutout_blong = 263   		# Long
cutout_blat = -3     		# Lat
cutout_radius = 20 			# The region is ~ a circle where "radius" is in reality the diameter. Do not ask why.
cutout_Coord = SkyCoord(cutout_blong,cutout_blat,frame='galactic',unit='deg')
cutout_RA, cutout_DEC = cutout_Coord.icrs.ra.deg, cutout_Coord.icrs.dec.deg

# Extract the cutout region
cutout = showmap.cutout(position=cutout_Coord, width=(cutout_radius * u.deg, cutout_radius * u.deg))

# Open the catalog .reg files and extract a list of all the sources contained in the region.
# Leave the "/2" in the argument of the function call!
good_sources_names, good_sources_coords = regExtractor(catalog_file_3FHL_regAssoc, cutout_RA, cutout_DEC, cutout_radius/2,
											ifWholeCatalog = False, fManageExtended = 0, verbosity = 0)

# ==========================================================================================
#                                       PLOTTING / CUTOUT
# ==========================================================================================
# Show the cutout plot
logging.info("Now plotting...")
fig = plt.figure(figsize = dimfigbig)
ax = cutout.plot(fig=fig, stretch="log", vmin = 0, vmax=np.max(showmap.data),cmap='gnuplot2',add_cbar = False)

# Modify the colorbar
if ifexpmap:
	cbarlabel = r"Flux [photons $cm^{-2}$ $s^{-1}$]"
else:
	cbarlabel = r"Flux [counts / bin]"
	
# Add all the known sources
ifprint = False # Whether to print the list of sources found in the ROI
offset = 0.5    # Displacement in long for the names w.r.t. the markers of the sources in the ROI

for i,name in enumerate(good_sources_names):
	# Scale the coordinates from RA/DEC to galactic
	source_coord = SkyCoord(good_sources_coords[i][0],good_sources_coords[i][1],frame='icrs',unit='deg')
	source_blong, source_blat = source_coord.galactic.l.deg, source_coord.galactic.b.deg

	# Printout, if needed
	if ifprint:
		print(f"Source {name} found, at long {source_blong:.2f} deg, lat {source_blat:.2f} deg")
	
	# Add the sources to the plot
	ax.plot(source_blong,source_blat,color='cyan',linestyle='',marker='x',markersize=40,
			transform=ax.get_transform("galactic"))
	ax.text(source_blong-offset,source_blat,name,fontsize=15,color='cyan',transform=ax.get_transform("galactic"))

# Modify the colorbar
thisImage = ax.get_images()[0]
cbar = fig.colorbar(thisImage, ax=ax,shrink=0.6)
cbar.ax.tick_params(labelsize=textfont*0.75)
cbar.set_label(cbarlabel,fontsize=textfont,rotation=270,labelpad=50)

# Refine the graphics
ax.grid()
lat = ax.coords['glat']
lon = ax.coords['glon']
lon.set_axislabel('Galactic Longitude', color = 'white', fontsize = 20)
lat.set_axislabel('Galactic Latitude', minpad = 5, fontsize = 20)
lon.set_ticklabel(color='white',fontsize=textfont)
lon.set_ticks(color='white')
lat.set_ticklabel(color='black',fontsize=textfont)
lat.set_ticks(color='white')
fig.set_tight_layout('tight')
fig.savefig(os.path.join(imgdir,'03_ds9',f'CutOutMap_{ifexpmap}_{cutout_blong}_{cutout_blat}_{cutout_radius}.pdf'),dpi=filedpi,bbox_inches='tight',pad_inches=0.05)
fig.savefig(os.path.join(imgdir,'03_ds9',f'CutOutMap_{ifexpmap}_{cutout_blong}_{cutout_blat}_{cutout_radius}.png'),dpi=filedpi,bbox_inches='tight',pad_inches=0.05)
plt.close(fig)
logging.info("Done!")

# ==========================================================================================
# Final printouts
logging.info(f"")
logging.info(f"Completed the execution of {__file__}")
logging.info("*"*75)