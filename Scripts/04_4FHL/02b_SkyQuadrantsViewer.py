'''
# ==========================================================================================
#                                       HEADER
# ==========================================================================================
The aim of this script is to visualize the Sky Quadrants used in the seeding stage

-------------------------------------------------------------------------------------------
Call syntax:
  	python3 02b_SkyQuadrantsViewer.py

'''
# ==========================================================================================
#                                 PREAMBLE (module load)
# ==========================================================================================
# Import of the Python Standard Library modules required for the analysis
import glob, os, subprocess, sys			# To act on the operative system
import json									# To read and write JSON files
import matplotlib as mpl					# For plotting
import matplotlib.pyplot as plt				# For plotting
import numpy as np							# For numerical analysis
import pandas as pd							# For reading and writing csv files
import warnings								# To deactivate the warnings
import time									# For script timing
import logging								# For logging purposes
import datetime								# For timestamp printing
import copy									# To copy objects

# Import of the astropy/gammapy/fermipy (& fermitools) modules necessary for astrophysics
from astropy.io import fits                 # To open fits files
from gammapy.maps import Map, WcsNDMap      # To work with sky maps

# Import of personal functions from custom modules
sys.path.append("../../Functions")
from MyFunctions import truehisto1D, histoplotter1D, truehisto2D, histoplotter2D
from AstroFunctions import gal2radec, angdist, duplicateRemoval

# Set the warnings of a few functions
np.seterr(divide='ignore', invalid='ignore')	# To avoid 0/0 errors
mpl.rcParams['backend'] = 'agg'					# Non-interactive backend for plotting
warnings.filterwarnings("ignore", category=DeprecationWarning) # To avoid deprecation warnings

# =========================================================================================
#                                      COMMON DEFINITIONS                                      
# =========================================================================================
# Define the dimension for the fonts in the plots, the figures dimensions and so on
textfont = 20			# Fontsize for labels and legends
dimfig = (12,7)			# Figure dimensions (A4-like)
dimfigbig = (16,12)		# Figure dimensions (a bit larger than A4-like)
dimfiglong = (16,25)	# Figure dimensions (a bit larger and much longer than A4-like)
filetype = '.pdf'		# File format to save the pictures
filedpi = 520			# Resolution used when saving pictures in .png/.jpeg
nptsfit = 10000			# Number of points used in plotting the fits

# ==========================================================================================
#                                  STATUS FILE DATA
# ==========================================================================================
# Open the status file
statusfile = '../statusfile.json'
with open(statusfile) as json_file:
	statusdata = json.load(json_file)

# Extract all the relevant variables from the file
datadir = statusdata['datadir']
imgdir = statusdata['imgdir']
tempdir = statusdata['tempdir']
dataSCdir = statusdata['dataSCdir']
dataPHdir = statusdata['dataPHdir']
catalog_file_4FGL = statusdata['catalog_file_4FGL']
catalog_file_3FHL = statusdata['catalog_file_3FHL']
catalog_file_3FHL_reg = statusdata['catalog_file_3FHL_reg']
catalog_file_3FHL_regAssoc = statusdata['catalog_file_3FHL_regAssoc']
catalog_file_2FGES_fits = statusdata['catalog_file_2FGES_fits']

# Define the name for the subfolders (in imgdir and tempdir) where we will save
# the temporary images and files
pretempdir = '04_4FHL'
imgdir = os.path.join(imgdir,pretempdir)
tempdir = os.path.join(tempdir,pretempdir)

# Setting of the logging module
logfile = "debug.log"
logging.basicConfig(format='[%(asctime)s] %(message)s',level=logging.INFO,
					handlers=[logging.FileHandler(logfile,mode='w'),
					logging.StreamHandler()])
logging.info("*"*75)
logging.info(f"Beginning the execution of {__file__}")
logging.info(f"")

# ==========================================================================================
#                                  SKY QUADRANTS LOADING
# ==========================================================================================
# Open the Sky Quadrants file
quadrantFile = "./SkyQuadrants.dat"
lat_min, lat_max, long_min, long_max = np.loadtxt(quadrantFile, skiprows = 1, unpack = True, dtype = np.float64,
													delimiter = "\t")
numQuadrants = np.shape(lat_min)[0]

# Small edit to the quadrants to improve graphical readibility
long_min[long_min == 180] += 0.1
long_max[long_max == 180] -= 0.1

# Load the Fermi full-sky counts map
fullSkyMap = os.path.join(tempdir,f"PH_after_gtbin_AIT_10.fits")
countmap_skyview = WcsNDMap.read(fullSkyMap)

# Smooth it
LAT_PSF = 1
showmap = countmap_skyview.smooth(LAT_PSF,kernel='gauss')

# ==========================================================================================
#                                  SKY QUADRANTS PLOTTING
# ==========================================================================================
# Now plot the map
fig = plt.figure(figsize = dimfigbig)
ax = showmap.plot(fig = fig, stretch='log', cmap='gnuplot2', add_cbar = False)

# Add the quadrants
npts = 1000
for i in range(numQuadrants):
    ax.plot(np.linspace(long_min[i],long_max[i],npts), np.zeros((npts,))+lat_min[i],
            color='red',linestyle='--',linewidth=2.5,transform=ax.get_transform("galactic"))
    ax.plot(np.zeros((npts,))+long_min[i], np.linspace(lat_min[i],lat_max[i],npts),
            color='red',linestyle='--',linewidth=2.5,transform=ax.get_transform("galactic"))
    ax.plot(np.linspace(long_min[i],long_max[i],npts), np.zeros((npts,))+lat_max[i],
            color='red',linestyle='--',linewidth=2.5,transform=ax.get_transform("galactic"))
    ax.plot(np.zeros((npts,))+long_max[i], np.linspace(lat_min[i],lat_max[i],npts),
            color='red',linestyle='--',linewidth=2.5,transform=ax.get_transform("galactic"))

# Regine the graphics
filename = os.path.join(imgdir,'02_SkyMaps','SkyQuadrants')

lat = ax.coords['glat']
lon = ax.coords['glon']
lon.set_axislabel('Galactic Longitude [deg]',fontsize=1.5*textfont,minpad=9)
lat.set_axislabel('Galactic Latitude [deg]',fontsize=1.5*textfont)#,minpad=3)
ax.tick_params(axis='glon',labelsize = textfont,color='white',labelcolor='white')
ax.tick_params(axis='glat',labelsize = textfont,color='black',labelcolor='black')
ax.coords.grid(color='white', alpha=0.5, linestyle='solid')
ax.patch.set_edgecolor('black')  
ax.patch.set_linewidth(1)
ax.texts[0].remove()
fig.set_tight_layout('tight')
fig.savefig(filename + '.pdf',dpi=filedpi)
fig.savefig(filename + '.png',dpi=filedpi)
plt.close(fig)

logging.info("Plot done!")

# ==========================================================================================
# Final printouts
logging.info(f"")
logging.info(f"Completed the execution of {__file__}")
logging.info("*"*75)