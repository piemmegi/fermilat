'''
# ==========================================================================================
#                                       HEADER
# ==========================================================================================
The aim of this script is to run pgwave2D on a ROI, whose simulated counts were created 
with the script 05a_simulate4pgwave

-------------------------------------------------------------------------------------------
Call syntax:
  	python3 05b_pgwaveOnSim.py [quadrant] [catalog] [scala] [otpix]

Arguments:
- quadrant: int, index of the quadrant to analyze
- catalog: "4FGL" or "3FHL", the model used for the analysis
- scala, otpix: float, values to use for the corresponding parameters of pgwave2D

'''
# ==========================================================================================
#                                 PREAMBLE (module load)
# ==========================================================================================
# Import of the Python Standard Library modules required for the analysis
import glob, os, subprocess, sys			# To act on the operative system
import json									# To read and write JSON files
import matplotlib as mpl					# For plotting
import matplotlib.pyplot as plt				# For plotting
import numpy as np							# For numerical analysis
import warnings								# To deactivate the warnings
import time									# For script timing
import logging								# For logging purposes
import datetime								# For timestamp printing
import copy									# To copy objects

import yaml                                 # For editing yaml files (necessary for fermipy)
from fermipy.gtanalysis import GTAnalysis   # To run fermipy utilities
from astropy.io import fits                 # To open fits files
from gammapy.maps import WcsNDMap           # To plot maps

import matplotlib.colors as colors          # Required to define histoplotter2D
import matplotlib.cm as pltmaps

# Import of personal functions from custom modules
sys.path.append("../../Functions")
from AstroFunctions import gtbin_CMAP, pgwave2D, radec2gal

# Set the warnings of a few functions
np.seterr(divide='ignore', invalid='ignore')	# To avoid 0/0 errors
mpl.rcParams['backend'] = 'agg'					# Non-interactive backend for plotting
warnings.filterwarnings("ignore", category=DeprecationWarning) # To avoid deprecation warnings

# =========================================================================================
#                                      COMMON DEFINITIONS                                      
# =========================================================================================
# Define the dimension for the fonts in the plots, the figures dimensions and so on
textfont = 20			# Fontsize for labels and legends
dimfig = (12,7)			# Figure dimensions (A4-like)
dimfigbig = (16,12)		# Figure dimensions (a bit larger than A4-like)
dimfiglong = (16,25)	# Figure dimensions (a bit larger and much longer than A4-like)
filetype = '.pdf'		# File format to save the pictures
filedpi = 520			# Resolution used when saving pictures in .png/.jpeg
nptsfit = 10000			# Number of points used in plotting the fits

# ==========================================================================================
#                                  STATUS FILE DATA
# ==========================================================================================
# Open the status file
statusfile = '../statusfile.json'
with open(statusfile) as json_file:
	statusdata = json.load(json_file)

# Extract all the relevant variables from the file
datadir = statusdata['datadir']
imgdir = statusdata['imgdir']
tempdir = statusdata['tempdir']
dataSCdir = statusdata['dataSCdir']
dataPHdir = statusdata['dataPHdir']
catalog_file_4FGL = statusdata['catalog_file_4FGL']
catalog_file_3FHL = statusdata['catalog_file_3FHL']
catalog_file_3FHL_reg = statusdata['catalog_file_3FHL_reg']
catalog_file_3FHL_regAssoc = statusdata['catalog_file_3FHL_regAssoc']

# Define the name for the subfolders (in imgdir and tempdir) where we will save
# the temporary images and files
pretempdir = '04_4FHL'
imgdir = os.path.join(imgdir,pretempdir)
tempdir = os.path.join(tempdir,pretempdir)

# Setting of the logging module
logfile = "debug.log"
logging.basicConfig(format='[%(asctime)s] %(message)s',level=logging.INFO,
					handlers=[logging.FileHandler(logfile,mode='w'),
					logging.StreamHandler()])

logging.info("*"*75)
logging.info(f"Beginning the execution of {__file__}")
logging.info(f"")

# Additional security printout because in this script we have to work with a different conda env
logging.info("*"*100)
logging.info("Are you sure to be running in a conda env with pgwave2D available? (e.g. fermi4pgwave)")
logging.info("\t\t\t\tCHECK IT!!!")
logging.info("If you are not, you may see an error like <time: cannot run pgwave2D: No such file or directory>")
logging.info("*"*100)

# ==========================================================================================
#                                 INPUT PARSING
# ==========================================================================================
# Parse the command-line to know the quadrant to be analyzed (see after)
try:
	quadrantInd = int(sys.argv[1])
except:
	quadrantInd = 0
	logging.info(f"quadrantInd variable was badly set from command line. Defaulting to {quadrantInd}...")
	logging.info("")

# Parse the command-line to know which model to use for the simulations
try:
    modelcatalog = sys.argv[2]
    if (modelcatalog != "3FHL") & (modelcatalog != "4FGL"):
        modelcatalog = "3FHL"
        logging.info(f"modelcatalog variable vas badly set from command line. Defaulting to {modelcatalog}...")    
except:
    modelcatalog = "3FHL"
    logging.info(f"modelcatalog variable vas badly set from command line. Defaulting to {modelcatalog}...")

# Parse the command-line to know the pgwave2D parameters
try:
    scala = float(sys.argv[3])
except:
    scala = 2.2
    logging.info(f"scala variable was badly set from command line. Defaulting to {scala}...")
    logging.info("")

try:
    otpix = int(sys.argv[4])
except:
    otpix = 5
    logging.info(f"otpix variable was badly set from command line. Defaulting to {scala}...")
    logging.info("")

logging.info(f"quadrantInd = {quadrantInd}; modelcatalog = {modelcatalog}; scala = {scala}; otpix = {otpix}")

# ==========================================================================================
#                              PGWAVE SETUP AND RUN
# ==========================================================================================
# Define the parameters for pgwave2D
simdir = f'05_{modelcatalog}_simCMAPs'
PH_cmap4pgwave = os.path.join(tempdir,simdir,f'PH_cmap4pgwave_Q{quadrantInd:02d}.fits') # File to be analyzed

proj = 'CAR'
if (proj == 'CAR'):
	circ_square = 's'
elif (proj == 'AIT'):
	circ_square = 'c'
	
bgk_choise = 'n'    # Do we have a bkg map? Probably not
median_box = 3      # Dimension for the median filter (in bkg reconstruction)
kappa = 3           # Number of sigmas relative to the statistical confidence
min_pix = int(max(2,float(scala)-1)+0.5) # Minimum separation between adjacent sources
m_num = otpix       # Repetition of otpix? Unclear, TBC
verbosity = 0       # Output verbosity. Advice: keep to zero, it talks too much

# Call pgwave2D
logging.info(f"Calling pgwave2D on simulated Sky Quadrant n. {quadrantInd}...")
pgwave2D(infile=PH_cmap4pgwave,bgk_choise=bgk_choise,circ_square=circ_square,
         scala=scala,otpix=otpix,median_box=median_box,kappa=kappa,min_pix=min_pix,
         m_num=m_num,verbosity=verbosity)

# ==========================================================================================
#                               REGION DEFINITION
# ==========================================================================================
# Open the data file with the quadrant coordinates and determine the latitude and longitude interval
# of the current quadrant. Leave a margin to be sure
quadrantFile = "./SkyQuadrants.dat"
lat_min, lat_max, long_min, long_max = np.loadtxt(quadrantFile, skiprows = 1, unpack = True, dtype = np.float64,
                                                    delimiter = "\t")
quadrantMarginAngle = 5
lat_min = lat_min[quadrantInd]
lat_max = lat_max[quadrantInd]
long_min = long_min[quadrantInd]
long_max = long_max[quadrantInd]

lat_center = int( (lat_max + lat_min) / 2 )
long_center = int( (long_max + long_min) / 2 )

lat_width = (lat_max - lat_min) + 2*quadrantMarginAngle
long_width = (long_max - long_min) + 2*quadrantMarginAngle

# ==========================================================================================
#                                 OUTPUT PARSING
# ==========================================================================================
# Open the output of pgwave2D and parse the localized sources (pay attention: the pgwave 
# .list output file has gal latitude and longitude switched in the columns)
pgoutfile = PH_cmap4pgwave.split('.')[0] + '.list'
pg_ID, pg_X, pg_Y, pg_l, pg_b, pg_pos_err, pg_snr, pg_k_signif, pg_counts, pg_sigC, pg_bkg, pg_sigbkg = np.loadtxt(pgoutfile,
                                                                        skiprows=1,unpack=True)

# Cut the sources located outside the nominal borders of the quadrant
condPG = (pg_l >= long_min-quadrantMarginAngle) & (pg_l <= long_max + quadrantMarginAngle) & \
        (pg_b >= lat_min-quadrantMarginAngle) & (pg_b <= lat_max + quadrantMarginAngle)
pg_ID = pg_ID[condPG]
pg_l = pg_l[condPG]
pg_b = pg_b[condPG]
pg_pos_err = pg_pos_err[condPG]
pg_counts = pg_counts[condPG]

pg_X = pg_X[condPG]
pg_Y = pg_Y[condPG]
pg_snr = pg_snr[condPG]
pg_k_signif = pg_k_signif[condPG]
pg_sigC = pg_sigC[condPG]
pg_bkg = pg_bkg[condPG]
pg_sigbkg = pg_sigbkg[condPG]

logging.info(f"{np.shape(condPG)[0]} sources were found by pgwave2D: {np.sum(condPG)} inside the nominal border of the ROI, {np.shape(condPG)[0] - np.sum(condPG)} in the safety margin")

# ==========================================================================================
#                                 OUTPUT SAVING
# ==========================================================================================
# Prepare a dictionary with the parameters tested in this configuration
param_dict = {"proj": proj,
              "circ_square": circ_square,
              "bgk_choise": bgk_choise,
              "kappa": kappa,
              "median_box": median_box,
              "min_pix": min_pix,
              "m_num": m_num}

# Prepare the output folder where data will be stored
preoutdir = f"05_{modelcatalog}_simPgwaveOut"
suboutdir = f"Q{quadrantInd:02d}"
outdir = os.path.join(tempdir,preoutdir,suboutdir)
if not os.path.exists(outdir):
    os.mkdir(outdir)

# Save all the outputs
outfilename = os.path.join(outdir,f'pgSIM_scala_{scala}_otpix_{otpix}.npz')

logging.info(f"")
logging.info(f"Saving the localized sources data in file {outfilename}")
np.savez_compressed(outfilename, pg_ID = pg_ID,
                                 pg_l = pg_l,
                                 pg_b = pg_b,
                                 pg_pos_err = pg_pos_err,
                                 pg_counts = pg_counts,
                                 pg_X = pg_X,
                                 pg_Y = pg_Y,
                                 pg_snr = pg_snr,
                                 pg_k_signif = pg_k_signif,
                                 pg_sigC = pg_sigC,
                                 pg_bkg = pg_bkg,
                                 pg_sigbkg = pg_sigbkg,
                                 param_dict = param_dict)
logging.info(f"Done!")

# ==========================================================================================
# Final printouts
logging.info(f"")
logging.info(f"Completed the execution of {__file__}")
logging.info("*"*75)