'''
# ==========================================================================================
#                                       HEADER
# ==========================================================================================
The aim of this script is to visualize a portion of the full sky map, including the seeds
extracted from various sources, after removal and merging; these are the seeds given as 
input to the likelihood stage

-------------------------------------------------------------------------------------------
Call syntax:
  	python3 04w_seedlist_visualizer.py

'''
# ==========================================================================================
#                                 PREAMBLE (module load)
# ==========================================================================================
# Import of the Python Standard Library modules required for the analysis
import glob, os, subprocess, sys			# To act on the operative system
import json									# To read and write JSON files
import matplotlib as mpl					# For plotting
import matplotlib.pyplot as plt				# For plotting
import numpy as np							# For numerical analysis
import pandas as pd							# For reading and writing csv files
import warnings								# To deactivate the warnings
import time									# For script timing
import logging								# For logging purposes
import datetime								# For timestamp printing
import copy									# To copy objects

# Import of the astropy/gammapy/fermipy (& fermitools) modules necessary for astrophysics
import astropy.units as u					# Celestial units
from astropy.io import fits                 # To open fits files
from gammapy.maps import Map, WcsNDMap		# To work with sky maps
from astropy.coordinates import SkyCoord 	# To work with sky frames

# Import of personal functions from custom modules
sys.path.append("../../Functions")
from MyFunctions import truehisto2D, histoplotter2D
from AstroFunctions import gal2radec, radec2gal, angdist, circlecalculator

# Set the warnings of a few functions
np.seterr(divide='ignore', invalid='ignore')	# To avoid 0/0 errors
mpl.rcParams['backend'] = 'agg'					# Non-interactive backend for plotting
warnings.filterwarnings("ignore", category=DeprecationWarning) # To avoid deprecation warnings

# =========================================================================================
#                                      COMMON DEFINITIONS                                      
# =========================================================================================
# Define the dimension for the fonts in the plots, the figures dimensions and so on
textfont = 20			# Fontsize for labels and legends
dimfig = (12,7)			# Figure dimensions (A4-like)
dimfigbig = (16,12)		# Figure dimensions (a bit larger than A4-like)
dimfiglong = (16,25)	# Figure dimensions (a bit larger and much longer than A4-like)
filetype = '.pdf'		# File format to save the pictures
filedpi = 520			# Resolution used when saving pictures in .png/.jpeg
nptsfit = 10000			# Number of points used in plotting the fits

# ==========================================================================================
#                                  STATUS FILE DATA
# ==========================================================================================
# Open the status file
statusfile = '../statusfile.json'
with open(statusfile) as json_file:
	statusdata = json.load(json_file)

# Extract all the relevant variables from the file
datadir = statusdata['datadir']
imgdir = statusdata['imgdir']
tempdir = statusdata['tempdir']
dataSCdir = statusdata['dataSCdir']
dataPHdir = statusdata['dataPHdir']
catalog_file_4FGL = statusdata['catalog_file_4FGL']
catalog_file_3FHL = statusdata['catalog_file_3FHL']
catalog_file_3FHL_reg = statusdata['catalog_file_3FHL_reg']
catalog_file_3FHL_regAssoc = statusdata['catalog_file_3FHL_regAssoc']
catalog_file_2FGES_fits = statusdata['catalog_file_2FGES_fits']

# Define the name for the subfolders (in imgdir and tempdir) where we will save
# the temporary images and files
pretempdir = '04_4FHL'
postdir = '04_seedmaps'
imgdir = os.path.join(imgdir,pretempdir,postdir)
tempdir = os.path.join(tempdir,pretempdir)

# Setting of the logging module
logfile = "debug.log"
logging.basicConfig(format='[%(asctime)s] %(message)s',level=logging.INFO,
					handlers=[logging.FileHandler(logfile,mode='w'),
					logging.StreamHandler()])
logging.info("*"*75)
logging.info(f"Beginning the execution of {__file__}")
logging.info(f"")

# ==========================================================================================
#                                  INPUT FILE DATA
# ==========================================================================================
# Define the name of the file with the photon data and extract them
data_PH_after_gtmktime = os.path.join(tempdir,f"PH_after_gtmktime.fits")
with fits.open(data_PH_after_gtmktime) as datafile:
    ind = 1
    data_blat = np.array(datafile[ind].data.field("B"),dtype=np.float64)
    data_blong = np.array(datafile[ind].data.field("L"),dtype=np.float64)
    data_times = np.array(datafile[ind].data.field("TIME"),dtype=np.float64)
    
logging.info(f"{np.shape(data_blat)[0]} events in the full-sky map")

# ==========================================================================================
#                                  CUTOUT EXTRACTION
# ==========================================================================================
# Define a cutout extraction in a center of given location
cutout_radius = 5           # Radius, deg
cutout_blong = 263   		# Long, deg
cutout_blat = -3     		# Lat, deg
#cutout_ra = 276.07
#cutout_dec = 56.76
#cutout_blong, cutout_blat = radec2gal(cutout_ra,cutout_dec)

# Perform the extraction
theangles = angdist(data_blat,data_blong,cutout_blat,cutout_blong,ifdeg=True)

cond = (theangles <= cutout_radius)
cut_blat = data_blat[cond]
cut_blong = data_blong[cond]
logging.info(f"{np.shape(cut_blat)[0]} events in the cutout region")

# ==========================================================================================
#                             POINT SOURCE SEEDS EXTRACTION
# ==========================================================================================
# Open the file with the final list of point source seeds, after merging and removing duplicates
outfilename = os.path.join(tempdir,f'seedlist_merged.npz')
seeds_blat = np.load(outfilename)['final_seeds_blat']
seeds_blong = np.load(outfilename)['final_seeds_blong']
numSeeds = np.shape(seeds_blat)[0]
logging.info(f"{numSeeds} seed(s) in the full-sky map")

# Perform the cut as above
theangles_seeds = angdist(seeds_blat,seeds_blong,cutout_blat,cutout_blong,ifdeg=True)

cond = (theangles_seeds <= cutout_radius)
seeds_blat = seeds_blat[cond]
seeds_blong = seeds_blong[cond]
numSeedsHere = np.shape(seeds_blat)[0]
logging.info(f"{numSeedsHere} seed(s) in the region")
logging.info("")

# ==========================================================================================
# Open also the extended source list
catalog_ind = 1
with fits.open(catalog_file_2FGES_fits) as f:
    extended_blat = f[catalog_ind].data.field('GLAT')
    extended_blong = f[catalog_ind].data.field('GLON')
    extended_r68 = f[catalog_ind].data.field("r_68")

numExtended = np.shape(extended_blat)[0]
logging.info(f"{numExtended} extended source(s) in the full-sky map")

# Perform the cut as above
theangles_extended = angdist(extended_blat,extended_blong,cutout_blat,cutout_blong,ifdeg=True)

cond = (theangles_extended <= cutout_radius)
extended_blat = extended_blat[cond]
extended_blong = extended_blong[cond]
extended_r68 = extended_r68[cond]
numExtendedHere = np.shape(extended_blat)[0]
logging.info(f"{numExtendedHere} extended source(s) in the region")
logging.info("")

# ==========================================================================================
#                                  PLOTTING
# ==========================================================================================
# Computing the 2D histogram of the photon counts in the region
nbins = 200
histo2D, binsX, binsY = truehisto2D(cut_blong,cut_blat,nbinsX=nbins,nbinsY=nbins)
logging.info(f"Showing the complete seed list in the cutout region...")

# Showing it, if needed
ifplot = True

if ifplot:
    # Plot
    figname = os.path.join(imgdir,"seedmaps_merged_histo2D.png")
    t0 = time.perf_counter()

    fig, ax = plt.subplots(figsize = dimfig)
    histoplotter2D(ax,binsX,binsY,histo2D,'gnuplot2',None,'Galactic longitude [°]','Galactic latitude [°]',
                   textfont,iflog=True,vmax=25,ifcbarfont=False,cbarlabel="Counts/bin")
    for i in range(numSeedsHere):
        if (i == 0):
            ax.plot(seeds_blong[i], seeds_blat[i], color='red',marker='x',markersize=20,linestyle='',
                    label = "Seeds")
        else:
            ax.plot(seeds_blong[i], seeds_blat[i], color='red',marker='x',markersize=20,linestyle='')

    for j in range(numExtendedHere):
        # Plot the center of the extended source
        if (j == 0):
            ax.plot(extended_blong[j], extended_blat[j], color='cyan',marker='x',markersize=20,linestyle='',
                    label = "Extended sources (2FGES)")
        else:
            ax.plot(extended_blong[j], extended_blat[j], color='cyan',marker='x',markersize=20,linestyle='')

        # Add the extension
        lat_vec, long_vec_plus, long_vec_minus = circlecalculator(extended_blat[j],extended_blong[j],extended_r68[j],ifdeg=True)
        if (j == 0):
            ax.fill_betweenx(lat_vec,long_vec_plus,long_vec_minus,color='cyan',alpha=0.2,label="r68")
        else:
            ax.fill_betweenx(lat_vec,long_vec_plus,long_vec_minus,color='cyan',alpha=0.2)
    
    ax.invert_xaxis()
    ax.legend(loc='lower left',framealpha=1,fontsize=0.75*textfont)
    ax.set_title("Seeds, after merger and duplicate removal",fontsize=1.5*textfont)
    fig.set_tight_layout('tight')
    fig.savefig(figname,dpi=filedpi)
    plt.close(fig)

    # Printout
    t1 = time.perf_counter()
    dt = t1-t0
    logging.info(f"2D map plot completed! ({dt:.2f} s elapsed for this plot)")
else:
    logging.info(f"2D map skipped!")

# ==========================================================================================
# Same as above but with a scatterplot
ifplot = True 

if ifplot:
    figname = os.path.join(imgdir,"seedmaps_merged_scatter.png")
    t2 = time.perf_counter()

    fig, ax = plt.subplots(figsize = dimfig)
    ax.plot(cut_blong,cut_blat,color='mediumblue',ls='',marker='o',markersize=0.2)
    for i in range(numSeedsHere):
        if (i == 0):
            ax.plot(seeds_blong[i], seeds_blat[i], color='red',marker='x',markersize=20,linestyle='',
                    label = "Seeds")
        else:
            ax.plot(seeds_blong[i], seeds_blat[i], color='red',marker='x',markersize=20,linestyle='')

    for j in range(numExtendedHere):
        # Plot the center of the extended source
        if (j == 0):
            ax.plot(extended_blong[j], extended_blat[j], color='black',marker='x',markersize=20,linestyle='',
                    label = "Extended sources (2FGES)")
        else:
            ax.plot(extended_blong[j], extended_blat[j], color='black',marker='x',markersize=20,linestyle='')

        # Add the extension
        lat_vec, long_vec_plus, long_vec_minus = circlecalculator(extended_blat[j],extended_blong[j],extended_r68[j],ifdeg=True)
        if (j == 0):
            ax.fill_betweenx(lat_vec,long_vec_plus,long_vec_minus,color='black',alpha=0.1,label="r68")
        else:
            ax.fill_betweenx(lat_vec,long_vec_plus,long_vec_minus,color='black',alpha=0.1)

    ax.invert_xaxis()
    ax.legend(loc='upper right',fontsize=0.75*textfont,framealpha=1)
    ax.set_title("Seeds, after merger and duplicate removal",fontsize=1.5*textfont)
    ax.grid(linewidth=0.5)
    ax.set_xlabel('Galactic longitude [°]',fontsize=textfont)
    ax.set_ylabel('Galactic latitude [°]',fontsize=textfont)
    ax.tick_params(labelsize = textfont)
    fig.set_tight_layout('tight')
    fig.savefig(figname,dpi=filedpi)
    plt.close(fig)

    t3 = time.perf_counter()
    dt = t3-t2
    logging.info(f"scatterplot completed! ({dt:.2f} s elapsed for this plot)")
else:
    logging.info(f"scatterplot skipped!")

# ==========================================================================================
# Final printouts
logging.info(f"")
logging.info(f"Completed the execution of {__file__}")
logging.info("*"*75)