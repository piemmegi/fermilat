'''
# ==========================================================================================
#                                       HEADER
# ==========================================================================================
The aim of this script is to analyze the 4FHL dataset to determine a list of seeds of VHE
sources, which will be later feed as input to the likelihood analysis stage. In this case,
the analysis is performed by applying a clustering algorithm on the raw photon positions.

-------------------------------------------------------------------------------------------
Call syntax:
  	python3 04a_seeder_clustering.py [quadrant] [clustering_algorithm] [numPtsMax]

'''
# ==========================================================================================
#                                 PREAMBLE (module load)
# ==========================================================================================
# Import of the Python Standard Library modules required for the analysis
import glob, os, subprocess, sys			# To act on the operative system
import json									# To read and write JSON files
import matplotlib as mpl					# For plotting
import matplotlib.pyplot as plt				# For plotting
import numpy as np							# For numerical analysis
import warnings								# To deactivate the warnings
import time									# For script timing
import logging								# For logging purposes
import datetime								# For timestamp printing
import copy									# To copy objects

# Imports specific to this script 
from astropy.io import fits                 # To open fits files
from sklearn.cluster import HDBSCAN 
from hdbscan import HDBSCAN

# Import of personal functions from custom modules
sys.path.append("../../Functions")
#from MyFunctions import ...
#from AstroFunctions import ...

# Set the warnings of a few functions
np.seterr(divide='ignore', invalid='ignore')	# To avoid 0/0 errors
mpl.rcParams['backend'] = 'agg'					# Non-interactive backend for plotting
warnings.filterwarnings("ignore", category=DeprecationWarning) # To avoid deprecation warnings

# =========================================================================================
#                                      COMMON DEFINITIONS                                      
# =========================================================================================
# Define the dimension for the fonts in the plots, the figures dimensions and so on
textfont = 20			# Fontsize for labels and legends
dimfig = (12,7)			# Figure dimensions (A4-like)
dimfigbig = (16,12)		# Figure dimensions (a bit larger than A4-like)
dimfiglong = (16,25)	# Figure dimensions (a bit larger and much longer than A4-like)
filetype = '.pdf'		# File format to save the pictures
filedpi = 520			# Resolution used when saving pictures in .png/.jpeg
nptsfit = 10000			# Number of points used in plotting the fits

# ==========================================================================================
#                                  STATUS FILE DATA
# ==========================================================================================
# Open the status file
statusfile = '../statusfile.json'
with open(statusfile) as json_file:
	statusdata = json.load(json_file)

# Extract all the relevant variables from the file
datadir = statusdata['datadir']
imgdir = statusdata['imgdir']
tempdir = statusdata['tempdir']
dataSCdir = statusdata['dataSCdir']
dataPHdir = statusdata['dataPHdir']
catalog_file_4FGL = statusdata['catalog_file_4FGL']
catalog_file_3FHL = statusdata['catalog_file_3FHL']
catalog_file_3FHL_reg = statusdata['catalog_file_3FHL_reg']
catalog_file_3FHL_regAssoc = statusdata['catalog_file_3FHL_regAssoc']

# Define the name for the subfolders (in imgdir and tempdir) where we will save
# the temporary images and files
pretempdir = '04_4FHL'
imgdir = os.path.join(imgdir,pretempdir)
tempdir = os.path.join(tempdir,pretempdir)

# Setting of the logging module
logfile = "debug.log"
logging.basicConfig(format='[%(asctime)s] %(message)s',level=logging.INFO,
					handlers=[logging.FileHandler(logfile,mode='w'),
					logging.StreamHandler()])
logging.info("*"*75)
logging.info(f"Beginning the execution of {__file__}")
logging.info(f"")

# ==========================================================================================
#                                 INPUT PARAMETERS
# ==========================================================================================
# Parse the command-line to know thq quadrant to be analyzed (see after)
try:
	quadrantInd = int(sys.argv[1])
except:
	quadrantInd = 0

# Hard-code all the other parameters.
cluster_algorithm = "HDBSCAN"
numPtsMax = 10000               # Cutoff on the statistics
quadrantMarginAngle = 2.5      # Margin on the quadrant borders, in deg

# ==========================================================================================
#                                 QUADRANT PARTITION
# ==========================================================================================
# To avoid having to compute the MST of the photons over the entire sky (which is pretty useless
# since distant points are assumed to be highly non correlated) the analysis is performed
# on one "quadrant" at a time. Quadrants are defined in CAR projections and are loaded from an
# external file.
quadrantFile = "./SkyQuadrants.dat"
lat_min, lat_max, long_min, long_max = np.loadtxt(quadrantFile, skiprows = 1, unpack = True, dtype = np.float32,
													delimiter = "\t")

# The user chooses the quadrant number to be analyzed + there's a fixed margin of extension
# to avoid border effects
lat_min = lat_min[quadrantInd] - quadrantMarginAngle
lat_max = lat_max[quadrantInd] + quadrantMarginAngle
long_min = long_min[quadrantInd] - quadrantMarginAngle
long_max = long_max[quadrantInd] + quadrantMarginAngle

# ==========================================================================================
#                                 DATA EXTRACTION
# ==========================================================================================
# Open the file with the list of the VHE photons detected in 4FHL and extract the photon
# coordinates and energies
data_PH_after_gtmktime = os.path.join(tempdir,f"PH_after_gtmktime.fits")
logging.info(f"Opening data file: {data_PH_after_gtmktime}")
with fits.open(data_PH_after_gtmktime) as datafile:
    ind = 1
    data_blat = np.array(datafile[ind].data.field("B"),dtype=np.float32)
    data_blong = np.array(datafile[ind].data.field("L"),dtype=np.float32)
    data_times = np.array(datafile[ind].data.field("TIME"),dtype=np.float32)
    
numPhotons = np.shape(data_blat)[0]
logging.info(f"File opened, {numPhotons} events recorded")

# Cutoff in the chosen quadrant
cutcond = (data_blat >= lat_min) & (data_blat <= lat_max) & (data_blong >= long_min) & (data_blong <= long_max)
data_blat = data_blat[cutcond]
data_blong = data_blong[cutcond]
data_times = data_times[cutcond]
numPhotonsQuadrant = np.shape(data_blat)[0]
logging.info(f"Quadrant {quadrantInd} under analysis:")
logging.info(f"\tLatitude from {lat_min} to {lat_max}")
logging.info(f"\tLongitude from {long_min} to {long_max}")
logging.info(f"\t{numPhotonsQuadrant} events recorded here")

# Do we want to limit the photon statistics?
if numPtsMax is not None:
	logging.info(f"Limiting the analysis to the first {numPtsMax} points...")
	data_blat = data_blat[:numPtsMax]
	data_blong = data_blong[:numPtsMax]
	data_times = data_times[:numPtsMax]

# Scale the coordinates from deg to radians and compact them in a single array
data_blat_rad = np.radians(data_blat)
data_blong_rad = np.radians(data_blong)
data_coords = np.vstack((data_blat_rad,data_blong_rad)).transpose()

# ==========================================================================================
#                                 CLUSTERING
# ==========================================================================================
# Define the clustering parameters
min_cluster_size = 40
n_jobs = -1
metric = "haversine"
hdbscan_cluster_selection_epsilon = float(np.radians(0.2))
hdbscan_cluster_selection_method = 'leaf'
hdbscan_store_centers = 'medoid'
hdbscan_min_samples = min_cluster_size
hdbscan_temp_outdir = tempdir
hdbscan_implementation = 'hdbscan' # otherwise "sklearn"

# Instantiate the clustering object
if (hdbscan_implementation == "sklearn"):
	cluster_obj = HDBSCAN(min_cluster_size=min_cluster_size,
		 				  metric=metric,
			              cluster_selection_epsilon=hdbscan_cluster_selection_epsilon,
			              cluster_selection_method=hdbscan_cluster_selection_method,
			              store_centers=hdbscan_store_centers,
			              n_jobs=n_jobs)
else:
	# This is the hdbscan implementation
	cluster_obj = HDBSCAN(min_cluster_size=min_cluster_size,
						  min_samples=hdbscan_min_samples,
		 				  metric=metric,
		 				  memory=hdbscan_temp_outdir,
			              cluster_selection_epsilon=hdbscan_cluster_selection_epsilon,
			              cluster_selection_method=hdbscan_cluster_selection_method,
			              core_dist_n_jobs=n_jobs)

logging.info(f"Clustering algorithm object: \n\n{cluster_obj}\n")

# Perform the clustering and time the operation
t0 = time.perf_counter()
logging.info(f"Starting the clustering...")
cluster_obj.fit(data_coords)
t1 = time.perf_counter()
dt = t1-t0

uniqueLabels = np.unique(cluster_obj.labels_)
numClusters = np.shape(uniqueLabels)[0]
numRealClusters = np.shape(uniqueLabels[uniqueLabels >= 0])[0]

logging.info(f"Clustering completed!")
logging.info(f"Elapsed time: {dt:.3f} s")
logging.info(f"{numClusters} unique cluster(s) found, {numRealClusters} of which are not labelled as noise (label > 0)")

# ==========================================================================================
#                                 AGGREGATION
# ==========================================================================================
# Compute the cluster sizes
pointlabels = cluster_obj.labels_
clusters_size = np.zeros((numClusters,))
for i in range(numClusters):
    clusters_size[i] = np.shape(pointlabels[pointlabels == i])[0]

# Compute some summary information
nMeanPointsInCluster = np.mean(clusters_size)
nStdPointsInCluster = np.std(clusters_size) / np.sqrt(numClusters)
logging.info(f"Average cluster size: {nMeanPointsInCluster:.3f} +/- {nStdPointsInCluster:.3f}")

# Extract the cluster centers
if (hdbscan_implementation == "sklearn"):
	# This is the sklearn implementation
	clusters_lat = np.degrees(cluster_obj.medoids_[:,0])
	clusters_long = np.degrees(cluster_obj.medoids_[:,1])
else:
	# This is the hdbscan implementation
	clusters_lat = np.zeros_like(clusters_size)
	clusters_long = np.zeros_like(clusters_size)
	for i in range(numRealClusters):
		clusters_lat[i], clusters_long[i] = cluster_obj.weighted_cluster_medoid(i)

	clusters_lat = np.degrees(clusters_lat)
	clusters_long = np.degrees(clusters_long)

# ==========================================================================================
#                                 OUTPUT STAGE
# ==========================================================================================
# Remove the sources outside the nominal borders of the quadrant
true_lat_min, true_lat_max, true_long_min, true_long_max = np.loadtxt(quadrantFile, skiprows = 1, 
                                                   unpack = True, dtype = np.float32, delimiter = "\t")
true_lat_min = true_lat_min[quadrantInd]
true_lat_max = true_lat_max[quadrantInd]
true_long_min = true_long_min[quadrantInd]
true_long_max = true_long_max[quadrantInd]
cond = (clusters_lat >= true_lat_min) & (clusters_lat <= true_lat_max) & (clusters_long >= true_long_min) & (clusters_long <= true_long_max)

# Prepare the output file, depending on the method chosen
outfilename = os.path.join(tempdir,"04_HDBSCAN",f'seedlist_clusters_{cluster_algorithm}_quadrant_{quadrantInd:02d}.npz')

logging.info(f"\nSaving the cluster data in file {outfilename}")
np.savez_compressed(outfilename, clusters_lat = clusters_lat[cond],
								 clusters_long = clusters_long[cond],
								 clusters_size = clusters_size[cond])
logging.info("Saving completed!")

# ==========================================================================================
# Final printouts
logging.info(f"")
logging.info(f"Completed the execution of {__file__}")
logging.info("*"*75)