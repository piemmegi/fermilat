#! /usr/bin/bash

# ==========================================================================================
#                                          HEADER
# ==========================================================================================
# The aim of this script is to call multiple times the 04e_seeder_pgwave script, changing
# from time to time the quadrant under analysis. 
#
# Syntax to call this script (supposing to be already in the "04_4FHL" folder):
# $ ./04e_quadrant_caller.sh
#
# ==========================================================================================
#                                        SCRIPT
# ==========================================================================================
# Count the number of lines in the Sky Quadrant file (== number of times to call the py script)
wc_output=$(wc -l "SkyQuadrants.dat")
wc_output=$(echo $wc_output)
numLines=${wc_output%% *}
numLines="$((numLines-1))"

# Iterate over every quadrant to be analyzed
echo "Bash script starting!"
for i in $(seq 0 $numLines)
do
    # Call the script
    python3 04e_seeder_pgwave.py "${i}"
done

# The end
echo "Bash script completed!"
