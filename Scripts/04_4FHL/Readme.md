# Towards the 4th Fermi High energy Level source catalog (4FHL)

Guide written and edited by [P. Monti-Guarnieri](mailto:pietro.monti-guarnieri@phd.units.it). 

Date of the last update of this guide: 2025/01/14

## Table Of Contents

[[_TOC_]]

## A general idea 

The scripts available in this folder are used to create from scratch the new 4FHL catalog.

List of files:
- `01_DataExtractor.py`: extract the 16-year data (gtselect, gtmktime) and compute livetime cube and exposure maps (gtltcube, gtexpcube2)
- `02_FullSkyMapViewer.py`: print some info on the 16-year data and show the full-sky count map, flux map and exposure map
- `03_ds9.py`: show a cutout of the full-sky map, including the positions of the 3FHL sources
- `04*.py`: script to compute a list of seeds of HE/VHE sources in different ways
	- `04a_seeder_clustering.py`: compute seeds as clusters of photons localized with HDBSCAN
	- `04c_seeder_4FGL.py`: the seeds are the 4FGL sources
	- `04d_seeder_3FHL.py`: the seeds are the 3FHL sources
	- `04e_seeder_pgwave.py`: the seeds are peaks in the count maps found with pgwave2D (**fermi4pgwave env**)
	- `04v_allseeds_visualizer.py`: plot a Sky Quadrant count map and overimpose all the raw seeds found there
	- `04w_seedlist_visualizer.py`: plot a Sky Quadrant count map and overimpose all the seeds, computed from the raw ones after merging and removing duplicates within a certain range
	- `04z_merger.py`: merge the list of seeds and remove duplicates within a certain distance
- `04*.sh`: scripts to automatize the call to the `04*.py` scripts
	- `04a_quadrant_caller.sh`: call `04a_seeder_clustering.py` on all Sky Quadrants (made for use in local)
	- `04a_quadrant_sbatcher.sh`: call `04a_seeder_clustering.py` on all Sky Quadrants (made to submit jobs via slurm)
	- `04e_quadrant_caller.sh`: call `04e_seeder_pgwave` on all Sky Quadrants (made for use in local) (**fermi4pgwave env**)
- `05*.py`: scripts to evaluate the performance / calibrate the response of pgwave2D through simulations
	- `05a_simulate4pgwave.py`: use fermipy to simulate the count map of a Sky Quadrant
	- `05b_pgwaveOnSim.py`: call pgwave2D on the simulated count map produced with `05a_simulate4pgwave.py`
	- `05c_optConfigFinder.py`: analyze the output of pgwave2D with different hyperparameters configurations and determine which gives the best performance in terms of TPR/FPR (**fermi4pgwave env**)
	- `05d_calibrate4pgwave.py`: collect the data of all Sky Quadrants with the optimal configuration and calibrate pgwave2D
- `05*.sh`: scripts to automatize the call to the 05*.py scripts
	- `05a_quadrant_caller.py`: call `05a_simulate4pgwave.py` on all Sky Quadrants
	- `05b_quadrant_caller.py`: call `05b_pgwaveOnSim.py` on all Sky Quadrants
- `SkyQuadrants.dat`: subdivision of the full sky view in quadrants, necessary for running the seeding tools
- `Tester*.ipynb`: Jupyter Notebooks useful for small tests which were done before writing the `.py` scripts
- `utils_configcreator.py`: utility script to define the parameters for the data extraction and processing
- `utils_ListPreparator.py`: utility script to prepare list of PH/SC files

Unless otherwise specified, the scripts should be run with the **fermi** environment. The only exceptions are the cases when pgwave2D is used, and thus when the **fermi4pgwave** environment should be used.

## Pipelines
### Seed list creation

- **Optional**: call `04a_quadrant_caller.sh` or `04a_quadrant_sbatcher.sh` to iteratively run HDBSCAN on all the sky quadrants
- Call `04e_quadrant_caller.sh` to iteratively run pgwave2D on all the sky quadrants
- Call `04c_seeder_4FGL.py` and `04d_seeder_3FHL.py` to get the seeds from the previous catalogs
- Call `04z_merger.py` to merge the seeds
- Cross-check your work with `04v_allseeds_visualizer.py` and `04w_seedlist_visualizer.py`

### pgwave2D characterization

- Call `05a_quadrant_caller.sh` to iteratively simulate all the quadrants in the sky using fermipy
- Call `05b_quadrant_caller.sh` to iteratively call pgwave2D on all the simulated sky quadrants, using different combinations of the pgwave2D parameters
- Call `05c_optConfigFinder.py` to determine what combination of the parameters gives the best performance for pgwave2D
- Call `05d_calibrate4pgwave.py` to determine the pgwave2D calibration coefficients with the best parameters configuration
