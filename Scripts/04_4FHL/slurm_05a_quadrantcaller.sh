#! /usr/bin/bash

# ==========================================================================================
#                                          HEADER
# ==========================================================================================
# The aim of this script is to call multiple times the 05a_simulate4pgwave script, changing
# from time to time the quadrant under analysis. This is done through the SLURM workload
# manager!
#
# Syntax to call this script:
# $ sbatch -J SkyQuadrantCaller -p milano -t 0-01:00:00 -A fermi -N 1 -c 1 --mem-per-cpu 32g \
#          -o /sdf/home/p/pietromg/pmg-home/logfile.txt ./slurm_05a_quadrant_caller.sh
#
# ==========================================================================================
#                                        SCRIPT
# ==========================================================================================
# Activate the fermitools
source /sdf/group/fermi/sw/conda/bin/activate fermitools-2.2.0

# Define what catalog to use for the analysis
catalog='4FGL'
#catalog='3FHL'

# Count the number of lines in the Sky Quadrant file (== number of times to call the py script)
wc_output=$(wc -l "SkyQuadrants.dat")
wc_output=$(echo $wc_output)
numLines=${wc_output%% *}
numLines="$((numLines-1))"

# Iterate over every quadrant to be analyzed
echo "Bash script starting!"
for i in $(seq 0 $numLines)
do
    # Call the script
    python3 05a_simulate4pgwave.py "${i}" "${catalog}"
done

# The end
echo "Bash script completed!"