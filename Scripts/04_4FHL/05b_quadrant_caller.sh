#! /usr/bin/bash

# ==========================================================================================
#                                          HEADER
# ==========================================================================================
# The aim of this script is to call multiple times the 05b_pgwaveOnSim script, changing
# from time to time the quadrant under analysis and some computational parameters. 
#
# Syntax to call this script (supposing to be already in the "04_4FHL" folder):
# $ ./05b_quadrant_caller.sh
#
# ==========================================================================================
#                                        SCRIPT
# ==========================================================================================
# Define what catalog to use for the analysis
catalog='4FGL'
#catalog='3FHL'

# Count the number of lines in the Sky Quadrant file (== number of quadrants onto which to call the py script)
wc_output=$(wc -l "SkyQuadrants.dat")
wc_output=$(echo $wc_output)
numLines=${wc_output%% *}
numLines="$((numLines-1))"
quadrantsToDo=($(seq 0 $numLines))

# Define the parameter arrays of pgwave2D to be tested
scala=($(seq 2 0.5 8))       # seq: (min step max) or (min max) at step 1
otpix=($(seq 1 1 8))

# Iterate over every quadrant to be analyzed
echo "Bash script starting!"
for i in "${quadrantsToDo[@]}"; do
    for thisScala in "${scala[@]}"; do
        for thisOtpix in "${otpix[@]}"; do
            # Call the script
            python3 05b_pgwaveOnSim.py "${i}" "${catalog}" "${thisScala}" "${thisOtpix}"
        done
    done
done

# The end
echo "Bash script completed!"
