#! /usr/bin/bash

# ==========================================================================================
#                                          HEADER
# ==========================================================================================
# The aim of this script is to call multiple times the 04a_seeder_clustering script, changing
# from time to time the quadrant under analysis. 
#
# Syntax to call this script (supposing to be already in the "04_4FHL" folder):
# $ ./04a_quadrant_caller.sh
#
# ==========================================================================================
#                                        SCRIPT
# ==========================================================================================
# Count the number of lines in the Sky Quadrant file (== number of times to call the py script)
wc_output=$(wc -l "SkyQuadrants.dat")
wc_output=$(echo $wc_output)
numLines=${wc_output%% *}

# Iterate over every quadrant to be analyzed
echo "Bash script starting!"
for i in $(seq 0 $numLines)
do
    # Call the script
    python3 04a_seeder_clustering.py "${i}"
done

# The end
echo "Bash script completed!"
