'''
# ==========================================================================================
#                                       HEADER
# ==========================================================================================
The aim of this script is to create a flux map of the full sky view, for a given energy
band and time window, with a relatively fine granularity. 

-------------------------------------------------------------------------------------------
Call syntax:
  	python3 AllSkyView.py [CMAP_proj] [CMAP_binsz]

Call arguments:
	- CMAP_proj = "AIT" or "CAR". Use this parameter to override the value written in the config creator
	- CMAP_binz = 0.1 or 0.05. Use this parameter to override the value written in the config creator

'''
# ==========================================================================================
#                                 PREAMBLE (module load)
# ==========================================================================================
# Import of the Python Standard Library modules required for the analysis
import glob, os, subprocess, sys             # To act on the operative system
import json                                  # To read and write JSON files
import matplotlib as mpl                     # For plotting
import matplotlib.pyplot as plt              # For plotting
import numpy as np                           # For numerical analysis
import pandas as pd                          # For reading and writing csv files
import warnings                              # To deactivate the warnings
import time                                  # For script timing
import logging                               # For logging purposes
import datetime                              # For timestamp printing
import copy                                  # To copy objects

# Import of the astropy/gammapy/fermipy (& fermitools) modules necessary for astrophysics
import astropy.units as u                    # Celestial units
from astropy.io import fits
from gammapy.maps import Map, WcsNDMap       # To work with sky maps
from astropy.coordinates import SkyCoord     # To work with sky frames

# Import of personal functions from custom modules
sys.path.append("../../Functions")
from AstroFunctions import gtselect, gtmktime, gtbin_CMAP, gtltcube, gtexpcube2
from MyFunctions import truehisto1D, histoplotter1D, fitter_powerlaw

# Set the warnings of a few functions
np.seterr(divide='ignore', invalid='ignore') # To avoid 0/0 errors
mpl.rcParams['backend'] = 'agg'              # Non-interactive backend for plotting
warnings.filterwarnings("ignore", category=DeprecationWarning) # To avoid deprecation warnings

# =========================================================================================
#                                      COMMON DEFINITIONS                                      
# =========================================================================================
# Define the dimension for the fonts in the plots, the figures dimensions and so on
textfont = 20			# Fontsize for labels and legends
dimfig = (12,7)			# Figure dimensions (A4-like)
dimfigbig = (16,12)		# Figure dimensions (a bit larger than A4-like)
dimfiglong = (16,25)	# Figure dimensions (a bit larger and much longer than A4-like)
filetype = '.pdf'		# File format to save the pictures
filedpi = 520			# Resolution used when saving pictures in .png/.jpeg
nptsfit = 10000			# Number of points used in plotting the fits

# ==========================================================================================
#                                  STATUS FILE DATA
# ==========================================================================================
# Open the status file
statusfile = '../statusfile.json'
with open(statusfile) as json_file:
	statusdata = json.load(json_file)

# Extract all the relevant variables from the file
datadir = statusdata['datadir']
imgdir = statusdata['imgdir']
tempdir = statusdata['tempdir']
dataSCdir = statusdata['dataSCdir']
dataPHdir = statusdata['dataPHdir']
catalog_file_4FGL = statusdata['catalog_file_4FGL']
catalog_file_3FHL = statusdata['catalog_file_3FHL']
catalog_file_3FHL_reg = statusdata['catalog_file_3FHL_reg']
catalog_file_3FHL_regAssoc = statusdata['catalog_file_3FHL_regAssoc']

# Define the name for the subfolders (in imgdir and tempdir) where we will save
# the temporary images and files
pretempdir = '04_4FHL'
imgdir = os.path.join(imgdir,pretempdir)
tempdir = os.path.join(tempdir,pretempdir)
subimgdir = os.path.join(imgdir,'02_SkyMaps')
if not os.path.exists(subimgdir):
	os.mkdir(subimgdir)

# Setting of the logging module
logfile = "FullSkyMapViewer.log"
logging.basicConfig(format='[%(asctime)s] %(message)s',level=logging.INFO,
					handlers=[logging.FileHandler(logfile,mode='w'),
					logging.StreamHandler()])
logging.info("*"*75)
logging.info(f"Beginning the execution of {__file__}")
logging.info(f"")

# ==========================================================================================
#                                 CONFIG FILE DATA
# ==========================================================================================
# Open the configuration file made for the 1st level trigger and extract the variables
# necessary for the analysis
configfile = os.path.join(tempdir,f'configfile_trignum_0.json')
with open(configfile) as json_file:
	configdata = json.load(json_file)

# Extract the useful information
Emin              = configdata['Emin']               # gtselect
Emax              = configdata['Emax']               # gtselect
zmax              = configdata['zmax']               # gtselect
evclass           = configdata['evclass']            # gtselect
evtype            = configdata['evtype']             # gtselect  
roicut            = configdata['roicut']             # gtmktime
filtercond        = configdata['filtercond']         # gtmktime
CMAP_proj         = configdata['CMAP_proj']          # gtbin
CMAP_binsz        = configdata['CMAP_binsz']         # gtbin

# =========================================================================================
#                                      INPUT PARSING
# =========================================================================================
# Determine from command line whether it is the case to override the default parameters
# written in the config creator
try:
	CMAP_proj = sys.argv[1]
	logging.info(f"CMAP_proj set to command-line value: {CMAP_proj}")
except IndexError:
	logging.info(f"CMAP_proj was left to the config value: {CMAP_proj}")

try:
	CMAP_binsz = float(sys.argv[2])
	logging.info(f"CMAP_binz set to command-line value: {CMAP_binsz}")
except IndexError:
	logging.info(f"CMAP_binz was left to the config file value: {CMAP_binsz}")

logging.info("")

# ==========================================================================================
#                                 TEMPORARY FILE NAMES
# ==========================================================================================
# Define the names of the .fits, .txt, .xml and .npz files to be opened in the execution
data_PH_after_gtmktime   = os.path.join(tempdir,f"PH_after_gtmktime.fits")
data_PH_after_gtbin      = os.path.join(tempdir,f"PH_after_gtbin_{CMAP_proj}_{int(100*CMAP_binsz):02d}.fits")
data_PH_after_gtexpcube2 = os.path.join(tempdir,f"PH_after_gtexpcube2_{CMAP_proj}_{int(100*CMAP_binsz):02d}.fits")

# ==========================================================================================
#                            PHOTON DATA AND ENERGY SPECTRUM
# ==========================================================================================
# Open the gtmktime output file and compute some relevant informations
with fits.open(data_PH_after_gtmktime) as hdul:
	# Compute the total ON time (sum of GTIs) and total mission time covered
	tStart = hdul[2].header['TSTART']
	tStop = hdul[2].header['TSTOP']
	elapsedTime = tStop-tStart
	onTime = hdul[2].header['ONTIME']
	onFraction = onTime / elapsedTime

	# Compute the number of photon events acquired and extract the data for later
	data_energy = np.array(hdul[1].data.field("ENERGY"),dtype=np.float64) / 1000 # scale to GeV
	data_times = np.array(hdul[1].data.field("TIME"),dtype=np.float64)
	nPhotons = hdul[1].header['NAXIS2']

# Print some informations
y2s = 365*24*3600 # how many s in 1 y

logging.info(f"Analyzing data from {tStart} to {tStop} [MET]")
logging.info(f"Total elapsed time: {elapsedTime:.3f} s ({elapsedTime/y2s:.3f} y)")
logging.info(f"Event class {evclass}, type {evtype}")
logging.info(f"Zenith cut: {zmax}°")
logging.info(f"Energy cut: from {Emin/1000} to {Emax/1000} GeV")
logging.info(f"Number of photon events: {nPhotons}")
logging.info(f"gtmktime cut condition: {filtercond} (roicut = {roicut})")
logging.info(f"Total observation time: {onTime:.3f} s ({onTime/y2s:.3f} y, {onFraction*100:.3f} % of the total)\n\n")

# More printouts
logging.info(f"Photon statistics:")
logging.info(f"\t Over  10 GeV: {np.sum( (data_energy > 10) ):08d}")
logging.info(f"\t Over  50 GeV: {np.sum( (data_energy > 50) ):08d}")
logging.info(f"\t Over 100 GeV: {np.sum( (data_energy > 100) ):08d}")
logging.info(f"\t Over 500 GeV: {np.sum( (data_energy > 500) ):08d}")

# Histogram of the photon energies
nbins = 500
histo, bins = truehisto1D(data_energy,nbins)

#norma = np.sum(histo)
#histo = histo / norma

# Fit
iffit = False 
if iffit:
	result, xth, yth = fitter_powerlaw(bins,histo,'P',fitcut_up = 100)
	N0, gamma = result.params['N0'].value, result.params['gamma'].value
	yth = yth / norma

# Plot
fig,ax = plt.subplots(figsize=dimfig)
histoplotter1D(ax,bins,histo,'Energy [GeV]','Counts','Data','best',textfont)
if iffit:
	ax.plot(xth,yth,color='red',linestyle='-',linewidth=1.5,label=f'Power-law fit: {N0/norma:.2f}*E^({gamma:.2f})')
	ax.legend(loc='upper right',framealpha=1,fontsize=textfont)
ax.set_xlim([0,750])
ax.set_yscale('log')
fig.set_tight_layout('tight')
fig.savefig(os.path.join(subimgdir,f'Fullsky_Espectrum.png'),dpi=filedpi)
fig.savefig(os.path.join(subimgdir,f'Fullsky_Espectrum.pdf'),dpi=filedpi)
plt.close(fig)


# ==========================================================================================
#		                       COUNT MAP EXTRACTION AND SMOOTHING
# ==========================================================================================
# Open the full-sky count map and exposure map
countmap_skyview = WcsNDMap.read(data_PH_after_gtbin)
expmap_skyview = WcsNDMap.read(data_PH_after_gtexpcube2).sum_over_axes(keepdims=False)

# Compute the flux map and remove outliars, then associate it to a proper gammapy object
fluxmap_data = countmap_skyview.data / expmap_skyview.data
badinds = (np.isinf(fluxmap_data)) | (np.isnan(fluxmap_data))
fluxmap_data[badinds] = 0

fluxmap_skyview = copy.deepcopy(countmap_skyview)
fluxmap_skyview.data = fluxmap_data

# Smooth the count and flux map
LAT_PSF = 0.5			# LAT PSF = 0.15° at 10 GeV, 0.1° at higher energies; smoothing 0.5° is visually nice

countmap_skyview_smoothed = countmap_skyview.smooth(LAT_PSF,kernel='gauss')
fluxmap_skyview_smoothed = fluxmap_skyview.smooth(LAT_PSF,kernel='gauss')

# ==========================================================================================
#                                       PLOTTING / SKY-WIDE
# ==========================================================================================
# Show all the maps in dedicated plots
allmaps = [countmap_skyview,expmap_skyview,fluxmap_skyview,countmap_skyview_smoothed,fluxmap_skyview_smoothed]
allylabels = [f"Number of photons [counts / bin]", f"Exposure", r"Flux [photons $cm^{-2}$ $s^{-1}$]",
              f"Number of photons [counts / bin]", r"Flux [photons $cm^{-2}$ $s^{-1}$]"]
allstretches = ['log','linear','log','log','log']
thiscmap = 'gnuplot2'
allnames = ['countmap','expmap','fluxmap','countmap_smooth','fluxmap_smooth']
normafactors = [10,1,10,10,10]

for i,showmap in enumerate(allmaps):
	# Show the map
	logging.info(f"Plotting {allnames[i]}...")
	fig = plt.figure(figsize = dimfigbig)
	ax = showmap.plot(fig = fig, stretch=allstretches[i], vmax=np.max(showmap.data)/normafactors[i], 
					  cmap=thiscmap, add_cbar = False)#, kwargs_colorbar={'fraction':0.5,'panchor':(1.0,0.8)})

	# Set the colorbar parameters
	thisImage = ax.get_images()[0]
	cbar = fig.colorbar(thisImage, ax=ax,shrink=0.6)
	cbar.ax.tick_params(labelsize=textfont*0.75)
	cbarlabel = allylabels[i]
	cbar.set_label(cbarlabel,fontsize=textfont,rotation=270,labelpad=50)

	# Set the axes
	lat = ax.coords['glat']
	lon = ax.coords['glon']
	if (CMAP_proj == "AIT"):
		lon.set_axislabel('Galactic Longitude [deg]',fontsize=textfont,minpad=12)
	else:
		lon.set_axislabel('Galactic Longitude [deg]',fontsize=textfont)
	lat.set_axislabel('Galactic Latitude [deg]',fontsize=textfont)

	# Refine the graphics and save the output
	figname = os.path.join(subimgdir,f"{allnames[i]}")
	ax.tick_params(axis='both',labelsize = textfont)
	ax.coords.grid(color='white', alpha=0.5, linestyle='solid')
	ax.patch.set_edgecolor('black')
	ax.patch.set_linewidth(1)
	ax.texts[0].remove()
	fig.set_tight_layout('tight')
	fig.savefig(figname + f'_{CMAP_proj}_{int(100*CMAP_binsz):02d}.png',dpi=filedpi,bbox_inches='tight',pad_inches=0.05)
	fig.savefig(figname + f'_{CMAP_proj}_{int(100*CMAP_binsz):02d}.pdf',dpi=filedpi,bbox_inches='tight',pad_inches=0.05)
	plt.close(fig)
	logging.info("Done!")
	logging.info("")

# ==========================================================================================
# Final printouts
logging.info(f"")
logging.info(f"Completed the execution of {__file__}")
logging.info("*"*75)
