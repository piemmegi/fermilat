'''
# ==========================================================================================
#                                       HEADER
# ==========================================================================================
The aim of this script is to create a json file which will be used for the execution of
the 01_DataExtractor script.

-------------------------------------------------------------------------------------------
Call syntax:
  	python3 utils_configcreator.py

'''
# ==========================================================================================
#                                 PREAMBLE (module load)
# ==========================================================================================
# Import of the Python Standard Library modules required for the analysis
import glob, os, subprocess, sys       # To act on the operative system
import json                            # To read and write JSON files
import matplotlib as mpl               # For plotting
import matplotlib.pyplot as plt        # For plotting
import numpy as np                     # For numerical analysis
import pandas as pd 				   # For reading and writing csv files
import warnings                        # To deactivate the warnings
import time                            # For script timing
import logging						   # For logging purposes

# Import of personal functions from custom modules
sys.path.append("../../Functions")
from AstroFunctions import J2000formatter

# ==========================================================================================
#                                  STATUS FILE DATA
# ==========================================================================================
# Open the status file
statusfile = '../statusfile.json'
with open(statusfile) as json_file:
	statusdata = json.load(json_file)

# Extract all the relevant variables from the file
datadir = statusdata['datadir']
imgdir = statusdata['imgdir']
tempdir = statusdata['tempdir']
dataSCdir = statusdata['dataSCdir']
dataPHdir = statusdata['dataPHdir']
catalog_file_4FGL = statusdata['catalog_file_4FGL']
catalog_file_3FHL = statusdata['catalog_file_3FHL']

# Define the name for the subfolders (in imgdir and tempdir) where we will save
# the temporary images and files
pretempdir = '04_4FHL'
imgdir = os.path.join(imgdir,pretempdir)
tempdir = os.path.join(tempdir,pretempdir)

# Setting of the logging module
logfile = "debug.log"
logging.basicConfig(format='[%(asctime)s] %(message)s',level=logging.INFO,
					handlers=[logging.FileHandler(logfile,mode='w'),
					logging.StreamHandler()])

logging.info("*"*75)
logging.info(f"Beginning the execution of {__file__}")
logging.info(f"")

# ==========================================================================================
#                                 HARD-CODED PARAMETERS
# ==========================================================================================
# In this section we define all the parameters needed to work with the fermitools.
# Specifically, we first define the parameters used in the data selection with gtselect,
# gtmktime and gtbin (or a generic fermitools utility)
trignum = 0							# [numerical] Used for saving the temp file generated with this script
tstart = "239557417"				# [MET] Beginning of the time window for the analysis
tstop = "744479021"		          	# [MET] End of the time window for the analysis
wstart = 9							# [Mission week] First mission week to be analyzed
wstop = 844						    # [Mission week] Last mission week to be analyzed
ROI_ra = ROI_dec = ROI_rad = "INDEF" # [deg] ROI center in RA/DEC
Emin = 10*1e3						# [MeV] Minimum photon energy
Emax = 2000*1e3						# [MeV] Maximum photon energy
zmax = 105							# [deg] Maximum apparent zenith angle
evclass = 128						# [int] Event class: P8R3_SOURCE class (128) or anything else
evtype = 3							# [int] Event conversion type: Front + Back class (3) or anything else
roicut = 'no'						# [str] Whether to apply a ROI cut in gtmktime
filtercond = '(IN_SAA!=T)&&(DATA_QUAL>0)&&(LAT_CONFIG==1)' # gtmktime selection cut
chatter = 2							# [int] Verbosity for the fermitools. 1: minimal. 2: medium. 4: extensive.
gtmode = "h"						# [str] Running mode of all fermitools: batch mode ("h") or interactive mode ("ql")

# Here we define the variables for gtltcube, gtexpmap and gtbin in CMAP mode
dcostheta = 0.025					# [deg] Angular step in the calculation of the LAT livetime
binsz = 0.25						# [deg] Angular step in the calculation of the exposure map
expmap_evtype = "INDEF"				# [str] Event type for gtexpmap
expmap_irfs = "CALDB"				# [str] IRFs for expmap
xmlclobber = 'yes'					# [str] Whether to overwrite or not the existing XML files
CMAP_algorithm = 'CMAP'				# Parameter for gtbin
CMAP_binsz = 0.1					# Binning step of the maps in x-y (x=long,y=lat)
#CMAP_binsz = 0.05					# Binning step of the maps in x-y (x=long,y=lat)
CMAP_nxpix = int(360/CMAP_binsz)	# Number of pixels in x for a full sky view
CMAP_nypix = int(180/CMAP_binsz)	# Number of pixels in y for a full sky view
CMAP_coordsys = 'GAL'				# Frame of reference
CMAP_xref = CMAP_yref = 0			# Center of the frame in x-y
CMAP_axisrot = 0					# Rotation angle in the x-y plane
CMAP_proj = 'AIT'					# Projection style
#CMAP_proj = 'CAR'					# Projection style
CMAP_indef = 'INDEF'				# For undefined parameters
expcubes_cmap = 'none'				# Parameter for gtexpcube2
expcubes_ebinalg = 'LOG'			# Binning in energy for gtexpcube2
expcubes_enumbins = 20				# Number of bins for gtexpcube2
expcubes_irfs = "P8R3_SOURCE_V3"    # IRFs

# ==========================================================================================
#                                 LIST FILE CREATION
# ==========================================================================================
# Here we define the names of the Spacecraft and Photon file lists (or files)
# necessary for the 1st level trigger analysis
listdir = os.path.join(tempdir,'Lists')
if (not os.path.exists(listdir)):
	os.mkdir(listdir)

PHlistname = os.path.join(listdir,f'PH_events_trig_{trignum}_w{wstart:03d}_w{wstop:03d}.list')
SClistname = os.path.join(listdir,f'SC_events_trig_{trignum}_w{wstart:03d}_w{wstop:03d}.list')

# Now that we know the weeks to be analyzed, we can produce the .list file containing the
# names of the photon and spacecraft files to be analyzed.
if (not os.path.exists(PHlistname)) | (not os.path.exists(SClistname)):
	logging.info(f"Missing the .list files. Let's fix this...")
	subprocess.run(['python3','utils_ListPreparator.py',f'{wstart}',f'{wstop}',
				PHlistname,SClistname])
else:
	logging.info(f"The .list files founds were correctly located!")

# Now we can call the HEASOFT fmerge function (non-python, but should be installed)
# to merge the spacecraft files listed in the .list file.
SCdir = os.path.join(tempdir,'SCfiles')
SCfilename = os.path.join(SCdir,f"SC_events_w{wstart:03d}_w{wstop:03d}.fits")

if (not os.path.exists(SCdir)):
	os.mkdir(SCdir)

if os.path.exists(SCfilename):
	logging.info(f"The spacecraft merged file already exists: we are not recreating it!")
else:
	logging.info(f"The spacecraft merged file does not exist: calling fmerge to create it...")
	subprocess.run(['punlearn','fmerge'])
	subprocess.run(['fmerge',f'@{SClistname}',SCfilename,'-','clobber=yes']) # clobber == enable overwriting

logging.info(f"Done!")
logging.info(f"")

# ==========================================================================================
#                                  JSON FILE PRODUCTION
# ==========================================================================================
# Here we create the json string to be saved
jsonstring =   {"wstart":				wstart,				# gtselect
				"wstop":				wstop,				# gtselect
			  	"tstart":				tstart,				# gtselect
			  	"tstop":				tstop,				# gtselect 
				"ROI_ra":				ROI_ra,				# gtselect
				"ROI_dec":				ROI_dec,			# gtselect
				"ROI_rad":				ROI_rad,			# gtselect
				"Emin":					Emin,				# gtselect
				"Emax":					Emax,				# gtselect
				"zmax":					zmax,				# gtselect
				"evclass":				evclass,			# gtselect
				"evtype":				evtype,				# gtselect
				"roicut":				roicut,				# gtmktime
				"filtercond":			filtercond,			# gtmktime
				"chatter":				chatter,			# All fermitools
				"gtmode":				gtmode,				# All fermitools
				"dcostheta":			dcostheta,			# gtltcube
				"binsz":				binsz,				# gtexpmap
				"expmap_evtype":		expmap_evtype,		# gtexpmap
				"expmap_irfs":			expmap_irfs,		# gtexpmap
				"CMAP_algorithm":		CMAP_algorithm,		# Flux maps
				"CMAP_binsz":			CMAP_binsz,			# Flux maps
				"CMAP_nxpix":			CMAP_nxpix,			# Flux maps
				"CMAP_nypix":			CMAP_nypix,			# Flux maps
				"CMAP_coordsys":		CMAP_coordsys,		# Flux maps
				"CMAP_xref":			CMAP_xref,			# Flux maps
				"CMAP_yref":			CMAP_yref,			# Flux maps
				"CMAP_axisrot":			CMAP_axisrot,		# Flux maps
				"CMAP_proj":			CMAP_proj,			# Flux maps
				"CMAP_indef": 			CMAP_indef,			# Flux maps
				"expcubes_cmap":		expcubes_cmap,		# gtexpcube2
				"expcubes_ebinalg":		expcubes_ebinalg,	# gtexpcube2
				"expcubes_enumbins":	expcubes_enumbins,	# gtexpcube2
				"expcubes_irfs":        expcubes_irfs,      # gtexpcube2
				"SCdir":				SCdir,				# File management
				"PHlistname":			PHlistname,			# File management
				"SClistname":			SClistname,			# File management
				"SCfilename":			SCfilename}			# File management

# Now converting the string in actual json, and then saving
json_string = json.dumps(jsonstring,indent=4)
filename = os.path.join(tempdir,f'configfile_trignum_{trignum}.json')
with open(filename, 'w') as outfile:
    outfile.write(json_string)

logging.info(f"The configuration file was correctly written at the following path:")
logging.info(filename)
logging.info(f"")
logging.info(f"Completed the execution of {__file__}")
logging.info("*"*75)