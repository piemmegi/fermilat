'''
# ==========================================================================================
#                                       HEADER
# ==========================================================================================
The aim of this script is to analyze the 4FHL dataset to determine a list of seeds of VHE
sources, which will be later feed as input to the likelihood analysis stage. In this case,
the analysis is performed using the pgwave2D tool on a subquadrant of the sky.

The idea of this script is:
- Parse inputs to understand on which quadrant to work
- Load the quadrant extension and center
- Call gtbin on the 16-year data to create the counts map of the quadrant
- Call pgwave2D on the counts map
- Save the putputs

-------------------------------------------------------------------------------------------
Call syntax:
  	python3 04e_seeder_pgwave.py [quadrant]

'''
# ==========================================================================================
#                                 PREAMBLE (module load)
# ==========================================================================================
# Import of the Python Standard Library modules required for the analysis
import glob, os, subprocess, sys			# To act on the operative system
import json									# To read and write JSON files
import matplotlib as mpl					# For plotting
import matplotlib.pyplot as plt				# For plotting
import numpy as np							# For numerical analysis
import pandas as pd							# For reading and writing csv files
import warnings								# To deactivate the warnings
import time									# For script timing
import logging								# For logging purposes
import datetime								# For timestamp printing
import copy									# To copy objects

# Import of personal functions from custom modules
sys.path.append("../../Functions")
from AstroFunctions import pgwave2D, gtbin_CMAP, gal2radec, radec2gal

# Set the warnings of a few functions
np.seterr(divide='ignore', invalid='ignore')	# To avoid 0/0 errors
mpl.rcParams['backend'] = 'agg'					# Non-interactive backend for plotting
warnings.filterwarnings("ignore", category=DeprecationWarning) # To avoid deprecation warnings

# =========================================================================================
#                                      COMMON DEFINITIONS                                      
# =========================================================================================
# Define the dimension for the fonts in the plots, the figures dimensions and so on
textfont = 20			# Fontsize for labels and legends
dimfig = (12,7)			# Figure dimensions (A4-like)
dimfigbig = (16,12)		# Figure dimensions (a bit larger than A4-like)
dimfiglong = (16,25)	# Figure dimensions (a bit larger and much longer than A4-like)
filetype = '.pdf'		# File format to save the pictures
filedpi = 520			# Resolution used when saving pictures in .png/.jpeg
nptsfit = 10000			# Number of points used in plotting the fits

# ==========================================================================================
#                                  STATUS FILE DATA
# ==========================================================================================
# Open the status file
statusfile = '../statusfile.json'
with open(statusfile) as json_file:
	statusdata = json.load(json_file)

# Extract all the relevant variables from the file
datadir = statusdata['datadir']
imgdir = statusdata['imgdir']
tempdir = statusdata['tempdir']
dataSCdir = statusdata['dataSCdir']
dataPHdir = statusdata['dataPHdir']
catalog_file_4FGL = statusdata['catalog_file_4FGL']
catalog_file_3FHL = statusdata['catalog_file_3FHL']
catalog_file_3FHL_reg = statusdata['catalog_file_3FHL_reg']
catalog_file_3FHL_regAssoc = statusdata['catalog_file_3FHL_regAssoc']

# Define the name for the subfolders (in imgdir and tempdir) where we will save
# the temporary images and files
pretempdir = '04_4FHL'
imgdir = os.path.join(imgdir,pretempdir)
tempdir = os.path.join(tempdir,pretempdir)

# Setting of the logging module
logfile = "debug.log"
logging.basicConfig(format='[%(asctime)s] %(message)s',level=logging.INFO,
					handlers=[logging.FileHandler(logfile,mode='w'),
					logging.StreamHandler()])
logging.info("*"*75)
logging.info(f"Beginning the execution of {__file__}")
logging.info(f"")

# Additional security printout because in this script we have to work with a different conda env
logging.info("*"*100)
logging.info("Are you sure to be running in a conda env with pgwave2D available? (e.g. fermi4pgwave)")
logging.info("\t\t\t\tCHECK IT!!!")
logging.info("If you are not, you may see an error like <time: cannot run pgwave2D: No such file or directory>")
logging.info("*"*100)

# ==========================================================================================
#                                 INPUT PARSING
# ==========================================================================================
# Parse the command-line to know the quadrant to be analyzed (see after)
try:
	quadrantInd = int(sys.argv[1])
except:
	quadrantInd = 0
	logging.info(f"quadrantInd variable was badly set from command line. Defaulting to {quadrantInd}...")
	logging.info("")

# ==========================================================================================
#                               REGION DEFINITION
# ==========================================================================================
# Open the data file with the quadrant coordinates and determine the latitude and longitude interval
# of the current quadrant. Leave a margin to be sure
quadrantMarginAngle = 5

quadrantFile = "./SkyQuadrants.dat"
lat_min, lat_max, long_min, long_max = np.loadtxt(quadrantFile, skiprows = 1, unpack = True, dtype = np.float64,
													delimiter = "\t")
lat_min = lat_min[quadrantInd]
lat_max = lat_max[quadrantInd]
long_min = long_min[quadrantInd]
long_max = long_max[quadrantInd]

lat_center = (lat_max + lat_min) / 2
long_center = (long_max + long_min) / 2

lat_width = (lat_max - lat_min) + 2*quadrantMarginAngle
long_width = (long_max - long_min) + 2*quadrantMarginAngle

# Are we gal or egal?
egal_cutoff = 10
ifegals = (np.abs(lat_center) >= egal_cutoff)
egalstr = 'egal' if ifegals else 'gal'

# ==========================================================================================
#                               COUNTS MAP PARAMETERS
# ==========================================================================================
# Open the config file used for the 16 years analysis to get some of the parameters required
# for the creation of the counts map
generalconfigfile = os.path.join(tempdir,'configfile_trignum_0.json')
with open(generalconfigfile) as json_file:
	generalconfigdata = json.load(json_file)

# Define the other parameters manually
subdir = "04_pgwave"
PH_after_gtmktime = os.path.join(tempdir,'PH_after_gtmktime.fits')
PH_after_gtbin = os.path.join(tempdir,subdir,f'PH_after_gtbin_Q{quadrantInd:02d}.fits')

max_lat = 10
if np.abs(lat_center) <= max_lat: # More spatial resolution in the galactic center
    binsz = 0.05        # Bin size for the gtbin maps, in deg
else:
    binsz = 0.1         # Bin size for the gtbin maps, in deg    
proj = 'CAR'                      # Projection type (AIT, CAR, etc.)
nxpix = int(long_width / binsz)   # Number of bins in longitude
nypix = int(lat_width / binsz)    # Number of bins in latitude

# ==========================================================================================
#                               COUNTS MAP CREATION
# ==========================================================================================
# Run gtbin to create the count map
ifDoAnyways = True
if ifDoAnyways:
	gtbin_CMAP(infile = PH_after_gtmktime, outfile = PH_after_gtbin, SCfile = generalconfigdata['SCfilename'],
	           algorithm = generalconfigdata['CMAP_algorithm'],
	           nxpix = nxpix, nypix = nypix, binsz = binsz, 
	           coordsys = generalconfigdata['CMAP_coordsys'], 
	           xref = long_center, yref = lat_center, proj = proj,
	           axisrot = generalconfigdata['CMAP_axisrot'], 
	           chatter = generalconfigdata['chatter'],
	           gtmode = generalconfigdata['gtmode'])
else:
	logging.info(f"Skipping gtbin...")
	logging.info(f"")

# ==========================================================================================
#                                 PGWAVE PARAMETERS
# ==========================================================================================
# Define the parameters for pgwave2D. If possible, load them from the hyperscan done in scripts 05*
modelcatalog = "4FGL"
pgwave_simdir = f'05_{modelcatalog}_simPgwaveOut'
optfilename = os.path.join(tempdir, pgwave_simdir, 'pgwave2D_bestconfig.npz')
try:
	with np.load(optfilename) as f:
		scala = float(f[f'best_{egalstr}_scala'])
		otpix = int(f[f'best_{egalstr}_otpix'])
	logging.info(f"pgwave2D optimal parameters were read from file {optfilename}")

except FileNotFoundError:
	scala = 2
	otpix = 4
	logging.info(f"pgwave2D optimal parameters were not found... using defaults instead")

logging.info(f"Using: scala = {scala} and otpix = {otpix}")

# Other parameters are defined by hand
proj = 'CAR'
if (proj == 'CAR'):
	circ_square = 's'
elif (proj == 'AIT'):
	circ_square = 'c'
	
bgk_choise = 'n'    # Do we have a bkg map? Probably not
median_box = 3      # Dimension for the median filter (in bkg reconstruction)
kappa = 3           # Number of sigmas relative to the statistical confidence
min_pix = int(max(2,float(scala)-1)+0.5) # Minimum separation between adjacent sources
m_num = otpix       # Repetition of otpix? Unclear, TBC
verbosity = 0       # Output verbosity. Advice: keep to zero, it talks too much

# ==========================================================================================
#                                  PGWAVE RUN
# ==========================================================================================
# Call the script
ifDoAnyways = True
if (ifDoAnyways):
	logging.info(f"Calling pgwave2D on Sky Quadrant n. {quadrantInd}...")
	pgwave2D(infile=PH_after_gtbin,bgk_choise=bgk_choise,circ_square=circ_square,
             scala=scala,otpix=otpix,median_box=median_box,kappa=kappa,min_pix=min_pix,
             m_num=m_num,verbosity=verbosity)
else:
	logging.info(f"Skipping pgwave2D...")
	logging.info(f"")

# ==========================================================================================
#                           OUTPUT PARSING: PGWAVE SOURCES
# ==========================================================================================
# Parse the output of the script. [Personal note: l and b are listed IN REVERSE in the .list
# files, but this does not agree with the .reg output file and also with the fact that l and b
# have certain domains (-pi/2 < b < pi/2, 0 < l < 2pi). These domains can be respected only if 
# I consider the default column ordering in the .list file to be wrong!]
pgoutfile = PH_after_gtbin.split('.')[0] + '.list'
ID, X, Y, l, b, pos_err, snr, k_signif, counts, sigC, bkg, sigbkg = np.loadtxt(pgoutfile,skiprows=1,unpack=True)

# Cut the sources outside the nominal borders of the quadrant, to avoid border effects
cond = (l >= long_min - quadrantMarginAngle) & (l <= long_max + quadrantMarginAngle) & \
	   (b >= lat_min - quadrantMarginAngle) & (b <= lat_max + quadrantMarginAngle)

logging.info(f"{np.shape(ID)[0]} sources were found by pgwave2D: {np.sum(cond)} inside the nominal border of the ROI, {np.shape(ID)[0] - np.sum(cond)} in the safety margin")

# ==========================================================================================
#                                     OUTPUT SAVING
# ==========================================================================================
# Save the outputs in the common form used by all the other scripts
outfilename = os.path.join(tempdir,"04_seedlists",f'seedlist_pgwave2D_Q{quadrantInd:02d}.npz')

logging.info(f"")
logging.info(f"Saving the localized sources data in file {outfilename}")
np.savez_compressed(outfilename, sources_ID = ID[cond],
                                 sources_X = X[cond],
                                 sources_Y = Y[cond],
                                 sources_l = l[cond],
                                 sources_b = b[cond],
                                 sources_pos_err = pos_err[cond],
                                 sources_snr = snr[cond],
                                 sources_k_signif = k_signif[cond],
                                 sources_counts = counts[cond],
                                 sources_sigC = sigC[cond],
                                 sources_bkg = bkg[cond],
                                 sources_sigbkg = sigbkg[cond])

logging.info("Saving completed!")

# ==========================================================================================
# Final printouts
logging.info(f"")
logging.info(f"Completed the execution of {__file__}")
logging.info("*"*75)