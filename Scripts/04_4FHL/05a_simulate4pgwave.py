'''
# ==========================================================================================
#                                       HEADER
# ==========================================================================================
The aim of this script is to perform a simulation of the counts obtained in a given ROI,
using fermipy. This is useful becaue it will allow us to understand the precision and robustness
of pgwave2D, while also performing a calibration of its output from raw counts to fluxes.

-------------------------------------------------------------------------------------------
Call syntax:
  	python3 05a_simulate4pgwave.py [quadrant] [modelcatalog]

Arguments:
- quadrant: int, index of the quadrant to analyze
- modelcatalog: "4FGL" or "3FHL", the model used for the analysis
'''
# ==========================================================================================
#                                 PREAMBLE (module load)
# ==========================================================================================
# Import of the Python Standard Library modules required for the analysis
import glob, os, subprocess, sys			# To act on the operative system
import json									# To read and write JSON files
import matplotlib as mpl					# For plotting
import matplotlib.pyplot as plt				# For plotting
import numpy as np							# For numerical analysis
import warnings								# To deactivate the warnings
import time									# For script timing
import logging								# For logging purposes
import datetime								# For timestamp printing
import copy									# To copy objects

import yaml                                 # For editing yaml files (necessary for fermipy)
from fermipy.gtanalysis import GTAnalysis   # To run fermipy utilities
from astropy.io import fits                 # To open fits files
from gammapy.maps import WcsNDMap           # To plot maps

import matplotlib.colors as colors          # Required to define histoplotter2D
import matplotlib.cm as pltmaps

# Import of personal functions from custom modules
sys.path.append("../../Functions")
from AstroFunctions import gtbin_CMAP, pgwave2D, radec2gal

# Set the warnings of a few functions
np.seterr(divide='ignore', invalid='ignore')	# To avoid 0/0 errors
mpl.rcParams['backend'] = 'agg'					# Non-interactive backend for plotting
warnings.filterwarnings("ignore", category=DeprecationWarning) # To avoid deprecation warnings

# Define a custom function usually located in MyFunctions
def histoplotter2D(ax,binsX,binsY,histo2D,colmap = 'jet',slope = None,xlabel = 'X axis',ylabel = 'Y axis',
                                  textfont = 15,iflog = False,vmin = None,vmax = None,ifcbarfont = False,
                                  cbarlabel = None):
    # Deduce what is the figure from which "ax" is drawn
    f = ax.get_figure()
    
    # Define the colormap and set the bad pixels to the same color of the minimum of the scale.
    my_cmap = eval('copy.copy(pltmaps.' + colmap + ')')
    my_cmap.set_bad(my_cmap(0))

    # Plot the 2D histogram transposed. Note that this is due to how plt.imshow() works, and it assumes
    # that the histogram has been produced with np.histogram2d() or equally with truehisto2D().
    # Choose the normalization as defined by the user.
    if iflog:
        # Assume a log scale and normalize as a consequence.
        thisnorm = colors.LogNorm(vmin = vmin, vmax = vmax)
    else:
        # Assume a linear scale, which can be either of the TwoSlope type or not
        if slope is None:
            thisnorm = colors.Normalize(vmin = vmin, vmax = vmax)
        else:
            thisnorm = colors.TwoSlopeNorm(slope, vmin = vmin, vmax = vmax)

    imhisto = ax.imshow(histo2D.transpose(),cmap = my_cmap,norm=thisnorm,aspect = 'auto',
                                        extent = [binsX[0],binsX[-1],binsY[-1],binsY[0]])

    # Set labels, grid, colorbar and axis propeties.
    ax.set_xlabel(xlabel,fontsize = textfont)
    ax.set_ylabel(ylabel,fontsize = textfont)
    ax.grid(linewidth=0.5)
    cb = f.colorbar(imhisto,ax=ax)
    if ifcbarfont:
        # Set the fontsize for the colorbar
        for el in cb.ax.get_yticklabels():
            el.set_fontsize(textfont)

    if cbarlabel is not None:
        cb.set_label(cbarlabel,fontsize=textfont,rotation=270,labelpad=50)
    ax.tick_params(labelsize = textfont)

    # If there exists only one bin in either direction, set a particular limit for the axis. 
    # Otherwise, use the bins edges.
    if np.min(binsX) == np.max(binsX):
        ax.set_xlim([np.min(binsX)-5,np.max(binsX)+5])
    else:
        ax.set_xlim([np.min(binsX),np.max(binsX)])
        
    if np.min(binsY) == np.max(binsY):
        ax.set_ylim([np.min(binsY)-5,np.max(binsY)+5])
    else:
        ax.set_ylim([np.min(binsY),np.max(binsY)])
    return()

# =========================================================================================
#                                      COMMON DEFINITIONS                                      
# =========================================================================================
# Define the dimension for the fonts in the plots, the figures dimensions and so on
textfont = 20			# Fontsize for labels and legends
dimfig = (12,7)			# Figure dimensions (A4-like)
dimfigbig = (16,12)		# Figure dimensions (a bit larger than A4-like)
dimfiglong = (16,25)	# Figure dimensions (a bit larger and much longer than A4-like)
filetype = '.pdf'		# File format to save the pictures
filedpi = 520			# Resolution used when saving pictures in .png/.jpeg
nptsfit = 10000			# Number of points used in plotting the fits

# ==========================================================================================
#                                  STATUS FILE DATA
# ==========================================================================================
# Open the status file
statusfile = '../statusfile.json'
with open(statusfile) as json_file:
	statusdata = json.load(json_file)

# Extract all the relevant variables from the file
datadir = statusdata['datadir']
imgdir = statusdata['imgdir']
tempdir = statusdata['tempdir']
dataSCdir = statusdata['dataSCdir']
dataPHdir = statusdata['dataPHdir']
catalog_file_4FGL = statusdata['catalog_file_4FGL']
catalog_file_3FHL = statusdata['catalog_file_3FHL']
catalog_file_3FHL_reg = statusdata['catalog_file_3FHL_reg']
catalog_file_3FHL_regAssoc = statusdata['catalog_file_3FHL_regAssoc']

# Define the name for the subfolders (in imgdir and tempdir) where we will save
# the temporary images and files
pretempdir = '04_4FHL'
imgdir = os.path.join(imgdir,pretempdir)
tempdir = os.path.join(tempdir,pretempdir)

# Setting of the logging module
logfile = "debug.log"
logging.basicConfig(format='[%(asctime)s] %(message)s',level=logging.INFO,
					handlers=[logging.FileHandler(logfile,mode='w'),
					logging.StreamHandler()])
logging.info("*"*75)
logging.info(f"Beginning the execution of {__file__}")
logging.info(f"")

# ==========================================================================================
#                                 INPUT PARSING
# ==========================================================================================
# Parse the command-line to know the quadrant to be analyzed (see after)
try:
    quadrantInd = int(sys.argv[1])
    logging.info(f"quadrantInd set to: {quadrantInd}")
except:
    quadrantInd = 0
    logging.info(f"quadrantInd variable was badly set from command line. Defaulting to {quadrantInd}...")
    logging.info("")

# Parse the command-line to know which model to use for the simulations
try:
    modelcatalog = sys.argv[2]
    if (modelcatalog != "3FHL") & (modelcatalog != "4FGL"):
        modelcatalog = "3FHL"
        logging.info(f"modelcatalog variable vas badly set from command line. Defaulting to {modelcatalog}...")    
except:
    modelcatalog = "3FHL"
    logging.info(f"modelcatalog variable vas badly set from command line. Defaulting to {modelcatalog}...")

logging.info(f"Simulating sources from the {modelcatalog}")

# ==========================================================================================
#                               REGION DEFINITION
# ==========================================================================================
# Open the data file with the quadrant coordinates and determine the latitude and longitude interval
# of the current quadrant. Leave a margin to be sure
quadrantMarginAngle = 5

quadrantFile = "./SkyQuadrants.dat"
lat_min, lat_max, long_min, long_max = np.loadtxt(quadrantFile, skiprows = 1, unpack = True, dtype = np.float64,delimiter = "\t")
lat_min = lat_min[quadrantInd]
lat_max = lat_max[quadrantInd]
long_min = long_min[quadrantInd]
long_max = long_max[quadrantInd]

lat_center = int( (lat_max + lat_min) / 2 )
long_center = int( (long_max + long_min) / 2 )

lat_width = (lat_max - lat_min) + 2*quadrantMarginAngle
long_width = (long_max - long_min) + 2*quadrantMarginAngle
roiwidth = int(np.sqrt( (lat_width/2)**2 + (long_width/2)**2 )) # Half-diagonal of a rectangle with sides lat_width, long_width

# ==========================================================================================
#                               FOLDER CLEARING
# ==========================================================================================
# Clear the fermipy service folder from trash left by previous runs of the script
fermipy_subdir = '05_fermipy'
allfiles = glob.glob(os.path.join(tempdir,fermipy_subdir,'*'))
extensionsToRemove = ['txt','fits','xml','npz','par','log']

logging.info(f"Clearing the fermipy service folder located at path:")
logging.info(f"{os.path.join(tempdir,fermipy_subdir)}")
logging.info("")

for i,el in enumerate(allfiles):
    fileExtension = el.split('.')[-1]
    if fileExtension in extensionsToRemove:
        logging.info(f"Removing file: {el}")
        os.remove(el)

logging.info("Done!")

# ==========================================================================================
#                              FERMIPY CONFIGURATION
# ==========================================================================================
# Open the config file used for the 16 years analysis to get important parameters for the 
# fermipy setup
generalconfigfile = os.path.join(tempdir,'configfile_trignum_0.json')
with open(generalconfigfile) as json_file:
	generalconfigdata = json.load(json_file)

# Define some more parameters for the analysis
max_lat = 10
if np.abs(lat_center) <= max_lat: # More spatial resolution in the galactic center
    binsz = 0.05        # Bin size for the gtbin maps, in deg
else:
    binsz = 0.1         # Bin size for the gtbin maps, in deg    

binsperdec = 8      # Bins per decade in energy, for exposure maps
enumbins = None     # Number of bins in energy, for exposure maps
projtype = 'WCS'    # Projection system (WCS or HPX)
proj = 'CAR'        # Projection type (AIT, CAR, etc.)
tstop = 3e8         # Stop time for the ROI (does not need to be 16-year long)

bkgdir = os.path.join(os.environ['FERMI_DIR'],'refdata','fermi','galdiffuse') # Path to the bkg model files
if (modelcatalog == "3FHL"):
    extdir = os.path.join(os.environ['CONDA_PREFIX'],'lib','python3.9','site-packages','fermipy','data',
					  'catalogs','Extended_archive_v18')
else:
    extdir = os.path.join(os.environ['CONDA_PREFIX'],'lib','python3.9','site-packages','fermipy','data',
                      'catalogs','Extended_14years')

src_roiwidth = roiwidth # Add to the model the sources within this distance from ROI center

edisp = False       # Consider energy dispersion in the fits?
edisp_bins = 0      # Number of bins for energy disperion corrections

logging.info(f"Building gtbin count maps with binning step: {binsz} deg")

if modelcatalog == "3FHL":
    modelpath = [os.path.join(datadir,'Catalogs','gll_psch_v13.fit')]
else:
    modelpath = [os.path.join(datadir,'Catalogs','gll_psc_v35.fit')]


# Write a yaml configuration file for fermipy with the correct parameters
logging.info("Configuring the yaml file for fermipy...")
t0 = time.perf_counter()

configfile = os.path.join(tempdir, 'config.yaml')
configdata = {'data': {
                        'evfile': os.path.join(tempdir,'PH_after_gtselect.fits'),
                        'scfile': os.path.join(tempdir,'SCfiles','SC_events_w009_w844.fits'),
                        'ltcube': os.path.join(tempdir,'PH_after_gtltcube.fits')
                      },
              'binning': {
                        'roiwidth'  : roiwidth,
                        'binsz'     : binsz,
                        'coordsys'  : generalconfigdata['CMAP_coordsys'],
                        'binsperdec': binsperdec,
                        'enumbins'  : enumbins,
                        'projtype'  : projtype,
                        'proj'      : proj
                      },
              'selection': {
                        'emin'    : generalconfigdata['Emin'],
                        'emax'    : generalconfigdata['Emax'],
                        'zmax'    : generalconfigdata['zmax'],
                        'evclass' : generalconfigdata['evclass'],
                        'evtype'  : generalconfigdata['evtype'],
                        'tmin'    : generalconfigdata['tstart'],
                        'tmax'    : tstop,
                        'filter'  : generalconfigdata['filtercond'],
                        'roicut'  : generalconfigdata['roicut'],
                        'glat'    : lat_center,
                        'glon'    : long_center
                      },
              'model': {
                        'src_roiwidth' : src_roiwidth,
                        'galdiff'      : os.path.join(bkgdir,'gll_iem_v07.fits'),
                        'isodiff'      : os.path.join(bkgdir,'iso_P8R3_SOURCE_V3_v1.txt'),
                        'catalogs'     : modelpath,
                        'extdir'       : extdir
                      },
              'gtlike': {
                        'edisp'      : edisp,
                        'edisp_bins' : edisp_bins,
                        'irfs'       : generalconfigdata['expcubes_irfs']
                      },
              'fileio': {
                        'outdir' : os.path.join(tempdir,'05_fermipy')
                      }
             }

logging.info(f"Writing .yaml configuration file for fermipy at path:")
logging.info(f"{configfile}")
logging.info(f"")
with open(configfile,'w') as file:
    yaml.dump(configdata, file)

logging.info(f"Configuration file written!")

# ==========================================================================================
#                              FERMIPY SETUP AND SIMULATION RUN
# ==========================================================================================
# Define the fermipy analysis object and perform the setup (== automatic call to gtselect, 
# gtmktime, gtbin, gtltcube, gtexpcube2). Then, extract the count map (we need it since we 
# will overwrite the file where it is stored in a few minutes... better to have it in RAM)
gta = GTAnalysis(configfile,logging={'verbosity' : 3})
gta.setup(init_sources = False, overwrite = True)
original_cmap = gta.counts_map().sum_over_axes(keepdims=False)

# Do the simulation: replace the count map with a simulation and extract it
gta.simulate_roi(name=None,randomize=True,restore=False)
simulated_cmap = gta.counts_map().sum_over_axes(keepdims=False)

# Now create a countmap with the same parameters of the fermipy MC count cube, but with 
# no energy binning (fermipy does it automatically, but we don't want it, to be comparable
# with the gtbin CMAPs)
PH_after_gtmktime = os.path.join(tempdir,'PH_after_gtmktime.fits')
simdir = f'05_{modelcatalog}_simCMAPs'
PH_cmap4pgwave = os.path.join(tempdir,simdir,f'PH_cmap4pgwave_Q{quadrantInd:02d}.fits')
print(PH_cmap4pgwave)
SCfile = os.path.join(tempdir,'SCfiles','SC_events_w009_w844.fits')
nxpix = nypix = int(roiwidth / binsz)

gtbin_CMAP(infile = PH_after_gtmktime, outfile = PH_cmap4pgwave, SCfile = SCfile,
           algorithm = generalconfigdata['CMAP_algorithm'], nxpix = nxpix, nypix = nypix,
           binsz = binsz, coordsys = generalconfigdata['CMAP_coordsys'], 
           xref = long_center, yref = lat_center,
           axisrot = generalconfigdata['CMAP_axisrot'], proj = proj,
           chatter = generalconfigdata['chatter'], gtmode = generalconfigdata['gtmode'])

# Open the CMAP file just created and replace its data
regenerated_cmap = WcsNDMap.read(PH_cmap4pgwave)
with fits.open(PH_cmap4pgwave,mode = 'update') as file:
	file[0].data = simulated_cmap.data
	file.flush()

# ==========================================================================================
#                              PLOTTING
# ==========================================================================================
# Show the maps
alltitles = ['Original CMAP','Simulated CMAP','Fermitools-regenerated CMAP']
allnames = [f'countmap_realdata_Q{quadrantInd:02d}',f'countmap_simulated_Q{quadrantInd:02d}',f'countmap_regen_Q{quadrantInd:02d}']
logging.info("Saving the real and simulated count maps...")

for i,thismap in enumerate([original_cmap,simulated_cmap,regenerated_cmap]):
	# Build coordinate arrays
	xbins = np.arange(long_center-nxpix*binsz/2,long_center+nxpix*binsz/2,binsz) + binsz/2
	ybins = np.arange(lat_center-nypix*binsz/2,lat_center+nypix*binsz/2,binsz) + binsz/2

	# Plot histogram
	fig, ax = plt.subplots(figsize = dimfig)
	histoplotter2D(ax,xbins,ybins,thismap.data.transpose(),'gnuplot2',0,'Galactic longitude [°]','Galactic latitude [°]',
				   textfont,iflog=True,ifcbarfont = True,cbarlabel='Counts / bin')

	# Refine graphics
	ax.set_title(alltitles[i],fontsize=1.5*textfont)
	fig.set_tight_layout('tight')
	fig.savefig(os.path.join(imgdir,fermipy_subdir,allnames[i]+'.pdf'))
	plt.close(fig)

logging.info(f"Plots done!")

# ==========================================================================================
#                                 OUTPUT PARSING
# ==========================================================================================
# First open the simulation data and extract the list of sources inserted there, along with
# their properties (coordinates, fluxes)
allsources = gta.get_sources()

allnames = []
allflux = []
allflux_err = []
all_blat = []
all_blong = []

sourcesToSkip = ['isodiff','galdiff']
logging.info("Parsing the fermipy outputs to obtain the fluxes of the simulated sources...")

for i,thisname in enumerate(allsources):
    if (thisname.name in sourcesToSkip):
        continue

    # Save the source name and location
    allnames.append(thisname.name)
    blong, blat = radec2gal(*gta.get_sources()[i].radec)
    all_blat.append(blat)
    all_blong.append(blong)

    # Save the source flux
    allflux.append(gta.get_src_model(thisname.name)['flux'])
    allflux_err.append(gta.get_src_model(thisname.name)['flux_err'])

all_blat = np.array(all_blat)
all_blong = np.array(all_blong)

# Cut the sources located outside the nominal borders of the quadrant
condReal = (all_blong >= long_min-quadrantMarginAngle) & (all_blong <= long_max + quadrantMarginAngle) & \
            (all_blat >= lat_min-quadrantMarginAngle) & (all_blat <= lat_max + quadrantMarginAngle)
all_blat = all_blat[condReal]
all_blong = all_blong[condReal]

allnames = np.array(allnames)[condReal]
allflux = np.array(allflux)[condReal]
allflux_err = np.array(allflux_err)[condReal]

logging.info("Parsing completed!")
logging.info(f"{np.shape(condReal)[0]} real sources were simulated with fermipy: {np.sum(condReal)} inside the nominal border of the ROI, {np.shape(condReal)[0] - np.sum(condReal)} in the safety margin")

# ==========================================================================================
#                                 OUTPUT SAVING
# ==========================================================================================
# Save all the outputs
outfilename = os.path.join(tempdir,simdir,f'pgSIM_fermisim_Q{quadrantInd:02d}.npz')

logging.info(f"")
logging.info(f"Saving the data of the real sources simulated with fermipy in file {outfilename}")
np.savez_compressed(outfilename, all_blat = all_blat,
                                 all_blong = all_blong,
                                 allnames = allnames,
                                 allflux = allflux,
                                 allflux_err = allflux_err)

t1 = time.perf_counter()
dt = t1-t0
logging.info(f"Time between beginning and end: {dt:.2f} s")

# ==========================================================================================
# Final printouts
logging.info(f"")
logging.info(f"Completed the execution of {__file__}")
logging.info("*"*75)
