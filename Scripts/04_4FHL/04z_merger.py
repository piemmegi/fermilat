'''
# ==========================================================================================
#                                       HEADER
# ==========================================================================================
The aim of this script is to merge the seeds created for the 4FHL catalog, removing duplicates.

-------------------------------------------------------------------------------------------
Call syntax:
  	python3 04z_merger.py

'''
# ==========================================================================================
#                                 PREAMBLE (module load)
# ==========================================================================================
# Import of the Python Standard Library modules required for the analysis
import glob, os, subprocess, sys			# To act on the operative system
import json									# To read and write JSON files
import matplotlib as mpl					# For plotting
import matplotlib.pyplot as plt				# For plotting
import numpy as np							# For numerical analysis
import pandas as pd							# For reading and writing csv files
import warnings								# To deactivate the warnings
import time									# For script timing
import logging								# For logging purposes
import datetime								# For timestamp printing
import copy									# To copy objects

# Import of the astropy/gammapy/fermipy (& fermitools) modules necessary for astrophysics
import astropy.units as u					# Celestial units
from astropy.io import fits                 # To open fits files
from gammapy.maps import Map, WcsNDMap		# To work with sky maps
from astropy.coordinates import SkyCoord 	# To work with sky frames

# Import of personal functions from custom modules
sys.path.append("../../Functions")
#from MyFunctions import ...
from AstroFunctions import angdist

# Set the warnings of a few functions
np.seterr(divide='ignore', invalid='ignore')	# To avoid 0/0 errors
mpl.rcParams['backend'] = 'agg'					# Non-interactive backend for plotting
warnings.filterwarnings("ignore", category=DeprecationWarning) # To avoid deprecation warnings

# =========================================================================================
#                                      COMMON DEFINITIONS                                      
# =========================================================================================
# Define the dimension for the fonts in the plots, the figures dimensions and so on
textfont = 20			# Fontsize for labels and legends
dimfig = (12,7)			# Figure dimensions (A4-like)
dimfigbig = (16,12)		# Figure dimensions (a bit larger than A4-like)
dimfiglong = (16,25)	# Figure dimensions (a bit larger and much longer than A4-like)
filetype = '.pdf'		# File format to save the pictures
filedpi = 520			# Resolution used when saving pictures in .png/.jpeg
nptsfit = 10000			# Number of points used in plotting the fits

# ==========================================================================================
#                                  STATUS FILE DATA
# ==========================================================================================
# Open the status file
statusfile = '../statusfile.json'
with open(statusfile) as json_file:
	statusdata = json.load(json_file)

# Extract all the relevant variables from the file
datadir = statusdata['datadir']
imgdir = statusdata['imgdir']
tempdir = statusdata['tempdir']
dataSCdir = statusdata['dataSCdir']
dataPHdir = statusdata['dataPHdir']
catalog_file_4FGL = statusdata['catalog_file_4FGL']
catalog_file_3FHL = statusdata['catalog_file_3FHL']
catalog_file_3FHL_reg = statusdata['catalog_file_3FHL_reg']
catalog_file_3FHL_regAssoc = statusdata['catalog_file_3FHL_regAssoc']
catalog_templates_3FHL_extended_dir = statusdata['catalog_templates_3FHL_extended_dir']
catalog_templates_3FHL_extended_file = statusdata['catalog_templates_3FHL_extended_file']
catalog_file_2FGES_fits = statusdata['catalog_file_2FGES_fits']

# Define the name for the subfolders (in imgdir and tempdir) where we will save
# the temporary images and files
pretempdir = '04_4FHL'
imgdir = os.path.join(imgdir,pretempdir)
tempdir = os.path.join(tempdir,pretempdir)

# Setting of the logging module
logfile = "debug.log"
logging.basicConfig(format='[%(asctime)s] %(message)s',level=logging.INFO,
					handlers=[logging.FileHandler(logfile,mode='w'),
					logging.StreamHandler()])
logging.info("*"*75)
logging.info(f"Beginning the execution of {__file__}")
logging.info(f"")

# ==========================================================================================
#                                 INPUT FILES
# ==========================================================================================
# Write the names of the files to be opened (note: for the seeding with the clustering
# algorithm we have to use a wildcard through glob)
subdir = "04_seedlists"
seedfile_3FHL = os.path.join(tempdir,subdir,'seedlist_3FHL.npz')
seedfile_4FGL = os.path.join(tempdir,subdir,'seedlist_4FGL.npz')
seedfile_HDBSCAN_list = glob.glob(os.path.join(tempdir,subdir,'seedlist_clusters_HDBSCAN*.npz'))
seedfile_pgwave_list = glob.glob(os.path.join(tempdir,subdir,'seedlist_pgwave2D*.npz'))

# ==========================================================================================
#                                 MERGING
# ==========================================================================================
# Write boolean flags to determine which files to include in the merging
if_3FHL = True
if_4FGL = False
if_HDBSCAN = False
if_pgwave = True

# Define a value associated to each method
origin_3FHL = 0
origin_4FGL = 1
origin_HDBSCAN = 2
origin_pgwave = 3
origin_dict = {"3FHL": origin_3FHL, 
			   "4FGL": origin_4FGL, 
 			   "HDBSCAN": origin_HDBSCAN, 
 			   "pgwave2D": origin_pgwave}

# Allocate arrays for the seed coordinates in RA/DEC and the seed origin traceback.
seeds_blat = np.zeros((0,))
seeds_blong = np.zeros((0,))
seeds_origin = np.zeros((0,))

# ==========================================================================================
# Fill the seed lists with the catalogs values, if we want it
if (if_3FHL):
	temp_blat = np.load(seedfile_3FHL)['source_blat']
	temp_blong = np.load(seedfile_3FHL)['source_blong']
	temp_numSeeds = temp_blong.shape[0]

	logging.info(f"Adding {temp_numSeeds} seeds from 3FHL...")
	seeds_blat = np.hstack( (seeds_blat, temp_blat) )
	seeds_blong = np.hstack( (seeds_blong, temp_blong) )
	seeds_origin = np.hstack( (seeds_origin, temp_blong*0+origin_3FHL))
	logging.info("")

if (if_4FGL):
	temp_blat = np.load(seedfile_4FGL)['source_blat']
	temp_blong = np.load(seedfile_4FGL)['source_blong']
	temp_numSeeds = temp_blong.shape[0]

	logging.info(f"Adding {temp_numSeeds} seeds from 4FGL-DR4...")
	seeds_blat = np.hstack( (seeds_blat, temp_blat) )
	seeds_blong = np.hstack( (seeds_blong, temp_blong) )
	seeds_origin = np.hstack( (seeds_origin, temp_blong*0+origin_4FGL))
	logging.info("")

# ==========================================================================================
# Fill with the seeds from the clustering of photon data with HDBSCAN, if we want it
if (if_HDBSCAN):
	# Iterate on all the available files
	numSeedsHDBSCAN = 0
	for i,el in enumerate(seedfile_HDBSCAN_list):
		temp_blat = np.load(el)['clusters_lat']
		temp_blong = np.load(el)['clusters_long']
		numSeedsThisStep = temp_blong.shape[0]

		logging.info(f"Adding {numSeedsThisStep:04d} seeds from HDBSCAN clustering on quadrant {i}...")
		seeds_blat = np.hstack( (seeds_blat, temp_blat) )
		seeds_blong = np.hstack( (seeds_blong, temp_blong) )
		seeds_origin = np.hstack( (seeds_origin, temp_blong*0+origin_HDBSCAN))
		numSeedsHDBSCAN += numSeedsThisStep	

	logging.info(f"Total number of seeds from HDBSCAN clustering: {numSeedsHDBSCAN:04d}")
	logging.info("")

# ==========================================================================================
# Fill with the seeds from pgwave2D, if we want it
if (if_pgwave):
	# Iterate on all the available files
	numSeedspgwave = 0
	for i,el in enumerate(seedfile_pgwave_list):
		temp_blat = np.load(el)['sources_b']
		temp_blong = np.load(el)['sources_l']
		numSeedsThisStep = temp_blong.shape[0]

		logging.info(f"Adding {numSeedsThisStep:04d} seeds from pgwave2D used on quadrant {i}...")
		seeds_blat = np.hstack( (seeds_blat, temp_blat) )
		seeds_blong = np.hstack( (seeds_blong, temp_blong) )
		seeds_origin = np.hstack( (seeds_origin, temp_blong*0+origin_pgwave))
		numSeedspgwave += numSeedsThisStep	

	logging.info(f"Total number of seeds from pgwave2D analysis: {numSeedspgwave:04d}")
	logging.info("")

numSeeds = np.shape(seeds_blong)[0]
logging.info(f"Final number of seeds (after merging): {numSeeds}")

# ==========================================================================================
#                          EXTENDED SOURCE LOADING
# ==========================================================================================
# Load the file with the 3FHL extended sources
catalog_ind = 1
with fits.open(catalog_templates_3FHL_extended_file) as f:
	extended_3FHL_blat = f[catalog_ind].data.field('GLAT')
	extended_3FHL_blong = f[catalog_ind].data.field('GLON')
	extended_3FHL_SemiMajor = f[catalog_ind].data.field('Model_SemiMajor')
	extended_3FHL_SemiMinor = f[catalog_ind].data.field('Model_SemiMinor')

# Load the file with the additional 2FGES extended sources
catalog_ind = 1
with fits.open(catalog_file_2FGES_fits) as f:
	extended_2FGES_blat = f[catalog_ind].data.field('GLAT')
	extended_2FGES_blong = f[catalog_ind].data.field('GLON')
	extended_2FGES_r68 = f[catalog_ind].data.field('r_68')
 
# We must allow sources to be inside the largest extended sources, so purge from the lists
# the largest ones
minAngExtension = 0.4
extended_cut_3FHL = (extended_3FHL_SemiMajor <= minAngExtension) | (extended_3FHL_SemiMinor <= minAngExtension)
extended_cut_2FGES = (extended_2FGES_r68 <= minAngExtension)

extended_3FHL_blat = extended_3FHL_blat[extended_cut_3FHL]
extended_3FHL_blong = extended_3FHL_blong[extended_cut_3FHL]

extended_2FGES_blat = extended_2FGES_blat[extended_cut_2FGES]
extended_2FGES_blong = extended_2FGES_blong[extended_cut_2FGES]

# Merge the two lists
extended_blat = np.hstack((extended_3FHL_blat,extended_2FGES_blat))
extended_blong = np.hstack((extended_3FHL_blong,extended_2FGES_blong))

# ==========================================================================================
#                          DUPLICATE REMOVAL
# ==========================================================================================
# Check on every source. Remove the duplicates AND the ones too close to the extended ones
minAngDist = 0.2 # deg
ifBeDistantFromExtended = True

final_seeds_blat = []
final_seeds_blong = []

printCheck = 500
numSeedsCloseToExtended = 0
logging.info(f"")
logging.info("Removing duplicates and sources too close to the extended ones...")

for i,(ith_lat,ith_long) in enumerate(zip(seeds_blat,seeds_blong)):
	if ((i % printCheck) == 0):
		logging.info(f"Step {i} / {numSeeds}")

	# Compute the distance from the i-th source to the extended sources. If the minimum
	# distance is under the given threshold, skip this source
	extended_distances = angdist(ith_lat,ith_long,extended_blat,extended_blong,ifdeg=True)
	if ((np.nanmin(extended_distances) < minAngDist) & (ifBeDistantFromExtended)):
		numSeedsCloseToExtended += 1
		continue

	# Compute the distance from the i-th sources to the remaining ones. Note: the i-th
	# seed must only be compared with the next seeds in the list, since the previous have 
	# already been checked. We want also to avoid computing the distance between a seed and itself.
	# Note that this means that the last element in the row actually must always be added, since
	# there's nothing to compare it against	
	if (i == numSeeds-1):
		final_seeds_blat.append(ith_lat)
		final_seeds_blong.append(ith_long)
	else:
		distances = angdist(ith_lat,ith_long,seeds_blat[i+1:],seeds_blong[i+1:],ifdeg=True)
		if (np.nanmin(distances) >= minAngDist):
			final_seeds_blat.append(ith_lat)
			final_seeds_blong.append(ith_long)

final_seeds_blat = np.array(final_seeds_blat)
final_seeds_blong = np.array(final_seeds_blong)

# Compute the final number of seeds
numSeedsClean = np.shape(final_seeds_blat)[0]
logging.info(f"")
logging.info(f"Final number of seeds (after merging + duplicate removal): {numSeedsClean}")
logging.info(f"Sources removed due to proxmity to extended sources: {numSeedsCloseToExtended}")
logging.info(f"Sources removed due to duplication: {numSeeds - numSeedsClean - numSeedsCloseToExtended}")

# ==========================================================================================
#                                 OUTPUT STAGE
# ==========================================================================================
# Prepare the output file
outfilename = os.path.join(tempdir,f'seedlist_merged.npz')

logging.info(f"")
logging.info(f"Saving the final list of seeds in file {outfilename}")
np.savez_compressed(outfilename, all_seeds_blat = seeds_blat,
								 all_seeds_blong = seeds_blong,
								 all_seeds_origin = seeds_origin,
								 final_seeds_blat = final_seeds_blat,
								 final_seeds_blong = final_seeds_blong,
								 if_3FHL = np.array(if_3FHL),
								 if_4FGL = np.array(if_4FGL),
								 if_HDBSCAN = np.array(if_HDBSCAN),
								 if_pgwave = np.array(if_pgwave),
								 origin_dict = np.array(origin_dict))
logging.info("Saving completed!")

# ==========================================================================================
# Final printouts
logging.info(f"")
logging.info(f"Completed the execution of {__file__}")
logging.info("*"*75)