'''
# ==========================================================================================
#                                       HEADER
# ==========================================================================================
The aim of this script is to create a pgwave-only catalog

-------------------------------------------------------------------------------------------
Call syntax:
  	python3 06a_pgonly_catalog.py

'''
# ==========================================================================================
#                                 PREAMBLE (module load)
# ==========================================================================================
# Import of the Python Standard Library modules required for the analysis
import glob, os, subprocess, sys			# To act on the operative system
import json									# To read and write JSON files
import matplotlib as mpl					# For plotting
import matplotlib.pyplot as plt				# For plotting
import numpy as np							# For numerical analysis
import pandas as pd							# For reading and writing csv files
import warnings								# To deactivate the warnings
import time									# For script timing
import logging								# For logging purposes
import datetime								# For timestamp printing
import copy									# To copy objects

# Import of the astropy/gammapy/fermipy (& fermitools) modules necessary for astrophysics
from astropy.io import fits                 # To open fits files
from gammapy.maps import Map, WcsNDMap      # To work with sky maps

# Import of personal functions from custom modules
sys.path.append("../../Functions")
from MyFunctions import truehisto1D, histoplotter1D, truehisto2D, histoplotter2D
from AstroFunctions import gal2radec, angdist, duplicateRemoval

# Set the warnings of a few functions
np.seterr(divide='ignore', invalid='ignore')	# To avoid 0/0 errors
mpl.rcParams['backend'] = 'agg'					# Non-interactive backend for plotting
warnings.filterwarnings("ignore", category=DeprecationWarning) # To avoid deprecation warnings

# =========================================================================================
#                                      COMMON DEFINITIONS                                      
# =========================================================================================
# Define the dimension for the fonts in the plots, the figures dimensions and so on
textfont = 20			# Fontsize for labels and legends
dimfig = (12,7)			# Figure dimensions (A4-like)
dimfigbig = (16,12)		# Figure dimensions (a bit larger than A4-like)
dimfiglong = (16,25)	# Figure dimensions (a bit larger and much longer than A4-like)
filetype = '.pdf'		# File format to save the pictures
filedpi = 520			# Resolution used when saving pictures in .png/.jpeg
nptsfit = 10000			# Number of points used in plotting the fits

# ==========================================================================================
#                                  STATUS FILE DATA
# ==========================================================================================
# Open the status file
statusfile = '../statusfile.json'
with open(statusfile) as json_file:
	statusdata = json.load(json_file)

# Extract all the relevant variables from the file
datadir = statusdata['datadir']
imgdir = statusdata['imgdir']
tempdir = statusdata['tempdir']
dataSCdir = statusdata['dataSCdir']
dataPHdir = statusdata['dataPHdir']
catalog_file_4FGL = statusdata['catalog_file_4FGL']
catalog_file_3FHL = statusdata['catalog_file_3FHL']
catalog_file_3FHL_reg = statusdata['catalog_file_3FHL_reg']
catalog_file_3FHL_regAssoc = statusdata['catalog_file_3FHL_regAssoc']
catalog_file_2FGES_fits = statusdata['catalog_file_2FGES_fits']

# Define the name for the subfolders (in imgdir and tempdir) where we will save
# the temporary images and files
pretempdir = '04_4FHL'
imgdir = os.path.join(imgdir,pretempdir)
tempdir = os.path.join(tempdir,pretempdir)

# Setting of the logging module
logfile = "debug.log"
logging.basicConfig(format='[%(asctime)s] %(message)s',level=logging.INFO,
					handlers=[logging.FileHandler(logfile,mode='w'),
					logging.StreamHandler()])
logging.info("*"*75)
logging.info(f"Beginning the execution of {__file__}")
logging.info(f"")

# ==========================================================================================
#                       PGWAVE CALIBRATION COEFFICIENTS LOADING
# ==========================================================================================
# Load the calibration coefficients for pgwave2D, to compute the fluxes starting from the
# counts and to compute the position uncertainities. Remembern: counts = m*flux + q
pgwave_simdir = '05_simPgwaveOut'
outfilename = os.path.join(tempdir, pgwave_simdir, 'pgwave2D_calcoeffs.npz')
with np.load(outfilename) as f:
	m = f['m']
	m_err = f['m_err']
	stdDistance = f['stdDistance']

# ==========================================================================================
#                              PGWAVE FILE LOADING
# ==========================================================================================
# Allocate arrays for the properties of the sources found with pgwave2D
sources_blat     = np.zeros((0,))
sources_blong    = np.zeros((0,))
sources_pos_err  = np.zeros((0,))
sources_counts   = np.zeros((0,))
sources_k        = np.zeros((0,))

# Open the file with the sources and stack them all together
subdir = "04_seedlists"
seedfile_pgwave_list = glob.glob(os.path.join(tempdir,subdir,'seedlist_pgwave2D*.npz'))

for i,el in enumerate(seedfile_pgwave_list):
	logging.info(f"Opening data from Sky Quadrant n. {i}")
	with np.load(el) as f:
		sources_blong = np.hstack((sources_blong,f['sources_l']))
		sources_blat = np.hstack((sources_blat,f['sources_b']))
		sources_pos_err = np.hstack((sources_pos_err,f['sources_pos_err']))
		sources_counts = np.hstack((sources_counts,f['sources_counts']))
		sources_k = np.hstack((sources_k,f['sources_k_signif']))

	logging.info(f"Done!")
	logging.info("")

logging.info("All quadrants opened!")
numSourcesRaw = np.shape(sources_blat)[0]

# ==========================================================================================
#                                     LIST CLEANING
# ==========================================================================================
# Compute which of the sources satisfies these requirements:
# - Sources too close one to each other
# - Sources too close to the extended sources (do we need this?)
# - Possible outliars
minAngDist = 0.25 # deg
logging.info("")
logging.info("Removing duplicate sources...")
indList_duplicate = duplicateRemoval(sources_blat,sources_blong,minAngDist,ifdeg=True)

outliar_thrK = 50
outliar_thrCounts = 20000
indList_outliars = np.where( (sources_k <= outliar_thrK) & (sources_counts >= outliar_thrCounts) )[0]
indList_outliars = list(indList_outliars)

# Merge the conditions 
all_indList = list(set(indList_duplicate + indList_outliars))

# Extract the good sources
sources_blong = sources_blong[all_indList]
sources_blat = sources_blat[all_indList]
sources_pos_err = sources_pos_err[all_indList]
sources_counts = sources_counts[all_indList]
sources_k = sources_k[all_indList]

# Some printouts
numSourcesDuplicate = len(indList_duplicate)
numSourcesOutliars = len(indList_outliars)
numSourcesFinal = np.shape(sources_blat)[0]

logging.info(f"")
logging.info(f"Initial number of sources: {numSourcesRaw}")
logging.info(f"Number of sources not passing duplicate removal cut: {numSourcesDuplicate}")
logging.info(f"Number of sources not passing outliars removal cut: {numSourcesOutliars}")
logging.info(f"Final number of sources: {numSourcesFinal}")
logging.info(f"Removed seeds: {numSourcesRaw - numSourcesFinal}")
logging.info(f"")

# ==========================================================================================
#                              FINAL SCALING
# ==========================================================================================
# Since we have already the final list of "good" sources, we can compute some additional
# data on them: coordinates in RA/DEC, fluxes, position errors.
# Start from a constant position error, computed with the simulation
sources_simul_pos_err = sources_pos_err*0 + stdDistance

# Convert the source coordinates in RA/DEC
sources_ra, sources_dec = gal2radec(sources_blong,sources_blat)

# Compute the counts errors and scale the counts to fluxes
sources_counts_err = np.sqrt(sources_counts)
sources_flux = sources_counts/m
errtype = 'statistical'
if (errtype == 'statistical'):
	sources_flux_err = np.sqrt(sources_counts)/m
elif (errtype == 'complete'):
	sources_flux_err = np.sqrt( ( sources_counts * m_err/m )**2 + sources_counts_err**2 ) / m
else:
	sources_flux_err = 0*flux

# ==========================================================================================
#                                     SORTING
# ==========================================================================================
# Sort the sources by ascending latitude, then ascending longitude
#sortway = "ra_dec"
sortway = "glat_glon"
if (sortway == "ra_dec"):
	inds = np.lexsort((sources_ra,sources_dec))
elif (sortway == "glat_glon"):
	inds = np.lexsort((sources_blong,sources_blat))

sources_ra       = sources_ra[inds]
sources_dec      = sources_dec[inds]
sources_blat     = sources_blat[inds]
sources_blong    = sources_blong[inds]
sources_pos_err  = sources_pos_err[inds]
sources_simul_pos_err = sources_simul_pos_err[inds]
sources_counts   = sources_counts[inds]
sources_counts_err = sources_counts_err[inds]
sources_flux     = sources_flux[inds]
sources_flux_err = sources_flux_err[inds]
sources_k        = sources_k

# Choose what error to save
the_pos_err = sources_simul_pos_err

# ==========================================================================================
#                                 CATALOG CREATION
# ==========================================================================================
# Create an output file with all the sources listed properly
subtempdir = "06_pgonly_catalog"
outfilename = os.path.join(tempdir,subtempdir,f'PG_FHL.txt')

numSources = np.shape(sources_flux)[0]
logging.info(f"Writing catalog file for {numSources} sources in file:")
logging.info(outfilename)
logging.info(f"")

printoutCheck = 250
#thisSep = "\t"
thisSep = "  "

with open(outfilename,'w') as f:
	txtstr = ""
	txtstr += f"ID{thisSep} "
	txtstr += f"GLAT [deg]{thisSep} GLONG [deg]{thisSep} "
	txtstr += f"RA [deg]{thisSep} DEC [deg]{thisSep} "
	txtstr += f"POS_ERR [deg]{thisSep} "
	txtstr += f"COUNTS{thisSep} COUNTS_ERR{thisSep} "
	txtstr += f"FLUX [ph/cm2/s]{thisSep} FLUX_ERR [ph/cm2/s]\n"
	f.write(txtstr)
	for i in range(numSources):
		if (i % printoutCheck == 0):
			logging.info(f"Writing source {i}...")

		txtstr = ""
		txtstr += f"{i:04d}{thisSep} "
		txtstr += f"{sources_blat[i]:03.3f}{thisSep} {sources_blong[i]:03.3f}{thisSep} "
		txtstr += f"{sources_ra[i]:03.3f}{thisSep} {sources_dec[i]:03.3f}{thisSep} "
		txtstr += f"{the_pos_err[i]:03.3f}{thisSep}"
		txtstr += f"{int(sources_counts[i])}{thisSep} {int(sources_counts_err[i])}{thisSep} "
		txtstr += f"{sources_flux[i]:.3e}{thisSep} {sources_flux_err[i]:.3e}\n"
		
		f.write(txtstr)

logging.info("Done!")
logging.info("")

# ==========================================================================================
#                                 CATALOG PLOTS: HISTO1Ds
# ==========================================================================================
# Compute and show the distribution of fluxes
fluxbins = np.logspace(-11,-8,50)
histoFlux, binsFlux = truehisto1D(sources_flux,bins=fluxbins)

logging.info(f"Plotting the histo1D (flux distibutions)...")
figname = os.path.join(imgdir,'06_pgonly_catalog','PG_FHL_histo1D_flux.pdf')

fig,ax = plt.subplots(figsize = dimfig)
histoplotter1D(ax,binsFlux,histoFlux,'Flux [photons/cm2/s]','Counts','Data','best',textfont)
ax.set_xscale('log')
fig.set_tight_layout('tight')
fig.savefig(figname)
plt.close(fig)
logging.info("Done!")
logging.info("")

# Compute and show the distribution of latitudes
nbins = 100
histoGlat, binsGlat = truehisto1D(np.abs(sources_blat),nbins,ifstep=False)

logging.info(f"Plotting the histo1D (absolute galactic latitude)...")
figname = os.path.join(imgdir,'06_pgonly_catalog','PG_FHL_histo1D_glat.pdf')

fig,ax = plt.subplots(figsize = dimfig)
histoplotter1D(ax,binsGlat,histoGlat,'Galactic latitude [deg]','Counts','Data','best',textfont)
fig.set_tight_layout('tight')
fig.savefig(figname)
plt.close(fig)
logging.info("Done!")
logging.info("")


# ==========================================================================================
#                                 CATALOG PLOTS
# ==========================================================================================
logging.info("Plotting the sky map with all the sources...")

# Show some plots. First of all, the distribution of the sources in the sky
# To do so, first open the full-sky count map to use its geometry in the plot
fullSkyMap = os.path.join(tempdir,f"PH_after_gtbin_AIT_10.fits")
countmap_skyview = WcsNDMap.read(fullSkyMap)

# Choose the settings:
#    - ifremove = True: uniform white background, colored crosses (color = flux)
#    - ifremove = False: counts map background, mono-color crosses
ifremove = True
if ifremove:
    # Use the LAT map as background
    showmap = copy.deepcopy(countmap_skyview)
    
    # Adjust the other parameters
    stretch = 'linear'            # Stretch of bkg
    cmap_formap = 'BuGn'          # Color of bkg
    
    cmap_forscatter = 'gnuplot2'  # Color map of crosses
    c = sources_flux              # Color values of crosses
    norm = 'log'                  # Color scaling of crosses
    
    gridcolor = 'black'           # Grid parameters
    gridalpha = 0.2
    
else:
    # Use the LAT smoothed map as background
    LAT_PSF = 0.5
    showmap = countmap_skyview.smooth(LAT_PSF,kernel='gauss')
    
    # Adjust the other parameters
    stretch = 'log'               # Stretch of bkg
    cmap_formap = 'gnuplot2'      # Color of bkg
    
    cmap_forscatter = None        # Color map of crosses
    c = 'cyan'                    # Color of crosses
    norm = None                   # Color scaling of crosses
    gridcolor = 'white'           # Grid parameters
    gridalpha = 0.3
    
    
# Now plot the map and add the point sources
fig = plt.figure(figsize = dimfigbig)
ax = showmap.plot(fig = fig, stretch=stretch, cmap=cmap_formap, add_cbar = False)
p = ax.scatter(sources_blong,sources_blat,c=c,marker='x',s=20,transform=ax.get_transform("galactic"),
				cmap = cmap_forscatter, norm=norm)

# Refine the graphics
thisImage = ax.get_images()[0]
if not ifremove:
    # Keep the original colorbar of the counts map
	cbar = fig.colorbar(thisImage, ax=ax,shrink=0.6)
	cbar.ax.tick_params(labelsize=textfont*0.75)
	cbar.set_label('Map counts / pixel',fontsize=textfont,rotation=270,labelpad=50)
else:
    # Produce a colorbar associated to the crosses
	cbar = fig.colorbar(p, ax=ax,shrink=0.6)
	cbar.ax.tick_params(labelsize=textfont*0.75)
	cbar.set_label('Point source flux [photons / cm2 / s]',fontsize=textfont,rotation=270,labelpad=50)

lat = ax.coords['glat']
lon = ax.coords['glon']
lon.set_axislabel('Galactic Longitude [deg]',fontsize=textfont,minpad=12)
lat.set_axislabel('Galactic Latitude [deg]',fontsize=textfont)#,minpad=3)
ax.tick_params(axis='both',labelsize = textfont,color=gridcolor,labelcolor=gridcolor)


ax.coords.grid(color=gridcolor, alpha=0.5, linestyle='solid')
ax.patch.set_edgecolor('black')  
ax.patch.set_linewidth(1)  

ax.set_title('PG-FHL catalog',fontsize=textfont*1.5, pad = 50)
ax.texts[0].remove()
fig.set_tight_layout('tight')

# Save and close
subimgdir = '06_pgonly_catalog'
figname = os.path.join(imgdir,subimgdir,'PG_FHL_loc_distribution.pdf')
fig.savefig(figname,dpi=filedpi)
plt.close(fig)

# ==========================================================================================
# Compute and show the distribution of the fluxes of the sources
nbins = 100
histo, bins = truehisto1D(sources_flux,nbins=nbins,ifstep=False)

figname = os.path.join(imgdir,subimgdir,'PG_FHL_flux_distribution.pdf')

fig, ax = plt.subplots(figsize = dimfig)
histoplotter1D(ax,bins,histo,"Flux [photons/cm2/s]","Counts","4FHL pgwave2D-only","best",textfont)
fig.set_tight_layout('tight')
fig.savefig(figname,dpi=filedpi)
plt.close(fig)

logging.info("Plotting done!")
logging.info("")

# ==========================================================================================
# Final printouts
logging.info(f"")
logging.info(f"Completed the execution of {__file__}")
logging.info("*"*75)