#!/bin/bash
#SBATCH --account=fermi:users
##SBATCH --partition=milano
#SBATCH --job-name=HE_photon_clustering_hdbscan
#SBATCH --output=output-%j.txt
#SBATCH --error=output-%j.txt
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=2
#SBATCH --mem-per-cpu=50G
#SBATCH --time=0-00:30:00
#SBATCH --chdir=/sdf/home/p/pietromg/pmg-home/FermiLAT/Scripts/04_4FHL/

# Run the quadrant caller bash script
#./04a_quadrant_caller.sh
python3 04a_seeder_clustering.py 0 > sbatchout.txt

