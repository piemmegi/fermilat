'''
# ==========================================================================================
#                                       HEADER
# ==========================================================================================
The aim of this script is to visualize a portion of the full sky map, including the seeds
extracted from various sources in different ways (possibly overlapping), before removal and
merging

-------------------------------------------------------------------------------------------
Call syntax:
  	python3 04v_allseeds_visualizer.py

'''
# ==========================================================================================
#                                 PREAMBLE (module load)
# ==========================================================================================
# Import of the Python Standard Library modules required for the analysis
import glob, os, subprocess, sys			# To act on the operative system
import json									# To read and write JSON files
import matplotlib as mpl					# For plotting
import matplotlib.pyplot as plt				# For plotting
import numpy as np							# For numerical analysis
import pandas as pd							# For reading and writing csv files
import warnings								# To deactivate the warnings
import time									# For script timing
import logging								# For logging purposes
import datetime								# For timestamp printing
import copy									# To copy objects

# Import of the astropy/gammapy/fermipy (& fermitools) modules necessary for astrophysics
import astropy.units as u					# Celestial units
from astropy.io import fits                 # To open fits files
from gammapy.maps import Map, WcsNDMap		# To work with sky maps
from astropy.coordinates import SkyCoord 	# To work with sky frames

# Import of personal functions from custom modules
sys.path.append("../../Functions")
from MyFunctions import truehisto2D, histoplotter2D
from AstroFunctions import gal2radec, radec2gal, angdist, circlecalculator

# Set the warnings of a few functions
np.seterr(divide='ignore', invalid='ignore')	# To avoid 0/0 errors
mpl.rcParams['backend'] = 'agg'					# Non-interactive backend for plotting
warnings.filterwarnings("ignore", category=DeprecationWarning) # To avoid deprecation warnings

# =========================================================================================
#                                      COMMON DEFINITIONS                                      
# =========================================================================================
# Define the dimension for the fonts in the plots, the figures dimensions and so on
textfont = 20			# Fontsize for labels and legends
dimfig = (12,7)			# Figure dimensions (A4-like)
dimfigbig = (16,12)		# Figure dimensions (a bit larger than A4-like)
dimfiglong = (16,25)	# Figure dimensions (a bit larger and much longer than A4-like)
filetype = '.pdf'		# File format to save the pictures
filedpi = 520			# Resolution used when saving pictures in .png/.jpeg
nptsfit = 10000			# Number of points used in plotting the fits

# ==========================================================================================
#                                  STATUS FILE DATA
# ==========================================================================================
# Open the status file
statusfile = '../statusfile.json'
with open(statusfile) as json_file:
	statusdata = json.load(json_file)

# Extract all the relevant variables from the file
datadir = statusdata['datadir']
imgdir = statusdata['imgdir']
tempdir = statusdata['tempdir']
dataSCdir = statusdata['dataSCdir']
dataPHdir = statusdata['dataPHdir']
catalog_file_4FGL = statusdata['catalog_file_4FGL']
catalog_file_3FHL = statusdata['catalog_file_3FHL']
catalog_file_3FHL_reg = statusdata['catalog_file_3FHL_reg']
catalog_file_3FHL_regAssoc = statusdata['catalog_file_3FHL_regAssoc']
catalog_file_2FGES_fits = statusdata['catalog_file_2FGES_fits']

# Define the name for the subfolders (in imgdir and tempdir) where we will save
# the temporary images and files
pretempdir = '04_4FHL'
postdir = '04_seedmaps'
imgdir = os.path.join(imgdir,pretempdir,postdir)
tempdir = os.path.join(tempdir,pretempdir)

# Setting of the logging module
logfile = "debug.log"
logging.basicConfig(format='[%(asctime)s] %(message)s',level=logging.INFO,
					handlers=[logging.FileHandler(logfile,mode='w'),
					logging.StreamHandler()])
logging.info("*"*75)
logging.info(f"Beginning the execution of {__file__}")
logging.info(f"")

# ==========================================================================================
#                                  INPUT FILE DATA
# ==========================================================================================
# Define the name of the file with the photon data and extract them
data_PH_after_gtmktime = os.path.join(tempdir,f"PH_after_gtmktime.fits")
with fits.open(data_PH_after_gtmktime) as datafile:
    ind = 1
    data_blat = np.array(datafile[ind].data.field("B"),dtype=np.float64)
    data_blong = np.array(datafile[ind].data.field("L"),dtype=np.float64)
    data_times = np.array(datafile[ind].data.field("TIME"),dtype=np.float64)
    
logging.info(f"{np.shape(data_blat)[0]} events in the full-sky map")

# ==========================================================================================
#                                  CUTOUT EXTRACTION
# ==========================================================================================
# Define a cutout extraction in a center of given location
cutout_radius = 5          # Radius, deg
cutout_blong = 263   		# Long, deg
cutout_blat = -3     		# Lat, deg
#cutout_ra = 276.07
#cutout_dec = 56.76
#cutout_blong, cutout_blat = radec2gal(cutout_ra,cutout_dec)

# Perform the extraction
theangles = angdist(data_blat,data_blong,cutout_blat,cutout_blong,ifdeg=True)

cond = (theangles <= cutout_radius)
cut_blat = data_blat[cond]
cut_blong = data_blong[cond]
logging.info(f"{np.shape(cut_blat)[0]} events in the cutout region")

# ==========================================================================================
#                             POINT SOURCE SEEDS EXTRACTION
# ==========================================================================================
# Open the file with the final list of seeds, after merging and removing duplicates
outfilename = os.path.join(tempdir,f'seedlist_merged.npz')
with np.load(outfilename,allow_pickle=True) as f:
    seeds_blat = f['all_seeds_blat']
    seeds_blong = f['all_seeds_blong']
    seeds_origin = f['all_seeds_origin']
    origin_dict = f['origin_dict'].tolist()  # Casting back to dict

numSeeds = np.shape(seeds_blat)[0]
logging.info(f"{numSeeds} seed(s) in the full-sky map (all methods included)")

# Perform the cut as above
theangles_seeds = angdist(seeds_blat,seeds_blong,cutout_blat,cutout_blong,ifdeg=True)

cond = (theangles_seeds <= cutout_radius)
seeds_blat = seeds_blat[cond]
seeds_blong = seeds_blong[cond]
seeds_origin = seeds_origin[cond]
numSeedsHere = np.shape(seeds_blat)[0]
logging.info(f"{numSeedsHere} seed(s) in the region")

# ==========================================================================================
# Open also the extended source list
catalog_ind = 1
with fits.open(catalog_file_2FGES_fits) as f:
    extended_blat = f[catalog_ind].data.field('GLAT')
    extended_blong = f[catalog_ind].data.field('GLON')
    extended_r68 = f[catalog_ind].data.field("r_68")

numExtended = np.shape(extended_blat)[0]
logging.info(f"{numExtended} extended source(s) in the full-sky map")

# Perform the cut as above
theangles_extended = angdist(extended_blat,extended_blong,cutout_blat,cutout_blong,ifdeg=True)

cond = (theangles_extended <= cutout_radius)
extended_blat = extended_blat[cond]
extended_blong = extended_blong[cond]
extended_r68 = extended_r68[cond]
numExtendedHere = np.shape(extended_blat)[0]
logging.info(f"{numExtendedHere} extended source(s) in the region")
logging.info("")

# ==========================================================================================
#                                  SELECTION OF THE METHODS
# ==========================================================================================
# Define which seeding methods to visualize. Note that the keys of this dictionary should be
# identical to the keys of "origin_dict" (cfr. 04z_merger script).
if_visualize = {"3FHL": True, 
                "4FGL": False, 
                "HDBSCAN": False, 
                "pgwave2D": True}

# Define the characterizing color, marker and marker size of each method
key_colors = {"3FHL": 'red',
              "4FGL": 'magenta',
              "HDBSCAN": 'cyan',
              "pgwave2D": 'lime'}

key_markers = {"3FHL": 'x',
               "4FGL": '+',
               "HDBSCAN": '^',
               "pgwave2D": '*'}

key_markersizes = {"3FHL": 300,
                   "4FGL": 300,
                   "HDBSCAN": 300,
                   "pgwave2D": 300}

# ==========================================================================================
#                             BACKGROUND PLOTTING
# ==========================================================================================
# Create two basic plots: the figure with the 2D count map and the scatterplot
nbins = 200
histo2D, binsX, binsY = truehisto2D(cut_blong,cut_blat,nbinsX=nbins,nbinsY=nbins)

fig1, ax1 = plt.subplots(figsize = dimfig)
histoplotter2D(ax1,binsX,binsY,histo2D,'gnuplot2',None,'Galactic longitude [°]','Galactic latitude [°]',
               textfont,iflog=True,vmax=25,ifcbarfont=False,cbarlabel="Counts/bin")

fig2, ax2 = plt.subplots(figsize = dimfig)
ax2.plot(cut_blong,cut_blat,color='mediumblue',ls='',marker='o',markersize=0.2)

# ==========================================================================================
#                             SEEDS PLOTTING
# ==========================================================================================
# Now add to both plots all the crosses of the specified class
figname_string = ''
for i,key in enumerate(origin_dict.keys()):
    # Check if this method must be shown. If not, continue
    if not if_visualize[key]:
        continue

    logging.info(f"Processing key {key}...")
    figname_string += f"_{key}"

    # Select only the seeds relevant for this method. Note that we want to match the signature
    # of the method (contained in the values of origin_dict) with the values stored in the 
    # seeds_origin array.
    cond = (seeds_origin == origin_dict[key])
    this_seeds_blat = seeds_blat[cond]
    this_seeds_blong = seeds_blong[cond]
    this_numSeeds = np.shape(this_seeds_blong)[0]
    logging.info(f"{this_numSeeds} seed(s) for this key were found")

    # Iterate on both figures and make the plot
    for j,ax in enumerate([ax1,ax2]):
        for k in range(this_numSeeds):
            # Add the label only the first time
            if k == 0:
                ax.scatter(this_seeds_blong[k], this_seeds_blat[k], c=key_colors[key],marker=key_markers[key],
                        s=key_markersizes[key], edgecolors = 'black', alpha = 1, label = f"{key}")
            else:
                ax.scatter(this_seeds_blong[k], this_seeds_blat[k], c=key_colors[key],marker=key_markers[key],
                        s=key_markersizes[key], edgecolors = 'black', alpha = 1)

    logging.info(f"Done!")
    logging.info("")

# ==========================================================================================
#                            EXTENDED SOURCES
# ==========================================================================================
# Add to both plots the extended sources
extended_color = 'cyan'
extended_fill_color = 'cyan'
extended_marker = 'X'
for j,ax in enumerate([ax1,ax2]):
    for k in range(numExtendedHere):
        # Plot the center of the extended source
        if (k == 0):
            ax.scatter(extended_blong[k], extended_blat[k], c=extended_color,marker=extended_marker,s=200,edgecolors='black',
                        alpha = 1, label = "Extended sources (2FGES)")
        else:
            ax.scatter(extended_blong[k], extended_blat[k], c=extended_color,marker=extended_marker,s=200,edgecolors='black',
                        alpha = 1)

        # Add the extension
        lat_vec, long_vec_plus, long_vec_minus = circlecalculator(extended_blat[k],extended_blong[k],extended_r68[k],ifdeg=True)
        if (k == 0):
            ax.fill_betweenx(lat_vec,long_vec_plus,long_vec_minus,color=extended_fill_color,alpha=0.2,label="r68")
        else:
            ax.fill_betweenx(lat_vec,long_vec_plus,long_vec_minus,color=extended_fill_color,alpha=0.2)

# ==========================================================================================
#                             FINISHING TOUCHES
# ==========================================================================================
# Refine the graphics
logging.info(f"Finishing touches...")
for ax in [ax1,ax2]:
    ax.invert_xaxis()
    ax.legend(loc='lower left',framealpha=1,fontsize=0.5*textfont)

ax2.grid(linewidth=0.5)
ax2.set_xlabel('Galactic longitude [°]',fontsize=textfont)
ax2.set_ylabel('Galactic latitude [°]',fontsize=textfont)
ax2.tick_params(labelsize = textfont)

# Save the figures
t0 = time.perf_counter()
logging.info("Saving...")
fignames = [os.path.join(imgdir,f"seedmaps_originals_histo2D{figname_string}.png"),
            os.path.join(imgdir,f"seedmaps_originals_scatter{figname_string}.png"),]
for i,fig in enumerate([fig1,fig2]):
    fig.set_tight_layout('tight')
    fig.savefig(fignames[i],dpi=filedpi)
    plt.close(fig)

t1 = time.perf_counter()
dt = t1-t0
logging.info(f"Time elapsed in the saving of the image: {dt:.2f} s")

# ==========================================================================================
# Final printouts
logging.info(f"")
logging.info(f"Completed the execution of {__file__}")
logging.info("*"*75)