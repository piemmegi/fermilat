'''
# ==========================================================================================
#                                       HEADER
# ==========================================================================================
The aim of this script is to create a json file which will be used in the analysis of OP 313

-------------------------------------------------------------------------------------------
Call syntax:
  	python3 utils_configcreator.py
'''
# ==========================================================================================
#                                 PREAMBLE (module load)
# ==========================================================================================
# Import of the Python Standard Library modules required for the analysis
import glob, os, subprocess, sys       # To act on the operative system
import json                            # To read and write JSON files
import matplotlib as mpl               # For plotting
import matplotlib.pyplot as plt        # For plotting
import numpy as np                     # For numerical analysis
import pandas as pd 				   # For reading and writing csv files
import warnings                        # To deactivate the warnings
import time                            # For script timing
import logging						   # For logging purposes

# Import of personal functions from custom modules
sys.path.append("../../Functions")
from AstroFunctions import J2000formatter

# ==========================================================================================
#                                  STATUS FILE DATA
# ==========================================================================================
# Open the status file
statusfile = '../statusfile.json'
with open(statusfile) as json_file:
	statusdata = json.load(json_file)

# Extract all the relevant variables from the file
datadir = statusdata['datadir']
imgdir = statusdata['imgdir']
tempdir = statusdata['tempdir']
dataSCdir = statusdata['dataSCdir']
dataPHdir = statusdata['dataPHdir']
catalog_file_4FGL = statusdata['catalog_file_4FGL']
catalog_file_3FHL = statusdata['catalog_file_3FHL']

# Define the name for the subfolders (in imgdir and tempdir) where we will save
# the temporary images and files
pretempdir = '05_OP313'
imgdir = os.path.join(imgdir,pretempdir)
tempdir = os.path.join(tempdir,pretempdir)

# Setting of the logging module
logfile = "debug.log"
logging.basicConfig(format='[%(asctime)s] %(message)s',level=logging.INFO,
                    handlers=[logging.FileHandler(logfile,mode='w'),
                              logging.StreamHandler()])

logging.info("*"*75)
logging.info(f"Beginning execution of {__file__}")
logging.info(f"")

# ==========================================================================================
#                                 HARD-CODED PARAMETERS
# ==========================================================================================
# In this section we define all the parameters needed to work with the fermitools.
# Specifically, here we define the parameters used in the data selection with gtselect,
# gtmktime and other such functions. Note that the center of the ROI in RA/DEC coordinates
# is here left undefined but saved, since other scripts will modify it later.
trignum = 0
wstart = 9                  # Time period to be analyzed: first week
wstop = 864                 # Time period to be analyzed: last week
ROI_ra = 197.619            # [°] Center of the ROI (RA), OP 313
ROI_dec = 32.345            # [°] Center of the ROI (DEC)
ROI_rad = 2.5               # [°] Radius of the ROI
Emin = 10*1e3               # [MeV] Minimum photon energy
Emax = 2000*1e3             # [MeV] Maximum photon energy
zmax = 105                  # [deg] Maximum apparent zenith angle
evclass = 128               # [int] Event class: P8R3_SOURCE class (128) or anything else
evtype = 3                  # [int] Event conversion type: Front + Back class (3) or anything else
roicut = 'no'               # [str] Whether to apply a ROI cut in gtmktime
filtercond = '(IN_SAA!=T)&&(DATA_QUAL>0)&&(LAT_CONFIG==1)' # gtmktime selection cut
algorithm="LC"              # [str] Argument to pass to gtbin
tbinalg = "LIN"             # [str] Argument to pass to gtbin
tbinfile = None             # [str or None] Argument to pass to gtbin
dtime = 30*24*3600          # [str] Binning step for gtbin in LC mode
irfs = 'P8R3_SOURCE_V3'     # [str] Event class: P8R3_SOURCE class or anything else
srcmdl = "none"             # [str] Argument to pass to gtexposure
specin = -2.1               # [float] Argument to pass to gtexposure
chatter = 1                 # Verbosity for the fermitools. 1: minimal. 2: medium. 4: extensive.
gtmode = "h"                # Running mode of all fermitools: batch mode ("h") or interactive mode ("ql")

# Arguments specific for the scripts used in 03_SED
dcostheta = 0.025               # [deg] Angular step in the calculation of the LAT livetime
binsz = 0.25                    # [deg] Angular step in the calculation of the exposure map and gttsmap
extROI_rad = 10                 # [deg] Extension of the square region centered in the ROI center which will
                                #   be investigated using gtexpmap
expmap_evtype = "INDEF"         # Event type for gtexpmap
expmap_irfs = "CALDB"           # IRFs for expmap
nbands_energy = 20              # Number of energy bands used in computing the exposure through gtexpmap
DRnum = 4                       # Release number of the 4FGL catalog to use with LATSourceModel
free_radius = 1e-9              # Service parameter for the LATSourceModel "SourceList" function
xmlclobber = 'yes'              # Whether to overwrite or not the existing XML files
gtlike_optimizer = 'NewMinuit'  # Type of gtlike optimizer
gtlike_verbosity = 0            # Verbosity of the call to gtlike (which we call using fermipy)

# ==========================================================================================
#                                 MET TO WEEK CONVERSION
# ==========================================================================================
# Now we convert the weeks in METs
tablefile = os.path.join(tempdir,'GTIdict.npz')

if (not os.path.exists(tablefile)):
	logging.info(f"Missing the GTIdict.npz file. Let's fix this ...")
	subprocess.run(['python3','utils_GTIanalyzer.py'])

# Open the file and extract the required vectors
with np.load(tablefile) as file:
	weeknumbers = file['weeknumbers'] # These are all floating point np.arrays
	firstGTIs = file['firstGTIs']
	lastGTIs = file['lastGTIs']

# ----------------------------------------------------------------------------
# Compute tstart
if (wstart <= 0):
	# Note that due to how this is written, we fall in this condition if
	# wstart is -1 but also -2, -3 and so on.
	tstart = int(firstGTIs[wstart])
	wstart = int(weeknumbers[wstart])
else:
	try:
		goodind = np.where(weeknumbers >= wstart)[0][0]
		tstart = int(firstGTIs[goodind])
	except IndexError:
		# This may happen if wstart is before the first week available
		tstart = int(firstGTIs[0])
		logging.info(f"IndexError thrown in the calculation of tstart - defaulting to {tstart}\n")

# Compute tstop, if wstop is known. Note that here we must account for the fact 
# that wstop may be either a week number (directly corresponding to a MET) or "-1" 
# (meaning that the last available week should be used). In the second case, we
# must re-compute the week number.
if (wstop <= 0):
	# Note that due to how this is written, we fall in this condition if
	# wstop is -1 but also -2, -3 and so on.
	tstop = int(lastGTIs[wstop])
	wstop = int(weeknumbers[wstop])
else:
	try:
		goodind = np.where(weeknumbers >= wstop)[0][0]
		tstop = int(lastGTIs[goodind])
	except IndexError:
		# This may happen if wstop is after the last week available
		tstop = int(lastGTIs[-1])

# ==========================================================================================
#                                 LIST FILE CREATION
# ==========================================================================================
# Here we define the names of the Spacecraft and Photon file lists (or files)
# necessary for the 1st level trigger analysis
listdir = os.path.join(tempdir,'Lists')
if (not os.path.exists(listdir)):
	os.mkdir(listdir)

PHlistname = os.path.join(listdir,f'PH_events_trig_{trignum}_w{wstart:03d}_w{wstop:03d}.list')
SClistname = os.path.join(listdir,f'SC_events_trig_{trignum}_w{wstart:03d}_w{wstop:03d}.list')

# Now that we know the weeks to be analyzed, we can produce the .list file containing the
# names of the photon and spacecraft files to be analyzed.
if (not os.path.exists(PHlistname)) | (not os.path.exists(SClistname)):
	logging.info(f"Missing the .list files. Let's fix this...")
	subprocess.run(['python3','utils_ListPreparator.py',f'{wstart}',f'{wstop}',
				PHlistname,SClistname])
else:
	logging.info(f"The .list files founds were correctly located!")

# Now we can call the HEASOFT fmerge function (non-python, but should be installed)
# to merge the spacecraft files listed in the .list file.
SCdir = os.path.join(tempdir,'SCfiles')
SCfilename = os.path.join(SCdir,f"SC_events_w{wstart:03d}_w{wstop:03d}.fits")

if (not os.path.exists(SCdir)):
	os.mkdir(SCdir)

if os.path.exists(SCfilename):
	logging.info(f"The spacecraft merged file already exists: we are not recreating it!")
else:
	logging.info(f"The spacecraft merged file does not exist: calling fmerge to create it...")
	subprocess.run(['punlearn','fmerge'])
	subprocess.run(['fmerge',f'@{SClistname}',SCfilename,'-','clobber=yes']) # clobber == enable overwriting

logging.info(f"Done!")
logging.info(f"")

# ==========================================================================================
#                                  JSON FILE PRODUCTION
# ==========================================================================================
# Here we create the json string to be saved
jsonstring = {"wstart":                     wstart,                 # Coupled to tstart/tstop
              "wstop":                      wstop,                  # Coupled to tstart/tstop
              "tstart":                     tstart,                 # gtselect
              "tstop":                      tstop,                  # gtselect 
              "ROI_ra":                     ROI_ra,                 # gtselect
              "ROI_dec":                    ROI_dec,                # gtselect
              "ROI_rad":                    ROI_rad,                # gtselect
              "Emin":                       Emin,                   # gtselect
              "Emax":                       Emax,                   # gtselect
              "zmax":                       zmax,                   # gtselect
              "evclass":                    evclass,                # gtselect
              "evtype":                     evtype,                 # gtselect
              "roicut":                     roicut,                 # gtmktime
              "filtercond":                 filtercond,             # gtmktime
              "algorithm":                  algorithm,              # gtbin
              "tbinalg":                    tbinalg,                # gtbin
              "tbinfile":                   tbinfile,               # gtbin
              "dtime":                      dtime,                  # gtbin
              "irfs":                       irfs,                   # gtexposure
              "srcmdl":                     srcmdl,                 # gtexposure
              "specin":                     specin,                 # gtexposure
              "dcostheta":                  dcostheta,              # gtltcube
              "binsz":                      binsz,                  # gtlike
              "extROI_rad":                 extROI_rad,             # gtexpmap
              "expmap_evtype":              expmap_evtype,          # gtexpmap
              "expmap_irfs":                expmap_irfs,            # gtexpmap
              "nbands_energy":              nbands_energy,          # gtexpmap
              "DRnum":                      DRnum,                  # gtdiffrsp
              "free_radius":                free_radius,            # gtdiffrsp
              "xmlclobber":                 xmlclobber,             # gtdiffrsp
              "gtlike_optimizer":           gtlike_optimizer,       # gtlike
              "gtlike_verbosity":           gtlike_verbosity,       # gtlike
              "chatter":                    chatter,                # All fermitools
              "gtmode":                     gtmode,                 # All fermitools
              "PHlistname":                 PHlistname,             # File management
              "SClistname":                 SClistname,             # File management
              "SCfilename":                 SCfilename}             # File management

# Now converting the string in actual json, and then saving
json_string = json.dumps(jsonstring,indent=4)
filename = os.path.join(tempdir,f'configfile_trignum_{trignum}.json')
with open(filename, 'w') as outfile:
    outfile.write(json_string)

logging.info(f"The configuration file was correctly written at the following path:")
logging.info(filename)
logging.info(f"")
logging.info(f"Completed the execution of {__file__}")
logging.info("*"*75)