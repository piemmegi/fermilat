'''
# ==========================================================================================
#                                       HEADER
# ==========================================================================================
The aim of this script is to fit the energy spectrum of OP 313 in multiple time windows,
to extract the time-dependence of its flux and fits parameters.

-------------------------------------------------------------------------------------------
Call syntax:
  	python3 03_MLgtlike.py [ifOffset]

Call arguments:
	ifOffset		[bool] shift all the bins by a given offset? (Default: False)

'''
# ==========================================================================================
#                                 PREAMBLE (module load)
# ==========================================================================================
# Import of the Python Standard Library modules required for the analysis
import glob, os, subprocess, sys       # To act on the operative system
import json                            # To read and write JSON files
import matplotlib as mpl               # For plotting
import matplotlib.pyplot as plt        # For plotting
import numpy as np                     # For numerical analysis
import pandas as pd 				   # For reading and writing csv files
import warnings                        # To deactivate the warnings
import time                            # For script timing
import logging						   # For logging purposes
import scipy.stats as sp  			   # For statistical analyses

# Import of the astropy/gammapy/fermipy (& fermitools) modules necessary for astrophysics
from LATSourceModel import SourceList      # To create the XML files required by the Fermitools

# Import of personal functions from custom modules
sys.path.append("../../Functions")
from MyFunctions import truebins, histoplotter2D, gc_cleaner
from AstroFunctions import TS2sigma, gtselect, gtmktime, gtdiffrsp, gtlike, gtexpmap, gtltcube

# Set the warnings of a few functions
np.seterr(divide='ignore', invalid='ignore') 					# To avoid 0/0 errors
mpl.rcParams['backend'] = 'agg'                                 # Non-interactive backend for plotting
warnings.filterwarnings("ignore", category=DeprecationWarning)  # To avoid deprecation warnings

# =========================================================================================
#                                      COMMON DEFINITIONS                                      
# =========================================================================================
# Define the dimension for the fonts in the plots, the figures dimensions and so on
textfont = 20         # Fontsize for labels and legends
dimfig = (12,7)       # Figure dimensions (A4-like)
dimfigbig = (16,12)   # Figure dimensions (a bit larger than A4-like)
dimfiglong = (16,25)  # Figure dimensions (a bit larger and much longer than A4-like)
filetype = '.pdf'     # File format to save the pictures
filedpi = 520         # Resolution used when saving pictures in .png/.jpeg
nptsfit = 10000       # Number of points used in plotting the fits

# ==========================================================================================
#                                  STATUS FILE DATA
# ==========================================================================================
# Open the status file
statusfile = '../statusfile.json'
with open(statusfile) as json_file:
	statusdata = json.load(json_file)

# Extract all the relevant variables from the file
datadir = statusdata['datadir']
imgdir = statusdata['imgdir']
tempdir = statusdata['tempdir']
dataSCdir = statusdata['dataSCdir']
dataPHdir = statusdata['dataPHdir']
catalog_file_4FGL = statusdata['catalog_file_4FGL']
catalog_file_3FHL = statusdata['catalog_file_3FHL']

# Define the name for the subfolders (in imgdir and tempdir) where we will save
# the temporary images and files
pretempdir = '05_OP313'
imgdir = os.path.join(imgdir,pretempdir)
tempdir = os.path.join(tempdir,pretempdir)

# Setting of the logging module
logfile = "debug.log"
logging.basicConfig(format='[%(asctime)s] %(message)s',level=logging.INFO,
					handlers=[logging.FileHandler(logfile,mode='w'),
				        	  logging.StreamHandler()])
logging.info("*"*75)
logging.info(f"Beginning execution of {__file__}")
logging.info(f"")

# ==========================================================================================
#                                 CONFIG FILE DATA
# ==========================================================================================
# Open the configuration file made for the 1st level trigger and extract the variables
# necessary for the analysis
configfile = os.path.join(tempdir,f'configfile_trignum_0.json')
with open(configfile) as json_file:
	configdata = json.load(json_file)

# Extract all the relevant variables from the file
tstart            = configdata['tstart']             # gtselect
tstop             = configdata['tstop']              # gtselect 
ROI_ra            = configdata['ROI_ra']             # gtselect
ROI_dec           = configdata['ROI_dec']            # gtselect
ROI_rad           = configdata['ROI_rad']            # gtselect
Emin              = configdata['Emin']               # gtselect
Emax              = configdata['Emax']               # gtselect
zmax              = configdata['zmax']               # gtselect
evclass           = configdata['evclass']            # gtselect
evtype            = configdata['evtype']             # gtselect
roicut            = configdata['roicut']             # gtmktime
filtercond        = configdata['filtercond']         # gtmktime
dcostheta         = configdata['dcostheta']          # gtltcube
binsz             = configdata['binsz']              # gtltcube
extROI_rad        = configdata['extROI_rad']         # gtexpmap
expmap_evtype     = configdata['expmap_evtype']      # gtexpmap
expmap_irfs       = configdata['expmap_irfs']        # gtexpmap
nbands_energy     = configdata['nbands_energy']      # gtexpmap
DRnum             = configdata['DRnum']              # gtdiffrsp
free_radius       = configdata['free_radius']        # gtdiffrsp
xmlclobber        = configdata['xmlclobber']         # gtlike
gtlike_optimizer  = configdata['gtlike_optimizer']   # gtlike
gtlike_verbosity  = configdata['gtlike_verbosity']   # gtlike
chatter           = configdata['chatter']            # All fermitools
gtmode            = configdata['gtmode']             # All fermitools
SCfilename        = configdata['SCfilename']         # File management

# Define the name of the file produced in output of 01_DataExtractor at the gtmktime level,
# containing all the photons emitted from this source mission-long. This will avoid us
# the necessity to call again gtselect on all the data from all the mission.
PHfilename = os.path.join(tempdir,f"PH_after_gtmktime.fits")

# =========================================================================================
#                                      INPUT PARSING
# =========================================================================================
# Determine from command line if we want to apply a shift to the bins
try:
	if (sys.argv[1] == 'True'):
		ifOffset = True
	else:
		ifOffset = False
except IndexError:
	ifOffset = False

if ifOffset:
	offsetStr = "_offset_"
else:
	offsetStr = ""

logging.info(f"Offset string: {offsetStr}")

# The likelihood analysis should be repeated in multiple time bins, here defined
# First, define the binning step
y2d = 365       # Days in 1 year
m2d = 30        # Days in 1 month
d2h = 24        # Hours in 1 day
h2s = 3600      # Seconds in 1 hour
tstep = 3*m2d*d2h*h2s

# Now define a binning vector ranging from the start to the finish at a certain step (1 week)
offset = 2*tstep
bins = np.arange(tstart,tstop,tstep) + ifOffset * offset
bins_low = bins[:-1]-0.5
bins_up = bins[1:]+0.5
logging.info(f"Using a pre-defined binning (step {tstep} s from MET = {tstart} s to MET = {tstop} s)")

bincenters = (bins_low + bins_up)/2

# ==========================================================================================
#                                 BINNING ANALYSIS
# ==========================================================================================
# Allocate the variables containing the outputs of the bin analysis
goodSourceName = '4FGL J1310.5+3221'
nbins = np.shape(bins_low)[0]

fitLikelihood = np.zeros((nbins,),dtype=np.float64)
NumFreePars = np.zeros_like(fitLikelihood)
TS = np.zeros_like(fitLikelihood)
sigma = np.zeros_like(fitLikelihood)
flux = np.zeros_like(fitLikelihood)
flux_err = np.zeros_like(fitLikelihood)

allnorm = np.zeros_like(fitLikelihood)      # gtlike fits with logparabola: free pars are norm, alpha, beta, Eb
allnorm_err = np.zeros_like(fitLikelihood)

allalpha = np.zeros_like(fitLikelihood)
allalpha_err = np.zeros_like(fitLikelihood)

allbeta = np.zeros_like(fitLikelihood)
allbeta_err = np.zeros_like(fitLikelihood)

allEb = np.zeros_like(fitLikelihood)
allEb_err = np.zeros_like(fitLikelihood)

# Now iterate on each bin
for i,el in enumerate(bins_low):
	logging.info("-"*75)
	logging.info(f"Looking at temporal bin n. {i}...")
	logging.info("-"*75)

	# ======================================================================================
	#                                 COMMON DEFINITIONS
	# ======================================================================================
	# Define the names of the files produced at this step in the iteration (we have to redo
	# the gtselect / gtmktime call each time, and the likelihood analysis, but not the ltcubes)
	data_PH_after_gtselect = os.path.join(tempdir,f"PH_time_{i:03d}_after_gtselect" + offsetStr + ".fits")
	data_PH_after_gtmktime = os.path.join(tempdir,f"PH_time_{i:03d}_after_gtmktime" + offsetStr + ".fits")
	data_PH_after_gtltcube = os.path.join(tempdir,f"PH_time_{i:03d}_after_gtltcube" + offsetStr + ".fits")
	data_PH_after_gtexpmap = os.path.join(tempdir,f"PH_time_{i:03d}_after_gtexpmap" + offsetStr + ".fits")
	data_xml_bkgmodel = os.path.join(tempdir,f"PH_time_{i:03d}_bkg_model" + offsetStr + ".xml")
	data_xml_gtlike = os.path.join(tempdir,f"PH_time_{i:03d}_gtlike" + offsetStr + ".xml")
	data_like_out = os.path.join(tempdir,f"PH_time_{i:03d}_after_gtlike" + offsetStr + ".npz")

	# ======================================================================================
	#                                   DATA EXTRACTION
	# ======================================================================================
	# Call gtselect to extract the data in the given ROI, energy band and temporal window
	# Then gtmktime to adjust the GTIs following the chosen ROI and other conditions
	ifDoAnyways = True
	if ( ifDoAnyways | (not os.path.exists(data_PH_after_gtselect)) | (not os.path.exists(data_PH_after_gtmktime)) ):
		logging.info(f"Calling gtselect and gtmktime...")
		gtselect(infile=PHfilename,outfile=data_PH_after_gtselect,
				 ROI_ra=ROI_ra,ROI_dec=ROI_dec,ROI_rad=ROI_rad,Emin=Emin,Emax=Emax,
				 zmax=zmax,tstart=bins_low[i],tstop=bins_up[i],evclass=evclass,evtype=evtype,
				 chatter=chatter,gtmode=gtmode)

		gtmktime(infile=data_PH_after_gtselect,outfile=data_PH_after_gtmktime,SCfile=SCfilename,
				 ROI_ra=ROI_ra,ROI_dec=ROI_dec,roicut=roicut,filtercond=filtercond,
				 chatter=chatter,gtmode=gtmode)

		logging.info(f"Done!")
		logging.info(f"")
	else:
		logging.info(f"Skipping gtselect and gtmktime...")
		logging.info(f"")

	# To perform an unbinned analysis, we must compute beforehand the livetime and exposure
	# of the LAT in the analyzed area.
	ifDoAnyways = True
	if ( ifDoAnyways | (not os.path.exists(data_PH_after_gtltcube)) ):
		logging.info(f"Calling gtltcube...")
		gtltcube(infile=data_PH_after_gtmktime,outfile=data_PH_after_gtltcube,SCfile=SCfilename,
	    	     zmax=zmax,dcostheta=dcostheta,binsz=binsz,chatter=chatter,gtmode=gtmode)
		logging.info(f"Done!")
		logging.info(f"")
	else:
		logging.info(f"Skipping gtltcube...")
		logging.info(f"")

	# Call gtexposure
	nbins_arc = int( (ROI_rad+extROI_rad) / binsz)
	ifDoAnyways = True
	if ( ifDoAnyways | (not os.path.exists(data_PH_after_gtexpmap)) ):
		logging.info(f"Calling gtexpmap...")
		gtexpmap(infile=data_PH_after_gtmktime,outfile=data_PH_after_gtexpmap,SCfile=SCfilename,
	         	 expcubefile=data_PH_after_gtltcube,evtype=expmap_evtype,irfs=expmap_irfs,
	         	 srcrad=ROI_rad+extROI_rad,nlong=nbins_arc,nlat=nbins_arc,nenergies=nbands_energy,
	         	 chatter=chatter,gtmode=gtmode)
		logging.info(f"Done!")
		logging.info(f"")
	else:
		logging.info(f"Skipping gtexpmap...")
		logging.info(f"")

	# ==========================================================================================
	#                          DIFFUSE RESPONSE CALCULATION
	# ==========================================================================================
	# Check if we want to do the likelihood analysis or not
	ifDoAnyways = True
	if (not ifDoAnyways):
		continue

	# Now we must create an xml file containing all the known sources localized within the ROI
	# and compute the corresponding expected contribution (i.e., expected number of emitted
	# photons).
	source_list = SourceList(catalog_file=catalog_file_4FGL,
	                     	 ROI=[ROI_ra,ROI_dec,ROI_rad],
	                     	 output_name=data_xml_bkgmodel,
	                     	 DR=DRnum)
	source_list.make_model(free_radius = free_radius,
						   max_free_radius = ROI_rad,
	                       variable_free = True)

	# Compute the diffuse response of the sources added to the xml file    
	logging.info(f"Calling gtdiffrsp...")
	gtdiffrsp(infile=data_PH_after_gtmktime,SCfile=SCfilename,srcmdl=data_xml_bkgmodel,
	          irfs=expmap_irfs,clobber=xmlclobber,chatter=chatter,gtmode=gtmode)    
	logging.info(f"Done!")
	logging.info(f"")

	# ==========================================================================================
	#                             LIKELIHOOD ANALYSIS
	# ==========================================================================================
	# Now perform the likelihood analysis
	logging.info(f"Calling gtlike...")
	like = gtlike(infile=data_PH_after_gtmktime,outfile=data_xml_gtlike,SCfile=SCfilename,
	       		  expcubefile=data_PH_after_gtltcube,expmapfile=data_PH_after_gtexpmap,srcmdl=data_xml_bkgmodel,
	       		  irfs=expmap_irfs,optimizer=gtlike_optimizer,verbosity=gtlike_verbosity)
	logging.info(f"Done!")
	logging.info(f"")

	# Save the data of the fitted source
	if like is not None:
		# Get useful parameters
		fitLikelihood[i] = -like.logLike.value()
		NumFreePars[i] = len(like.freePars(goodSourceName))
		TS[i] = like.Ts(goodSourceName)
		sigma[i] = TS2sigma(TS[i],NumFreePars[i])
		flux[i] = like.flux(goodSourceName,emin=Emin,emax=Emax)
		flux_err[i] = like.fluxError(goodSourceName,emin=Emin,emax=Emax)
		
		# Get fit parameters
		allnorm[i] = like.model[goodSourceName].funcs['Spectrum'].params['norm'].value()
		allnorm_err[i] = like.model[goodSourceName].funcs['Spectrum'].params['norm'].error()

		allalpha[i] = like.model[goodSourceName].funcs['Spectrum'].params['alpha'].value()
		allalpha_err[i] = like.model[goodSourceName].funcs['Spectrum'].params['alpha'].error()

		allbeta[i] = like.model[goodSourceName].funcs['Spectrum'].params['beta'].value()
		allbeta_err[i] = like.model[goodSourceName].funcs['Spectrum'].params['beta'].error()

		allEb[i] = like.model[goodSourceName].funcs['Spectrum'].params['Eb'].value()
		allEb_err[i] = like.model[goodSourceName].funcs['Spectrum'].params['Eb'].error()

		# Save everything
		np.savez_compressed(data_like_out, 
							fitLikelihood = fitLikelihood[i],
							NumFreePars = NumFreePars[i],
							TS = TS[i],
							sigma = sigma[i],
							flux = flux[i],
							flux_err = flux_err[i],
							norm = allnorm[i],
							norm_err = allnorm_err[i],
							alpha = allalpha[i],
							alpha_err = allalpha_err[i],
							beta = allbeta[i],
							beta_err = allbeta_err[i],
							Eb = allEb[i],
							Eb_err = allEb_err[i])

	# ==========================================================================================
	#                             SINGLE TIME PLOT
	# ==========================================================================================
	# Plot the energy spectrum recorded in this bin
	energies = truebins(like.energies)
	source_counts = np.zeros_like(energies)
	bkg_counts = np.zeros_like(energies)

	for sourceName in like.sourceNames():
		# Iterate over every source in the XML files. Divide them in two classes: flares and background.
	    if sourceName == goodSourceName:
	        source_counts += like._srcCnts(sourceName)
	    else:
	        bkg_counts += like._srcCnts(sourceName)

	all_counts = source_counts + bkg_counts
	GeVenergies = energies / 1000

	# Now plot everything
	figname = os.path.join(imgdir,f'03_gtlikecurve_time_Espectra_{i:03d}' + filetype)

	fig, ax = plt.subplots(figsize = dimfig)
	ax.plot(GeVenergies,source_counts,color='red',label=f'OP 313',linestyle='--',linewidth=2.5)
	ax.plot(GeVenergies,bkg_counts,color='black',label='Background',linestyle='--',linewidth=2.5)
	ax.plot(GeVenergies,all_counts,color='cyan',linestyle='-',linewidth=2.5,label='All counts')
	ax.errorbar(GeVenergies,like.nobs,yerr=np.sqrt(like.nobs),linestyle='',
					label='Data',marker='.',markersize=15,color='mediumblue')
	ax.legend(loc='best',fontsize=0.75*textfont,framealpha=1)
	ax.set_xlabel(f"Energy [GeV]",fontsize = textfont)
	ax.set_ylabel(f"Counts",fontsize = textfont)
	ax.set_xscale('log')
	ax.grid(linewidth=0.5)
	ax.tick_params(labelsize = textfont)
	ax.set_title(sourceName,fontsize=1.5*textfont)
	fig.set_tight_layout('tight')
	fig.savefig(figname,dpi=filedpi)
	plt.close(fig)

	logging.info(f"Plot done!")
	logging.info(f"")

	# Clean the environment
	gc_cleaner()

logging.info("Cycle finished!")
logging.info("")

# ==========================================================================================
# Final plot: show how the flux changes as a function of time
figname = os.path.join(imgdir,f'03_MLgtlike_flux' + filetype)
fig, ax = plt.subplots(figsize = dimfig)

ax.errorbar(bincenters,flux,flux_err,color='mediumblue',linestyle='',marker='.',markersize=10)
ax.set_xlabel(f"Energy [GeV]",fontsize = textfont)
ax.set_ylabel(f"Flux [photons/cm2 * s]",fontsize = textfont)
ax.grid(linewidth=0.5)
ax.tick_params(labelsize = textfont)

fig.set_tight_layout('tight')
fig.savefig(figname,dpi=filedpi)
plt.close(fig)

# ==========================================================================================
# Final plot: show how the normalization factor and the hardness change as a function of time
figname = os.path.join(imgdir,f'03_MLgtlike_fitpars_all' + filetype)
fig = plt.figure(figsize = dimfig)
allaxes = []

for j in range(4):
	allaxes.append(fig.add_subplot(2,2,j+1))


allaxes[0].errorbar(bincenters,allnorm,allnorm_err,color='mediumblue',linestyle='',marker='.',markersize=10)
allaxes[0].set_ylabel(f"Norm",fontsize = textfont)

allaxes[1].errorbar(bincenters,allalpha,allalpha_err,color='mediumblue',linestyle='',marker='.',markersize=10)
allaxes[1].set_ylabel(f"Alpha",fontsize = textfont)

allaxes[2].errorbar(bincenters,allbetanorm,allbetanorm_err,color='mediumblue',linestyle='',marker='.',markersize=10)
allaxes[2].set_ylabel(f"Beta",fontsize = textfont)

allaxes[3].errorbar(bincenters,allEb,allEb_err,color='mediumblue',linestyle='',marker='.',markersize=10)
allaxes[3].set_ylabel(f"Eb",fontsize = textfont)

for j in range(4):
	allaxes[j].set_xlabel(f"Energy [GeV]",fontsize = textfont)
	allaxes[j].grid(linewidth=0.5)
	allaxes[j].tick_params(labelsize = textfont)

fig.set_tight_layout('tight')
fig.savefig(figname,dpi=filedpi)
plt.close(fig)

logging.info(f"Plotting done!")
logging.info(f"")
logging.info(f"Completed the execution of {__file__}")
logging.info("*"*75)