'''
# ==========================================================================================
#                                          HEADER
# ==========================================================================================
This is a utility script which can be used to construct a .npz file containing three 
variables, concerning the weekly photon files available in the system:
    - The numbers of the weeks
    - The METs corresponding to the beginning and ending of each week

-------------------------------------------------------------------------------------------
Call syntax:
    python3 utils_GTIanalyzer.py

i.e., there are no parameters to pass to the script.
    
'''
# ==========================================================================================
#                                 PREAMBLE (module load)
# ==========================================================================================
# Import of the Python Standard Library modules required for the analysis
import glob, os, subprocess, sys       # To act on the operative system
import json                            # To read and write JSON files
import matplotlib as mpl               # For plotting
import matplotlib.pyplot as plt        # For plotting
import numpy as np                     # For numerical analysis
import pandas as pd                    # For reading and writing csv files
import warnings                        # To deactivate the warnings
import time                            # For script timing
import logging                         # For logging purposes

# ==========================================================================================
#                                  STATUS FILE DATA
# ==========================================================================================
# Open the status file
statusfile = '../statusfile.json'
with open(statusfile) as json_file:
    statusdata = json.load(json_file)

# Extract all the relevant variables from the file
datadir = statusdata['datadir']
imgdir = statusdata['imgdir']
tempdir = statusdata['tempdir']
dataSCdir = statusdata['dataSCdir']
dataPHdir = statusdata['dataPHdir']
catalog_file_4FGL = statusdata['catalog_file_4FGL']
catalog_file_3FHL = statusdata['catalog_file_3FHL']

# Define the name for the subfolders (in imgdir and tempdir) where we will save
# the temporary images and files
pretempdir = '05_OP313'
imgdir = os.path.join(imgdir,pretempdir)
tempdir = os.path.join(tempdir,pretempdir)

# Setting of the logging module
logfile = "debug.log"
logging.basicConfig(format='[%(asctime)s] %(message)s',level=logging.INFO,
                    handlers=[logging.FileHandler(logfile,mode='a'),
                              logging.StreamHandler()])

logging.info("*"*75)
logging.info("Beginning execution of utils_GTIanalyzer.py ...")
logging.info(f"")

# ==========================================================================================
#                                       SCRIPT
# ==========================================================================================
# Retrieve the unsorted list of LAT photon files from its location
filenames = glob.glob(dataPHdir+'lat_photon_weekly_w*_p305_v001.fits')

# Iterate over every file to record its beginning and ending in METs.
# At each step, call gtvcut on the analyzed file and use its output to compute the METs.
ind1 = 7           # Index of the line in the gtvcut output containing the week initial MET
ind2 = -7          # Index of the line in the gtvcut output containing the week final MET
checkvalue = 100   # Printout a warning every such number of weeks analyzed
numfiles = len(filenames)
weeknumbers = np.zeros((numfiles,),dtype=np.int64)
firstGTIs = np.zeros_like(weeknumbers)
lastGTIs = np.zeros_like(weeknumbers)

logging.info(f"Now iterating on the weekly photon files...")
for i,el in enumerate(filenames):
    if (i % checkvalue == 0):
        logging.info(f"Opening file {i} / {numfiles-1}")
    
    # Determine the current week (remember that they may be unordered and also there may be
    # some missing file, so we can't just use "i" to determine the week number. It is way
    # more consistent to just determine the number from the file name, knowing its syntax.
    lastname = el.split('lat_photon_weekly_w')[-1] # e.g. "lat_photon_weekly_w56*_p305_v001.fits"
    weeknumbers[i] = np.int64(lastname[:3])
    
    # Call gtvcut and capture the output
    result = subprocess.run(["gtvcut",f"infile={el}","table=EVENTS","chatter=4","suppress_gtis=no",
                             "debug=no","gui=no","mode=h"],stdout=subprocess.PIPE)

    # Parse the output and deduce the initial and final MET
    all_lines = result.stdout.decode('utf-8')
    split_lines = all_lines.split('\n')
    tstart = split_lines[ind1].split('  ')[0]
    tstop = split_lines[ind2].split('  ')[-1]

    # Save the outputs
    firstGTIs[i] = np.floor(np.float64(tstart))
    lastGTIs[i] = np.ceil(np.float64(tstop))

# Sort the weeks before saving the data
arginds = np.argsort(weeknumbers)
weeknumbers = weeknumbers[arginds]
firstGTIs = firstGTIs[arginds]
lastGTIs = lastGTIs[arginds]

# ==========================================================================================
#                                       OUTPUTS
# ==========================================================================================
# Save the outputs
outfilenpz = os.path.join(tempdir,'GTIdict.npz')
np.savez_compressed(outfilenpz,weeknumbers = weeknumbers,
                               firstGTIs = firstGTIs,
                               lastGTIs = lastGTIs)

logging.info(f"")
logging.info(f"The .npz output file was correctly written at the following path:")
logging.info(outfilenpz)
logging.info(f"")
logging.info(f"Completed the execution of utils_GTIanalyzer.py!")
logging.info("*"*75)