# Analysis of the VHE emission in time from FSRQ OP 313

Guide written and edited by [P. Monti-Guarnieri](mailto:pietro.monti-guarnieri@phd.units.it). 

Date of the last update of this guide: 2024/12/26

## Table Of Contents

[[_TOC_]]

## A general idea 

In this folder we want to analyze the time-resolved emission of high energy photons from FSRQ OP 313.

List of files:
- 01_DataExtractor.py: extract mission-long data from OP 313 (gtselect, gtmktime) and create Light Curve (gtbin, gtexposure)
- 02_LCvis.py: visualize mission-long Light Curve created by 01_DataExtractor
- 03_MLgtlike.py: perform likelihood analysis in time bins and shows how the flux and fit parameters change as a function of time
- utils_configcreator.py: creates config file necessary to run all the scripts
- utils_GTIanalyzer.py: utility called by utils_configcreator, creates .npz file with the GTIs for all the mission weeks available
- utils_ListPreparator.py: utility called by utils_configcreator, creates .list files for a given interval of mission weeks