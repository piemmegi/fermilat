'''
# ==========================================================================================
#                                       HEADER
# ==========================================================================================
The aim of this script is to extract the mission-long data of OP 313, applying gtselect & gtmktime,
and producing a mission-long Light Curve (with gtbin and gtexposure)

-------------------------------------------------------------------------------------------
Call syntax:
  	python3 01_DataExtractor.py

'''
# ==========================================================================================
#                                 PREAMBLE (module load)
# ==========================================================================================
# Import of the Python Standard Library modules required for the analysis
import glob, os, subprocess, sys			# To act on the operative system
import json									# To read and write JSON files
import matplotlib as mpl					# For plotting
import matplotlib.pyplot as plt				# For plotting
import numpy as np							# For numerical analysis
import pandas as pd							# For reading and writing csv files
import warnings								# To deactivate the warnings
import time									# For script timing
import logging								# For logging purposes
import datetime								# For timestamp printing
import copy									# To copy objects

# Import of the astropy/gammapy/fermipy (& fermitools) modules necessary for astrophysics
import astropy.units as u					# Celestial units
from astropy.io import fits
from gammapy.maps import Map, WcsNDMap		# To work with sky maps
from astropy.coordinates import SkyCoord 	# To work with sky frames

# Import of personal functions from custom modules
sys.path.append("../../Functions")
from AstroFunctions import gtselect, gtmktime, gtbin, gtexposure
from MyFunctions import wmean, truehisto1D, histoplotter1D

# Set the warnings of a few functions
np.seterr(divide='ignore', invalid='ignore')	# To avoid 0/0 errors
mpl.rcParams['backend'] = 'agg'					# Non-interactive backend for plotting
warnings.filterwarnings("ignore", category=DeprecationWarning) # To avoid deprecation warnings

# =========================================================================================
#                                      COMMON DEFINITIONS                                      
# =========================================================================================
# Define the dimension for the fonts in the plots, the figures dimensions and so on
textfont = 20			# Fontsize for labels and legends
dimfig = (12,7)			# Figure dimensions (A4-like)
dimfigbig = (16,12)		# Figure dimensions (a bit larger than A4-like)
dimfiglong = (16,25)	# Figure dimensions (a bit larger and much longer than A4-like)
filetype = '.pdf'		# File format to save the pictures
filedpi = 520			# Resolution used when saving pictures in .png/.jpeg
nptsfit = 10000			# Number of points used in plotting the fits

# ==========================================================================================
#                                  STATUS FILE DATA
# ==========================================================================================
# Open the status file
statusfile = '../statusfile.json'
with open(statusfile) as json_file:
	statusdata = json.load(json_file)

# Extract all the relevant variables from the file
datadir = statusdata['datadir']
imgdir = statusdata['imgdir']
tempdir = statusdata['tempdir']
dataSCdir = statusdata['dataSCdir']
dataPHdir = statusdata['dataPHdir']
catalog_file_4FGL = statusdata['catalog_file_4FGL']
catalog_file_3FHL = statusdata['catalog_file_3FHL']
catalog_file_3FHL_reg = statusdata['catalog_file_3FHL_reg']
catalog_file_3FHL_regAssoc = statusdata['catalog_file_3FHL_regAssoc']

# Define the name for the subfolders (in imgdir and tempdir) where we will save
# the temporary images and files
pretempdir = '05_OP313'
imgdir = os.path.join(imgdir,pretempdir)
tempdir = os.path.join(tempdir,pretempdir)

# Setting of the logging module
logfile = "debug.log"
logging.basicConfig(format='[%(asctime)s] %(message)s',level=logging.INFO,
					handlers=[logging.FileHandler(logfile,mode='w'),
					logging.StreamHandler()])
logging.info("*"*75)
logging.info(f"Beginning the execution of {__file__}")
logging.info(f"")

# ==========================================================================================
#                                 CONFIG FILE DATA
# ==========================================================================================
# Open the configuration file made for the 1st level trigger and extract the variables
# necessary for the analysis
configfile = os.path.join(tempdir,f'configfile_trignum_0.json')
with open(configfile) as json_file:
	configdata = json.load(json_file)

# Extract all the relevant variables from the file
tstart            = configdata['tstart']             # gtselect
tstop             = configdata['tstop']              # gtselect 
ROI_ra            = configdata['ROI_ra']             # gtselect
ROI_dec           = configdata['ROI_dec']            # gtselect
ROI_rad           = configdata['ROI_rad']            # gtselect
Emin              = configdata['Emin']               # gtselect
Emax              = configdata['Emax']               # gtselect
zmax              = configdata['zmax']               # gtselect
evclass           = configdata['evclass']            # gtselect
evtype            = configdata['evtype']             # gtselect  
roicut            = configdata['roicut']             # gtmktime
filtercond        = configdata['filtercond']         # gtmktime
tbinalg           = configdata['tbinalg']            # gtbin
tbinfile          = configdata['tbinfile']           # gtbin
dtime             = configdata['dtime']              # gtbin
algorithm         = configdata['algorithm']          # gtbin
irfs              = configdata['irfs']               # gtexposure
srcmdl            = configdata['srcmdl']             # gtexposure
specin            = configdata['specin']             # gtexposure
chatter           = configdata['chatter']            # All fermitools
gtmode            = configdata['gtmode']             # All fermitools
PHlistname        = configdata['PHlistname']         # File management
SClistname        = configdata['SClistname']         # File management
SCfilename        = configdata['SCfilename']         # File management


# ==========================================================================================
#                                 TEMPORARY FILE NAMES
# ==========================================================================================
# Define the names of the .fits, .txt, .xml and .npz files which will be produced in the execution
data_PH_after_gtselect   = os.path.join(tempdir,f"PH_after_gtselect.fits")
data_PH_after_gtmktime   = os.path.join(tempdir,f"PH_after_gtmktime.fits")
data_PH_after_gtbin      = os.path.join(tempdir,f"PH_after_gtbin.fits")

# ==========================================================================================
#                                   DATA EXTRACTION
# ==========================================================================================
# Call gtselect to extract the data in the given ROI, energy band and temporal window
ifDoAnyways = False
if ifDoAnyways:
	logging.info(f"Calling gtselect...")
	gtselect(infile=PHlistname,outfile=data_PH_after_gtselect,
			 ROI_ra=ROI_ra,ROI_dec=ROI_dec,ROI_rad=ROI_rad,Emin=Emin,Emax=Emax,
			 zmax=zmax,tstart=tstart,tstop=tstop,evclass=evclass,evtype=evtype,
			 chatter=chatter,gtmode=gtmode)
	logging.info(f"Done!")
	logging.info(f"")
else:
	logging.info(f"Skipping gtselect...")
	logging.info(f"")

# Call gtmktime to adjust the GTIs following the chosen ROI and other conditions
ifDoAnyways = False
if ifDoAnyways:
	logging.info(f"Calling gtmktime...")
	gtmktime(infile=data_PH_after_gtselect,outfile=data_PH_after_gtmktime,SCfile=SCfilename,
			 ROI_ra=ROI_ra,ROI_dec=ROI_dec,roicut=roicut,filtercond=filtercond,
			 chatter=chatter,gtmode=gtmode)
	logging.info(f"Done!")
	logging.info(f"")
else:
	logging.info(f"Skipping gtmktime...")
	logging.info(f"")

# ==========================================================================================
#                                    LIGHT CURVE ANALYSIS
# ==========================================================================================
# Create the binning for the Light Curve using gtbindef and then the Light Curve using gtbin
ifDoAnyways = True
if ifDoAnyways:
	# Create the Light Curve
	logging.info(f"Calling gtbin and gtexposure...")
	gtbin(infile=data_PH_after_gtmktime,outfile=data_PH_after_gtbin,SCfile=SCfilename,
		  tstart=tstart,tstop=tstop,tbinalg=tbinalg,tbinfile=tbinfile,dtime=dtime,algorithm=algorithm,
		  chatter=chatter,gtmode=gtmode)

	# Adjust the Light Curve to account for the LAT exposure
	gtexposure(infile=data_PH_after_gtbin,SCfile=SCfilename,irfs=irfs,scrmdl=srcmdl,specin=specin,
			   chatter=chatter,gtmode=gtmode)
	logging.info(f"Done!")
	logging.info(f"")
else:
	logging.info(f"Skipping gtbin and gtexposure...")
	logging.info(f"")


# ==========================================================================================
# Final printouts
logging.info(f"")
logging.info(f"Completed the execution of {__file__}")
logging.info("*"*75)