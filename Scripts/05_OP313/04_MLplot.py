'''
# ==========================================================================================
#                                       HEADER
# ==========================================================================================
The aim of this script is to just plot the data obtained in the analysis done in 03_MLgtlike

-------------------------------------------------------------------------------------------
Call syntax:
  	python3 04_SED.py [ifOffset]

Call arguments:
	ifOffset		[bool] shift all the bins by a given offset? (Default: False)

'''
# ==========================================================================================
#                                 PREAMBLE (module load)
# ==========================================================================================
# Import of the Python Standard Library modules required for the analysis
import glob, os, subprocess, sys       # To act on the operative system
import json                            # To read and write JSON files
import matplotlib as mpl               # For plotting
import matplotlib.pyplot as plt        # For plotting
import numpy as np                     # For numerical analysis
import pandas as pd 				   # For reading and writing csv files
import warnings                        # To deactivate the warnings
import time                            # For script timing
import logging						   # For logging purposes
import scipy.stats as sp  			   # For statistical analyses

# Import of the astropy/gammapy/fermipy (& fermitools) modules necessary for astrophysics
from LATSourceModel import SourceList      # To create the XML files required by the Fermitools

# Import of personal functions from custom modules
sys.path.append("../../Functions")
from MyFunctions import truebins, histoplotter2D, gc_cleaner
from AstroFunctions import TS2sigma, gtselect, gtmktime, gtdiffrsp, gtlike, gtexpmap, gtltcube

# Set the warnings of a few functions
np.seterr(divide='ignore', invalid='ignore') 					# To avoid 0/0 errors
mpl.rcParams['backend'] = 'agg'                                 # Non-interactive backend for plotting
warnings.filterwarnings("ignore", category=DeprecationWarning)  # To avoid deprecation warnings

# =========================================================================================
#                                      COMMON DEFINITIONS                                      
# =========================================================================================
# Define the dimension for the fonts in the plots, the figures dimensions and so on
textfont = 20         # Fontsize for labels and legends
dimfig = (12,7)       # Figure dimensions (A4-like)
dimfigbig = (16,12)   # Figure dimensions (a bit larger than A4-like)
dimfiglong = (16,25)  # Figure dimensions (a bit larger and much longer than A4-like)
filetype = '.pdf'     # File format to save the pictures
filedpi = 520         # Resolution used when saving pictures in .png/.jpeg
nptsfit = 10000       # Number of points used in plotting the fits

# ==========================================================================================
#                                  STATUS FILE DATA
# ==========================================================================================
# Open the status file
statusfile = '../statusfile.json'
with open(statusfile) as json_file:
	statusdata = json.load(json_file)

# Extract all the relevant variables from the file
datadir = statusdata['datadir']
imgdir = statusdata['imgdir']
tempdir = statusdata['tempdir']
dataSCdir = statusdata['dataSCdir']
dataPHdir = statusdata['dataPHdir']
catalog_file_4FGL = statusdata['catalog_file_4FGL']
catalog_file_3FHL = statusdata['catalog_file_3FHL']

# Define the name for the subfolders (in imgdir and tempdir) where we will save
# the temporary images and files
pretempdir = '05_OP313'
imgdir = os.path.join(imgdir,pretempdir)
tempdir = os.path.join(tempdir,pretempdir)

# Setting of the logging module
logfile = "debug.log"
logging.basicConfig(format='[%(asctime)s] %(message)s',level=logging.INFO,
					handlers=[logging.FileHandler(logfile,mode='w'),
				        	  logging.StreamHandler()])
logging.info("*"*75)
logging.info(f"Beginning execution of {__file__}")
logging.info(f"")

# ==========================================================================================
#                                 CONFIG FILE DATA
# ==========================================================================================
# Open the configuration file made for the 1st level trigger and extract the variables
# necessary for the analysis
configfile = os.path.join(tempdir,f'configfile_trignum_0.json')
with open(configfile) as json_file:
	configdata = json.load(json_file)

# Extract all the relevant variables from the file
tstart            = configdata['tstart']             # gtselect
tstop             = configdata['tstop']              # gtselect 
ROI_ra            = configdata['ROI_ra']             # gtselect
ROI_dec           = configdata['ROI_dec']            # gtselect
ROI_rad           = configdata['ROI_rad']            # gtselect
Emin              = configdata['Emin']               # gtselect
Emax              = configdata['Emax']               # gtselect
zmax              = configdata['zmax']               # gtselect
evclass           = configdata['evclass']            # gtselect
evtype            = configdata['evtype']             # gtselect
roicut            = configdata['roicut']             # gtmktime
filtercond        = configdata['filtercond']         # gtmktime
dcostheta         = configdata['dcostheta']          # gtltcube
binsz             = configdata['binsz']              # gtltcube
extROI_rad        = configdata['extROI_rad']         # gtexpmap
expmap_evtype     = configdata['expmap_evtype']      # gtexpmap
expmap_irfs       = configdata['expmap_irfs']        # gtexpmap
nbands_energy     = configdata['nbands_energy']      # gtexpmap
DRnum             = configdata['DRnum']              # gtdiffrsp
free_radius       = configdata['free_radius']        # gtdiffrsp
xmlclobber        = configdata['xmlclobber']         # gtlike
gtlike_optimizer  = configdata['gtlike_optimizer']   # gtlike
gtlike_verbosity  = configdata['gtlike_verbosity']   # gtlike
chatter           = configdata['chatter']            # All fermitools
gtmode            = configdata['gtmode']             # All fermitools
SCfilename        = configdata['SCfilename']         # File management

# Define the name of the file produced in output of 01_DataExtractor at the gtmktime level,
# containing all the photons emitted from this source mission-long. This will avoid us
# the necessity to call again gtselect on all the data from all the mission.
PHfilename = os.path.join(tempdir,f"PH_after_gtmktime.fits")

# =========================================================================================
#                                      INPUT PARSING
# =========================================================================================
# Determine from command line if we want to apply a shift to the bins
try:
	if (sys.argv[1] == 'True'):
		ifOffset = True
	else:
		ifOffset = False
except IndexError:
	ifOffset = False

if ifOffset:
	offsetStr = "_offset_"
else:
	offsetStr = ""

logging.info(f"Offset string: {offsetStr}")

# The likelihood analysis should be repeated in multiple time bins, here defined
# First, define the binning step
y2d = 365       # Days in 1 year
m2d = 30        # Days in 1 month
d2h = 24        # Hours in 1 day
h2s = 3600      # Seconds in 1 hour
tstep = 3*m2d*d2h*h2s

# Now define a binning vector ranging from the start to the finish at a certain step (1 week)
offset = 2*tstep
bins = np.arange(tstart,tstop,tstep) + ifOffset * offset
bins_low = bins[:-1]-0.5
bins_up = bins[1:]+0.5
logging.info(f"Using a pre-defined binning (step {tstep} s from MET = {tstart} s to MET = {tstop} s)")

bincenters = (bins_low + bins_up)/2

# ==========================================================================================
#                                 BINNING ANALYSIS
# ==========================================================================================
# Allocate the variables containing the outputs of the bin analysis
goodSourceName = '4FGL J1310.5+3221'
nbins = np.shape(bins_low)[0]

fitLikelihood = np.zeros((nbins,),dtype=np.float64)
NumFreePars = np.zeros_like(fitLikelihood)
TS = np.zeros_like(fitLikelihood)
sigma = np.zeros_like(fitLikelihood)
flux = np.zeros_like(fitLikelihood)
flux_err = np.zeros_like(fitLikelihood)

allnorm = np.zeros_like(fitLikelihood)      # gtlike fits with logparabola: free pars are norm, alpha, beta, Eb
allnorm_err = np.zeros_like(fitLikelihood)

allalpha = np.zeros_like(fitLikelihood)
allalpha_err = np.zeros_like(fitLikelihood)

allbeta = np.zeros_like(fitLikelihood)
allbeta_err = np.zeros_like(fitLikelihood)

allEb = np.zeros_like(fitLikelihood)
allEb_err = np.zeros_like(fitLikelihood)

printoutCheck = 25

# Now iterate on each bin
for i,el in enumerate(bins_low):
	if (i % printoutCheck == 0):
		logging.info("-"*75)
		logging.info(f"Looking at temporal bin n. {i}...")
		logging.info("-"*75)

	# ======================================================================================
	#                                 FILE LOADING
	# ======================================================================================
	# Load the file with the output of gtlike and extract the relevant parameters
	data_like_out = os.path.join(tempdir,f"PH_time_{i:03d}_after_gtlike" + offsetStr + ".npz")
	with np.load(data_like_out) as file:
		flux[i] = file['flux']
		flux_err[i] = file['flux_err']
		allnorm[i] = file['norm']
		allnorm_err[i] = file['norm_err']
		allalpha[i] = file['alpha']
		allalpha_err[i] = file['alpha_err']
		allbeta[i] = file['beta']
		allbeta_err[i] = file['beta_err']
		allEb[i] = file['Eb']
		allEb_err[i] = file['Eb_err']

# End of cycle
logging.info("Cycle finished!")
logging.info("")

# ==========================================================================================
# Final plot: show how the flux changes as a function of time
figname = os.path.join(imgdir,f'04_MLplot_flux' + filetype)
fig, ax = plt.subplots(figsize = dimfig)

ax.errorbar(bincenters,flux,flux_err,color='mediumblue',linestyle='',marker='.',markersize=10)
ax.set_xlabel(f"Energy [GeV]",fontsize = textfont)
ax.set_ylabel(f"Flux [photons/cm2 * s]",fontsize = textfont)
ax.grid(linewidth=0.5)
ax.tick_params(labelsize = textfont)

fig.set_tight_layout('tight')
fig.savefig(figname,dpi=filedpi)
plt.close(fig)

# ==========================================================================================
# Final plot: show how the normalization factor and the hardness change as a function of time
figname = os.path.join(imgdir,f'04_MLplot_fitpars_all' + filetype)
fig = plt.figure(figsize = dimfig)
allaxes = []

for j in range(4):
	allaxes.append(fig.add_subplot(2,2,j+1))

#allaxes[0].errorbar(bincenters,allnorm,allnorm_err,color='mediumblue',linestyle='',marker='.',markersize=10)
allaxes[0].plot(bincenters,allnorm,color='mediumblue',linestyle='',marker='.',markersize=10)
allaxes[0].set_ylabel(f"Norm",fontsize = textfont)

#allaxes[1].errorbar(bincenters,allalpha,allalpha_err,color='mediumblue',linestyle='',marker='.',markersize=10)
allaxes[1].plot(bincenters,allalpha,color='mediumblue',linestyle='',marker='.',markersize=10)
allaxes[1].set_ylabel(f"Alpha",fontsize = textfont)

#allaxes[2].errorbar(bincenters,allbeta,allbeta_err,color='mediumblue',linestyle='',marker='.',markersize=10)
allaxes[2].plot(bincenters,allbeta,color='mediumblue',linestyle='',marker='.',markersize=10)
allaxes[2].set_ylabel(f"Beta",fontsize = textfont)

allaxes[3].errorbar(bincenters,allEb,allEb_err,color='mediumblue',linestyle='',marker='.',markersize=10)
allaxes[3].set_ylabel(f"Eb",fontsize = textfont)

for j in range(4):
	allaxes[j].set_xlabel(f"Energy [GeV]",fontsize = textfont)
	allaxes[j].grid(linewidth=0.5)
	allaxes[j].tick_params(labelsize = textfont)

fig.set_tight_layout('tight')
fig.savefig(figname,dpi=filedpi)
plt.close(fig)

logging.info(f"Plotting done!")
logging.info(f"")
logging.info(f"Completed the execution of {__file__}")
logging.info("*"*75)