'''
# ==========================================================================================
#                                       HEADER
# ==========================================================================================
The aim of this script is to view the mission-long Light Curve of OP 313, made here using the
data extracted in 01_DataExtractor. 

-------------------------------------------------------------------------------------------
Call syntax:
  	python3 02_LCvis.py

'''
# ==========================================================================================
#                                 PREAMBLE (module load)
# ==========================================================================================
# Import of the Python Standard Library modules required for the analysis
import glob, os, subprocess, sys       # To act on the operative system
import json                            # To read and write JSON files
import matplotlib as mpl               # For plotting
import matplotlib.pyplot as plt        # For plotting
import numpy as np                     # For numerical analysis
import pandas as pd 				   # For reading and writing csv files
import warnings                        # To deactivate the warnings
import time                            # For script timing
import logging						   # For logging purposes

# Import of the astropy/gammapy/fermipy (& fermitools) modules necessary for astrophysics
from astropy.io import fits                # I/O of .fits files
from astropy.stats import bayesian_blocks  # Bayesian Block segmentation algorithm

# Import of personal functions from custom modules
sys.path.append("../../Functions")
from MyFunctions import wmean, truebins
from AstroFunctions import nu_estimator, tot_intervals

# Set the warnings of a few functions
np.seterr(divide='ignore', invalid='ignore') 					# To avoid 0/0 errors
mpl.rcParams['backend'] = 'agg'                                 # Non-interactive backend for plotting
warnings.filterwarnings("ignore", category=DeprecationWarning)  # To avoid deprecation warnings

# =========================================================================================
#                                      COMMON DEFINITIONS                                      
# =========================================================================================
# Define the dimension for the fonts in the plots, the figures dimensions and so on
textfont = 20         # Fontsize for labels and legends
dimfig = (12,7)       # Figure dimensions (A4-like)
dimfigbig = (16,12)   # Figure dimensions (a bit larger than A4-like)
dimfiglong = (16,25)  # Figure dimensions (a bit larger and much longer than A4-like)
filetype = '.pdf'     # File format to save the pictures
filedpi = 520         # Resolution used when saving pictures in .png/.jpeg
nptsfit = 10000       # Number of points used in plotting the fits

# ==========================================================================================
#                                  STATUS FILE DATA
# ==========================================================================================
# Open the status file
statusfile = '../statusfile.json'
with open(statusfile) as json_file:
	statusdata = json.load(json_file)

# Extract all the relevant variables from the file
datadir = statusdata['datadir']
imgdir = statusdata['imgdir']
tempdir = statusdata['tempdir']
dataSCdir = statusdata['dataSCdir']
dataPHdir = statusdata['dataPHdir']
catalog_file_4FGL = statusdata['catalog_file_4FGL']
catalog_file_3FHL = statusdata['catalog_file_3FHL']

# Define the name for the subfolders (in imgdir and tempdir) where we will save
# the temporary images and files
pretempdir = '05_OP313'
imgdir = os.path.join(imgdir,pretempdir)
tempdir = os.path.join(tempdir,pretempdir)

# Setting of the logging module
logfile = "debug.log"
logging.basicConfig(format='[%(asctime)s] %(message)s',level=logging.INFO,
					handlers=[logging.FileHandler(logfile,mode='w'),
				        	  logging.StreamHandler()])

logging.info("*"*75)
logging.info(f"Beginning the execution of {__file__}")
logging.info(f"")

# ==========================================================================================
#                                 TEMPORARY FILE NAMES
# ==========================================================================================
# Define the names of the .fits, .txt, .xml and .npz files which will be produced in the execution
data_PH_after_gtbin = os.path.join(tempdir,f"PH_after_gtbin.fits")

# ==========================================================================================
#                                LIGHT CURVE EXTRACTION
# ==========================================================================================
# Open the Light Curve file and extract times, counts and exposures to compute the rate vs MET
with fits.open(data_PH_after_gtbin) as hdul:
    ind = 1
    LC_time = np.array(hdul[ind].data.field('TIME'),dtype=np.float64)
    LC_counts = np.array(hdul[ind].data.field('COUNTS'),dtype=np.float64)
    LC_exposure = np.array(hdul[ind].data.field('EXPOSURE'),dtype=np.float64)

LC_rate = LC_counts/LC_exposure
LC_rate_err = np.sqrt(LC_counts)/LC_exposure
LC_weights = 1/LC_rate_err**2

# Compute the average flux from the Light Curve
good_inds = (np.isnan(LC_weights) == False) & (np.isinf(LC_weights) == False)
LC_av_rate, _ = wmean(LC_rate[good_inds],LC_weights[good_inds])
	
logging.info(f"Average flux mission-long: {LC_av_rate:.2e} photons * cm(-2) * s(-1)")	
logging.info(f"")

# ==========================================================================================
# Divide the curve in Bayesian Blocks
LC_significance = ( LC_rate - np.mean(LC_rate)) / np.std(LC_rate)
LC_significance_err = None
p0 = 0.25
BayesEdges = bayesian_blocks(LC_time,LC_significance,LC_significance_err,"measures",p0 = p0,ncp_prior = None)
BayesOffset = 500
if BayesEdges[0] >= LC_time[0]:
	# There are data_times before the first bayesian edge: correct it!
	BayesEdges[0] = LC_time[0] - BayesOffset

if BayesEdges[-1] <= LC_time[-1]:
	# There are data_times after the last bayesian edge: correct it!
	BayesEdges[-1] = LC_time[-1] + BayesOffset

logging.info(f"Number of Bayesian Blocks used to segmentate the LC: {np.shape(BayesEdges)[0]-1}")
logging.info(f"")

# Write down the Basyesian Edges in the stdout
nBayesBins = len(BayesEdges)-1
logging.warning("Bayes bins (edges given in MET):")
for i in range(nBayesBins):
	logging.warning(f"{BayesEdges[i]:.3f} {BayesEdges[i+1]:.3f}\n")

# We want to show the BB in the same picture as the flux: compute the corresponding curve
BayesBinCenters = np.zeros((nBayesBins,))
BayesRates = np.zeros_like(BayesEdges)

for i in range(nBayesBins):
    # Extract the i-th bin
    BayesBinCenters[i] = (BayesEdges[i] + BayesEdges[i+1])/2
    cond = (LC_time >= BayesEdges[i]) & (LC_time <= BayesEdges[i+1])

    # Compute the average rate in the bin
    BayesRates[i+1] = np.mean(LC_rate[cond])
    
# Set the first points to the same value of the second (they consitute TOGETHER the first
# bin, since they are EDGES)
BayesRates[0] = BayesRates[1]

# ==========================================================================================
#                                LIGHT CURVE VISUALIZATION
# ==========================================================================================
# Plot the flux curve
figname = os.path.join(imgdir,f'02_LC_complete.pdf')

fig, ax = plt.subplots(figsize = dimfig)
ax.errorbar(LC_time,LC_rate,LC_rate_err,color='mediumblue',marker='.', markersize=5,
				linestyle='',label = 'Data')
ax.plot(LC_time,LC_time*0+LC_av_rate,color='red',linewidth=3,linestyle='--',
		label=f"Mission-long average")
ax.plot(BayesEdges,BayesRates,color='darkorange',linestyle='-',marker='',linewidth=1.5,
				drawstyle='steps-pre',label='Bayesian Blocks')
ax.set_xlabel('MET [s]',fontsize=textfont)
ax.set_ylabel(r'Flux [cm$^{-2}$ s$^{-1}$]',fontsize=textfont)
ax.set_yscale('log')
ax.set_title("OP 313",fontsize=1.5*textfont)
ax.legend(loc='center left',fontsize=textfont,framealpha=0.75)
ax.grid(linewidth=0.5)
ax.tick_params(labelsize = textfont)
fig.set_tight_layout('tight')
fig.savefig(figname,dpi=filedpi)
plt.close(fig)

logging.info("")
logging.warning(f"Completed the execution of {__file__}")
logging.info("*"*75)