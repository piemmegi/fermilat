# List of the available sub-folders / projects

Last update of this guide: 2024/12/13


| Folder name          | Content |
|:--------------------:|:-------:|
|[`00_Utilities`](Scripts/00_Utilities) | General-purpose scripts for the management of the Fermi LAT data (usually they are developed here and then moved in other folders.). |
|[`01_FAVA_tentatives`](Scripts/01_FAVA_tentatives)  | First tests with the `Fermitools` and `Fermipy`, including: a first (tentative) implementation of a FAVA-like analysis (01a + py script); a semi-automated time-domain analysis of the photon arrival statistics (01b, 01c, 01d); some tests with Healpix and Fibonacci grids (01e, 01f), some plots useful for explaining how the HEPA pipeline works (01g, 01h). |
|[`02_HEPA`](Scripts/02_HEPA) | Tentative implementation of the High Energy Photon flare Advocate (HEPA): an automated tool which should allow the identification and detection of short-lived transient sources, characterized by a significant emission of high energy photons. |
|[`03_CalOnly`](Scripts/03_CalOnly) | Development of the CalOnly classes. |
|[`04_4FHL`](Scripts/04_4FHL) | First tests towards the development of a 16-years-long catalog of VHE gamma-ray sources |
|[`05_OP313`](Scripts/05_OP313) | Analysis of the VHE emission of OP 313 |
|[`06_s3df_utils`](Scripts/06_s3df_utils) | Utility scripts for the management of files on s3df |