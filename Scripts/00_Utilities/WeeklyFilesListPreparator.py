# ==========================================================================================
#                                          HEADER
# ==========================================================================================
# Lo scopo di questo script è produrre due file .list:
#     - Il primo contiene un elenco dei file settimanali di fotoni da analizzare con gtselect
#     - Il secondo contiene un elenco dei file settimanali di spacecraft da usare con gtmktime
# Sono incluse per default tutte le run da una settimana data in input a un'altra.
# Se non sono dati input espliciti, le settimane sono tutte quelle coperte dalla missione
# (dalla w009 alla w809)
#
# Sintassi di call:
#   python3 WeeklyFilesListPreparator.py wBEGIN wEND
#
# Esempio:
#   python3 WeeklyFilesListPreparator.py 9 809
#
# ==========================================================================================
#                                  PREAMBOLO (load moduli)
# ==========================================================================================
import sys
import os

# ==========================================================================================
#                                  CODIFICA DEI FILE DATI
# ==========================================================================================
# Definisco dove si trovano i weekly photon/SC files
dataPHdir = '/home/pmgunix/pmg-home/FermiLAT/Data/LAT_weekly_photons/weekly/photon/'
dataSCdir = '/home/pmgunix/pmg-home/FermiLAT/Data/LAT_weekly_spacecraft/weekly/spacecraft/'

# Definisco come si chiamano i weekly photon/SC files
PHprename = 'lat_photon_weekly_w'
PHpostname = '_p305_v001.fits'
SCprename = 'lat_spacecraft_weekly_w'
SCpostname = '_p310_v001.fits'

# Definisco da che run a che run analizzare i dati
wTHR = 9
try:
    wGUESS = int(sys.argv[1])
    if wGUESS < wTHR:
        wBEGIN = wTHR
    else:
        wBEGIN = wGUESS
except:
    wBEGIN = wTHR

try:
    wEND = int(sys.argv[2])
except:
    wEND = 809

# Definisco come si chiameranno i file di output
PHoutname = f'./PH_events_w{wBEGIN:03d}_w{wEND:03d}.list'
SCoutname = f'./SC_events_w{wBEGIN:03d}_w{wEND:03d}.list'


# ==========================================================================================
#                                     SCRITTURA
# ==========================================================================================
# Scrivo la lista dei PH files
print(f"")
print(f"Writing list of photon files to be analyzed (PH; from week n. {wBEGIN:03d} to week n. {wEND:03d})\n")
with open(PHoutname,'w') as file:
    for wNUM in range(wBEGIN,wEND+1):
        file.write(os.path.join(dataPHdir,PHprename + f"{wNUM:03d}" + PHpostname) + '\n')

print(f"Output file written:")
print(PHoutname)
print(f"")

# Scrivo la lista degli SC files
print(f"Writing list of spacecraft files to be analyzed (SC; from week n. {wBEGIN:03d} to week n. {wEND:03d})\n")
with open(SCoutname,'w') as file:
    for wNUM in range(wBEGIN,wEND+1):
        file.write(os.path.join(dataSCdir,SCprename + f"{wNUM:03d}" + SCpostname) + '\n')

print(f"Output file written:")
print(SCoutname)
print(f"Job done! \n")