# ==========================================================================================
#                                          HEADER
# ==========================================================================================
# Lo scopo di questo script è creare un file di output contenente tutti i GTI di inizio
# e fine settimana per tutte le settimane di presa dati del LAT
#
# Sintassi di call:
#   python3 PrinterGTIdict.py
#
# Esempio:
#   python3 PrinterGTIdict.py
#
# ==========================================================================================
#                                  PREAMBOLO (load moduli)
# ==========================================================================================
import sys
import os
import glob
import subprocess
import numpy as np

# ==========================================================================================
#                                       SCRIPT
# ==========================================================================================
# Definisco quali file vanno analizzati (tutti quelli disponibili nella cartella dei weekly)
filepath = f"/home/pmgunix/pmg-home/FermiLAT/Data/LAT_weekly_photons/weekly/photon/"
filenames = glob.glob(filepath+'lat_photon_weekly_w*_p305_v001.fits')

# Itero su ciascun file per determinare start e stop dei GTIs
ind1 = 7        # Riga dell'output dove si trova il primo GTI interval
ind2 = -7       # Come sopra, ma ultimo GTI
checkvalue = 25 # Ogni quanto stampare un warning
numfiles = len(filenames)
weeknumbers = np.zeros((numfiles,),dtype=np.int64)
firstGTIs = np.zeros_like(weeknumbers)
lastGTIs = np.zeros_like(weeknumbers)

print(f"\nBeginning iteration on the weekly photon files...")
for i,el in enumerate(filenames):
    if (i % checkvalue == 0):
        print(f"Opening file {i} / {numfiles-1}")
    # Ricavo la settimana corrente
    lastname = el.split('lat_photon_weekly_w')[-1] # Nome del tipo "lat_photon_weekly_w56*_p305_v001.fits"
    weeknumbers[i] = np.int64(lastname[:3])
    
    # Chiamo gtvcut sul file corrente e ne catturo l'output
    result = subprocess.run(["gtvcut",f"infile={el}","table=EVENTS","chatter=4","suppress_gtis=no",
                             "debug=no","gui=no","mode=h"],stdout=subprocess.PIPE)

    # Faccio il parsing dell'output in righe
    all_lines = result.stdout.decode('utf-8')
    split_lines = all_lines.split('\n')

    # Ricavo primo e ultimo GTI e ne costruisco il macro-GTI
    tstart = split_lines[ind1].split('  ')[0]
    tstop = split_lines[ind2].split('  ')[-1]

    firstGTIs[i] = np.floor(np.float64(tstart))
    lastGTIs[i] = np.ceil(np.float64(tstop))

# Ordino i dati in funzione della settimana
arginds = np.argsort(weeknumbers)
weeknumbers = weeknumbers[arginds]
firstGTIs = firstGTIs[arginds]
lastGTIs = lastGTIs[arginds]

# ==========================================================================================
#                                       OUTPUTS
# ==========================================================================================
# Salvo gli output in un apposito file di testo
outfile = 'GTIdict.txt'
with open(outfile,'w') as file:
    file.write("WEEK NUMBER & START [MET] & STOP [MET] \\\\ \n")
    for i in range(numfiles):
        file.write(f"{weeknumbers[i]:03d} & {firstGTIs[i]:03d} & {lastGTIs[i]:03d} \\\\ \n")


# Salvo gli output in un file numpy per sicurezza
outfilenpz = 'GTIdict.npz'
np.savez_compressed(outfilenpz,weeknumbers = weeknumbers,
                               firstGTIs = firstGTIs,
                               lastGTIs = lastGTIs)

print(f"\nOutput files saved at:")
print(outfile)
print(outfilenpz)
print(f"\nJob done!\n")