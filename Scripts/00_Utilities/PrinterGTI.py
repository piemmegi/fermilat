# ==========================================================================================
#                                          HEADER
# ==========================================================================================
# Lo scopo di questo script è stampare i GTI contenuti in un file .fits
#
# Sintassi di call:
#   python3 PrinterGTI.py wTIMES
#
# Esempio:
#   python3 PrinterGTI.py 100
#
# ==========================================================================================
#                                  PREAMBOLO (load moduli)
# ==========================================================================================
import sys
import os
from GtApp import GtApp

# ==========================================================================================
#                                  CODIFICA DEI FILE DATI
# ==========================================================================================
# Definisco la settimana da analizzare
try:
    wTIMES = sys.argv[1]
except:
    wTIMES = "009"

print(f"")
print(f"Settimana analizzata: {wTIMES}")

# Definisco il path dove si trovano i file
prename = f"/home/pmgunix/pmg-home/FermiLAT/Data/LAT_weekly_photons/weekly/photon/"
postname = f"lat_photon_weekly_w{wTIMES}_p305_v001.fits"
filename = os.path.join(prename,postname)

# Importo l'oggetto gtvcut in Python e lo preparo
vcutter = GtApp('gtvcut','Likelihood')
vcutter['infile'] = filename
vcutter['table'] = 'EVENTS'
vcutter['chatter'] = 4
vcutter['suppress_gtis'] = 'no'

# Runno
vcutter.run()

print(f"Job done! \n")
