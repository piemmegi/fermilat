'''
# ==========================================================================================
#                                       HEADER
# ==========================================================================================
The aim of this script is simply to clean every temporary file present in the HEPA temporary
folder and picture folder.

-------------------------------------------------------------------------------------------
Call syntax:
  	python3 utils_tempcleaner.py 

'''
# ==========================================================================================
#                                 PREAMBLE (module load)
# ==========================================================================================
# Import of the Python Standard Library modules required for the analysis
import glob, os, subprocess, sys       # To act on the operative system
import json                            # To read and write JSON files
import matplotlib as mpl               # For plotting
import matplotlib.pyplot as plt        # For plotting
import numpy as np                     # For numerical analysis
import warnings                        # To deactivate the warnings
import time                            # For script timing
import logging						   # For logging purposes

# ==========================================================================================
#                                  STATUS FILE DATA
# ==========================================================================================
# Open the status file
statusfile = '../statusfile.json'
with open(statusfile) as json_file:
	statusdata = json.load(json_file)

# Extract all the relevant variables from the file
datadir = statusdata['datadir']
imgdir = statusdata['imgdir']
tempdir = statusdata['tempdir']
dataSCdir = statusdata['dataSCdir']
dataPHdir = statusdata['dataPHdir']
catalog_file_4FGL = statusdata['catalog_file_4FGL']
catalog_file_3FHL = statusdata['catalog_file_3FHL']

# Define the name for the subfolders (in imgdir and tempdir) where we will save
# the temporary images and files
pretempdir = '02_HEPA'
imgdir = os.path.join(imgdir,pretempdir)
tempdir = os.path.join(tempdir,pretempdir)

# Setting of the logging module
logfile = "HEPA_debug.log"
logging.basicConfig(format='[%(asctime)s] %(message)s',level=logging.INFO,
					handlers=[logging.FileHandler(logfile,mode='a'),
				        	  logging.StreamHandler()])
logging.info("*"*75)
logging.info("Beginning execution of utils_tempcleaner.py ...")
logging.info(f"")

# ==========================================================================================
#                                 IMGDIR CLEANING
# ==========================================================================================
# We now clean the folder with the pictures
extensionsToBeCleaned = ['png','jpg','jpeg','pdf']

# Iterate over every element in the imgdir
logging.info(f"Removing garbage files from {imgdir} and its subdirectories ...")
for ext in extensionsToBeCleaned:
	logging.info(f"Removing .{ext} files...")
	filesToBeCleaned = glob.glob(imgdir + f'/**/*.{ext}',recursive=True)
	for file in filesToBeCleaned:
		os.remove(file)

logging.info(f"Done!\n")

# ==========================================================================================
#                                 TEMPDIR CLEANING
# ==========================================================================================
# We now clean the folder with the temporary files.
extensionsToBeCleaned = ['txt','dat','fits','reg','xml','list','npz','json']
exemptFolders = ['SCfiles']

# Iterate over every element in the tempdir
logging.info(f"Removing garbage files from {tempdir} and its subdirectories, ignoring the exempt folders ...")
for ext in extensionsToBeCleaned:
	logging.info(f"Removing .{ext} files...")
	filesToBeCleaned = glob.glob(tempdir + f'/**/*.{ext}',recursive=True)
	for file in filesToBeCleaned:
		# Check if the given file is in any of the exempt folders
		cond = False
		for folder in exemptFolders:
			cond |= (f'{folder}' in file)

		# Remove the file only if it is not in the exempt folders
		if not cond:
			os.remove(file)

logging.info(f"Done!")
logging.info(f"")
logging.info(f"Completed the execution of utils_tempcleaner.py!")
logging.info("*"*75)