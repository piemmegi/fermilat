'''
# ==========================================================================================
#                                       HEADER
# ==========================================================================================
The aim of this script is to perform a 1st level trigger analysis on a given ROI, in a given
time interval and energy band. All the parameters for the analysis must be set through the
utils_configcreator.py script, using the option "trignum = 1", otherwise this script won't
find the configuration file at all.

-------------------------------------------------------------------------------------------
Call syntax:
  	python3 trigger_lv1.py ifplot ifCalculatingBKG

Call arguments:
	- ifplot			[bool] whether to save the plots produced at the end of the script
						  as figures on-disk, or not (default value: True).
	- ifCalculatingBKG	[bool] set this flag to True if you are computing the background
						  rates for saving purposes and thus you want to force the "ifLoadAverageRates"
						  to be "False" just in the execution of this script. Otherwise,
						  leave to False (default value: False)

'''
# ==========================================================================================
#                                 PREAMBLE (module load)
# ==========================================================================================
# Import of the Python Standard Library modules required for the analysis
import glob, os, subprocess, sys       # To act on the operative system
import json                            # To read and write JSON files
import matplotlib as mpl               # For plotting
import matplotlib.pyplot as plt        # For plotting
import numpy as np                     # For numerical analysis
import pandas as pd 				   # For reading and writing csv files
import warnings                        # To deactivate the warnings
import time                            # For script timing
import logging						   # For logging purposes

# Import of the astropy/gammapy/fermipy (& fermitools) modules necessary for astrophysics
from astropy.io import fits                # I/O of .fits files
from astropy.stats import bayesian_blocks  # Bayesian Block segmentation algorithm

# Import of personal functions from custom modules
sys.path.append("../../Functions")
from MyFunctions import wmean, truehisto1D, histoplotter1D
from AstroFunctions import nu_estimator, tot_intervals
from AstroFunctions import gtselect, gtmktime, gtbindef, gtbin, gtexposure

# Set the warnings of a few functions
np.seterr(divide='ignore', invalid='ignore') 					# To avoid 0/0 errors
mpl.rcParams['backend'] = 'agg'                                 # Non-interactive backend for plotting
warnings.filterwarnings("ignore", category=DeprecationWarning)  # To avoid deprecation warnings

# =========================================================================================
#                                      COMMON DEFINITIONS                                      
# =========================================================================================
# Define the dimension for the fonts in the plots, the figures dimensions and so on
textfont = 20         # Fontsize for labels and legends
dimfig = (12,7)       # Figure dimensions (A4-like)
dimfigbig = (16,12)   # Figure dimensions (a bit larger than A4-like)
dimfiglong = (16,25)  # Figure dimensions (a bit larger and much longer than A4-like)
filetype = '.pdf'     # File format to save the pictures
filedpi = 520         # Resolution used when saving pictures in .png/.jpeg
nptsfit = 10000       # Number of points used in plotting the fits

# ==========================================================================================
#                                  STATUS FILE DATA
# ==========================================================================================
# Open the status file
statusfile = '../statusfile.json'
with open(statusfile) as json_file:
	statusdata = json.load(json_file)

# Extract all the relevant variables from the file
datadir = statusdata['datadir']
imgdir = statusdata['imgdir']
tempdir = statusdata['tempdir']
dataSCdir = statusdata['dataSCdir']
dataPHdir = statusdata['dataPHdir']
catalog_file_4FGL = statusdata['catalog_file_4FGL']
catalog_file_3FHL = statusdata['catalog_file_3FHL']

# Define the name for the subfolders (in imgdir and tempdir) where we will save
# the temporary images and files
pretempdir = '02_HEPA'
imgdir = os.path.join(imgdir,pretempdir)
tempdir = os.path.join(tempdir,pretempdir)

# Setting of the logging module
logfile = "HEPA_debug.log"
logging.basicConfig(format='[%(asctime)s] %(message)s',level=logging.INFO,
					handlers=[logging.FileHandler(logfile,mode='a'),
				        	  logging.StreamHandler()])

logging.info("*"*75)
logging.info("Beginning execution of trigger_lv1.py ...")
logging.info(f"")

# ==========================================================================================
#                                 CONFIG FILE DATA
# ==========================================================================================
# Open the configuration file made for the 1st level trigger and extract the variables
# necessary for the analysis
configfile = os.path.join(tempdir,f'configfile_trignum_1.json')
with open(configfile) as json_file:
	configdata = json.load(json_file)

# Extract all the relevant variables from the file
wstart               = configdata['wstart']                # Coupled to tstart/tstop
wstop                = configdata['wstop']                 # Coupled to tstart/tstop
tstart               = configdata['tstart']                # gtselect
tstop                = configdata['tstop']                 # gtselect 
ROI_ra               = configdata['ROI_ra']                # gtselect
ROI_dec              = configdata['ROI_dec']               # gtselect
ROI_rad              = configdata['ROI_rad']               # gtselect
Emin                 = configdata['Emin']                  # gtselect
Emax                 = configdata['Emax']                  # gtselect
zmax                 = configdata['zmax']                  # gtselect
evclass              = configdata['evclass']               # gtselect
evtype               = configdata['evtype']                # gtselect
roicut               = configdata['roicut']                # gtmktime
SUNsepmin            = configdata['SUNsepmin']             # gtmktime
filtercond           = configdata['filtercond']            # gtmktime
bindef_bintype       = configdata['bindef_bintype']        # gtbindef
bindef_tbinalg       = configdata['bindef_tbinalg']        # gtbin
bindef_algorithm     = configdata['bindef_algorithm']      # gtbin
irfs                 = configdata['irfs']                  # gtexposure
srcmdl               = configdata['srcmdl']                # gtexposure
specin               = configdata['specin']                # gtexposure
chatter              = configdata['chatter']               # All fermitools
gtmode               = configdata['gtmode']                # All fermitools
FDnum                = configdata['FDnum']                 # 1st level trigger
FD_errorscale        = configdata['FD_errorscale']         # 1st level trigger
FD_errortype 		 = configdata['FD_errortype']		   # 1st level trigger
FD_signiftype 		 = configdata['FD_signiftype']		   # 1st level trigger
BayesOffset          = configdata['BayesOffset']           # 1st level trigger
start_p0             = configdata['start_p0']              # 1st level trigger
start_ncp            = configdata['start_ncp']             # 1st level trigger
timestep             = configdata['timestep']              # 1st level trigger
mergedist            = configdata['mergedist']             # 1st level trigger
trigger1_thrRAW      = configdata['trigger1_thrRAW']       # 1st level trigger
trigger1_thrLC       = configdata['trigger1_thrLC']        # 1st level trigger
ifLoadAverageRates   = configdata['ifLoadAverageRates']    # 1st level trigger
pathLoadAverageRates = configdata['pathLoadAverageRates']  # 1st level trigger
databasedtypes       = configdata['databasedtypes']        # 1st level trigger
sourcename           = configdata['sourcename']            # File management
PHlistname           = configdata['PHlistname']            # File management
SClistname           = configdata['SClistname']            # File management
SCfilename           = configdata['SCfilename']            # File management
imgsubdir            = configdata['imgsubdir']             # File management
imgdir_www           = configdata['imgdir_www']            # File management

# Correct the "databasedtypes" variable to switch from strings to dtypes...
for key in databasedtypes.keys():
	databasedtypes[key] = eval(databasedtypes[key])

# ==========================================================================================
#                                 INPUT DATA
# ==========================================================================================
# Read from command line whether the plots are to be saved as pictures or not
try:
	ifplot = (sys.argv[1] == 'True')
except:
	ifplot = True
	logging.info(f"Input problem --- Defaulting variable 'ifplot' to '{ifplot}'")
	logging.info(f"")

# Read if it is necessary to force ifLoadAverageRates to be "False" (and thus we must compute
# the background rates and save them)
try:
	ifCalculatingBKG = (sys.argv[2] == 'True')
except:
	ifCalculatingBKG = False
	logging.info(f"Input problem --- Defaulting variable 'ifCalculatingBKG' to '{ifCalculatingBKG}'")
	logging.info(f"")

if ifCalculatingBKG:
	ifLoadAverageRates = False

# ==========================================================================================
#                                 TEMPORARY FILE NAMES
# ==========================================================================================
# Define the names of the .fits, .txt, .xml and .npz files which will be produced in the execution
data_PH_after_gtselect  = os.path.join(tempdir,sourcename,f"PH_after_gtselect.fits")
data_PH_after_gtmktime  = os.path.join(tempdir,sourcename,f"PH_after_gtmktime.fits")
data_PH_after_gtbin     = os.path.join(tempdir,sourcename,f"PH_after_gtbin.fits")
data_PH_before_timebins = os.path.join(tempdir,sourcename,f"PH_before_timebins.txt")
data_PH_after_timebins  = os.path.join(tempdir,sourcename,f"PH_after_timebins.fits")
clusterfile = os.path.join(tempdir,sourcename,f"clusters.npz")

# ==========================================================================================
#                                   DATA EXTRACTION
# ==========================================================================================
# Printouts
logging.info(f"First level trigger analysis initiated for source {sourcename}...")
logging.info(f"")

# Call gtselect to extract the data in the given ROI, energy band and temporal window
ifDoAnyways = True
if ifDoAnyways:
	logging.info(f"Calling gtselect...")
	gtselect(infile=PHlistname,outfile=data_PH_after_gtselect,
			 ROI_ra=ROI_ra,ROI_dec=ROI_dec,ROI_rad=ROI_rad,Emin=Emin,Emax=Emax,zmax=zmax,
			 tstart=tstart,tstop=tstop,evclass=evclass,evtype=evtype,chatter=chatter,gtmode=gtmode)
	logging.info(f"Done!")
	logging.info(f"")
else:
	logging.info(f"Skipping gtselect...")
	logging.info(f"")

# Call gtmaketime to adjust the GTIs following the chosen ROI and other conditions
ifDoAnyways = True
if ifDoAnyways:
	logging.info(f"Calling gtmktime...")
	gtmktime(infile=data_PH_after_gtselect,outfile=data_PH_after_gtmktime,SCfile=SCfilename,
			 ROI_ra=ROI_ra,ROI_dec=ROI_dec,roicut=roicut,SUNsep=SUNsepmin,
	         filtercond=filtercond,chatter=chatter,gtmode=gtmode)
	logging.info(f"Done!")
	logging.info(f"")
else:
	logging.info(f"Skipping gtmktime...")
	logging.info(f"")

# ==========================================================================================
#                          FD TIMING AND BAYESIAN BLOCK SEGMENTATION
# ==========================================================================================
# Open the file produced with gtmktime and extract the data times and energies
with fits.open(data_PH_after_gtmktime) as datafile:
    ind = 1
    data_energy = np.array(datafile[ind].data.field("ENERGY"),dtype=np.float64) / 1000 # scale to GeV
    data_times = np.array(datafile[ind].data.field("TIME"),dtype=np.float64)

# Compute the number of available events and verify that it is not zero.
nevents = np.shape(data_energy)[0]

if (nevents <= 2):
	# Rare case: there are no photon events in the desired time and space window.
	# What to do? We should directly exit the script, because, without events, several of
	# the commands in the remaining part of the script would immediately fail and throw errors.
	# However, for compatibility with HEPA_singlesource, we should still save an output
	# file with no flares in it.
	cluster_times = np.zeros((0,2),dtype=np.int64)
	emptyarr = np.zeros((1,))
	np.savez_compressed(clusterfile, cluster_times = cluster_times,
                                 FD_times = emptyarr, FD_rates = emptyarr, av_rate = emptyarr, std_rate = emptyarr,
                                 LC_time = emptyarr, LC_rate = emptyarr, LC_av_rate = emptyarr)
	logging.warning(f"Less than 2 photon events detected: aborting the script!")
	sys.exit(1)

# Compute the Finite Difference between the arrival times
logging.info(f"Beginning the FD calculation...")

FD_times = data_times[FDnum:]
FD_rates = (FDnum+1) / (data_times[FDnum:] - data_times[:-FDnum])
if FD_errortype == 'proportional':
	FD_errors = FD_errorscale*FD_rates
elif FD_errortype == 'mean':
	FD_errors = np.mean(FD_rates)/2
elif (FD_errortype is None) | (FD_errortype == 'None'):
	FD_errors = None

# Compute the average rate over the entire analyzed time or load it from an external file
ntime = (data_times[-1] - data_times[0])

if ifLoadAverageRates:
	# Load the file
	try:
		av_source = pd.read_csv(pathLoadAverageRates,header=0,sep=';',index_col=0,dtype=databasedtypes)
		av_rate = np.float64(av_source.loc[sourcename,'Av_FD'] )
		std_rate = np.float64(av_source.loc[sourcename,'Std_FD'] )

		# Check if the average rate is null, or divergent. In such cases, recompute it.
		if (np.isnan(av_rate)) | (np.isinf(av_rate)) | (av_rate == 0):
			av_rate = np.mean(FD_rates)
			std_rate = np.std(FD_rates)
			logging.info(f"There was a problem in loading data from {pathLoadAverageRates}. Defaulting the background rates to the measured averages...")
	except:
		av_rate = np.mean(FD_rates)
		std_rate = np.std(FD_rates)
		logging.info(f"There was a problem in loading data from {pathLoadAverageRates}. Defaulting the background rates to the measured averages...")
else:
	av_rate = np.mean(FD_rates)
	std_rate = np.std(FD_rates)
	
logging.info(f"{nevents} photon events were detected in {ntime:.2e} s")
logging.info(f"Average rate (FD): {av_rate:.2e} +/- {std_rate:.2e} events/s")

# Compute the significance of the observation of the arrival times
if (FD_signiftype == 'geom_increase'):
	FD_significance = FD_rates/av_rate

elif (FD_signiftype == 'cubed_difference'):
	FD_significance = ((FD_rates - av_rate)**3) / (av_rate**3)

elif (FD_signiftype == 'sigma_distance'):
	FD_significance = (FD_rates - av_rate) / std_rate

# Compute the Bayesian Block segmentation of the significance of the FD times
# and check that the edges contain all the data points
BayesEdges = bayesian_blocks(FD_times,FD_significance,FD_errors,"measures",p0 = start_p0,ncp_prior=start_ncp)

if BayesEdges[0] >= data_times[0]:
	# There are data_times before the first bayesian edge: correct it!
	BayesEdges[0] = data_times[0] - BayesOffset

if BayesEdges[-1] <= data_times[-1]:
	# There are data_times after the last bayesian edge: correct it!
	BayesEdges[-1] = data_times[-1] + BayesOffset

logging.info(f"Number of Bayesian Blocks used to segmentate the FD curve: {np.shape(BayesEdges)[0]-1}")
logging.info(f"")

# Write down the Basyesian Edges in a text file
nBayesBins = len(BayesEdges)-1
with open(data_PH_before_timebins,"w") as f:
    for i in range(nBayesBins):
        f.write(f"{BayesEdges[i]:.3f} {BayesEdges[i+1]:.3f}\n")

# ==========================================================================================
#                                    LIGHT CURVE ANALYSIS
# ==========================================================================================
# Create the binning for the Light Curve using gtbindef and then the Light Curve using gtbin
ifDoAnyways = True
if ifDoAnyways:
	# Create the Light Curve
	logging.info(f"Calling gtbindef, gtbin and gtexposure...")
	gtbindef(binfile=data_PH_before_timebins,outfile=data_PH_after_timebins,bintype=bindef_bintype,
			 chatter=chatter,gtmode=gtmode)

	gtbin(infile=data_PH_after_gtmktime,outfile=data_PH_after_gtbin,SCfile=SCfilename,
		  tstart=tstart,tstop=tstop,tbinfile=data_PH_after_timebins,algorithm=bindef_algorithm,
		  tbinalg=bindef_tbinalg,chatter=chatter,gtmode=gtmode)

	# Adjust the Light Curve to account for the LAT exposure
	gtexposure(infile=data_PH_after_gtbin,SCfile=SCfilename,irfs=irfs,scrmdl=srcmdl,specin=specin,
			   chatter=chatter,gtmode=gtmode)
	logging.info(f"Done!")
	logging.info(f"")
else:
	logging.info(f"Skipping gtbindef, gtbin and gtexposure...")
	logging.info(f"")

# Open the Light Curve file and extract times, counts and exposures to compute the rate vs MET
with fits.open(data_PH_after_gtbin) as hdul:
    ind = 1
    LC_time = np.array(hdul[ind].data.field('TIME'),dtype=np.float64)
    LC_counts = np.array(hdul[ind].data.field('COUNTS'),dtype=np.float64)
    LC_exposure = np.array(hdul[ind].data.field('EXPOSURE'),dtype=np.float64)

LC_rate = LC_counts/LC_exposure
LC_rate_err = np.sqrt(LC_counts)/LC_exposure
LC_weights = 1/LC_rate_err**2

# Compute the average flux from the Light Curve and use it to compute the Light Curve
# significance (in a geometrical sense, as above). Note that the average flux can also
# be loaded from an external file (as above)
good_inds = (np.isnan(LC_weights) == False) & (np.isinf(LC_weights) == False)

if ifLoadAverageRates:
	# Load the file
	try:
		LC_av_source = pd.read_csv(pathLoadAverageRates,header=0,sep=';',index_col=0,dtype=databasedtypes)
		LC_av_rate = np.float64(LC_av_source.loc[sourcename,'Av_LC'] )

		# Check if the average rate is null, or divergent. In such cases, recompute it.
		if (np.isnan(LC_av_rate)) | (np.isinf(LC_av_rate)) | (LC_av_rate == 0):
			LC_av_rate, _ = wmean(LC_rate[good_inds],LC_weights[good_inds])
			logging.info(f"There was a problem in loading data from {pathLoadAverageRates}. Defaulting the background rates to the measured averages...")
	except:
		LC_av_rate, _ = wmean(LC_rate[good_inds],LC_weights[good_inds])
		logging.info(f"There was a problem in loading data from {pathLoadAverageRates}. Defaulting the background rates to the measured averages...")
else:
	LC_av_rate, _ = wmean(LC_rate[good_inds],LC_weights[good_inds])
	
logging.info(f"Average rate (LC, measured): {LC_av_rate:.2e} events * cm(-2) * s(-1)")	
logging.info(f"")
LC_significance = LC_rate/LC_av_rate

# ==========================================================================================
#                                        AVERAGE SAVINGS
# ==========================================================================================
# In case the average FD and LC rates were computed manually, they may be saved to the external
# source given in input. This will allow to use these values in future calls. This should
# NOT be done in the case the rates loaded from the external source, because in such case we
# already have a reliable estimate.
if (not ifLoadAverageRates):
	logging.info(f"Saving the average FD and LC rates in {pathLoadAverageRates} for future usage...")
	df = pd.read_csv(pathLoadAverageRates,header=0,sep=';',index_col=0,dtype=databasedtypes)
	df.loc[sourcename,'Av_FD'] = av_rate
	df.loc[sourcename,'Std_FD'] = std_rate
	df.loc[sourcename,'Av_LC'] = LC_av_rate
	df.to_csv(pathLoadAverageRates,sep=';',float_format='%5.15f')
	logging.info(f"Saving completed!")
	logging.info(f"")

# ==========================================================================================
#                                    TRIGGER DEFINITION
# ==========================================================================================
# Compute the points over threshold in the FD curve and the Light Curve.
# In this case we obtaining as output of the function "tot_intervals" a list where each 
# element is a list of indices/x values/y values of the points above threshold. Thus,
# we are effectively performing a clustering by continuity.
_, FD_clu_x, FD_clu_y = tot_intervals(FD_times,FD_significance,trigger1_thrRAW,mergedist=None)
_, LC_clu_x, LC_clu_y = tot_intervals(LC_time,LC_significance,trigger1_thrLC,mergedist=None)

# Create a time binning ranging from tstart to stop at fixed binning. This will be used
# to determine the intervals to analyze with the 2nd level trigger. Then we create
# a vector with the same dimension, containing 0 if the corresponding bin should be skipped
# and 1 otherwise.
time_binning = np.arange(tstart,tstop+timestep,timestep)
iftriggered = np.zeros_like(time_binning,dtype=np.bool)

for i,el in enumerate(FD_clu_x + LC_clu_x):
	# Note that "FD_clu_x" and "LC_clu_x" are lists, so they can be summed in iteration.
	# In this case, we're iterating over every cluster of contiguous points above threshold.
	# Then we search for the closest binning edges, to set the corresponding interval in
	# "iftriggered" to 1.
    ext_inf = timestep*((el[0] // timestep))
    ext_sup = timestep*((el[-1] // timestep) + 1)
    cond = (time_binning >= ext_inf) & (time_binning <= ext_sup)
    iftriggered[cond] = 1

# Now we can perform a clustering on the intervals: we merge the ones whose edges
# (upper vs lower) are distant less than a given value. Note that we use a threshold of
# 0.5 in the clustering, since the y values are necessairly 0 or 1.
allclu_inds, allclu_x, _ = tot_intervals(time_binning,iftriggered,thr=0.5,mergedist=mergedist)

# Now we can build and save a np.array containing the start and stop of each time interval
# to be analyzed
cluster_times = np.zeros((len(allclu_x),2),dtype=np.int64)
for i,el in enumerate(allclu_x):
    cluster_times[i,0] = el[0]-timestep
    cluster_times[i,1] = el[-1]+timestep

np.savez_compressed(clusterfile, cluster_times = cluster_times,
                                 FD_times = FD_times,
                                 FD_rates = FD_rates,
                                 av_rate = av_rate,
                                 std_rate = std_rate,
                                 LC_time = LC_time,
                                 LC_rate = LC_rate,
                                 LC_av_rate = LC_av_rate)

logging.info(f"Time clusters to be analyzed in the 2nd level trigger:")
logging.info(cluster_times)
logging.info(f"The times have been correctly written in the file stored at the following path:")
logging.info(clusterfile)
logging.info(f"")

'''
*************************************************************************************************
*************************************************************************************************
*************************************************************************************************
'''

# ==========================================================================================
#                                        PLOTTING / 1a
# ==========================================================================================
logging.info(f"Now it's plotting time...")

# We want to show in a single picture: how the FD change as a function of the MET,
# what is the mission-long average and what are the corresponding values of the Bayesian Blocks
BayesBinCenters = np.zeros((nBayesBins,))
BayesRates = np.zeros_like(BayesEdges)

for i in range(nBayesBins):
    # Extract the i-th bin
    BayesBinCenters[i] = (BayesEdges[i] + BayesEdges[i+1])/2
    cond = (FD_times >= BayesEdges[i]) & (FD_times <= BayesEdges[i+1])

    # Compute the average rate in the bin
    BayesRates[i+1] = np.mean(FD_rates[cond])
    
# Set the first points to the same value of the second (they consitute TOGETHER the first
# bin, since they are EDGES)
BayesRates[0] = BayesRates[1]

# Now plotting, if wanted
av_curve = FD_times*0+av_rate
if ifplot:
	# Plot of the FD curve
	figname = os.path.join(imgsubdir,f'trig1a_ratevsMETwithBB' + filetype) 
	fig, ax = plt.subplots(figsize = dimfig)
	ax.plot(FD_times,FD_rates,color='mediumblue',linestyle='',marker='.',markersize=10,label=f"Data")
	ax.plot(FD_times,av_curve,color='cyan',linewidth=3,linestyle='--',label='Mission average')
	ax.fill_between(FD_times,av_curve-std_rate,av_curve+std_rate,label=r'Mission average $\pm 1 \sigma$',
					color='gold',alpha=0.1,linewidth=0.5,linestyle='--')
	ax.plot(BayesEdges,BayesRates,color='darkorange',linestyle='-',marker='',linewidth=1.5,
				drawstyle='steps-pre',label='Bayesian Blocks')

	# Plot of the threshold for detection
	if (FD_signiftype == 'geom_increase'):
		rate_threshold = av_rate*trigger1_thrRAW
	elif (FD_signiftype == 'cubed_difference'):
		rate_threshold = av_rate*(1+trigger1_thrRAW)**(1/3)
	elif (FD_signiftype == 'sigma_distance'):
		rate_threshold = av_rate + trigger1_thrRAW * std_rate

	thr_curve = FD_times*0+rate_threshold
	ax.plot(FD_times,thr_curve,color='red',linewidth=3,linestyle='--',label='Threshold')
		
	# Finish graphics
	ax.set_xlabel("Time [MET]",fontsize=textfont)
	ax.set_ylabel("FD-estimated rate [1/s]",fontsize=textfont)
	ax.set_title(sourcename,fontsize=1.5*textfont)
	ax.legend(loc='upper left',fontsize=0.75*textfont,framealpha=0.8)
	ax.grid(linewidth=0.5)
	ax.tick_params(labelsize = textfont)
	fig.set_tight_layout('tight')
	fig.savefig(figname,dpi=filedpi)
	plt.close(fig)

# ==========================================================================================
#                                        PLOTTING / 1b
# ==========================================================================================
# Now plotting, if required
if ifplot:
	figname = os.path.join(imgsubdir,f'trig1a_signifvsMET' + filetype) 

	fig, ax = plt.subplots(figsize = dimfig)
	ax.plot(FD_times,FD_significance,color='mediumblue',linestyle='',marker='.',markersize=10,label=f"Data")
	ax.plot(FD_times,FD_significance*0+trigger1_thrRAW,color='red',linewidth=3,linestyle='--',label='Threshold')
	ax.set_xlabel("Time [MET]",fontsize=textfont)
	ax.set_ylabel("FD significance",fontsize=textfont)
	ax.set_title(sourcename,fontsize=1.5*textfont)
	ax.legend(loc='lower left',fontsize=0.75*textfont,framealpha=0.8)
	ax.grid(linewidth=0.5)
	ax.tick_params(labelsize = textfont)
	fig.set_tight_layout('tight')
	fig.savefig(figname,dpi=filedpi)
	plt.close(fig)

# ==========================================================================================
#                                        PLOTTING / 2
# ==========================================================================================
# We want now to show in a single picture: the recomputed Light Curve and the trigger threshold.
# Do the plotting only if required
LC_rate_BBplot = np.hstack((LC_rate[0],LC_rate))

if ifplot:
	figname = os.path.join(imgsubdir,f'trig1b_LCwithBB' + filetype)

	fig, ax = plt.subplots(figsize = dimfig)
	ax.errorbar(LC_time,LC_rate,LC_rate_err,color='mediumblue',marker='.', markersize=5,
					linestyle='',label = 'Data')
	ax.plot(BayesEdges,BayesEdges*0+LC_av_rate,color='cyan',linestyle='--',linewidth = 3,label = 'Mission average')
	ax.plot(FD_times,FD_times*0+LC_av_rate*trigger1_thrLC,color='red',linewidth=3,linestyle='--',label='Threshold')
	ax.plot(BayesEdges,LC_rate_BBplot,color='darkorange',linestyle='-',marker='',linewidth=1.5,
				drawstyle='steps-pre',label='Bayesian Blocks')
	ax.set_xlabel('MET [s]',fontsize=textfont)
	ax.set_ylabel(r'Flux [cm$^{-2}$ s$^{-1}$]',fontsize=textfont)
	ax.set_yscale('log')
	ax.set_title(sourcename,fontsize=1.5*textfont)
	ax.legend(loc='center left',fontsize=textfont,framealpha=0.75)
	ax.grid(linewidth=0.5)
	ax.tick_params(labelsize = textfont)
	fig.set_tight_layout('tight')
	fig.savefig(figname,dpi=filedpi)
	plt.close(fig)

# ==========================================================================================
#                                        PLOTTING / 3
# ==========================================================================================
# Create a vector equal to 0 whenever a point is not over the 1st level trigger threshold,
# and 1 otherwise.
iftriggeredALL = np.zeros_like(time_binning,dtype=np.bool)

for i,el in enumerate(allclu_x):
    ext_inf = timestep*((el[0] // timestep))
    ext_sup = timestep*((el[-1] // timestep) + 1)
    cond = (time_binning >= ext_inf) & (time_binning <= ext_sup)
    iftriggeredALL[cond] = 1

# Show the vector, if required
if ifplot:
	figname = os.path.join(imgsubdir,f'trig1c_triggercurve' + filetype)
	fig, ax = plt.subplots(figsize=dimfig)
	ax.plot(time_binning[iftriggeredALL>0],iftriggeredALL[iftriggeredALL>0],
	         color='red',linestyle='',marker='*',markersize=10,
	         label='Over threshold')
	ax.plot(time_binning[iftriggeredALL<1],iftriggeredALL[iftriggeredALL<1],
	         color='mediumblue',linestyle='--',marker='',
	         label='Under threshold')
	ax.set_title(sourcename,fontsize=1.5*textfont)
	ax.legend(loc='center left',framealpha=1,fontsize=textfont)
	ax.set_xlabel('MET [s]',fontsize = textfont)
	ax.set_ylabel('Point value wrt threshold',fontsize = textfont)
	ax.set_ylim([-0.1,1.1])
	ax.grid(linewidth=0.5)
	ax.tick_params(labelsize = textfont)
	fig.set_tight_layout('tight')
	fig.savefig(figname,dpi=filedpi)
	plt.close(fig)

# ==========================================================================================
#                                        PLOTTING / 4
# ==========================================================================================
# Show some histograms and plots regarding the energy and FD values recorded
if ifplot:
	# Define the bin numbers and the FD vector
	nbins = 150
	figname = os.path.join(imgsubdir,f'trig1d_energytimedata' + filetype)

	# Histo 1D: energy of the photons
	fig = plt.figure(figsize=dimfigbig)
	histo, bins = truehisto1D(data_energy,nbins)
	ax = fig.add_subplot(2,2,1)
	histoplotter1D(ax,bins,histo,'Energy [GeV]','Counts','Data','best',textfont)

	# Histo 1D: FD of the photons
	FDvalues = data_times[FDnum:] - data_times[:-FDnum]
	histo, bins = truehisto1D(FDvalues,nbins)
	ax = fig.add_subplot(2,2,2)
	histoplotter1D(ax,bins,histo,'FD of the arrival times [MET]','Counts','Data','best',textfont)
	
	# Scatterplot 1D: time vs energy
	ax = fig.add_subplot(2,2,3)
	ax.plot(data_times,data_energy,color='mediumblue',linestyle='',marker='.',markersize=10)
	ax.set_xlabel("Time [MET]",fontsize=textfont)
	ax.set_ylabel("Energy [GeV]",fontsize=textfont)
	ax.grid(linewidth=0.5)
	ax.tick_params(labelsize = textfont)

	# Scatterplot 2D: time vs FD
	ax = fig.add_subplot(2,2,4)
	ax.plot(FD_times,FDvalues,color='mediumblue',linestyle='',marker='.',markersize=10)
	ax.set_xlabel("Time [MET]",fontsize=textfont)
	ax.set_ylabel("FD of the arrival times [MET]",fontsize=textfont)
	ax.grid(linewidth=0.5)
	ax.tick_params(labelsize = textfont)

	# Refine the graphics
	fig.suptitle(sourcename,fontsize=1.5*textfont)
	fig.set_tight_layout('tight')
	fig.savefig(figname,dpi=filedpi)
	plt.close(fig)

logging.info(f"Plotting done!")
logging.info(f"")
logging.warning(f"Completed the execution of trigger_lv1.py!")
logging.info("*"*75)