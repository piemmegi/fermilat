'''
# ==========================================================================================
#                                       HEADER
# ==========================================================================================
The aim of this script is to perform an ordered call to the HEPA pipeline for a single source,
whose parameters are given in an external .csv file.

-------------------------------------------------------------------------------------------
Call syntax:
  	python3 HEPA_singlepoint.py sourcename

Call arguments:
	- sourcename		str, the name of the source to be analyzed (default value: "GRB_180720B")

'''
# ==========================================================================================
#                                 PREAMBLE (module load)
# ==========================================================================================
# Import of the Python Standard Library modules required for the analysis
import glob, os, subprocess, sys       # To act on the operative system
import json                            # To read and write JSON files
import matplotlib as mpl               # For plotting
import matplotlib.pyplot as plt        # For plotting
import numpy as np                     # For numerical analysis
import pandas as pd 				   # For reading and writing csv files
import warnings                        # To deactivate the warnings
import time                            # For script timing
import logging						   # For logging purposes

# Import of personal functions from custom modules
sys.path.append("../../Functions")
from AstroFunctions import J2000formatter

# Set the warnings of a few functions
np.seterr(divide='ignore', invalid='ignore') 					# To avoid 0/0 errors
mpl.rcParams['backend'] = 'agg'                                 # Non-interactive backend for plotting
warnings.filterwarnings("ignore", category=DeprecationWarning)  # To avoid deprecation warnings

# =========================================================================================
#                                      COMMON DEFINITIONS                                      
# =========================================================================================
# Define the dimension for the fonts in the plots, the figures dimensions and so on
textfont = 20         # Fontsize for labels and legends
dimfig = (12,7)       # Figure dimensions (A4-like)
dimfigbig = (16,12)   # Figure dimensions (a bit larger than A4-like)
dimfiglong = (16,25)  # Figure dimensions (a bit larger and much longer than A4-like)
filetype = '.pdf'     # File format to save the pictures
filedpi = 520         # Resolution used when saving pictures in .png/.jpeg
nptsfit = 10000       # Number of points used in plotting the fits

# ==========================================================================================
#                                  STATUS FILE DATA
# ==========================================================================================
# Open the status file
statusfile = '../statusfile.json'
with open(statusfile) as json_file:
	statusdata = json.load(json_file)

# Extract all the relevant variables from the file
datadir = statusdata['datadir']
imgdir = statusdata['imgdir']
tempdir = statusdata['tempdir']
dataSCdir = statusdata['dataSCdir']
dataPHdir = statusdata['dataPHdir']
catalog_file_4FGL = statusdata['catalog_file_4FGL']
catalog_file_3FHL = statusdata['catalog_file_3FHL']

# Define the name for the subfolders (in imgdir and tempdir) where we will save
# the temporary images and files
pretempdir = '02_HEPA'
imgdir = os.path.join(imgdir,pretempdir)
tempdir = os.path.join(tempdir,pretempdir)

# Setting of the logging module
logfile = "HEPA_debug.log"
if os.path.exists(logfile):
	os.remove(logfile)

logging.basicConfig(format='[%(asctime)s] %(message)s',level=logging.WARNING,
					handlers=[logging.FileHandler(logfile,mode='a'),
				        	  logging.StreamHandler()])

logging.info("*"*75)
logging.info("Beginning execution of HEPA_singlesource.py ...")
logging.info(f"")

# ==========================================================================================
#                                 INPUT DATA
# ==========================================================================================
# Parse the user input containing the name of the source to be analyzed
logging.info(f"")
try:
	sourcename = sys.argv[1]
except:
	sourcename = 'GRB_180720B'
	logging.info(f"Input problem --- Defaulting variable 'sourcename' to '{sourcename}'")
	logging.info(f"")
	

# Open the csv file containing the source database and extract the relevant parameters
# Note that I'm passing the dtype structure of the file in a weird way because this is the SAME CODE
# used for in the trigger_lv1.py script. So, if it does not work here, it won't work there.
sourcedatabase = 'SourceDatabase.csv'
databasedtypes = {"Emin_GeV"   : 'np.float64', # Note: these are string, we'll need to convert them to dtypes
	              "wstart"     : 'np.int64',   #  before any actual use. This can be done easily with eval(value)!
	              "wstop"      : 'np.int64',
	              "ROI_ra_deg" : 'np.float64',
	              "ROI_dec_deg": 'np.float64',
	              "ROI_rad_deg": 'np.float64',
	              "Status"     : 'str',
	              "Av_FD"      : 'np.float64',
	              "Std_FD"	   : 'np.float64',
	              "Av_LC"      : 'np.float64'}
for key in databasedtypes.keys():
	databasedtypes[key] = eval(databasedtypes[key])

# Now read and extract
logging.info(f"Reading parameters for the {sourcename} source from {sourcedatabase}")
df = pd.read_csv(sourcedatabase,header=0,sep=';',index_col=0,dtype=databasedtypes)
Emin       = df.loc[sourcename,'Emin_GeV']
wstart     = df.loc[sourcename,'wstart']
wstop      = df.loc[sourcename,'wstop']
ROI_ra     = df.loc[sourcename,'ROI_ra_deg']
ROI_dec    = df.loc[sourcename,'ROI_dec_deg']
ROI_rad    = df.loc[sourcename,'ROI_rad_deg']

# Call the script for the HEPA analysis
subprocess.run(["python3","HEPA_singlepoint.py",f"{Emin}",f"{wstart}",f"{wstop}",
									 			 f"{ROI_ra}",f"{ROI_dec}",f"{ROI_rad}",
									 			 f"{sourcename}",f"{sourcedatabase}"])

logging.info(f"")
logging.info(f"Completed the execution of HEPA_singlesource.py!")
logging.info("*"*75)