'''
# ==========================================================================================
#                                          HEADER
# ==========================================================================================
The aim of this script is to produce two .list files, each one containing a list of weekly
photon/spacecraft files to be analyzed. The script accepts as argument the first and last
weeks to be analyzed, and does also every week in-between.

-------------------------------------------------------------------------------------------
Call syntax:
    python3 utils_ListPreparator.py wBEGIN wEND PHoutname SCoutname

Call arguments:
    - wBEGIN            int, the first week to be analyzed (defualt value: the first week
                          available in the data files)
    - wEND              int, the last week to be analyed (defualt value: the last week
                          available in the data files)
    - PHoutname         str, the path where to save the .list file of the photon events
                          (default value: the tempdir)
    - SCoutname         str, the path where to save the .list file of the spacecraft events
                          (default value: the tempdir)
'''
# ==========================================================================================
#                                 PREAMBLE (module load)
# ==========================================================================================
# Import of the Python Standard Library modules required for the analysis
import glob, os, subprocess, sys       # To act on the operative system
import json                            # To read and write JSON files
import matplotlib as mpl               # For plotting
import matplotlib.pyplot as plt        # For plotting
import numpy as np                     # For numerical analysis
import pandas as pd                    # For reading and writing csv files
import warnings                        # To deactivate the warnings
import time                            # For script timing
import logging                         # For logging purposes

# ==========================================================================================
#                                  STATUS FILE DATA
# ==========================================================================================
# Open the status file
statusfile = '../statusfile.json'
with open(statusfile) as json_file:
    statusdata = json.load(json_file)

# Extract all the relevan variables from the file
datadir = statusdata['datadir']
imgdir = statusdata['imgdir']
tempdir = statusdata['tempdir']
dataSCdir = statusdata['dataSCdir']
dataPHdir = statusdata['dataPHdir']
catalog_file_4FGL = statusdata['catalog_file_4FGL']
catalog_file_3FHL = statusdata['catalog_file_3FHL']

# Define the name for the subfolders (in imgdir and tempdir) where we will save
# the temporary images and files
pretempdir = '02_HEPA'
imgdir = os.path.join(imgdir,pretempdir)
tempdir = os.path.join(tempdir,pretempdir)

# Setting of the logging module
logfile = "HEPA_debug.log"
logging.basicConfig(format='[%(asctime)s] %(message)s',level=logging.WARNING,
                    handlers=[logging.FileHandler(logfile,mode='a'),
                              logging.StreamHandler()])

logging.info("*"*75)
logging.info("Beginning execution of utils_ListPreparator.py ...")
logging.info(f"")

# ==========================================================================================
#                                        SCRIPT
# ==========================================================================================
# Define the names of the photon and spacecraft files
PHprename = 'lat_photon_weekly_w'
PHpostname = '_p305_v001.fits'
SCprename = 'lat_spacecraft_weekly_w'
SCpostname = '_p310_v001.fits'

# Define the lower and higher weeks available in the data files. Note that here we exploit
# the fact that we know how the photon file names are formatted: essentially we reconstruct
# what is in the names between "_w" and "_p", which we know it is the integer corresponding
# to the week name.
filenames = glob.glob(os.path.join(dataPHdir,f'{PHprename}*{PHpostname}'))
listnames = [int(el.split('_w')[-1].split('_p')[0]) for el in filenames]
arraynames = np.array(listnames)
wMIN = np.min(arraynames)          # Take the min and max to know the boundaries
wMAX = np.max(arraynames)          # (note that glob.glob() returns unordered results!)

# Define from the first and last week to put in the list (user input!)
# Note: if the value for the first week is smaller than the first mission week available,
# actually available, then we should default to the latter. Same discourse for the last
# week, and same discourse for undefined inputs.
try:
    wGUESS = int(sys.argv[1])
    if wGUESS < wMIN:
        wBEGIN = wMIN
    else:
        wBEGIN = wGUESS
except:
    wBEGIN = wMIN
    logging.info(f"Input problem --- Defaulting variable 'wBEGIN' to {wBEGIN} \n")
    logging.info(f"")

try:
    wGUESS = int(sys.argv[2])
    if wGUESS > wMAX:
        wEND = wMAX
    else:
        wEND = wGUESS
except:
    wEND = wMAX
    logging.info(f"Input problem --- Defaulting variable 'wEND' to {wEND} \n")
    logging.info(f"")

# Define the names for the .list files
try:
    PHoutname = sys.argv[3]
except:
    PHoutname = os.path.join(tempdir,f'PH_events_w{wBEGIN:03d}_w{wEND:03d}.list')
    logging.info(f"Input problem --- Defaulting variable 'PHoutname' to {PHoutname} \n")
    logging.info(f"")

try:
    SCoutname = sys.argv[4]
except:
    SCoutname = os.path.join(tempdir,f'SC_events_w{wBEGIN:03d}_w{wEND:03d}.list')
    logging.info(f"Input problem --- Defaulting variable 'SCoutname' to {SCoutname} \n")
    logging.info(f"")

# ==========================================================================================
#                                     OUTPUT
# ==========================================================================================
# List the files to be inserted in the files and remove the ones corresponding to broken
# weeks or files.
wFORBIDDEN = [512]
weeksToBeDone = list(range(wBEGIN,wEND+1))
for el in wFORBIDDEN:
    if el in weeksToBeDone:
        weeksToBeDone.remove(el)

# Write the list of the photon files
logging.info(f"Writing list of photon files to be analyzed (PH; from week n. {wBEGIN:03d} to week n. {wEND:03d}) ...")
with open(PHoutname,'w') as file:
    for wNUM in weeksToBeDone:
        file.write(os.path.join(dataPHdir,PHprename + f"{wNUM:03d}" + PHpostname) + '\n')

logging.info(f"The .list file was correctly written at the following path:")
logging.info(PHoutname)
logging.info(f"")

# Write the list of the spacecraft files
logging.info(f"Writing list of spacecraft files to be analyzed (SC; from week n. {wBEGIN:03d} to week n. {wEND:03d}) ...")
with open(SCoutname,'w') as file:
    for wNUM in weeksToBeDone:
        file.write(os.path.join(dataSCdir,SCprename + f"{wNUM:03d}" + SCpostname) + '\n')

logging.info(f"The .list file was correctly written at the following path:")
logging.info(SCoutname)
logging.info(f"")
logging.info(f"Completed the execution of utils_ListPreparator.py!")
logging.info("*"*75)