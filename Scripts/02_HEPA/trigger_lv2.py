'''
# ==========================================================================================
#                                       HEADER
# ==========================================================================================
The aim of this script is to perform a 2nd level trigger analysis on a given ROI, in a given
time interval and energy band. Specifically, the time window refers to the one found by the
first level trigger analysis and indicized as a certain number of flare. All the parameters
for this analysis must be set through the utils_configcreator.py script, using the option 
"trignum = 2", which is correctly done already in this script so there is no need for an
external input.

-------------------------------------------------------------------------------------------
Call syntax:
  	python3 trigger_lv2.py flarenum ifplot

Call arguments:
	- flarenum			int, the number of flare to be analyzed.
	- ifplot			bool, whether to save the plots produced at the end of the script
						  as figures on-disk, or not (default value: True).


'''
# ==========================================================================================
#                                 PREAMBLE (module load)
# ==========================================================================================
# Import of the Python Standard Library modules required for the analysis
import glob, os, subprocess, sys       # To act on the operative system
import json                            # To read and write JSON files
import matplotlib as mpl               # For plotting
import matplotlib.pyplot as plt        # For plotting
import numpy as np                     # For numerical analysis
import pandas as pd 				   # For reading and writing csv files
import warnings                        # To deactivate the warnings
import time                            # For script timing
import logging						   # For logging purposes
import scipy.stats as sp  			   # For statistical analyses

# Import of the astropy/gammapy/fermipy (& fermitools) modules necessary for astrophysics
from LATSourceModel import SourceList      # To create the XML files required by the Fermitools

# Import of personal functions from custom modules
sys.path.append("../../Functions")
from MyFunctions import truebins, histoplotter2D
from AstroFunctions import getgttsmap, tsmapclustering, TS2sigma
from AstroFunctions import gtselect, gtmktime, gtltcube, gtexpmap, gtdiffrsp, gttsmap, gtlike, gtfindsrc

# Set the warnings of a few functions
np.seterr(divide='ignore', invalid='ignore') 					# To avoid 0/0 errors
mpl.rcParams['backend'] = 'agg'                                 # Non-interactive backend for plotting
warnings.filterwarnings("ignore", category=DeprecationWarning)  # To avoid deprecation warnings

# =========================================================================================
#                                      COMMON DEFINITIONS                                      
# =========================================================================================
# Define the dimension for the fonts in the plots, the figures dimensions and so on
textfont = 20         # Fontsize for labels and legends
dimfig = (12,7)       # Figure dimensions (A4-like)
dimfigbig = (16,12)   # Figure dimensions (a bit larger than A4-like)
dimfiglong = (16,25)  # Figure dimensions (a bit larger and much longer than A4-like)
filetype = '.pdf'     # File format to save the pictures
filedpi = 520         # Resolution used when saving pictures in .png/.jpeg
nptsfit = 10000       # Number of points used in plotting the fits

# ==========================================================================================
#                                  STATUS FILE DATA
# ==========================================================================================
# Open the status file
statusfile = '../statusfile.json'
with open(statusfile) as json_file:
	statusdata = json.load(json_file)

# Extract all the relevant variables from the file
datadir = statusdata['datadir']
imgdir = statusdata['imgdir']
tempdir = statusdata['tempdir']
dataSCdir = statusdata['dataSCdir']
dataPHdir = statusdata['dataPHdir']
catalog_file_4FGL = statusdata['catalog_file_4FGL']
catalog_file_3FHL = statusdata['catalog_file_3FHL']

# Define the name for the subfolders (in imgdir and tempdir) where we will save
# the temporary images and files
pretempdir = '02_HEPA'
imgdir = os.path.join(imgdir,pretempdir)
tempdir = os.path.join(tempdir,pretempdir)

# Setting of the logging module
logfile = "HEPA_debug.log"
logging.basicConfig(format='[%(asctime)s] %(message)s',level=logging.WARNING,
					handlers=[logging.FileHandler(logfile,mode='a'),
				        	  logging.StreamHandler()])
logging.info("*"*75)
logging.info("Beginning execution of trigger_lv2.py ...")
logging.info(f"")

# ==========================================================================================
#                                 CONFIG FILE DATA
# ==========================================================================================
# Open the configuration file made for the 1st level trigger and extract the variables
# necessary for the analysis
configfile = os.path.join(tempdir,f'configfile_trignum_1.json')
with open(configfile) as json_file:
	configdata = json.load(json_file)

# Extract all the relevant variables from the file.
ROI_ra                = configdata['ROI_ra']                 # gtselect
ROI_dec               = configdata['ROI_dec']                # gtselect
ROI_rad               = configdata['ROI_rad']                # gtselect
Emin                  = configdata['Emin']                   # gtselect
Emax                  = configdata['Emax']                   # gtselect
zmax                  = configdata['zmax']                   # gtselect
evclass               = configdata['evclass']                # gtselect
evtype                = configdata['evtype']                 # gtselect
roicut                = configdata['roicut']                 # gtmktime
SUNsepmin             = configdata['SUNsepmin']              # gtmktime
filtercond            = configdata['filtercond']             # gtmktime
chatter               = configdata['chatter']                # All fermitools
gtmode                = configdata['gtmode']                 # All fermitools
dcostheta             = configdata['dcostheta']              # gtltcube
nbands_energy         = configdata['nbands_energy']          # gtexpmap
extROI_rad            = configdata['extROI_rad']             # gtexpmap
binsz                 = configdata['binsz']                  # gtexpmap
expmap_evtype         = configdata['expmap_evtype']          # gtexpmap
expmap_irfs           = configdata['expmap_irfs']            # gtexpmap
xmlclobber            = configdata['xmlclobber']             # SourceModel
DRnum                 = configdata['DRnum']                  # SourceModel
free_radius           = configdata['free_radius']            # SourceModel
tsmap_statistics      = configdata['tsmap_statistics']       # gttsmap
tsmap_optimizer       = configdata['tsmap_optimizer']        # gttsmap
tsmap_coordsys        = configdata['tsmap_coordsys']         # gttsmap
tsmap_proj            = configdata['tsmap_proj']             # gttsmap
tsmap_thr             = configdata['tsmap_thr']              # gttsmap
tsmap_mode            = configdata['tsmap_mode']             # gttsmap
tsmap_ifrescaleoutput = configdata['tsmap_ifrescaleoutput']  # gttsmap
findsrc_reopt         = configdata['findsrc_reopt']          # gtfindsrc
findsrc_posacc        = configdata['findsrc_posacc']         # gtfindsrc
ifLikeWithSeededSources = configdata['ifLikeWithSeededSources'] # gtlike
flare_spectrum_model  = configdata['flare_spectrum_model']   # gtlike
gtlike_optimizer      = configdata['gtlike_optimizer']       # gtlike
gtlike_verbosity      = configdata['gtlike_verbosity']       # gtlike
pathLoadAverageRates  = configdata['pathLoadAverageRates']   # 1st level trigger
trigger2_thrTS 	      = configdata['trigger2_thrTS']		 # 2nd level trigger threshold
trigger2_thrKSNormIncrease = configdata['trigger2_thrKSNormIncrease'] # 2nd level trigger threshold
sourcename            = configdata['sourcename']             # File management
imgsubdir             = configdata['imgsubdir']              # File management
imgdir_www            = configdata['imgdir_www']             # File management

# ==========================================================================================
#                                 USER INPUTS
# ==========================================================================================
# The user should give as input the number of flare to be analyzed (among the ones seen
# in the 1st level trigger analysis)
try:
	flarenum = int(sys.argv[1])
except:
	flarenum = 0
	logging.info(f"Input problem --- Defaulting variable 'flarenum' to {flarenum}\n")

# Read from command line whether the plots are to be saved as pictures or not
try:
	ifplot = (sys.argv[2] == 'True')
except:
	ifplot = True
	logging.info(f"Input problem --- Defaulting variable 'ifplot' to '{ifplot}'")
	logging.info(f"")

# Open the file containing all the flares detected by the 1st level trigger and compute
# the tstart and tstop of the one required
clusterfile = os.path.join(tempdir,sourcename,f"clusters.npz")
with np.load(clusterfile) as f:
	cluster_times = f['cluster_times'] 
	tstart, tstop = cluster_times[flarenum,:]

# ==========================================================================================
#                       SECOND LEVEL CONFIG GENERATION
# ==========================================================================================
# We now must generate the config file to perform the analysis of the 2nd level trigger.
# Note that we will not need all the informations contained in the config file, but we MUST
# call it in order to create the list of the photon files to be analyzed, and also the
# spacecraft file.
trignum = 2
subprocess.run(['python3','utils_configcreator.py',f"{Emin}",f"{tstart}",f"{tstop}",
				f"{ROI_ra}",f"{ROI_dec}",f"{ROI_rad}",f"{trignum}",f"{sourcename}",f"{pathLoadAverageRates}"])

# Now we open the new config file and load all the remaining relevant informations from there
configfile2 = os.path.join(tempdir,f'configfile_trignum_2.json')
with open(configfile2) as json_file2:
	configdata2 = json.load(json_file2)

wstart           = configdata2['wstart']              # Coupled to tstart/tstop
wstop            = configdata2['wstop']               # Coupled to tstart/tstop
tstart           = configdata2['tstart']              # gtselect
tstop            = configdata2['tstop']               # gtselect 
numSourcesAfterLike = configdata2['numSourcesAfterLike'] # gtlike
PHlistname       = configdata2['PHlistname']          # File management
SClistname       = configdata2['SClistname']          # File management
SCfilename       = configdata2['SCfilename']          # File management

# ==========================================================================================
#                                 TEMPORARY FILE NAMES
# ==========================================================================================
# Define the names of the .fits, .txt, .xml and .npz files which will be produced in the execution
data_PH_after_gtselect 	= os.path.join(tempdir,sourcename,f"PH_flare_{flarenum:03d}_after_gtselect.fits")
data_PH_after_gtmktime 	= os.path.join(tempdir,sourcename,f"PH_flare_{flarenum:03d}_after_gtmktime.fits")
data_PH_after_gtltcube 	= os.path.join(tempdir,sourcename,f"PH_flare_{flarenum:03d}_after_gtltcube.fits")
data_PH_after_gtexpmap 	= os.path.join(tempdir,sourcename,f"PH_flare_{flarenum:03d}_after_gtexpmap.fits")
data_PH_after_gttsmap  	= os.path.join(tempdir,sourcename,f"PH_flare_{flarenum:03d}_after_gttsmap.fits")
data_xml_bkgmodel      	= os.path.join(tempdir,sourcename,f"PH_flare_{flarenum:03d}_bkg_model.xml")
data_xml_gtlike        	= os.path.join(tempdir,sourcename,f"PH_flare_{flarenum:03d}_gtlike.xml")
flarefile_npz 			= os.path.join(tempdir,sourcename,f"flare_{flarenum:03d}.npz")
flarefile_txt 			= os.path.join(tempdir,sourcename,f"flare_{flarenum:03d}.txt")

# ==========================================================================================
#                                   DATA EXTRACTION
# ==========================================================================================
# Printouts
logging.info(f"Second level trigger analysis initiated for source {sourcename}, flare {flarenum} (counting from 0) ...")
logging.info(f"")

# Call gtselect to extract the data in the given ROI, energy band and temporal window
ifDoAnyways = True
if ifDoAnyways:
	logging.info(f"Calling gtselect...")
	gtselect(infile=PHlistname,outfile=data_PH_after_gtselect,
			 ROI_ra=ROI_ra,ROI_dec=ROI_dec,ROI_rad=ROI_rad,Emin=Emin,Emax=Emax,zmax=zmax,
			 tstart=tstart,tstop=tstop,evclass=evclass,evtype=evtype,chatter=chatter,gtmode=gtmode)
	logging.info(f"Done!")
	logging.info(f"")
else:
	logging.info(f"Skipping gtselect...")
	logging.info(f"")

# Call gtmaketime to adjust the GTIs following the chosen ROI and other conditions
ifDoAnyways = True
if ifDoAnyways:
	logging.info(f"Calling gtmktime...")
	gtmktime(infile=data_PH_after_gtselect,outfile=data_PH_after_gtmktime,SCfile=SCfilename,
			 ROI_ra=ROI_ra,ROI_dec=ROI_dec,roicut=roicut,SUNsep=SUNsepmin,
	         filtercond=filtercond,chatter=chatter,gtmode=gtmode)    
	logging.info(f"Done!")
	logging.info(f"")
else:
	logging.info(f"Skipping gtmktime...")
	logging.info(f"")

# ==========================================================================================
#                          LIVETIME AND EXPOSURE CALCULATION
# ==========================================================================================
# To perform an unbinned analysis, we must compute beforehand the livetime and exposure
# of the LAT in the analyzed area.
ifDoAnyways = True
if ifDoAnyways:
	logging.info(f"Calling gtltcube...")
	gtltcube(infile=data_PH_after_gtmktime,outfile=data_PH_after_gtltcube,SCfile=SCfilename,
    	     zmax=zmax,dcostheta=dcostheta,binsz=binsz,chatter=chatter,gtmode=gtmode)
	logging.info(f"Done!")
	logging.info(f"")
else:
	logging.info(f"Skipping gtltcube...")
	logging.info(f"")

# Now we compute the exposure
nbins_arc = int( (ROI_rad+extROI_rad) / binsz)
ifDoAnyways = True
if ifDoAnyways:
	logging.info(f"Calling gtexpmap...")
	gtexpmap(infile=data_PH_after_gtmktime,outfile=data_PH_after_gtexpmap,SCfile=SCfilename,
         	 expcubefile=data_PH_after_gtltcube,evtype=expmap_evtype,irfs=expmap_irfs,
         	 srcrad=ROI_rad+extROI_rad,nlong=nbins_arc,nlat=nbins_arc,nenergies=nbands_energy,
         	 chatter=chatter,gtmode=gtmode)
	logging.info(f"Done!")
	logging.info(f"")
else:
	logging.info(f"Skipping gtexpmap...")
	logging.info(f"")

# ==========================================================================================
#                          DIFFUSE RESPONSE CALCULATION
# ==========================================================================================
# Now we must create an xml file containing all the known sources localized within the ROI
# and compute the corresponding expected contribution (i.e., expected number of emitted
# photons).
source_list = SourceList(catalog_file=catalog_file_4FGL,
                         ROI=[ROI_ra,ROI_dec,ROI_rad],
                         output_name=data_xml_bkgmodel,
                         DR=DRnum)
source_list.make_model(free_radius = free_radius,
                       extra_radius = extROI_rad,
                       max_free_radius = ROI_rad,
                       variable_free = True)

# Compute the diffuse response of the sources added to the xml file    
logging.info(f"Calling gtdiffrsp...")
gtdiffrsp(infile=data_PH_after_gtmktime,SCfile=SCfilename,srcmdl=data_xml_bkgmodel,
          irfs=expmap_irfs,clobber=xmlclobber,chatter=chatter,gtmode=gtmode)    
logging.info(f"Done!")
logging.info(f"")

# ==========================================================================================
#                             FLARE TSMAP ANALYSIS
# ==========================================================================================
# Now we must determine how many simultaneous flares occurred in the same time interval in
# the ROI which is being analyzed. To do so, we first use the gttsmap tool, then we convert
# it into a binary map through a thresholding. Finally, we apply a custom clustering algorithm
# to the thresholded map to obtain the list of flare centers and radiuses.
nxpix = nypix = int(extROI_rad / binsz)
ifDoAnyways = True
if ifDoAnyways:
	# Call gttsmap
	logging.info(f"Calling gttsmap...")
	gttsmap(infile=data_PH_after_gtmktime,outfile=data_PH_after_gttsmap,SCfile=SCfilename,
            expcubefile=data_PH_after_gtltcube,expmapfile=data_PH_after_gtexpmap,
            srcmdl=data_xml_bkgmodel,statistics=tsmap_statistics,irfs=expmap_irfs,optimizer=tsmap_optimizer,
            nxpix=nxpix,nypix=nypix,binsz=binsz,coordsys=tsmap_coordsys,xref=ROI_ra,yref=ROI_dec,
            proj=tsmap_proj,chatter=chatter,gtmode=gtmode)
	logging.info(f"Done!")
	logging.info(f"")
else:
	logging.info(f"Skipping gttsmap...")
	logging.info(f"")

# Consistency check. It MAY happen that gttsmap gets a "Floating point exception" and thus crashes.
# In a such a case, we would still want to go on with the script, simply assuming that no flare was
# found.
ifTSmapExists = os.path.exists(data_PH_after_gttsmap)
if ifTSmapExists:
	# Extract the tsmap with a custom function and print the maximum TS value. Note that we are setting
	# to zero any negative TS value, by using "ifzero=True").
	tsmap, binsx, binsy = getgttsmap(data_PH_after_gttsmap,ROI_ra,ROI_dec,nxpix,nypix,binsz,ifzero=True)
	logging.info(f"Maximum TS measured in the TS map: {np.max(tsmap):.2f}")
	logging.info(f"Equivalent significance: {np.sqrt(np.max(tsmap)):.2f} sigma")
	logging.info(f"")

	# Thresholding and clustering
	tsmap_bool = np.array(tsmap >= tsmap_thr,dtype=float)
	clusterPoints,clusterData = tsmapclustering(tsmap, tsmap_bool,tsmap_mode,tsmap_ifrescaleoutput,binsx,binsy)
	clusterCenters, clusterRadius = clusterData['clusterMaxCenters'], clusterData['clusterGeomRadius']
	numclusters = np.shape(clusterCenters)[0]
else:
	# Set to None all the variables that need to exist but do not have physical meaning
	logging.warning(f"The TS map was found to be non existent!")
	logging.warning(f"Let's assume that there were no new flares here, but please check this later...")
	tsmap = binsx = binsy = tsmap_bool = None
	clusterPoints = clusterData = clusterCenters = clusterRadius = None
	numclusters = 0

# ==========================================================================================
#                             FINDSRC ANALYSIS
# ==========================================================================================
# Now we take again the XML model file produced for the TS map analysis and we add as 
# putative sources a number of point-like Power-law sources, each one centered in a different
# flare recorded by the algorithm. The centers are not directly the outputs of the clustering
# stage, but rather the result of a call to gtfindsrc.
if (numclusters > 0) & (ifLikeWithSeededSources):
	# Physical case: there is at least one significant flare. Add the flares to the file.
	# Note: since the file already exists, we do not need to call again the make_model method.
	# We can simply iterate over the flares and, each time, add the corresponding source.
	logging.info(f"{numclusters} cluster were found in the map. Adding their specs to the XML source list file, after a call to gtfindsrc...")
	
	for i in range(numclusters):
	    # Add to the model file the seeded source
	    candidate_name = f'Flare_{i:02d}'
	    source_list.add_point_source(source_name=candidate_name,
		                             RA=clusterCenters[i,0],
		                             DEC=clusterCenters[i,1],
		                             spectrum_model=flare_spectrum_model,
		                             update_reg=False,
		                             overwrite=True)

	    # Run gtfindsrc to improve the localization position
	    data_PH_after_gtfindsrc = os.path.join(tempdir,sourcename,f"PH_flare_{flarenum:03d}_after_gtfindsrc_seed_{i:02d}.txt")
	    
	    gtfindsrc(infile=data_PH_after_gtmktime,outfile=data_PH_after_gtfindsrc,SCfile=SCfilename,
	              expcubefile=data_PH_after_gtltcube,expmapfile=data_PH_after_gtexpmap,srcmdl=data_xml_bkgmodel,
	              target=candidate_name,coordsys=tsmap_coordsys,RA=clusterCenters[i][0],DEC=clusterCenters[i][1],
	              irfs=expmap_irfs,posacc=findsrc_posacc,optimizer=tsmap_optimizer,reopt=findsrc_reopt,
	              clobber=xmlclobber,chatter=chatter,gtmode=gtmode)

	    # Note that gtfindsrc automatically updates the xml file to include the final (corrected) position,
	    # so we don't need to change it manually!

	logging.info(f"XML file corrected!")
	logging.info(f"")

# ==========================================================================================
#                             LIKELIHOOD ANALYSIS
# ==========================================================================================
# Now perform the likelihood analysis. Note that we do it even if there
# are no additional flares, since the flaring source may be already known (e.g., an AGN)
logging.info(f"Calling gtlike...")
like = gtlike(infile=data_PH_after_gtmktime,outfile=data_xml_gtlike,SCfile=SCfilename,
       		  expcubefile=data_PH_after_gtltcube,expmapfile=data_PH_after_gtexpmap,srcmdl=data_xml_bkgmodel,
       		  irfs=expmap_irfs,optimizer=gtlike_optimizer,verbosity=gtlike_verbosity)
logging.info(f"Done!")
logging.info(f"")

# ==========================================================================================
#                             EXTRACTION OF THE FLARING SOURCES
# ==========================================================================================
# Define some lists to be iteratively filled while going through all the sources fitted by gtlike
allGoodSources = []					# Name of the sources left free in the fit
allTS = [] 							# Their TS
allSigmas = []						# Their significance in sigmas
allFluxes = []						# Their flux in photons/cm2/s
allFluxesErrors = []				# Their flux errors in photons/cm2/s
allNumFreePars = []					# Their numbers of free parameters in the fit
allFlareTS = []						# If flares fitted with a Gamma function: TS (redundant, but easier to check
									#   if the 2nd level trigger threshold is passed)
allFlarePrefactor = []				# If flares fitted with a Gamma function: prefactor (fitted)
allFlareIndex = []					# If flares fitted with a Gamma function: gamma index (fitted)
allKnownSourcesTS = []				# If known source: TS (redundant, but easier to check if the 2nd level trigger 
									#   threshold is passed)
allKnownSourceFitNormParValue = []	# If known source: normalization value (fitted)
allKnownSourceRefNormParValue = []	# If known source: normalization value (from catalog)
allKnownSourceNormIncrease = []		# If known source: catalog to analysis increase of the normalization

# Iterate on all sources and skip the background ones and those that were fixed in the fit
if like is not None:
	# Get the list of all sources contained in the xml file produced as output by gtlike
	allSourceNames = list(like.sourceNames())
	bkgSourceNames = ['gll_iem_v07','iso_P8R3_SOURCE_V3_v1']   # These are BKG and will be skipped
	fitLikelihood = -like.logLike.value()	# Simply the log likelihood of the fit, to be saved

	for i,thisSource in enumerate(allSourceNames):
	    if (thisSource not in bkgSourceNames) & (like.freePars(thisSource) != ()):
	        # If the source passed the checks, save its name and compute the corresponding
	        # TS and significance, and the flux
	        NumFreePars = len(like.freePars(thisSource))
	        allGoodSources.append(thisSource)
	        allTS.append(like.Ts(thisSource))
	        allSigmas.append(TS2sigma(allTS[-1],NumFreePars))
	        allFluxes.append(like.flux(thisSource,emin=Emin,emax=Emax))
	        allFluxesErrors.append(like.fluxError(thisSource,emin=Emin,emax=Emax))
	        allNumFreePars.append(NumFreePars)

	        # Depending on the type of source, the analysis changes
	        if "Flare" in thisSource:
	            # The source is a flare manually added from the gttsmap analysis and
	            # fitted with a power law function: get the value of the fitted parameters
	            allFlareTS.append(like.Ts(thisSource))
	            freePars = like.freePars(thisSource)
	            for j,thisPar in enumerate(freePars):
	                thisParName = thisPar.getName()
	                thisParTrueValue = thisPar.getTrueValue()
	                
	                # Append the results. Note: we do this in two steps for to make the code
	                # more understandable and readable...
	                if (thisParName == 'Prefactor'):
	                    allFlarePrefactor.append(thisParTrueValue)
	                elif (thisParName == 'Index'):
	                    allFlareIndex.append(thisParTrueValue)
	        else:
	            # The source is known: get the value of the normalization factor
	            # and its reference value from the catalog
	            allKnownSourcesTS.append(like.Ts(thisSource))
	            normParName = like.normPar(thisSource).getName()
	            fitNormParValue = like.normPar(thisSource).getTrueValue()
	            refNormParValue = source_list.sources[thisSource]['spectrum'][normParName]
	            normIncrease = fitNormParValue/refNormParValue

	            # Append the results. Note: we do this in two steps for to make the code
	            # more understandable and readable...
	            allKnownSourceFitNormParValue.append(fitNormParValue)
	            allKnownSourceRefNormParValue.append(refNormParValue)
	            allKnownSourceNormIncrease.append(normIncrease)
else:
	fitLikelihood = np.nan

# Turn the outputs in arrays
allGoodSources     = np.array(allGoodSources,dtype='<U32')
allTS              = np.array(allTS,dtype=np.float64)
allSigmas          = np.array(allSigmas,dtype=np.float64)
allFluxes          = np.array(allFluxes,dtype=np.float64)
allFluxesErrors    = np.array(allFluxesErrors,dtype=np.float64)
allNumFreePars	   = np.array(allNumFreePars,dtype=np.float64)
allFlareTS		   = np.array(allFlareTS,dtype=np.float64)
allFlarePrefactor  = np.array(allFlarePrefactor,dtype=np.float64)
allFlareIndex      = np.array(allFlareIndex,dtype=np.float64)
allKnownSourcesTS  = np.array(allKnownSourcesTS,dtype=np.float64)
allKnownSourceFitNormParValue = np.array(allKnownSourceFitNormParValue,dtype=np.float64)
allKnownSourceRefNormParValue = np.array(allKnownSourceRefNormParValue,dtype=np.float64)
allKnownSourceNormIncrease    = np.array(allKnownSourceNormIncrease,dtype=np.float64)

# Perform a thresholding and determine if there is at least a flaring source in this time window.
# The conditions are:
#	- For seeded sources (unknown): TS >= thr (e.g. 25)
#	- For known sources: TS >= thr (e.g. 25) and flux increase w.r.t. catalog >= thr (e.g. 1.5)
flarecond_Seeds = np.any(allFlareTS >= trigger2_thrTS)
flarecond_KnownSources = np.any((allKnownSourcesTS >= trigger2_thrTS) & (allKnownSourceNormIncrease >= trigger2_thrKSNormIncrease))
flarecond = (flarecond_Seeds | flarecond_KnownSources)

# ==========================================================================================
#                             OUTPUT SAVING
# ==========================================================================================
# Save the results in a npz file
np.savez_compressed(flarefile_npz,
					ROI_ra  			= ROI_ra,
					ROI_dec  			= ROI_dec,
					flarecond 			= flarecond,
					clusterCenters     	= clusterCenters,
					clusterRadius      	= clusterRadius,
					fitLikelihood 	  	= fitLikelihood,
					allGoodSources     	= allGoodSources,
					allTS              	= allTS,
					allSigmas          	= allSigmas,
					allFluxes          	= allFluxes,
					allFluxesErrors     = allFluxesErrors,
					allNumFreePars      = allNumFreePars,
					allFlarePrefactor  	= allFlarePrefactor,
					allFlareIndex      	= allFlareIndex,
					allKnownSourceFitNormParValue = allKnownSourceFitNormParValue,
					allKnownSourceRefNormParValue = allKnownSourceRefNormParValue,
					allKnownSourceNormIncrease    = allKnownSourceNormIncrease,
					wstart            	= wstart,
					wstop              	= wstop,
					tstart             	= tstart,
					tstop              	= tstop,
					like                = np.array(like,dtype=object))

logging.info(f"The output .npz file was correctly saved at the following path:")
logging.info(flarefile_npz)
logging.info(f"")

# Save the results in a text file, more descriptive
counterFlares = 0
counterKnownSources = 0

with open(flarefile_txt,'w',encoding="utf-8") as outfile:
    # Write a header
    outfile.write(f"-"*50 + f'\n')
    outfile.write(f"\t\t FLARE NUMBER {flarenum:d}\n")
    outfile.write(f"-"*50 + f'\n')
    outfile.write(f"Time window (2nd level trigger): from {tstart} to {tstop} (MET; the corresponding mission weeks are {wstart} - {wstop}) \n\n")
    outfile.write(f"Negative log-likelihood of the fit: {fitLikelihood:3.3f}\n")
    outfile.write(f"Sources fitted with gtlike, which had at least one free parameter:\n")
    
    # Write some detail for all sources
    if len(allGoodSources) == 0:
    	outfile.write(f"[not finding any source... most likely the fit has failed, check it manually!]\n")

    for i,thisSource in enumerate(allGoodSources):
        outfile.write(f"\n")
        
        # Write the source name, TS, sigma and flux
        outfile.write(f"Source name: {allGoodSources[i]}\n")
        outfile.write(f"Source TS: {allTS[i]:3.2f}\n")
        outfile.write(f"Source significance: {allSigmas[i]:3.2f} sigmas\n")
        outfile.write(f"Source flux (photons / cm2 / s): {allFluxes[i]:.4}\n")
        outfile.write(f"Source flux error (photons / cm2 / s): {allFluxesErrors[i]:.4}\n")
        outfile.write(f"Number of free parameters in the fit (for this source): {allNumFreePars[i]:.4}\n")

        # Depending on the source type, write some more details
        if "Flare" in thisSource:
            # Flare: 
            outfile.write(f"Prefactor (fitted): {allFlarePrefactor[counterFlares]:.4}\n")
            outfile.write(f"Gamma index (fitted): {allFlareIndex[counterFlares]:.4}\n")
            counterFlares += 1
        else:
            # Known source: add the normalization factor
            outfile.write(f"Normalization factor (fitted): {allKnownSourceFitNormParValue[counterKnownSources]:.4}\n")
            outfile.write(f"Normalization factor (catalog): {allKnownSourceRefNormParValue[counterKnownSources]:.4}\n")
            outfile.write(f"Observed increase of the normalization coefficient: {allKnownSourceNormIncrease[counterKnownSources]:.4}\n")
            counterKnownSources += 1

        outfile.write(f"\n")

    # Write some footer
    outfile.write(f"-"*50 + f'\n\n')

logging.info(f"The output .txt file was correctly saved at the following path:")
logging.info(flarefile_txt)
logging.info(f"")

'''
*************************************************************************************************
*************************************************************************************************
*************************************************************************************************
'''

# ==========================================================================================
#                                        PLOTTING / 1
# ==========================================================================================
logging.info(f"Now it's plotting time...")

# Define some common parameter for the plots
colmap = 'gnuplot2'
colmap_bw = 'bone'
clustercolors = ['lime','cyan','mediumorchid','magenta','red','darkorange']
clustercolors *= 20

# We want to show the tsmap before and after the thresholding
if ifplot & ifTSmapExists:
	# Before thresholding (TS map)
	figname = os.path.join(imgsubdir,f'trig2_flare_{flarenum:02d}a_gttsmap' + filetype)

	fig,ax = plt.subplots(figsize = dimfig)
	histoplotter2D(ax,binsx,binsy,tsmap,colmap,None,'RA [°]','DEC [°]',
	               textfont,iflog=False)
	ax.plot(binsx,binsx*0+ROI_dec,color='cyan',linestyle='--',linewidth=2.5,label='ROI center')
	ax.plot(binsy*0+ROI_ra,binsy,color='cyan',linestyle='--',linewidth=2.5)

	if numclusters > 0:
	    # If there are no clusters, we do not need to iterate over them
	    for i in range(numclusters):
	        ax.plot(clusterCenters[i][0],clusterCenters[i][1],color=clustercolors[i],
	                marker='X',markersize=10,linestyle='')
	        circle = plt.Circle(clusterCenters[i],clusterRadius[i],edgecolor=clustercolors[i],
	                           linestyle = '--', linewidth = 2.5, facecolor = "none", label = f'Seed {i:02d}')
	        ax.add_patch(circle)

	ax.legend(loc='upper right',framealpha = 0.75, fontsize = textfont)
	ax.set_title(sourcename,fontsize=1.5*textfont)
	fig.set_tight_layout('tight')
	fig.savefig(figname,dpi=filedpi)
	plt.close(fig)

	# ---------------------------------------------------------------------------------------------
	# After thresholding (flare positions)
	figname = os.path.join(imgsubdir,f'trig2_flare_{flarenum:02d}b_gttsmapTHR' + filetype)

	fig,ax = plt.subplots(figsize = dimfig)
	histoplotter2D(ax,binsx,binsy,tsmap_bool,colmap_bw,None,'RA [°]','DEC [°]',
	                textfont,iflog=False)
	ax.plot(binsx,binsx*0+ROI_dec,color='cyan',linestyle='--',linewidth=2.5,label='ROI center')
	ax.plot(binsy*0+ROI_ra,binsy,color='cyan',linestyle='--',linewidth=2.5)

	if numclusters > 0:
		# If there are no clusters, we do not need to iterate over them
		for i in range(numclusters):
		    ax.plot(clusterCenters[i][0],clusterCenters[i][1],color=clustercolors[i],
		            marker='X',markersize=10,linestyle='')
		    circle = plt.Circle(clusterCenters[i],clusterRadius[i],edgecolor=clustercolors[i],
		                       linestyle = '--', linewidth = 2.5, facecolor = "none", label = f'Seed {i:02d}')
		    ax.add_patch(circle)

		ax.legend(loc='best',fontsize=0.75*textfont)
	ax.set_title(sourcename,fontsize=1.5*textfont)
	fig.set_tight_layout('tight')
	fig.savefig(figname,dpi=filedpi)
	plt.close(fig)

# ==========================================================================================
#                                        PLOTTING / 2
# ==========================================================================================
# We want to show now the counts curve and the corresponding fitted components (i.e., each 
# of the flares contributions separately). But we must compute them first. Do this plot only 
# if there is at least a flare over threshold and if it is required!
if ifplot & (like is not None):
	energies = truebins(like.energies)
	flare_counts = np.zeros((numclusters,np.shape(energies)[0]))
	bkg_counts = np.zeros_like(energies)
	i = 0
	for sourceName in like.sourceNames():
		# Iterate over every source in the XML files. Divide them in two classes: flares and background.
	    if 'Flare' in sourceName:
	        flare_counts[i,:] += like._srcCnts(sourceName)
	        i += 1
	    else:
	        bkg_counts += like._srcCnts(sourceName)

	all_counts = flare_counts.sum(axis=0) + bkg_counts
	GeVenergies = energies / 1000

	# Now plot everything
	figname = os.path.join(imgsubdir,f'trig2_flare_{flarenum:02d}c_gtlikecurve' + filetype)

	fig, ax = plt.subplots(figsize = dimfig)
	for i in range(numclusters):
	    ax.plot(GeVenergies,flare_counts[i,:],color=clustercolors[i],
	            label=f'Model (Power-Law, seeded source n. {i:02d})',linestyle='--',linewidth=2.5)
	ax.plot(GeVenergies,bkg_counts,color='black',label='Model (all 4FGL known sources)',
	        linestyle='--',linewidth=2.5)
	ax.plot(GeVenergies,all_counts,linestyle='-',linewidth=2.5,label='All models combined')
	ax.errorbar(GeVenergies,like.nobs,yerr=np.sqrt(like.nobs),linestyle='',
	            label='Data',marker='.',markersize=15,color='mediumblue')
	ax.legend(loc='best',fontsize=0.75*textfont,framealpha=1)
	ax.set_xlabel(f"Energy [GeV]",fontsize = textfont)
	ax.set_ylabel(f"Counts",fontsize = textfont)
	ax.set_xscale('log')
	ax.grid(linewidth=0.5)
	ax.tick_params(labelsize = textfont)
	ax.set_title(sourcename,fontsize=1.5*textfont)
	fig.set_tight_layout('tight')
	fig.savefig(figname,dpi=filedpi)
	plt.close(fig)

logging.info(f"Plotting done!")
logging.info(f"")
logging.info(f"Completed the execution of trigger_lv2.py!")
logging.info("*"*75)