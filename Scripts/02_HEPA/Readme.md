# The High-Energy Photon flare Advocate (HEPA)

Guide written and edited by [P. Monti-Guarnieri](mailto:pietro.monti-guarnieri@phd.units.it). 

Date of the last update of this guide: 2024.07.03

## Table Of Contents

[[_TOC_]]

## Requirements

In order for this scripts to work, you should have installed on their system:
- The `fermitools` and `fermipy` (possibly within a `conda` environment, see e.g. [here](https://github.com/fermi-lat/Fermitools-conda/wiki/Quickstart-Guide) for some reference)
- The `fmerge` command-line function, made available within the [HEASoft](https://heasarc.gsfc.nasa.gov/docs/software/heasoft/) package
- A working release of `python` version 3.9 or higher, compliant with the `fermitools` and `fermipy`. Several submodules are necessary for these scripts to work, such as `numpy`, `matplotlib`, `LATSourceModel`, `astropy`, `gammapy`. Other functions may be indicated in the [`AstroFunctions.py`](Functions/AstroFunctions.py) and [`MyFunctions.py`](Functions/MyFunctions.py) scripts and should be installed, but they are probably not really necessary.

To avoid problems, a [sample yml file](Documentation/fermi_droplet.yml) for a working conda environment is provided in the documentation folder.

## A general idea 

With this code we provide an **identification pipeline** named HEPA (High Energy Photon flare Advocate), which can be used to analyze the time series of the photons emitted by a certain region of the sky, within a certain time window and an energy band. The aim of this pipeline is to determine if the point in space under analysis is **a flaring source**. This is a useful information, because it can be used to re-point the ground-based observatories towards sources of interest: for instance, this is exactly the case of the Extra-Galactic Survey of the Cherenkov Telescope Array (CTA).

The pipeline here implemented is based on a double-trigger analysis. The idea is that each point of the sky should be analyzed and the presence of a *hypothetical* flare should be determined, through a **first level trigger**. Afterwards, only the regions where the first level trigger was activated should be further investigated, through a **second level trigger**. If the second level trigger threshold is passed, then the point in space can be declared a flaring source and the information can be passed down to the telescopes, for instance through an e-mail system, or a GCN (note that neither of which have been implemented here - we only provide the working code for the analysis!).

### The 1st level trigger

The **first level trigger** implements an estimation of the source emission rate as a function of the time. Two rates are estimated:
- One through a **finite difference** analysis, meaning that the Finite Difference (FD) of the photon arrival times ($\{t_i\}_{i=1}^N$) is used for the estimation:
$$\nu_i = \frac{n+1}{t_{i+n} - t_i} $$
	where $\nu$ is the emission rate and $n$ a characteristic parameter of the algorithm. For $n=1$, the denominator is simply the first derivative of the emission times. However, with $n=3$ or $n=4$ (typical values used in this pipeline), the denominator becomes a function null everywhere except in sudden variations of the inter-arrival times. Thus, the estimated rate should rise to a high value when the photons are more bunched. Physically, $n$ could be thought at the minimum number of photons which must arrive "together", in order for the flare to be detected. Another way of thinking is to see the denominator of this expression as the n-th finite derivative of the arrival times.
- Another through a **Light Curve** (LC) calculation, performed with the standard Fermi analysis methods (i.e., using *gtbin* and *gtexposure*). The key point in this case is that the binning vector of the Light Curve is not regularly spaced, otherwise a short flare could be missed. Instead, the binning vector is derived from the $\nu$ curve estimated with the FD analysis, by applying a Bayesian Block algorithm. Note that in this way we can also easily accomodate the study of sources in the sky with much different rates, even spanning orders of magnitudes.

The 1st level trigger is constructed as the logical OR of two conditions:
- A **significance** value, derived from $\nu_i$, is larger than a given threshold. There exist several options for the significance calculation, such as:
	- The ratio of the instantaneous rate and the mission-long value $\nu_{bkg}$):
$$ s_i = \frac{\nu_i}{\nu_{bkg}} $$
	- The z-score associated to the instantaneous rate, defined as (with $\sigma_{bkg}$ being the standard deviation of the mission-long recorded rates):
$$ s_i = \frac{\nu_i - \nu_{bkg}}{\sigma_{bkg}} $$
	- The cubed difference between the instantaneous rate and the mission-long value (the rationale of this choice is that by taking the cube of this difference we enhance significantly the points farther away from the mean, but preserving the monotonicity with respect to the mission-long value - moreover, we normalize to the mission-long value to have an adimensional quantity):
$$ s_i = \left( \frac{\nu_i - \nu_{bkg} }{ \nu_{bkg} } \right) $$

- The LC computed rate is larger than a given threshold

The two triggers are taken in **"OR"**, meaning that we consider for the second level trigger analysis any point in time where *at least one threshold* was passed. In this way we do not need to explicitly model the source of the photons and instead we just find any interval where the source was emitting for just a brief period of time more photons than usual. This concept is very similar to the idea behind the [Fermi All-Sky Variability](https://fermi.gsfc.nasa.gov/ssc/data/access/lat/FAVA/) (FAVA) analysis, already performed by the Fermi-LAT collaboration. However, in that case the time window for the analysis is fixed to one week, while in this case we have more flexibility (thanks to the FD calculation and the Bayesian Block segmentation).

### The 2nd level trigger

The **second level trigger** is based on an unbinned likelihood analysis. In other words, we perform a regular Fermi analysis with *gtselect, gtmktime, gtltcube, gtexposure* in the given region and time window. Afterwards, we analyze the region with *gttsmap* (unbinned) and try to identify unknown sources, using as known sources the ones available from the 4FGL-DR4 catalog. Afterwards, we perform a likelihood analysis with *gtlike*, leaving as free in the fit only some parameters:
- The normalization factor of any source in the region which is flagged (in the catalog) as highly variable
- The normalization factor of the diffuse and galactic background
- The parameters necessary to fit a new source with a power-law spectrum, whereas the new source is centered on the center of gravity of a cluster of points which yielded TS $\geq 9$ in the TS map. If there are multiple clusters which statisfy this condition, a separate source is added for each of them.

Finally, we declare the existence of a flare if:
- A known source has a normalization factor higher than the catalog value by at least a factor of 1.5 (and the fit yielded TS $\geq 25$)
- An unknown source was fitted with TS $\geq 25$

___

When analyzing the full sky view, we define a grid of points on the celestial sphere and then call the HEPA pipeline on each point (with a corresponding circular region, with a 5° radius). It can be demonstrated that there is no way to build a perfectly equally spaced grid of points on the sphere. Thus, we chose to employ the **golden spiral** method, which is well described [here](https://extremelearning.com.au/how-to-evenly-distribute-points-on-a-sphere-more-effectively-than-the-canonical-fibonacci-lattice/). To see how we actually realized the grid, we refer to the sample [jupyter notebook](/Scripts/01_FAVA_tentatives/01f_grids.ipynb) where we tested this solution, or even to the [piemmegi website](https://piemmegi.web.cern.ch/Subpages/FermiLAT/mainpage.html), where we are showing a very simplified version of the outputs of the HEPA scan of the sky.

___

The HEPA pipeline is implemented in the Python script named [`HEPA_singlepoint.py`](/Scripts/02_HEPA/HEPA_singlepoint.py). However, this script is not so much easy to use stand-alone, since it requires several inputs to be passed by command line and certain environment variables to be previously set by some utility script (e.g., [`utils_configcreator.py`](/Scripts/02_HEPA/utils_configcreator.py)). Instead, it is possible to use one of two wrappers around the pipeline script:
- [`HEPA_singlesource.py`](/Scripts/02_HEPA/HEPA_singlesource.py) can be used to apply the HEPA pipeline to a single source, whose location in the sky (in RA/DEC) is known, in a given time period (specified either in mission weeks or Mission Elapsed Time (MET)) and energy band. In this case, the parameters of the source should be set inside the [`SourceDatabase.csv`](/Scripts/02_HEPA/SourceDatabase.csv) file, following the syntax already employed there. Then, some parameter should be set in the [`utils_configcreator.py`](/Scripts/02_HEPA/utils_configcreator.py) script, such as `ifLoadAverageRates`, which is a boolean flag defining whether the background rates for the 1st level trigger should be loaded from the `.csv` file (*True*), or computed using the entire time window given for the analysis (*False*). Then one simply should call the [`HEPA_singlesource.py`](/Scripts/02_HEPA/HEPA_singlepoint.py) script, passing by command line the name of the source to be investigated. The outputs of this script are a text file and some plots.
- [`HEPA_skyscanner.py`](/Scripts/02_HEPA/HEPA_skyscanner.py) can be used to iteratively apply the HEPA pipeline to a grid of regions covering the entire gamma ray sky (or even to a sub-grid, with a set of known sources). It is possible both to perform a single round-trip of the sky in a given time window, or to instantiate a monitor which continuously scans the sky and looks only at the latest data available (e.g., the last 3 months). The former case is known as **Fixed Time** (FT) mode, while the latter as **Live Monitor** (LM). The outputs of both modes of operations are a summarizing text file and several plots.

To better explain how the sky scanner works, please see the following mermaid charts.

**HEPA_singlepoint.py**

```mermaid
%%{init: {'theme':'forest'}}%%
flowchart TB
    A{"`**Start**`"}-->B[Parse input parameters
    &lpar;ROI center and radius, time window, Emin&rpar;]
    B-->C[Call utils_configcreator.py to set environment variables]
    C-->D[Call trigger_lv1.py]
    D-->E[Is any candidate flare still to be analyzed?]
    E-->|Yes|F[Call trigger_lv2.py]
    F-->E
    E-->|No|G[Produce outputs]
    G-->H{"`**End**`"}
```

**HEPA_skyscanner.py**

```mermaid
%%{init: {'theme':'forest'}}%%
flowchart TB
    A{"`**Start**`"}-->B[Parse input parameters
    &lpar;scanmode, time window, Emin, ifdebug&rpar;]
    B-->C[Define or load sky grid]
    C-->D[Call utils_configcreator to retrieve
    ifLoadAverageRates and switch on its value]
    D -->|True| E[Iterate over each region]
    E --> F[Compute background FD and LC rates 
    with a fixed window]
    F --> E
    D -->|False| G[Load background rates
    from external csv file]
    subgraph SB["Sky grid cycle"]
        H[wget new PH/SC files
        Compute observation window]
        H --> I[Iterate over each region]
        I --> J[Call HEPA_singlepoint.py]
        J --> K[Clear temporary outputs]
        K --> I
        K -->|At the end| L[Write text/graphical outputs]
        L -->|Mode is LM?| H
    end
    F -->|At the end| H
    G --> H
    L -->|Mode is FT?| M{"`**End**`"}
```

## Description of the HEPA auxiliary scripts

Now it may be useful to briefly review the principles of operation of all the scripts called by the two described above. This description assumes that the reader is already familiar with the [**fermitools**](https://fermi.gsfc.nasa.gov/ssc/data/analysis/), their implementation in Python and the structure of the [LAT data products](https://fermi.gsfc.nasa.gov/ssc/data/access/lat/)

### [**`utils_AllSkyFluxMap.py`**](/Scripts/02_HEPA/utils_AllSkyFluxMap.py)

The script is used to produce a full-sky-view count map of the photons emitted during a specific time window. The production is made by following the standard fermitools order. Any flare previously found by the 2nd level trigger analysis is also shown in the count map.


### [**`utils_GTIanalyzer.py`**](/Scripts/02_HEPA/utils_GTIanalyzer.py)

The script is used to produce a GTI-week conversion table, i.e., a file with a certain format where there is a unique correspondence between week number and initial and final GTI (Good Time Interval) of the week, expressed in MET. To do so, the script simply iterates over every weekly photon file available and determines the GTI through a clever parsing of the output of the [gtvcut](https://raw.githubusercontent.com/fermi-lat/fermitools-fhelp/master/fhelp_files/gtvcut.txt) function.

### [**`utils_ListPreparator.py`**](/Scripts/02_HEPA/utils_ListPreparator.py)

This script is used to produce two `.list` files, each one containing a list of weekly photon or spacecraft files to be analyzed. The script accepts as input the initial and last week and fills every other value in-between.

### [**`utils_tempcleaner.py`**](/Scripts/02_HEPA/utils_tempcleaner.py)

This script is used to remove all the temporary figures and files from the [`Images`](Images/) and [`Tempfiles`](Tempfiles/) folders and the respective subfolder. Note that only certain classes of files are considered for removal. For the pictures, they are: `.png`, `.jpg`, `.jpeg` and `.pdf`. For temporary files, they are: `.txt`, `.dat`, `.fits`, `.reg`, `.xml`, `.list`, `.npz` and `.json`.


### [**`utils_configcreator.py`**](/Scripts/02_HEPA/utils_configcreator.py)

The script is used to create a configuration file, required for the 1st and 2nd level trigger analyses. It performs the following actions:
1. Define some hard-coded parameters, which will be used in all the subsequent analyses. Among these, there are also a few parameters which can be defined by the user through command-line input:
	- Center of the ROI to be analyzed (RA, DEC).
	- Energy band to be analyzed (minimum value only, the upper is fixed to 2 TeV).
	- Time window to be analyzed (either expressed as mission weeks or in MET).
	- Trigger number for which this script is being called
2. Parse the times to determine the weeks from the METs and vice versa. This operation requires to have at hand a GTI-week conversion table (see above). If such file does not exist, `utils_GTIanalyzer.py` is called to produce it.
3. Produce `.list` files containing all the photon and spacecraft weekly files to be analyzed. These files are produced through the `utils_ListPreparator.py` script. Note that the spacecraft files must be merged through the [`fmerge`](https://heasarc.gsfc.nasa.gov/ftools/caldb/help/fmerge.txt) function made available by HEASoft, since some of the fermitools (e.g. [gtltcube](https://raw.githubusercontent.com/fermi-lat/fermitools-fhelp/master/fhelp_files/gtltcube.txt) do not accept them listed in a `.list` file, but require a single, longer file.

### [**`trigger_lv1.py`**](/Scripts/02_HEPA/trigger_lv1.py)

The script is used to perform a 1st level trigger analysis on a given ROI, in a given energy band and time window. To do so, the script performs the following actions:
1. Read the configuration file produced for this analysis and load its content in the workspace.
2. Select the photon events from the raw data through [`gtselect`](https://raw.githubusercontent.com/fermi-lat/fermitools-fhelp/master/fhelp_files/gtselect.txt) and [`gtmktime`](https://raw.githubusercontent.com/fermi-lat/fermitools-fhelp/master/fhelp_files/gtmktime.txt). 
3. Compute the n-th FD between the photon arrival times and derive the corresponding rate. The background rate can be either calculated by averaging the FD rate over the time window, or by loading the value from an external file.
4. Compute a Bayesian Block segmentation of the FD curve.
5. Use the segmentation to compute a Light Curve through the ordinary fermitools ([`gtbindef`](https://raw.githubusercontent.com/fermi-lat/fermitools-fhelp/master/fhelp_files/gtbindef.txt) to define the binning, [`gtbin`](https://raw.githubusercontent.com/fermi-lat/fermitools-fhelp/master/fhelp_files/gtbin.txt) to produce the curve and [`gtexposure`](https://raw.githubusercontent.com/fermi-lat/fermitools-fhelp/master/fhelp_files/gtexposure.txt) to correct for the exposure). Compute also the average LC value from the entire time window analyzed, or load the value from an external file.
6. Compute and the intervals which pass the 1st level trigger threshold, either in the FD significance curve, in the LC or in both.
7. Do some plots (FD curve, LC curve and intervals that pass the 1st level trigger, etc.).

### [**`trigger_lv2.py`**](/Scripts/02_HEPA/trigger_lv2.py)
The script is used to perform a 2nd level trigger analysis on a given ROI, in a given energy band and time window, which passed the 1st level trigger. To do so, the script performs the following actions:

1. Read the configuration file produced for the 1st level analysis and load some of its content in the workspace.
2. Read the output file of the 1st level trigger analysis and load the data relevant for the candidate flare which is being investigated.
3. Produce the configuration relevant for the 2nd level analysis.
4. Select the photon events from the raw data through [`gtselect`](https://raw.githubusercontent.com/fermi-lat/fermitools-fhelp/master/fhelp_files/gtselect.txt) and [`gtmktime`](https://raw.githubusercontent.com/fermi-lat/fermitools-fhelp/master/fhelp_files/gtmktime.txt).
5. Compute the LAT livetime and exposure through [`gtltcube`](https://raw.githubusercontent.com/fermi-lat/fermitools-fhelp/master/fhelp_files/gtltcube.txt) and [`gtexpmap`](https://raw.githubusercontent.com/fermi-lat/fermitools-fhelp/master/fhelp_files/gtexpmap.txt).
6. Define an XML file containing all the 4FGL-DR4 sources appearing in the analyzed ROI and compute their diffuse response through [`gtdiffrsp`](https://raw.githubusercontent.com/fermi-lat/fermitools-fhelp/master/fhelp_files/gtdiffrsp.txt).
7. Use [`gttsmap`](https://raw.githubusercontent.com/fermi-lat/fermitools-fhelp/master/fhelp_files/gttsmap.txt) to analyze the region around the center of the ROI.
8. Determine a list of "seeds" of the flaring sources by thresholding the gttsmap and clustering it with a PACMAN-style algorithm.
9. Add the seeds to the XML file produced at point 6. and perform a likelihood analysis with [`gtlike`](https://raw.githubusercontent.com/fermi-lat/fermitools-fhelp/master/fhelp_files/gtlike.txt), in order to determine whether the flare was actually present or not. Note that if no flare is above a certain threshold in the gttsmap, then no flare will be fitted here.
10. Save the outputs and do some plots.

### [**`HEPA_singlepoint.py`**](/Scripts/02_HEPA/HEPA_singlepoint.py)

The script is used to run the HEPA pipeline in a given point in space, time and energy. To do so, the script performs the following actions:

- Read the parameters passed by command line as input.
- Create the configuration file for the 1st level trigger analysis.
- Call the 1st level trigger analysis.
- Read the output of the 1st level trigger analysis, determining the candidate flares to be investigated with the 2nd level trigger analysis.
- Iterate over each candidate and call the 2nd level trigger analysis.
- Merge all the outputs.

## Developer's guide

### How to run this code

In principles, running the HEPA monitor is relatively simple:
1. Clone the entire repository in your folder of preference
2. Make sure that all the photon and spacecraft files are available locally, and that all the templates for the diffuse and background sources are up to date
3. Edit the [**`statuscreator`**](/Scripts/statuscreator.py) script and make sure that the following path variables are correctly set:
	- `mainfolder`: path where the repository has been cloned (including the name `FermiLAT`)
	- `dataSCdir`, `dataPHdir`: paths where the weekly photon and spacecraft files are stored.
	- `catalog_file_4FGL`: path where the 4FGL catalog file (something like `gll_psc_v32.xml`) is stored.
4. Run the status configuration script:
	```bash
	python3 statuscreator.py
	```
5. Edit the [**`configcreator`**](/Scripts/configcreator.py) script and make sure that all the variables are correctly set. The most important ones (and also the ones which more realistically should be touched) are:
	- `ifLoadAverageRates`: boolean, whether the background rates for the 1st level trigger analysis should be loaded from an external source (*True*) or computed on-the-fly with the available data (*False*). We recommend to set the value to *True* and use the `bkg_type` variable to decide the time period used for the calculation. 
	- `trigger1_thrRAW`, `trigger1_thrLC`, `tsmap_thr`, `trigger2_thr`: these are all floating point thresholds, which determine how the 1st and 2nd level trigger work. The 1st level trigger requires to measure a Finite-Difference estimated rate larger than `trigger1_thrRAW`, or a Light Curve estimated flux larger than `trigger1_thrLC`, in order for a point in time to be investigated. In the 2nd level trigger analysis, a square region centered on the hypothetical source is scanned with the [`gttsmap`](https://raw.githubusercontent.com/fermi-lat/fermitools-fhelp/master/fhelp_files/gttsmap.txt) utility, and all the clusters with a TS value larger than `tsmap_thr` are set as candidate sources in the likelihood analysis performed through [`gtlike`](https://raw.githubusercontent.com/fermi-lat/fermitools-fhelp/master/fhelp_files/gtlike.txt). Finally, all the points with a TS larger than `trigger2_thr` after the 2nd level analysis are coloured in yellow, and the remaining ones in cyan. 
	- `imgdir_www`: string, defines a path-like object where some of the figures will be saved, beyond the [`Images`](Images) folder and its subfolders. This is useful if there is an HTML webpage somewhere showing in live the results.
6. It is not necessary to *actually* run the configuration script. This happens already in the correct points of the HEPA pipeline. Instead, it is enough to directly run the [`HEPA_skyscanner.py`](/Scripts/02_HEPA/HEPA_skyscanner.py) script. In this case, one should pay attention to the arguments passed by command line and to the hard-coded `outputfile_www` variable, which is the analog of the `imgdir_www` variable, but for the textual output file. We recall that the call signature for the **Live Monitor** mode and the **Fixed Time** modes is:
```bash
	python3 HEPA_skyscanner.py LM numweeks Emin False
	python3 HEPA_skyscanner.py FT timelist Emin False
```
where `numweeks` is an integer, describing how many weeks to look at once in live, while `timelist` is a list of two floating points, giving the first and last mission week (or MET) to be analyzed. Finally, in both cases, `Emin` is the minimum energy of the photons for the analysis. In both cases we explicitly pass `False` as last argument, since this is the debug argument (and should be set to *True* only when debugging the script).

The HEPA pipeline comes, by default, with a detailed logging of everything happening under the hood. All scrips are configured to hide the basic logs from the output, since the messages are passed as `logging.info` entities, while every script (except the [`HEPA_skyscanner.py`](/Scripts/02_HEPA/HEPA_skyscanner.py)) is configured to only save the outputs from `logging.warning` priority onwards. For additional debugging, it may be convenient to set every script to save also the `logging.info` messages, even if this may clutter a bit the outputs.

### Optimization suggestions

While running [**`HEPA_skyscanner.py`**](/Scripts/02_HEPA/HEPA_skyscanner.py) it is possible to find out that the scanner pipeline takes too much time to complete a full round-trip of the $\gamma$ ray sky. This is, in principles, strictly due to the speed of the code which implements the pipeline (<insert here a rant about how Python is slow and one should/could do better in C++, and yadda yadda>) and to the resources available for the calculations (e.g., number of cores and threads/core, RAM, etc.). However, it is still possible to do some improvement. Here we report some advice about how to optimize the execution of the pipeline:
- If working in LM mode, change the **time window** of the analysis through the command-line inputs given when launching the script. Moreover, change the time window for the **background calculation** directly in the configuration file script (look for the `bkg_type`, `bkg_tstart` and `bkg_tstop` variables). For instance, it could be useful and sensated to analyze only the last month of data, instead of the last 3-6-12 months at any given round.
- Change the number of ROIs to be looked at, in the scanner script file (look for the `grid_n` variable). Clearly, reducing the number of ROIs would increase the minimum radius per ROI, required to cover the full sky view. The radius can be changed by modifying the `grid_radvalues` and `grid_thrsblat` variables in the [**`HEPA_skyscanner.py`**](Scripts/02_HEPA/HEPA_skyscanner.py) script.
- Avoid producing and saving the plots at each call of the [**`trigger_lv1.py`**](Scripts/02_HEPA/trigger_lv1.py) script. This can be done by simply setting to `False` the `ifplot` variable.

However, using more resources for the calculation is probably still the best way to speed up the code execution.

# To Do list

Things to be implemented soon:
- (ALWAYS) A general update of the documentation
- Some tests:
    - Blind search on a dataset generated through [gtorbssim](https://fermi.gsfc.nasa.gov/ssc/data/p7rep/analysis/scitools/help/gtobssim.txt), covering 1y of data and including 3 flaring AGNs, 3 GRBs etc.
    - Test studies with some known sources, such as PSR B1259-63 (RA 195.7°, DEC -63.8°), CTA 102, B2 2234+28A, or others taken from the **3FHL catalog** (i.e., the ones considered to be highly variable) and from V.S. Paliya (2024) *Very High-Energy (>50 GeV) Gamma-ray Flux Variability of Bright Fermi Blazars*

Exemplary sources: PSR_B1259-63, PG_1553+113, all the 3FHL variable sources, BL Lacertae, Mrk 501, PKS 0346-27, PKS 0903-57, TXS 1508+572, PKS 0402−362, 3C 454.3, 3C 279, CTA 102, B3 1343+451, B3 1428+422

---

Things to be implemented in the far future:
- Allow the user to choose one of multiple modes of operation in the configuration file. Specifically, the modes should be: **AGN monitoring, GRB monitoring** and **solar flare monitoring**. In each mode, a different set of thresholds for the triggers and the energy cuts should be used. In the case of solar flare monitoring, the SUNSEP cut (in the gtmktime function call, from [AstroFunctions](Functions/AstroFunctions.py)) should be set to negative, i.e., we should require the sun to be *inside* the ROI. Otherwise, the standard cut should be used. Moreover, the difference between AGN and GRB monitoring is that, in the first case, the SOURCE class events should be used, while in the second case the TRANSIENT class events (see [here](https://fermi.gsfc.nasa.gov/ssc/data/analysis/documentation/Pass8_usage.html) for reference) should be used. This requires to:
	- Modify [utils_ListPreparator.py](Scripts/02_HEPA/utils_ListPreparator.py) and add the production of a third .list file for the extended hierarchy
	- Modify [utils_configcreator.py](Scripts/02_HEPA/utils_configcreator.py) adding a check: if any of the three .list files is missing, call the list preparator. Finally, before giving the outputs, switch on the event class type: if it is "TRANSIENT*", then use the extended .list as PH file. Otherwise, use the ordinary photon file.
- Once done, some further tests with GRB could be done (e.g., how many GRBs do we see from 5-10 GeV upwards?). GRB 130427A is, as usual, a good test case. For all visible GRBs it could be interesting to understand if the computed gamma index is the same as the officially quoted one (i.e., the one computed using the full energy interval accessible by the LAT)
