'''
# ==========================================================================================
#                                       HEADER
# ==========================================================================================
The aim of this script is to perform an ordered call to the HEPA pipeline for a single point
in time and space (and a single energy band).

-------------------------------------------------------------------------------------------
Call syntax:
  	python3 HEPA_singlepoint.py Emin tstart tstop ROI_ra ROI_dec ROI_rad sourcename

Call arguments:
	- Emin 					float, the minimum energy of the photons for this analysis
	- tstart 				float, the beginning of the time window for this analysis
						  	  (either MET or week number is ok)
	- tstop 				float, the end of the time window for this analysis
						  	  (either MET or week number is ok)
	- ROI_ra				float, the center of the ROI to be analyzed (RA), expressed in decimal deg
	- ROI_dec 				float, the center of the ROI to be analyzed (DEC), expressed in decimal deg
	- ROI_rad				float, the radius of the ROI to be analyzed, expressed in decimal deg
	- sourcename			str, the name of the source to be analyzed (this is passed down to the
						  	  config file generator) (if None or unset, defaults to J2000 standard 
						  	  for the given ROI).
	- pathLoadAverageRates 	str, the name of the path where the average rates are saved. This
							  can be used either to load the averages from the file, or to save
							  there the values measured experimentally (default value: SourceDatabase.csv)

Default values of these parameters are simply whatever is initialized down here.
At the moment of writing this guide (2024/02/08), they point to the GRB180720B in the [1,2000] GeV
energy band and with proper time constraints, but this may change in the future.

'''
# ==========================================================================================
#                                 PREAMBLE (module load)
# ==========================================================================================
# Import of the Python Standard Library modules required for the analysis
import glob, os, subprocess, sys       # To act on the operative system
import json                            # To read and write JSON files
import matplotlib as mpl               # For plotting
import matplotlib.pyplot as plt        # For plotting
import numpy as np                     # For numerical analysis
import pandas as pd 				   # For reading and writing csv files
import warnings                        # To deactivate the warnings
import time                            # For script timing
import logging						   # For logging purposes

# Import of personal functions from custom modules
sys.path.append("../../Functions")
from AstroFunctions import J2000formatter

# Set the warnings of a few functions
np.seterr(divide='ignore', invalid='ignore') 					# To avoid 0/0 errors
mpl.rcParams['backend'] = 'agg'                                 # Non-interactive backend for plotting
warnings.filterwarnings("ignore", category=DeprecationWarning)  # To avoid deprecation warnings

# =========================================================================================
#                                      COMMON DEFINITIONS                                      
# =========================================================================================
# Define the dimension for the fonts in the plots, the figures dimensions and so on
textfont = 20         # Fontsize for labels and legends
dimfig = (12,7)       # Figure dimensions (A4-like)
dimfigbig = (16,12)   # Figure dimensions (a bit larger than A4-like)
dimfiglong = (16,25)  # Figure dimensions (a bit larger and much longer than A4-like)
filetype = '.pdf'     # File format to save the pictures
filedpi = 520         # Resolution used when saving pictures in .png/.jpeg
nptsfit = 10000       # Number of points used in plotting the fits

# ==========================================================================================
#                                  STATUS FILE DATA
# ==========================================================================================
# Open the status file
statusfile = '../statusfile.json'
with open(statusfile) as json_file:
	statusdata = json.load(json_file)

# Extract all the relevant variables from the file
datadir = statusdata['datadir']
imgdir = statusdata['imgdir']
tempdir = statusdata['tempdir']
dataSCdir = statusdata['dataSCdir']
dataPHdir = statusdata['dataPHdir']
catalog_file_4FGL = statusdata['catalog_file_4FGL']
catalog_file_3FHL = statusdata['catalog_file_3FHL']

# Define the name for the subfolders (in imgdir and tempdir) where we will save
# the temporary images and files
pretempdir = '02_HEPA'
imgdir = os.path.join(imgdir,pretempdir)
tempdir = os.path.join(tempdir,pretempdir)

# Setting of the logging module
logfile = "HEPA_debug.log"
logging.basicConfig(format='[%(asctime)s] %(message)s',level=logging.WARNING,
					handlers=[logging.FileHandler(logfile,mode='a'),
				        	  logging.StreamHandler()])
logging.info("*"*75)
logging.info("Beginning execution of HEPA_singlepoint.py ...")
logging.info(f"")

# ==========================================================================================
#                                 INPUT DATA
# ==========================================================================================
# Begin the timing
pytiming_t0 = time.perf_counter()

# Define the ROI to be analyzed, the time window and the energy cut + whether we
# want to actually produce the config file for the 1st level trigger, or if it
# is enough to just open it (meaning that it already exist) and edit (which makes
# sense if we only changed the ROI RA/DEC, since this does not require to recompute
# all the regions and files)
Emin = 1              # [GeV] Minimum energy
tstart = 520          # [week or MET] Lower bound of the time window
tstop = 567           # [week or MET] Upper bound of the time window
ROI_ra = 0.5317       # [deg] RA of the center of the ROI
ROI_dec = -2.935      # [deg] DEC of the center of the ROI
ROI_rad = 2.5         # [deg] radius of the ROI

# If the user is providing inputs, overwrite the hard-coded values
try:
	Emin = float(sys.argv[1])
	tstart = int(sys.argv[2])
	tstop = int(sys.argv[3])
	ROI_ra = float(sys.argv[4])
	ROI_dec = float(sys.argv[5])
	ROI_rad = float(sys.argv[6])
except:
	logging.info(f"No input was given: assuming the hard-coded default configuration for the source parameters...")
	logging.info(f"")

# Determine if the input times are expressed in METs or weeks
timethr = 1e4
timeflag = (tstart >= timethr) & (tstop >= timethr)
if timeflag:
	timetype = "[MET]"
else:
	timetype = "[weeks]"

# Determine the sourcename
try:
	if (sys.argv[7] == "None"):
		sourcename = J2000formatter(ROI_ra,ROI_dec)
	else:
		sourcename = sys.argv[7]
except:
	sourcename = J2000formatter(ROI_ra,ROI_dec)
	logging.info(f"Input problem --- Defaulting variable 'sourcename' to the J2000 standard: {sourcename}")
	

# Determine the path of the file containing the average rates of the FD and LC curves
try:
	if sys.argv[8] == "None":
		pathLoadAverageRates = 'SourceDatabase.csv'
	else:
		pathLoadAverageRates = sys.argv[8]
except:
	pathLoadAverageRates = 'SourceDatabase.csv'
	logging.info(f"Input problem --- Defaulting variable 'pathLoadAverageRates' to {pathLoadAverageRates}")

# Some printout
logging.info(f"Parameters for the single point HEPA analyis:")
logging.info(f"\t Source: {sourcename}")
logging.info(f"\t Photon energies: from {Emin} GeV upwards")
logging.info(f"\t Time: from {tstart} to {tstop} {timetype}")
logging.info(f"\t ROI: RA  = {ROI_ra:.04f}, DEC = {ROI_dec:.04f}, radius = {ROI_rad:.04f}")
logging.info(f"")

# ==========================================================================================
#                                 1st LEVEL TRIGGER
# ==========================================================================================
# Call the configcreator script and pass to it any required parameter.
configfile = os.path.join(tempdir,f'configfile_trignum_1.json')

trignum = 1       # The call is for 1st level trigger, so we should pass this info to utils_configcreator.py
logging.info(f"Creating the configuration file to update the parameters for the 1st level analysis...")
subprocess.run(['python3',"utils_configcreator.py",f"{Emin}",f"{tstart}",f"{tstop}",
								f"{ROI_ra}",f"{ROI_dec}",f"{ROI_rad}",f"{trignum}",f"{sourcename}",
								f"{pathLoadAverageRates}"])

# Call the 1st level trigger
ifplot = True             # Save the plots?
ifCalculatingBKG = False  # Force the calculation of the bkg estimations?
logging.info(f"Now doing the 1st level trigger analysis...")
logging.info(f"")
subprocess.run(['python3','trigger_lv1.py',f"{ifplot}",f"{ifCalculatingBKG}"])

# Update the timing
pytiming_t1 = time.perf_counter()

# ==========================================================================================
#                                 2nd LEVEL TRIGGER
# ==========================================================================================
# Open the output file produced by the 1st level trigger to count the number of flares
# which have been suggested in the given time interval (+ extract some data useful for the
# final printouts)
logging.info(f"Checking the output of the 1st level trigger analysis before calling the 2nd level...")
clusterfile = os.path.join(tempdir,sourcename,f"clusters.npz")
with np.load(clusterfile) as file:
	cluster_times = file['cluster_times']
	trig1_av_rate = file['av_rate']
	trig1_std_rate = file['std_rate']
	trig1_LC_time = file['LC_time']
	trig1_LC_rate = file['LC_rate']
	trig1_LC_av_rate = file['LC_av_rate']

numflares = np.shape(cluster_times)[0]

# Open the configuration file produced during the 1st level trigger to determine the 2nd
# level trigger threshold.
with open(configfile) as json_file:
	configdata = json.load(json_file)
	tsmap_thr = configdata['tsmap_thr']
	trigger1_thrRAW = configdata['trigger1_thrRAW']
	trigger1_thrLC = configdata['trigger1_thrLC']
	numSourcesAfterLike = configdata['numSourcesAfterLike']

	# Parse also some info useful for the final printout
	Emax = configdata['Emax']
	trig1_wstart = configdata['wstart']
	trig1_wstop = configdata['wstop']
	trig1_tstart = configdata['tstart']
	trig1_tstop = configdata['tstop']

# Iterate over every suggested flare and apply the 2nd level trigger analysis.
# Then, save the name of the corresponding output file. Note that the outputs
# will be extracted in lists and not arrays since there is no constraint on 
# the number of flares which will be seen in a given time window!
all_flarefilenames = []  	# List of the file names
ifplot = True              	# Save the 2nd level trigger plots?

if (numflares < 1):
	logging.info(f"No flaring time was found with the 1st level trigger: skipping the 2nd level trigger analysis!")
else:
	for i in range(numflares):
		# Call the script
		logging.info(f"Analyzing flare number {i:02d}")
		subprocess.run(['python3','trigger_lv2.py',f"{i}",f"{ifplot}"])

		# Save the output
		all_flarefilenames.append(os.path.join(tempdir,sourcename,f"flare_{i:03d}.txt"))

	logging.info(f"All the flares were examined!")

logging.info(f"")

# Update the timing
pytiming_t2 = time.perf_counter()

# ==========================================================================================
#                                 OUTPUT PRODUCTION
# ==========================================================================================
# Now open an output file and fill with all the relevant informations from the 2nd level
# trigger. For now, assume that this file should be always overwritten.
outfile = os.path.join(tempdir,sourcename,f"allflares_output.txt")

with open(outfile,'w',encoding="utf-8") as file:
	# Write some header
	file.write(f"-"*75 + "\n")
	file.write(f"1st level analysis parameters:\n\n")
	file.write(f"Source name: {sourcename}\n")
	file.write(f"ROI: circular, with center in RA = {ROI_ra:.04f}, DEC = {ROI_dec:.04f} and radius = {ROI_rad:.04f}\n")
	file.write(f"Energy: from {Emin} GeV to {Emax} GeV\n")
	file.write(f"Time window (1st level trigger):\n")
	file.write(f"\tMission weeks: from {trig1_wstart} to {trig1_wstop}\n")
	file.write(f"\tMET: from {trig1_tstart} to {trig1_tstop} s \n\n")
	file.write(f"-"*75 + "\n\n")

	# Write the results of the 1st level trigger analysis
	file.write(f"Average source rate (FD): {trig1_av_rate:.4} +/- {trig1_std_rate:.4} 1/s\n")
	file.write(f"Average source flux (LC): {trig1_LC_av_rate:.4} photons / cm2 / s\n\n")
	file.write(f"Full Light Curve:\n\n")
	file.write(f"\tTime [s]\tFlux [photons / cm2 / s]\n")
	for i in range(np.shape(trig1_LC_time)[0]):
		file.write(f"\t{trig1_LC_time[i]:.4}\t{trig1_LC_rate[i]:.4}\n")
	file.write("\n" + f"-"*75 + "\n\n")

	# Iterate over each of the flares and write the outputs
	if numflares == 0:
		file.write(f"No flare was found above the 1st level trigger thresholds")
		file.write(f" (which are: FD = {trigger1_thrRAW}; LC = {trigger1_thrLC})\n\n")
	else:
		file.write(f"Now we list the flares found above the 1st level trigger thresholds (FD: {trigger1_thrRAW}; LC = {trigger1_thrLC})\n")
		file.write(f"Note that for each flare examined we list flaring sources of two types:\n")
		file.write(f"\t - Sources named like '4FGL J****.*+****' or '4FGL J****.*-****', which are known from the 4FGL-DR4 catalog and where there flagged as highly variable. These sources were fitted in this analysis with the model used in the catalog, leaving free to vary only the normalization parameter;\n")
		file.write(f"\t - Sources named like 'Flare_**', which are candidate flares identified as such with gttsmap and fitted with a Power Law function. Note that the flare 'number' is not consistent over time (i.e., in different time windows, flare 00 may not be always the same source).\n\n")

		for i,flarefile in enumerate(all_flarefilenames):
			# Open the output file of the i-th flare
			try:
				with open(flarefile,'r') as f:
					for line in f.readlines():
						file.write(line)

			except FileNotFoundError:
				file.write(f"-"*50 + f'\n')
				file.write(f"No file found for flare n. {i:d}, needs investigation! \n")
				file.write(f"-"*50 + f'\n')

			file.write(f"\n\n")
			
# Write the output...
logging.info(f"The outputs were written in the .npz file located at the following path:")
logging.info(outfile)
logging.info(f"")


# ==========================================================================================
#                                      CONCLUSIONS
# ==========================================================================================
# Print the timing
logging.info(f"Time between the beginning of HEPA_singlepoint and the end of the 1st level trigger analysis: {(pytiming_t1-pytiming_t0):.3f} s")
logging.info(f"Time between the end of the 1st level trigger analysis and the end of the 2nd level: {(pytiming_t2-pytiming_t1):.3f} s")
logging.info(f"")
logging.info(f"Completed the execution of HEPA_singlepoint.py!")
logging.info("*"*75)