'''
# ==========================================================================================
#                                       HEADER
# ==========================================================================================
The aim of this script is to create a json file which will be used for the execution of
the 1st level trigger of the HEPA pipeline. Here we should define as hard-coded
every parameter which shouldn't change between the trigger calls (e.g., the ROI radius,
or the maximum zenith angle acceptable in the analysis) and we should define as user-input
the others. Specifically, the latter are:
	- The ROI center, expressed in RA/DEC coordinates
	- The time interval to be analyzed, expressed either in MET or in mission weeks
	- The trigger number for which this script is being called
	- The source name, if existent
	- The path of the file where the average rates to be used in the 1st level analysis 
	  may be loaded, if required

-------------------------------------------------------------------------------------------
Call syntax:
  	python3 utils_configcreator.py Emin timestart timestop ROI_ra ROI_dec ROI_rad trignum sourcename pathLoadAverageRates

Call arguments:
    - Emin                  int or None, minimum energy in GeV of the energy cut, and None defaults to 10
    - timestart             float or None, minimum value of the time cut. If it is a float value smaller 
                              than 10 000, we assume that it is a week number. If it is larger, we assume
                              that it is a MET. The MET-week conversion is hard-coded. If it is None, it 
                              defaults to week n. 009
    - timestop              float or None, maximum time of the time cut. The same convention as above is
                              used, with "None" defaulting to the last week available in the data.
    - ROI_ra, ROI_dec       float or None, the values of the center of the ROI, expressed in RA and DEC 
                              decimal coordinates. If None, or unset, defaults to 0.
    - ROI_rad               float, the value of the ROI radius, expressed in decimal degrees. If None,
                              or unset, defaults to 2.5
    - trignum               int or None, number of trigger level for which this script is called (which
                              determines the name of the output file). If None or unset, defaults to 1.
    - sourcename            str or None, the name of the source to be analyzed here. If None or unset,
                              defaults to the J2000 standard for the given ROI.
    - pathLoadAverageRates  str or None, the name of the file where the average FD and LC rates can
                              be loaded, if it is required (check the ifLoadAverageRates flag)

Example (with MET values of tstart and tstop):
	python3 utils_configcreator.py 10 2.4e8 3.0e8 264.9 27.4 5 1 GRB_180720B SourceDatabase.csv

'''
# ==========================================================================================
#                                 PREAMBLE (module load)
# ==========================================================================================
# Import of the Python Standard Library modules required for the analysis
import glob, os, subprocess, sys       # To act on the operative system
import json                            # To read and write JSON files
import matplotlib as mpl               # For plotting
import matplotlib.pyplot as plt        # For plotting
import numpy as np                     # For numerical analysis
import pandas as pd 				   # For reading and writing csv files
import warnings                        # To deactivate the warnings
import time                            # For script timing
import logging						   # For logging purposes

# Import of personal functions from custom modules
sys.path.append("../../Functions")
from AstroFunctions import J2000formatter

# ==========================================================================================
#                                  STATUS FILE DATA
# ==========================================================================================
# Open the status file
statusfile = '../statusfile.json'
with open(statusfile) as json_file:
	statusdata = json.load(json_file)

# Extract all the relevant variables from the file
datadir = statusdata['datadir']
imgdir = statusdata['imgdir']
tempdir = statusdata['tempdir']
dataSCdir = statusdata['dataSCdir']
dataPHdir = statusdata['dataPHdir']
catalog_file_4FGL = statusdata['catalog_file_4FGL']
catalog_file_3FHL = statusdata['catalog_file_3FHL']

# Define the name for the subfolders (in imgdir and tempdir) where we will save
# the temporary images and files
pretempdir = '02_HEPA'
imgdir = os.path.join(imgdir,pretempdir)
tempdir = os.path.join(tempdir,pretempdir)

# Setting of the logging module
logfile = "HEPA_debug.log"
logging.basicConfig(format='[%(asctime)s] %(message)s',level=logging.INFO,
                    handlers=[logging.FileHandler(logfile,mode='a'),
                              logging.StreamHandler()])

logging.info("*"*75)
logging.info("Beginning execution of utils_configcreator.py ...")
logging.info(f"")

# ==========================================================================================
#                                 USER-INPUT PARAMETERS
# ==========================================================================================
# In this section we define all the parameters needed to work with the fermitools,
# which should be input by the user via command-line.
# 
# We begin with the minimum energy to be used in the cuts
try:
	if sys.argv[1] == "None":
		Emin = 10
	else:
		Emin = float(sys.argv[1])
except:
	Emin = 10
	logging.info(f"Input problem --- Defaulting variable 'Emin' to {Emin} GeV")
	logging.info(f"")

Emin = Emin * 1e3         # from GeV to MeV

# Now we define the minimum and maximum time for the time cut. 
# We assume that the user input is a MET if and only if it is larger than 10 000 
# (since the mission weeks range from 009 to about 900, while the METs start from 
# around 2.4e8 at the first mission day and go up to more than 7e8).
# We set the non-inputted variable to "TBC" to indicate that we should recompute it.
week2METthr = 1e4
try:
	if sys.argv[2] == "None":
		tstart = "TBC"
		wstart = 9
	elif float(sys.argv[2]) >= week2METthr:
		tstart = float(sys.argv[2])
		wstart = "TBC"
	else:
		tstart = "TBC"
		wstart = int(sys.argv[2])
except:
	tstart = "TBC"
	wstart = 9
	logging.info(f"Input problem --- Defaulting variable 'tstart' to {tstart} and 'wstart' to {wstart}")
	logging.info(f"")

try:
	if sys.argv[3] == "None":
		tstop = "TBC"
		wstop = -1
	elif float(sys.argv[3]) >= week2METthr:
		tstop = float(sys.argv[3])
		wstop = "TBC"
	else:
		tstop = "TBC"
		wstop = int(sys.argv[3])
except:
	tstop = "TBC"
	wstop = -1
	logging.info(f"Input problem --- Defaulting variable 'tstop' to {tstop} and 'wstop' to {wstop}")
	logging.info(f"")

# Now we define the ROI center in RA/DEC coordinates
try:
	if (sys.argv[4] == "None") | (sys.argv[5] == "None"):
		ROI_ra = ROI_dec = 0
	else:
		ROI_ra = float(sys.argv[4])
		ROI_dec = float(sys.argv[5])
except:
	ROI_ra = ROI_dec = 0
	logging.info(f"Input problem --- Defaulting variable 'ROI_ra' to {ROI_ra:.2f} and 'ROI_dec' to {ROI_dec:.2f}")
	logging.info(f"")

# Now we define the ROI radius
try:
	if (sys.argv[6] == "None"):
		ROI_rad = 2.5
	else:
		ROI_rad = float(sys.argv[6])
except:
	ROI_rad = 2.5
	logging.info(f"Input problem --- Defaulting variable 'ROI_rad' to {ROI_rad}")
	logging.info(f"")


# Now we define the integer number representing for which trigger the config
# file is being created
try:
	if (sys.argv[7] == "None"):
		trignum = 1
	else:
		trignum = int(sys.argv[7])
except:
	trignum = 1
	logging.info(f"Input problem --- Defaulting variable 'trignum' to {trignum}")
	logging.info(f"")

# Now we define the name of the source
try:
	if (sys.argv[8] == "None"):
		sourcename = J2000formatter(ROI_ra,ROI_dec)
	else:
		sourcename = sys.argv[8]
except:
	sourcename = J2000formatter(ROI_ra,ROI_dec)
	logging.info(f"Input problem --- Defaulting variable 'sourcename' to the J2000 standard: {sourcename}")
	logging.info(f"")
	
# Now we define the name of the file from which we load the average rates (if required)
try:
	if sys.argv[9] == "None":
		pathLoadAverageRates = 'SourceDatabase.csv'
	else:
		pathLoadAverageRates = sys.argv[9]
except:
	pathLoadAverageRates = 'SourceDatabase.csv'
	logging.info(f"Input problem --- Defaulting variable 'pathLoadAverageRates' to {pathLoadAverageRates}")
	logging.info(f"")

# ==========================================================================================
#                                 HARD-CODED PARAMETERS
# ==========================================================================================
# In this section we define all the parameters needed to work with the fermitools.
# Specifically, here we define the parameters used in the data selection with gtselect,
# gtmktime and other such functions. Note that the center of the ROI in RA/DEC coordinates
# is here left undefined but saved, since other scripts will modify it later.
Emax = 2000*1e3             # [MeV] Maximum photon energy
zmax = 105                  # [deg] Maximum apparent zenith angle
evclass = 128               # [int] Event class: P8R3_SOURCE class (128) or anything else
evtype = 3                  # [int] Event conversion type: Front + Back class (3) or anything else
roicut = 'no'               # [str] Whether to apply a ROI cut in gtmktime
SUNsepmin = None            # [deg] Minimum separation between the photon incidence angles and the Sun
filtercond = '(IN_SAA!=T)&&(DATA_QUAL>0)&&(LAT_CONFIG==1)' # gtmktime selection cut
bindef_bintype = "T"        # [str] Argument to pass to gtbindef
bindef_tbinalg = "FILE"     # [str] Argument to pass to gtbin
bindef_algorithm = "LC"     # [str] Argument to pass to gtbin
irfs = 'P8R3_SOURCE_V3'     # [str] Event class: P8R3_SOURCE class or anything else
srcmdl = "none"             # [str] Argument to pass to gtexposure
specin = -2.1               # [float] Argument to pass to gtexposure
chatter = 1                 # Verbosity for the fermitools. 1: minimal. 2: medium. 4: extensive.
gtmode = "h"                # Running mode of all fermitools: batch mode ("h") or interactive mode ("ql")

# Here we define the parameters needed to perform the 1st level trigger analysis
FDnum = 3                   # [int] Parameter for the FD calculation: vec[FDnum:]-vec[:-FDnum]
FD_errorscale = 0.1         # [float] We define the errors on the FD-estimated rates as this factor 
                            #   times the rates themselves
FD_errortype = 'None'       # [str] The way to compute the errors associated to the inter-arrival times.
                            #   Note: such errors are "only" used in the Bayesian Block segmentation,
                            #   and the results depends a lot on how they are given. Options:
                            #   - "mean": the error is half of the average inter-arrival time
                            #   - "proportional": the error is proportional to the inter-arrival times,
                            #     with a scaling factor given by FD_errorscale
                            #   - "None" (or a None pointer): the errors are not used
FD_signiftype = 'sigma_distance'    # [str] The wat to compute the significance, starting from the estimated
                                    #   inter-arrival times. Options (with "average" the global average of
                                    #   the rates, and "std" the standard deviation):
                                    #   - "geom_increase": signif = rate / average
                                    #   - "cubed_difference": signif = (rate - average)**3 / average**3
                                    #   - "sigma_distance": signif = (rate - average) / std
BayesOffset = 500           # [int] Number of seconds to keep as margin if the edges of the Bayesian
                            #   Block segmentation are outside of the events data
start_p0 = 0.25             # [float or None] Starting point for the p0 parameter in the Bayesian Block 
                            #   binning of the FD curve
start_ncp = None            # [float or None] Starting point for the gamma_ncp parameter in the Bayesian
                            #   Block binning of the FD curve. Advice: 1.32 + 0.577 *np.log10(Npoints)
h2s = 3600                  # [int] Seconds in an hour
d2h = 24                    # [int] Hours in a day
timestep = 7*d2h*h2s        # [int] Binning step for the final time vector used before the 2nd level trigger
mergedist = 2               # [int or None] Merging distance used in the calculation of the intervals 
                            #   which will be examined in the 2nd level trigger (**see trigger_lv1.py)
ifLoadAverageRates = False  # [bool] Whether to load the average FD and LC rates from an external file
                            #   (True) or to compute manually them with the data and then save them (False)

# Here we define the structure of the files used for the FD and LC average rate loading (in terms of
# class of each column)
if (pathLoadAverageRates == 'SourceDatabase.csv'):
	databasedtypes = {"Emin_GeV"   : 'np.float64', # Note: these are string, we'll need to convert them to dtypes
                      "wstart"     : 'np.int64',   #  before any actual use. This can be done easily with eval(value)!
                      "wstop"      : 'np.int64',
                      "ROI_ra_deg" : 'np.float64',
                      "ROI_dec_deg": 'np.float64',
                      "ROI_rad_deg": 'np.float64',
                      "Status"     : 'str',
                      "Av_FD"      : 'np.float64',
                      "Std_FD"	   : 'np.float64',
                      "Av_LC"      : 'np.float64'}
else:
	# Including the case of: (pathLoadAverageRates == 'SkyGridDatabase.csv')
	databasedtypes = {"ROI_ra_deg"   : 'np.float64',
                      "ROI_dec_deg"  : 'np.float64',
                      "ROI_blat_deg" : 'np.float64',
                      "ROI_blong_deg": 'np.float64',
                      "ROI_rad_deg"  : 'np.float64',
                      "Av_FD"        : 'np.float64',
                      "Std_FD"	     : 'np.float64',
                      "Av_LC"        : 'np.float64'}

# Here we define other variables useful for the calculation of the FD and LC background rates
# in the HEPA_skyscanner.py application
bkg_type = 'first6months'		# [str] this variable determines which time window to use in the calculation
								#   (if ifLoadAverageRates is True). The available values are:
								#	- "firstyear": 1st mission year (from week 9 to 60, extremes included)
								#	- "lastyear": last 52 weeks available in the data
								# 	- "all": all the weeks available
								# 	- otherwise: the user chooses the extremes
if bkg_type == 'firstyear':
	# Note that these values are the numbers of the designated weeks.
	bkg_tstart = 9
	bkg_tstop = 60
elif bkg_type == 'lastyear':
	bkg_tstart = -52
	bkg_tstop = -1
elif bkg_type == 'all':
	bkg_tstart = 9
	bkg_tstop = -1
elif bkg_type == "last6months":
	bkg_tstart = -26
	bkg_tstop = -1
elif bkg_type == "first6months":
	bkg_tstart = 9
	bkg_tstop = 32
else:
	# This is an arbitrary choice (-13:-1 means last 3 months)
	bkg_tstart = -13
	bkg_tstop = -1

# Here we define the parameters needed to perform the 2nd level trigger analysis
extROI_rad = 5                     # [deg] Extension of the square region centered in the ROI center which will
                                   #   be considered as "border" in likelihood analyses to avoid PSF limitations
nbands_energy = 20                 # Number of energy bands used in computing the exposure through gtexpmap
dcostheta = 0.025                  # [deg] Angular step in the calculation of the LAT livetime
binsz = 0.25                       # [deg] Angular step in the calculation of the exposure map and gttsmap
expmap_evtype = "INDEF"            # Event type for gtexpmap
expmap_irfs = "CALDB"              # IRFs for expmap
xmlclobber = 'yes'                 # Whether to overwrite or not the existing XML files
DRnum = 4                          # Release number of the 4FGL catalog to use with LATSourceModel
free_radius = 1e-9                 # Service parameter for the LATSourceModel "SourceList" function
tsmap_statistics = "UNBINNED"      # Type of gttsmap analysis
tsmap_optimizer = "NEWMINUIT"      # Type of gttsmap optimizer
tsmap_coordsys = "CEL"             # Type of gttsmap coordinate system
tsmap_proj = "CAR"                 # Type of gttsmap projection
tsmap_thr = 16                     # Threshold for the TS obtained in gttsmap in order to be candidate flares
tsmap_mode = "1NN"                 # Type of gttsmap clustering strategy
tsmap_ifrescaleoutput = True       # Whether to scale the gttsmap axes in RA/DEC units or leave them in indices
findsrc_reopt = 'yes'              # Parameter to be passed to gtfindsrc
findsrc_posacc = 0.01              # Parameter to be passed to gtfindsrc
flare_spectrum_model = "PowerLaw"  # Energy spectrum function of the flaring source
gtlike_optimizer = 'NewMinuit'     # Type of gtlike optimizer
gtlike_verbosity = 0               # Verbosity of the call to gtlike (which we call using fermipy)
numSourcesAfterLike = 3            # How many sources should I show after a call to gtlike in the 2nd level trigger?
ifLikeWithSeededSources = True     # Do I want to add the centers of the gttsmap clusters as seeded sources for
                                   #   the likelihood analysis?

# Here we define the paths where the temporary images and files of the current source
# will be saved. If such paths do not exist, then the corresponding folders are created.
# We also want to define any folder where the pictures for the display in web page should
# be saved.
imgsubdir = os.path.join(imgdir,sourcename)
if not os.path.exists(imgsubdir):
	os.mkdir(imgsubdir)

tempsubdir = os.path.join(tempdir,sourcename)
if not os.path.exists(tempsubdir):
	os.mkdir(tempsubdir)

imgdir_www = '/eos/user/p/pmontigu/www/Images/FermiLAT'

# Here we define the thresholds for the HEPA trigger (both 1st and 2nd level)
trigger1_thrRAW = 3               # 1st level trigger, threshold(s) on the value of the observation significance
                                  #  [e.g., raw nu_source / nu_bkg, or something else]
trigger1_thrLC = 3                # 1st level trigger, threshold on the value of [Light Curve nu / nu_bkg]
trigger2_thrTS = 25               # 2nd level trigger, threshold on the value of the TS for the discovery
trigger2_thrKSNormIncrease = 1.5  # 2nd level trigger, threshold on the value of the flux increase for declaring
                                  #  as flaring a source which is already known

# Here we define the variables for the production of full-sky-view flux maps
# (this is done only at the end of a long round of HEPA_skyscanner)
CMAP_algorithm = 'CMAP'           # Parameter for gtbin
CMAP_binsz = 0.5                  # Binning step of the maps in x-y (x=long,y=lat)
CMAP_nxpix = int(360/CMAP_binsz)  # Number of pixels in x for a full sky view
CMAP_nypix = int(180/CMAP_binsz)  # Number of pixels in y for a full sky view
CMAP_coordsys = 'GAL'             # Frame of reference
CMAP_xref = CMAP_yref = 0         # Center of the frame in x-y
CMAP_axisrot = 0                  # Rotation angle in the x-y plane
CMAP_proj = 'AIT'                 # Projection style
CMAP_indef = 'INDEF'              # For undefined parameters
expcubes_cmap = 'none'            # Parameter for gtexpcube2
expcubes_ebinalg = 'LOG'          # Binning in energy for gtexpcube2
expcubes_enumbins = 20            # Number of bins for gtexpcube2

# ==========================================================================================
#                                 MET TO WEEK CONVERSION
# ==========================================================================================
# Now we have to set any unset input variable among the following: tstart, wstart, tstop, wstop.
# To do so, we should use a npz file containing the conversion table. If this file does not
# exist, we should produce it
tablefile = os.path.join(tempdir,'GTIdict.npz')
ifForceGTI = True & (trignum == 1)  # If we want to always recompute the GTIdict file (at least for trigger lv1)

if (not os.path.exists(tablefile)) | ifForceGTI:
	logging.info(f"Missing the GTIdict.npz file. Let's fix this ...")
	subprocess.run(['python3','utils_GTIanalyzer.py'])

# Open the file and extract the required vectors
with np.load(tablefile) as file:
	weeknumbers = file['weeknumbers'] # These are all floating point np.arrays
	firstGTIs = file['firstGTIs']
	lastGTIs = file['lastGTIs']

# Check for consistency that tstart and tstop (if set) are NOT beyond the
# mission time available. If so, force a manual correction
if (tstart != "TBC"):
	if (tstart < firstGTIs[0]):
		tstart = int(firstGTIs[0])
	elif (tstart > firstGTIs[-1]):
		tstart = int(firstGTIs[-1])

if (tstop != "TBC"):
	if (tstop < lastGTIs[0]):
		tstop = int(lastGTIs[0])
	elif (tstop > lastGTIs[-1]):
		tstop = int(lastGTIs[-1])

# ----------------------------------------------------------------------------
# Compute tstart, if wstart is known
if (tstart == "TBC"):
	if (wstart <= 0):
		# Note that due to how this is written, we fall in this condition if
		# wstart is -1 but also -2, -3 and so on.
		tstart = int(firstGTIs[wstart])
		wstart = int(weeknumbers[wstart])
	else:
		try:
			goodind = np.where(weeknumbers >= wstart)[0][0]
			tstart = int(firstGTIs[goodind])
		except IndexError:
			# This may happen if wstart is before the first week available
			tstart = int(firstGTIs[0])
			logging.info(f"IndexError thrown in the calculation of tstart - defaulting to {tstart}\n")

# Compute wstart, if tstart is known
if (wstart == "TBC"):
	# Find the first MET corresponding to the beginning of a week, before 
	# the required start
	goodind = np.where(firstGTIs <= tstart)[0][-1] # Index of the week
	wstart = int(weeknumbers[goodind])

# Compute tstop, if wstop is known. Note that here we must account for the fact 
# that wstop may be either a week number (directly corresponding to a MET) or "-1" 
# (meaning that the last available week should be used). In the second case, we
# must re-compute the week number.
if (tstop == "TBC"):
	if (wstop <= 0):
		# Note that due to how this is written, we fall in this condition if
		# wstop is -1 but also -2, -3 and so on.
		tstop = int(lastGTIs[wstop])
		wstop = int(weeknumbers[wstop])
	else:
		try:
			goodind = np.where(weeknumbers >= wstop)[0][0]
			tstop = int(lastGTIs[goodind])
		except IndexError:
			# This may happen if wstop is after the last week available
			tstop = int(lastGTIs[-1])

# Compute wstop, if tstop is known. Here we DON'T HAVE to account for 
# the possibility of wstop being -1, since this is a forbidden case!
if (wstop == "TBC"):
	# Find the first MET corresponding to the end of a week, after the
	# required stop
	goodind = np.where(lastGTIs >= tstop)[0][0] # Index of the week
	wstop = int(weeknumbers[goodind])

# ==========================================================================================
#                                 LIST FILE CREATION
# ==========================================================================================
# Here we define the names of the Spacecraft and Photon file lists (or files)
# necessary for the 1st level trigger analysis
listdir = os.path.join(tempdir,'Lists')
if (not os.path.exists(listdir)):
	os.mkdir(listdir)

PHlistname = os.path.join(listdir,f'PH_events_trig_{trignum}_w{wstart:03d}_w{wstop:03d}.list')
SClistname = os.path.join(listdir,f'SC_events_trig_{trignum}_w{wstart:03d}_w{wstop:03d}.list')

# Now that we know the weeks to be analyzed, we can produce the .list file containing the
# names of the photon and spacecraft files to be analyzed.
if (not os.path.exists(PHlistname)) | (not os.path.exists(SClistname)):
	logging.info(f"Missing the .list files. Let's fix this...")
	subprocess.run(['python3','utils_ListPreparator.py',f'{wstart}',f'{wstop}',
				PHlistname,SClistname])
	logging.info(".list files prepared!")

else:
	logging.info(f"The .list files founds were correctly located!")

logging.info(f"Photon list file: {PHlistname}")
logging.info(f"Spacecraft list file: {SClistname}\n")

# Now we can call the HEASOFT fmerge function (non-python, but should be installed)
# to merge the spacecraft files listed in the .list file.
SCdir = os.path.join(tempdir,'SCfiles')
SCfilename = os.path.join(SCdir,f"SC_events_w{wstart:03d}_w{wstop:03d}.fits")

if (not os.path.exists(SCdir)):
	os.mkdir(SCdir)

if os.path.exists(SCfilename):
	logging.info(f"The spacecraft merged file already exists: we are not recreating it!")
else:
	logging.info(f"The spacecraft merged file does not exist: calling fmerge to create it...")
	subprocess.run(['punlearn','fmerge'])
	subprocess.run(['fmerge',f'@{SClistname}',SCfilename,'-','clobber=yes']) # clobber == enable overwriting

logging.info(f"Done!")
logging.info(f"")

# ==========================================================================================
#                                  JSON FILE PRODUCTION
# ==========================================================================================
# Here we create the json string to be saved
jsonstring = {"wstart":                     wstart,                 # Coupled to tstart/tstop
              "wstop":                      wstop,                  # Coupled to tstart/tstop
              "tstart":                     tstart,                 # gtselect
              "tstop":                      tstop,                  # gtselect 
              "ROI_ra":                     ROI_ra,                 # gtselect
              "ROI_dec":                    ROI_dec,                # gtselect
              "ROI_rad":                    ROI_rad,                # gtselect
              "Emin":                       Emin,                   # gtselect
              "Emax":                       Emax,                   # gtselect
              "zmax":                       zmax,                   # gtselect
              "evclass":                    evclass,                # gtselect
              "evtype":                     evtype,                 # gtselect
              "roicut":                     roicut,                 # gtmktime
              "SUNsepmin":                  SUNsepmin,              # gtmktime
              "filtercond":                 filtercond,             # gtmktime
              "bindef_bintype":             bindef_bintype,         # gtbindef
              "bindef_tbinalg":             bindef_tbinalg,         # gtbin
              "bindef_algorithm":           bindef_algorithm,       # gtbin
              "irfs":                       irfs,                   # gtexposure
              "srcmdl":                     srcmdl,                 # gtexposure
              "specin":                     specin,                 # gtexposure
              "chatter":                    chatter,                # All fermitools
              "gtmode":                     gtmode,                 # All fermitools
              "FDnum":                      FDnum,                  # 1st level trigger
              "FD_errorscale":              FD_errorscale,          # 1st level trigger
              "FD_errortype":               FD_errortype,           # 1st level trigger
              "FD_signiftype":              FD_signiftype,          # 1st level trigger
              "BayesOffset":                BayesOffset,            # 1st level trigger
              "start_p0":                   start_p0,               # 1st level trigger
              "start_ncp":                  start_ncp,              # 1st level trigger
              "timestep":                   timestep,               # 1st level trigger
              "mergedist":                  mergedist,              # 1st level trigger
              "ifLoadAverageRates":         ifLoadAverageRates,     # 1st level trigger
              "pathLoadAverageRates":       pathLoadAverageRates,   # 1st level trigger
              "databasedtypes":             databasedtypes,         # 1st level trigger
              "bkg_type":                   bkg_type,               # Background calculation before 1st level trigger
              "bkg_tstart":                 bkg_tstart,             # Background calculation before 1st level trigger
              "bkg_tstop":                  bkg_tstop,              # Background calculation before 1st level trigger
              "dcostheta":                  dcostheta,              # gtltcube
              "nbands_energy":              nbands_energy,          # gtexpmap
              "extROI_rad":                 extROI_rad,             # gtexpmap
              "binsz":                      binsz,                  # gtexpmap
              "expmap_evtype":              expmap_evtype,          # gtexpmap
              "expmap_irfs":                expmap_irfs,            # gtexpmap
              "xmlclobber":                 xmlclobber,             # SourceModel
              "DRnum":                      DRnum,                  # SourceModel
              "free_radius":                free_radius,            # SourceModel
              "tsmap_statistics":           tsmap_statistics,       # gttsmap
              "tsmap_optimizer":            tsmap_optimizer,        # gttsmap
              "tsmap_coordsys":             tsmap_coordsys,         # gttsmap
              "tsmap_proj":                 tsmap_proj,             # gttsmap
              "tsmap_thr":                  tsmap_thr,              # gttsmap
              "tsmap_mode":                 tsmap_mode,             # gttsmap
              "tsmap_ifrescaleoutput":      tsmap_ifrescaleoutput,  # gttsmap
              "findsrc_reopt":              findsrc_reopt,          # gtfindsrc
							"findsrc_posacc":             findsrc_posacc,         # gtfindsrc
              "flare_spectrum_model":	      flare_spectrum_model,   # gtlike
              "gtlike_optimizer":           gtlike_optimizer,       # gtlike
              "gtlike_verbosity":           gtlike_verbosity,       # gtlike
              "numSourcesAfterLike":        numSourcesAfterLike,    # gtlike
              "ifLikeWithSeededSources":    ifLikeWithSeededSources,# gtlike
              "trigger1_thrRAW":            trigger1_thrRAW,        # 1st level threshold
              "trigger1_thrLC":             trigger1_thrLC,         # 1st level threshold
              "trigger2_thrTS":             trigger2_thrTS,         # 2nd level threshold
              "trigger2_thrKSNormIncrease": trigger2_thrKSNormIncrease, # 2nd level threshold
              "CMAP_algorithm":             CMAP_algorithm,         # Flux maps
              "CMAP_binsz":                 CMAP_binsz,             # Flux maps
              "CMAP_nxpix":                 CMAP_nxpix,             # Flux maps
              "CMAP_nypix":                 CMAP_nypix,             # Flux maps
              "CMAP_coordsys":              CMAP_coordsys,          # Flux maps
              "CMAP_xref":                  CMAP_xref,              # Flux maps
              "CMAP_yref":                  CMAP_yref,              # Flux maps
              "CMAP_axisrot":               CMAP_axisrot,           # Flux maps
              "CMAP_proj":                  CMAP_proj,              # Flux maps
              "CMAP_indef":                 CMAP_indef,             # Flux maps
              "expcubes_cmap":              expcubes_cmap,          # gtexpcube2
              "expcubes_ebinalg":           expcubes_ebinalg,       # gtexpcube2
              "expcubes_enumbins":          expcubes_enumbins,      # gtexpcube2
              "sourcename":                 sourcename,             # File management
              "imgsubdir":                  imgsubdir,              # File management
              "imgdir_www":                 imgdir_www,             # File management
              "tempsubdir":                 tempsubdir,             # File management
              "SCdir":                      SCdir,                  # File management
              "PHlistname":                 PHlistname,             # File management
              "SClistname":                 SClistname,             # File management
              "SCfilename":                 SCfilename}             # File management

# Now converting the string in actual json, and then saving
json_string = json.dumps(jsonstring,indent=4)
filename = os.path.join(tempdir,f'configfile_trignum_{trignum}.json')
with open(filename, 'w') as outfile:
    outfile.write(json_string)

logging.info(f"The configuration file was correctly written at the following path:")
logging.info(filename)
logging.info(f"")
logging.info(f"Completed the execution of utils_configcreator.py!")
logging.info("*"*75)