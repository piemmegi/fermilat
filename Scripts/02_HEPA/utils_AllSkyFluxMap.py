'''
# ==========================================================================================
#                                       HEADER
# ==========================================================================================
The aim of this script is to create a flux map of the full sky view, for a given energy
band and time window, with a relatively fine granularity. Note that such parameters are 
not given by command-line, but rather they are read from a specific configuration file
(marked with a "trigger level" value of 0, e.g., "configfile_trignum_0").

This script is essentially called only stand-alone in very specific situations, or it is
called at the end of an iteration on the full sky with the HEPA_skyscanner.py monitor.
In fact, several markers are automatically plotted on the flux map, each one corresponding
to a detected flare (i.e., some flare which passed the 1st level trigger threshold).
Different colours are used depending on the confidence level on the identification of the 
flare/cluster with the 2nd level trigger analysis.

-------------------------------------------------------------------------------------------
Call syntax:
  	python3 utils_AllSkyFluxMap.py sourcename ifexpmap

Call arguments:
	- sourcename			str, name of the subfolders which will be created in the tempdir and imgdir
							  folders, to contain the temporary files and pictures created in the production
							  of the full-sky-view map (default value: "SkyMap")
	- ifexpmap				bool, whether to produce exposure-corrected flux maps (True) or simply count maps
							  (False). It is adviced to do an exposure correction only if a large enough
							  statistics is available, and there is no contraint on the script execution time,
							  since the full-sky-view gtltcube call can be quite long (default value: False).

Example:
	python3 utils_AllSkyFluxMap.py SkyMap False
'''
# ==========================================================================================
#                                 PREAMBLE (module load)
# ==========================================================================================
# Import of the Python Standard Library modules required for the analysis
import glob, os, subprocess, sys         # To act on the operative system
import json                              # To read and write JSON files
import matplotlib as mpl                 # For plotting
import matplotlib.pyplot as plt          # For plotting
import numpy as np                       # For numerical analysis
import pandas as pd                      # For reading and writing csv files
import warnings                          # To deactivate the warnings
import time                              # For script timing
import logging                           # For logging purposes
import datetime                          # For timestamp printing

# Import of the astropy/gammapy/fermipy (& fermitools) modules necessary for astrophysics
from gammapy.maps import Map, WcsNDMap             # To work with sky maps
from astropy.coordinates import SkyCoord # To work with sky frames

# Import of personal functions from custom modules
sys.path.append("../../Functions")
from AstroFunctions import gtselect, gtmktime, gtbin_CMAP, gtltcube, gtexpcube2

# Set the warnings of a few functions
np.seterr(divide='ignore', invalid='ignore') 					# To avoid 0/0 errors
mpl.rcParams['backend'] = 'agg'                                 # Non-interactive backend for plotting
warnings.filterwarnings("ignore", category=DeprecationWarning)  # To avoid deprecation warnings

# =========================================================================================
#                                      COMMON DEFINITIONS                                      
# =========================================================================================
# Define the dimension for the fonts in the plots, the figures dimensions and so on
textfont = 20         # Fontsize for labels and legends
dimfig = (12,7)       # Figure dimensions (A4-like)
dimfigbig = (16,12)   # Figure dimensions (a bit larger than A4-like)
dimfiglong = (16,25)  # Figure dimensions (a bit larger and much longer than A4-like)
filetype = '.pdf'     # File format to save the pictures
filedpi = 520         # Resolution used when saving pictures in .png/.jpeg
nptsfit = 10000       # Number of points used in plotting the fits

# ==========================================================================================
#                                  STATUS FILE DATA
# ==========================================================================================
# Open the status file
statusfile = '../statusfile.json'
with open(statusfile) as json_file:
	statusdata = json.load(json_file)

# Extract all the relevant variables from the file
datadir = statusdata['datadir']
imgdir = statusdata['imgdir']
tempdir = statusdata['tempdir']
dataSCdir = statusdata['dataSCdir']
dataPHdir = statusdata['dataPHdir']
catalog_file_4FGL = statusdata['catalog_file_4FGL']
catalog_file_3FHL = statusdata['catalog_file_3FHL']

# Define the name for the subfolders (in imgdir and tempdir) where we will save
# the temporary images and files
pretempdir = '02_HEPA'
imgdir = os.path.join(imgdir,pretempdir)
tempdir = os.path.join(tempdir,pretempdir)

# Setting of the logging module
logfile = "HEPA_debug.log"
logging.basicConfig(format='[%(asctime)s] %(message)s',level=logging.INFO,
					handlers=[logging.FileHandler(logfile,mode='a'),
				        	  logging.StreamHandler()])
logging.info("*"*75)
logging.info("Beginning execution of utils_AllSkyFluxMap.py ...")
logging.info(f"")

# ==========================================================================================
#                                 CONFIG FILE DATA
# ==========================================================================================
# Open the configuration file made for the 1st level trigger and extract the variables
# necessary for the analysis
configfile = os.path.join(tempdir,f'configfile_trignum_0.json')
with open(configfile) as json_file:
	configdata = json.load(json_file)

# Extract all the relevant variables from the file
tstart               = configdata['tstart']                	# gtselect
tstop                = configdata['tstop']                 	# gtselect 
Emin                 = configdata['Emin']                  	# gtselect
Emax                 = configdata['Emax']                  	# gtselect
zmax                 = configdata['zmax']                  	# gtselect
evclass              = configdata['evclass']               	# gtselect
evtype               = configdata['evtype']                	# gtselect
roicut               = configdata['roicut']                	# gtmktime
SUNsepmin            = configdata['SUNsepmin']             	# gtmktime
filtercond           = configdata['filtercond']            	# gtmktime
dcostheta            = configdata['dcostheta']             	# gtltcube
binsz                = configdata['binsz']                 	# gtexpmap
chatter              = configdata['chatter']               	# All fermitools
gtmode               = configdata['gtmode']                	# All fermitools
PHlistname           = configdata['PHlistname']            	# File management
SClistname           = configdata['SClistname']            	# File management
SCfilename           = configdata['SCfilename']            	# File management
imgsubdir            = configdata['imgsubdir']             	# File management
imgdir_www           = configdata['imgdir_www']             # File management

CMAP_algorithm       = configdata['CMAP_algorithm']     	# Flux maps
CMAP_binsz           = configdata['CMAP_binsz']       		# Flux maps
CMAP_nxpix           = configdata['CMAP_nxpix']        		# Flux maps
CMAP_nypix           = configdata['CMAP_nypix']        		# Flux maps
CMAP_coordsys        = configdata['CMAP_coordsys']        	# Flux maps
CMAP_xref            = configdata['CMAP_xref']        		# Flux maps
CMAP_yref            = configdata['CMAP_yref']        		# Flux maps
CMAP_axisrot         = configdata['CMAP_axisrot']        	# Flux maps
CMAP_proj            = configdata['CMAP_proj']        		# Flux maps
CMAP_indef			 = configdata['CMAP_indef']             # Flux maps
expmap_evtype        = configdata['expmap_evtype']          # gtexpcube2
expmap_irfs          = configdata['expmap_irfs']            # gtexpcube2
expcubes_cmap 		 = configdata['expcubes_cmap']          # gtexpcube2
expcubes_ebinalg     = configdata['expcubes_ebinalg']       # gtexpcube2
expcubes_enumbins    = configdata['expcubes_enumbins']      # gtexpcube2

# ==========================================================================================
#                                 INPUT DATA
# ==========================================================================================
# Read from command line the name of the subfolder where we can set the temporary files
try:
	sourcename = sys.argv[1]
except:
	sourcename = "SkyMap"
	logging.info(f"Input problem --- Defaulting variable 'sourcename' to '{sourcename}'")
	logging.info(f"")

# Read from command line whether to show a count map or a flux map
try:
	ifexpmap = (sys.argv[2] == 'True')
except:
	ifexpmap = False
	logging.info(f"Input problem --- Defaulting variable 'ifexpmap' to '{ifexpmap}'")
	logging.info(f"")

# ==========================================================================================
#                                 TEMPORARY FILE NAMES
# ==========================================================================================
# Define the names of the .fits, .txt, .xml and .npz files which will be produced in the execution
data_PH_after_gtselect   = os.path.join(tempdir,f"{sourcename}",f"FluxMaps_after_gtselect.fits")
data_PH_after_gtmktime   = os.path.join(tempdir,f"{sourcename}",f"FluxMaps_after_gtmktime.fits")
data_PH_after_gtbin      = os.path.join(tempdir,f"{sourcename}",f"FluxMaps_after_gtbin.fits")
if ifexpmap:
	data_PH_after_gtltcube   = os.path.join(tempdir,f"{sourcename}",f"FluxMaps_after_gtltcube.fits")
	data_PH_after_gtexpcube2 = os.path.join(tempdir,f"{sourcename}",f"FluxMaps_after_gtexpcube2.fits")

# ==========================================================================================
#                                   DATA EXTRACTION
# ==========================================================================================
# Call gtselect to extract the data in the given ROI, energy band and temporal window
ifDoAnyways = True
if ifDoAnyways:
	logging.info(f"Calling gtselect...")
	gtselect(infile=PHlistname,outfile=data_PH_after_gtselect,
			 ROI_ra=CMAP_indef,ROI_dec=CMAP_indef,ROI_rad=CMAP_indef,Emin=Emin,Emax=Emax,
			 zmax=zmax,tstart=tstart,tstop=tstop,evclass=evclass,evtype=evtype,
			 chatter=chatter,gtmode=gtmode)
	logging.info(f"Done!")
	logging.info(f"")
else:
	logging.info(f"Skipping gtselect...")
	logging.info(f"")

# Call gtmaketime to adjust the GTIs following the chosen ROI and other conditions
ifDoAnyways = True
if ifDoAnyways:
	logging.info(f"Calling gtmktime...")
	gtmktime(infile=data_PH_after_gtselect,outfile=data_PH_after_gtmktime,SCfile=SCfilename,
			 ROI_ra=CMAP_indef,ROI_dec=CMAP_indef,roicut=roicut,SUNsep=None,
	         filtercond=filtercond,chatter=chatter,gtmode=gtmode)
	logging.info(f"Done!")
	logging.info(f"")
else:
	logging.info(f"Skipping gtmktime...")
	logging.info(f"")

# ==========================================================================================
#                        COUNT MAP PRODUCTION AND EXPOSURE CORRECTION
# ==========================================================================================
# Call gtbin to produce a count map of the sky
ifDoAnyways = True
if ifDoAnyways:
	logging.info(f"Calling gtbin...")
	gtbin_CMAP(infile=data_PH_after_gtmktime,outfile=data_PH_after_gtbin,SCfile=SCfilename,
			   algorithm=CMAP_algorithm,nxpix=CMAP_nxpix,nypix=CMAP_nypix,binsz=CMAP_binsz,
			   coordsys=CMAP_coordsys,xref=CMAP_xref,yref=CMAP_yref,axisrot=CMAP_axisrot,
			   proj=CMAP_proj,chatter=chatter,gtmode=gtmode)
	logging.info(f"Done!")
	logging.info(f"")
else:
	logging.info(f"Skipping gtbin...")
	logging.info(f"")

# If it is necessary, do the exposure correction
if ifexpmap:
	# Call gtltcube to produce a map of the LAT livetime
	ifDoAnyways = True
	if ifDoAnyways:
		logging.info(f"Calling gtltcube...")
		gtltcube(infile=data_PH_after_gtmktime,outfile=data_PH_after_gtltcube,SCfile=SCfilename,
	    	     zmax=zmax,dcostheta=dcostheta,binsz=binsz,chatter=chatter,gtmode=gtmode)
		logging.info(f"Done!")
		logging.info(f"")
	else:
		logging.info(f"Skipping gtltcube...")
		logging.info(f"")

	# Call gtexpcube2 to produce an exposure map of the LAT
	ifDoAnyways = True
	if ifDoAnyways:
		logging.info(f"Calling gtexpcube2...")
		gtexpcube2(infile=data_PH_after_gtltcube,outfile=data_PH_after_gtexpcube2,cmap=expcubes_cmap,
			       irfs=expmap_irfs,evtype=expmap_evtype,nxpix=CMAP_nxpix,nypix=CMAP_nypix,binsz=CMAP_binsz,
			       coordsys=CMAP_coordsys,xref=CMAP_xref,yref=CMAP_yref,axisrot=CMAP_axisrot,
			       proj=CMAP_proj,ebinalg=expcubes_ebinalg,Emin=Emin,Emax=Emax,enumbins=expcubes_enumbins,
			       chatter=chatter,gtmode=gtmode)
		logging.info(f"Done!")
		logging.info(f"")
	else:
		logging.info(f"Skipping gtexpcube2...")
		logging.info(f"")

# ==========================================================================================
#			                        COUNT MAP EXTRACTION
# ==========================================================================================
# Open the output files from the previous steps and compute the count or flux map
#countmap_skyview = Map.read(data_PH_after_gtbin)
countmap_skyview = WcsNDMap.read(data_PH_after_gtbin)
if ifexpmap:
	#expmap_skyview = Map.read(data_PH_after_gtexpcube2).sum_over_axes(keepdims=False)
	expmap_skyview = WcsNDMap.read(data_PH_after_gtexpcube2).sum_over_axes(keepdims=False)
	fluxmap_data = countmap_skyview.data / expmap_skyview.data

	# Remove non numerical values from the flux map (e.g., the output of 0/0 calculations)
	badinds = (np.isinf(fluxmap_data)) | (np.isnan(fluxmap_data))
	fluxmap_data[badinds] = 0

	# Associate the flux map to a proper gammapy object 
	fluxmap_skyview = copy.deepcopy(countmap_skyview)
	fluxmap_skyview.data = fluxmap_data

# Possibly apply a smoothing due to the LAT PSF at these energies
LAT_PSF = 0.1
ifsmooth = True
if ifexpmap:
	showmap = fluxmap_skyview
	if ifsmooth:
	    showmap = showmap.smooth(LAT_PSF,kernel='gauss')
else:
	showmap = countmap_skyview
	if ifsmooth:
	    showmap = showmap.smooth(LAT_PSF,kernel='gauss')	    

# ==========================================================================================
#                                       FLARE DETERMINATION
# ==========================================================================================
# Determine the flares which were identified with the 2nd level trigger analysis.
filesToBeOpened = glob.glob(tempdir + f'/**/flare_*.npz',recursive=True)
all_flares_flarecond = np.array([],dtype=np.float64)
all_flares_bLAT = np.array([],dtype=np.float64)
all_flares_bLONG = np.array([],dtype=np.float64)

for file in filesToBeOpened:
	# Open the file containing the details about each available flare. There is one
	# for each time the 1st level trigger threshold was over-passed.
	with np.load(file,allow_pickle=True) as f:
		# Load the flag telling if the 2nd level trigger threshold was passed and the ROI
		# center (to be converted from RA/DEC to Galactic coordinates)
		all_flares_flarecond = np.hstack((all_flares_flarecond,np.array(f['flarecond'])))
		cord = SkyCoord(f['ROI_ra'],f['ROI_dec'], frame='icrs', unit='deg')
		all_flares_bLAT = np.hstack((all_flares_bLAT,cord.galactic.b.deg))
		all_flares_bLONG = np.hstack((all_flares_bLONG,cord.galactic.l.deg))

# Extract two separated sets of arrays, one for the points above the 2nd level threshold
# and one for the points below. In this way we will be able to plot them differently.
AT_cond = (all_flares_flarecond == True) 		# AT = Above Threshold
AT_flares_bLAT = all_flares_bLAT[AT_cond]
AT_flares_bLONG = all_flares_bLONG[AT_cond]
AT_numflares = np.sum(AT_cond)

BT_cond = (all_flares_flarecond == False)		# BT = Below Threshold
BT_flares_bLAT = all_flares_bLAT[BT_cond]
BT_flares_bLONG = all_flares_bLONG[BT_cond]
BT_numflares = np.sum(BT_cond)

# ==========================================================================================
#                                       PLOTTING
# ==========================================================================================
# Show the flux or count map
logging.info(f"Now it's plotting time...")
figname = os.path.join(imgsubdir,f'SkyViewMap' + filetype)

fig = plt.figure(figsize = dimfigbig)
ax = showmap.plot(fig = fig, stretch="log", vmax=np.max(showmap.data), cmap='gnuplot2',
				  add_cbar = False)#, kwargs_colorbar={'fraction':0.5,'panchor':(1.0,0.8)})

# Modify the colorbar
if ifexpmap:
	cbarlabel = r"Flux [photons $cm^{-2}$ $s^{-1}$]"
else:
	cbarlabel = r"Flux [counts / bin]"

thisImage = ax.get_images()[0]
cbar = fig.colorbar(thisImage, ax=ax,shrink=0.6)
cbar.ax.tick_params(labelsize=textfont*0.75)
cbar.set_label(cbarlabel,fontsize=textfont,rotation=270,labelpad=50)
#cbar.orientation = 'horizontal'
#print(cbar.ax.get_anchor())

# Show the flares
AT_color = 'yellow'           # Flares with high TS
BT_color = 'cyan'             # Flares with low TS (but higher than the gttsmap threshold)

if (BT_numflares > 0):
	ax.plot(BT_flares_bLONG,BT_flares_bLAT,color=BT_color,linestyle = '', marker = 'x',
			markersize = 15, label = 'Flares below the 2nd level threshold', transform=ax.get_transform("galactic"))
if (AT_numflares > 0):
	ax.plot(AT_flares_bLONG,AT_flares_bLAT,color=AT_color,linestyle = '', marker = '*',
			markersize = 12.5, label = 'Flares above the 2nd level threshold', transform=ax.get_transform("galactic"))

# Refine the graphics and save the image
lat = ax.coords['glat']
lon = ax.coords['glon']
lon.set_axislabel('Galactic Longitude [deg]',minpad=12,fontsize=textfont)
lat.set_axislabel('Galactic Latitude [deg]',fontsize=textfont)
if (AT_numflares > 0) | (BT_numflares > 0):
	leg = ax.legend(loc=(-0.1,-0.2),fontsize=0.6*textfont,framealpha=1,facecolor='thistle',edgecolor='black')
	numhandles = len(leg.legendHandles)
	for i in range(numhandles):
		leg.legendHandles[i]._markersize = 10

# Set the title adding the current timestamp
time_now = time.localtime()
time_readable = time.strftime("%H:%M:%S", time_now)
ax.set_title(f"{datetime.date.today()}, {time_readable}",fontsize=textfont*1.5,pad=30)

# Refine the graphics and save the output
ax.tick_params(labelsize = textfont)
fig.set_tight_layout('tight')
fig.savefig(figname,dpi=filedpi,bbox_inches='tight',pad_inches=0.05)
if False & (imgdir_www is not None):
	# If the web directory where the image files can be saved exists, then save the figure also there
	figname_www = os.path.join(imgdir_www,f'SkyViewMap')
	fig.savefig(figname_www + '.pdf',dpi=filedpi,bbox_inches='tight',pad_inches=0.05)
	fig.savefig(figname_www + '.png',dpi=filedpi,bbox_inches='tight',pad_inches=0.05)

plt.close(fig)

# Final printouts
logging.info(f"")
logging.info(f"Completed the execution of utils_AllSkyFluxMap.py!")
logging.info("*"*75)