'''
# ==========================================================================================
#                                       HEADER
# ==========================================================================================
The aim of this script is to perform an ordered call to the HEPA pipeline while scanning
over the entire sky, either in a fixed time period, or with live updates on the present.

-------------------------------------------------------------------------------------------
Call syntax:
  	python3 HEPA_skyscanner.py scanmode numweeks/timelist Emin gridtype ifdebug

Call arguments:
	[1] scanmode        str, the type of scan to perform on the sky. With the option "0" or "LM"
                          (Live Monitor) we take a look to the data acquired by the LAT in the last
                          few weeks and we continue repeating the analysis every time it finishes,
                          so that this is actually a Live Monitor. With the option "1" or "FT" 
                          (Fixed Time), instead, we take a look to a specific time window just once.
                          (default value: "LM")
	[2] numweeks        int, the number of weeks to look at in each iteration of the sky scanner
                          (default value: 13, i.e., 3 months). Note that this argument is read in this
                          way only if scanmode is set to LM, otherwise "timelist" takes precedence.
	[2] timelist        list of floats, containing the start and stop of the time window to be analyzed.
                          The times may be expressed either as METs or as mission weeks: the configcreator
                          will parse the input and determine which is which (if unset, defaults to LM scanmode
                          with 13 weeks of data taking). Note that this argument is read in this way 
                          only if scanmode is set to FT, otherwise "numweeks" takes precedence.
	[3] Emin            int, the minimum energy of the analyzed photons, in GeV (default value: 10 GeV)
	[4] gridtype        str, the name of the grid to be used. If the value is set to "FullSkyGrid" or unset,
                          then a custom grid is created, with ROIs that cover the entire sky view. Otherwise,
                          it is assumed that the value is the name (with full path but with no extension)
                          of a .csv file, which contains in each row a different source of the grid.
                          Note that the syntax of these files should be the same of the default grid file
                          (i.e., there should be the same columns as the default) (default value: "FullSkyGrid")
	[5] ifdebug         bool, if the script should be launched in debug mode (default value: False).
                          Note that this changes a lot how the script works. For instance, we override the
                          parameter "gridtype" and set the grid to a custom set of two sources in the sky.

Example of call in "LM" mode with a 3 months time window and a 10 GeV energy cut:
    python3 HEPA_skyscanner.py LM 13 10 FullSkyGrid False

Example of call in "FT" mode with a fixed time window (given in weeks) and a 10 GeV energy cut:
    python3 HEPA_skyscanner.py FT [9,22] 10 FullSkyGrid False

Example of call in debug mode:
    python3 HEPA_skyscanner.py FT [233,259] 10 FullSkyGrid True
    python3 HEPA_skyscanner.py LM 13 10 FullSkyGrid True

'''
# ==========================================================================================
#                                 PREAMBLE (module load)
# ==========================================================================================
# Import of the Python Standard Library modules required for the analysis
import glob, os, subprocess, sys		# To act on the operative system
import json								# To read and write JSON files
import matplotlib as mpl				# For plotting
import matplotlib.pyplot as plt			# For plotting
import numpy as np						# For numerical analysis
import pandas as pd						# For reading and writing csv files
import warnings							# To deactivate the warnings
import time								# For script timing
import logging							# For logging purposes
import shutil							# For copying files
import PyPDF2							# For merging pdf files
import datetime                         # For timestamp printing

# Import of personal functions from custom modules
sys.path.append("../../Functions")
from MyFunctions import str2list
from AstroFunctions import skygridbuilder, J2000formatter

# Set the warnings of a few functions
np.seterr(divide='ignore', invalid='ignore') 					# To avoid 0/0 errors
mpl.rcParams['backend'] = 'agg'                                 # Non-interactive backend for plotting
warnings.filterwarnings("ignore", category=DeprecationWarning)  # To avoid deprecation warnings

# =========================================================================================
#                                      COMMON DEFINITIONS                                      
# =========================================================================================
# Define the dimension for the fonts in the plots, the figures dimensions and so on
textfont = 20         # Fontsize for labels and legends
dimfig = (12,7)       # Figure dimensions (A4-like)
dimfigbig = (16,12)   # Figure dimensions (a bit larger than A4-like)
dimfiglong = (16,25)  # Figure dimensions (a bit larger and much longer than A4-like)
filetype = '.pdf'     # File format to save the pictures
filedpi = 520         # Resolution used when saving pictures in .png/.jpeg
nptsfit = 10000       # Number of points used in plotting the fits

# ==========================================================================================
#                                  STATUS FILE DATA
# ==========================================================================================
# Open the status file
statusfile = '../statusfile.json'
with open(statusfile) as json_file:
	statusdata = json.load(json_file)

# Extract all the relevant variables from the file
datadir = statusdata['datadir']
imgdir = statusdata['imgdir']
tempdir = statusdata['tempdir']
dataSCdir = statusdata['dataSCdir']
dataPHdir = statusdata['dataPHdir']
catalog_file_4FGL = statusdata['catalog_file_4FGL']
catalog_file_3FHL = statusdata['catalog_file_3FHL']

# Define the name for the subfolders (in imgdir and tempdir) where we will save
# the temporary images and files
pretempdir = '02_HEPA'
imgdir = os.path.join(imgdir,pretempdir)
tempdir = os.path.join(tempdir,pretempdir)

# Setting of the logging module
logfile = "HEPA_debug.log"
if os.path.exists(logfile):
	os.remove(logfile)

logging.basicConfig(format='[%(asctime)s] %(message)s',level=logging.WARNING,
					handlers=[logging.FileHandler(logfile,mode='a'),
				        	  logging.StreamHandler()])
logging.info("*"*75)
logging.info("Beginning execution of HEPA_skyscanner.py ...")
logging.info(f"")

# ==========================================================================================
#                                 INPUT DATA
# ==========================================================================================
# Parse the user input data and determine how this monitor should operate:
# - by examining the present day with continuous updates ("Live Monitor" -> "LM" or "0")
# - by examining a well-defined past time period ("Fixed Time" -> "FT" or "1")
#
# Depending on the value of scanmode, define a temporal range:
# - In LM mode, we only need to know how many weeks to look, starting from "today"
# - In FT mode, we need to know both the beginning and end of the time window
#
# Note that in the second case we can accept either METs or week numbers,
# but we don't have to determine which is which here, since this job is up to the
# configuration file creator script (configcreator.py)
try:
	if (sys.argv[1] == '0') | (sys.argv[1] == 'LM'):
		# Live Monitor option: retrieve the number of weeks to look at
		scanmode = 'LM'
		numweeks = int(sys.argv[2])
	elif (sys.argv[1] == '1') | (sys.argv[1] == 'FT'):
		# Fixed Time option: retrieve the start and stop of the time window
		scanmode = 'FT'
		timelist = str2list(sys.argv[2],int)
		tstart, tstop = timelist[0], timelist[-1]
	else:
		scanmode = 'LM'
		numweeks = 13
		logging.warning(f"Input problem --- Defaulting variable 'scanmode' to '{scanmode} and 'numweeks' to '{numweeks}'")
		logging.warning(f"")
except:
	scanmode = 'LM'
	numweeks = 13
	logging.warning(f"Input problem --- Defaulting variable 'scanmode' to '{scanmode} and 'numweeks' to '{numweeks}'")
	logging.warning(f"")

# Define the energy threshold for the photon analysis
try:
	Emin = float(sys.argv[3])
except:
	Emin = 10
	logging.warning(f"Input problem --- Defaulting variable 'Emin' to '{Emin}'")
	logging.warning(f"")

# Define the grid type: use a custom full-sky-view, or something else? In the second case,
# there should be a csv file with the grid details and the same name as "gridtype"
try:
	if sys.argv[4] == 'None':
		gridtype = 'FullSkyGrid'
	else:
		gridtype = sys.argv[4]
except:
	gridtype = 'FullSkyGrid'
	logging.warning(f"Input problem --- Defaulting variable 'gridtype' to '{gridtype}'")
	logging.warning(f"")

# Define if we are in debug mode
try:
	ifdebug = (sys.argv[5] == 'True')
except:
	ifdebug = False
	logging.warning(f"Input problem --- Defaulting variable 'ifdebug' to '{ifdebug}'")
	logging.warning(f"")


# ==========================================================================================
#                                  TIMING AND CLEANUP
# ==========================================================================================
# Begin the timing of the script. We will use these informations to monitor the performance
# of this pipeline.
pytiming_t0 = time.perf_counter()

# Now clean up the temporary and figure folders
subprocess.run(['python3','utils_tempcleaner.py'])

# ==========================================================================================
#                                  SKY GRID
# ==========================================================================================
# Switch on the type of grid to be created. Note that the syntax for the grid columns
# is the same in all cases.
colnames = ['ROI_ra_deg','ROI_dec_deg','ROI_blat_deg','ROI_blong_deg','ROI_rad_deg','Av_FD','Av_LC']
ifCompute = True       # If the grid is "FullSkyGrid", should the rates be calculated (True) or loaded
					   # from the file (False)?

if ifCompute & (gridtype == 'FullSkyGrid'):
	# Create a grid to cover the full sky view using an arbitrary setting. This grid is composed
	# through the "golden spiral" method, see the documentation of skygridbuilder (from AstroFunctions)
	# for more details about that. Anyways, the idea is simple: we generate a grid of points 
	# almost-equally spaced in the sky, and then assign a radius to each one, which is constant 
	# and weakly larger than the minimum required to fully cover the sky.
	#
	# First of all, instantiate the parameters for skygridbuilder.
	grid_n = 750                  # Number of points in the grid
	grid_epsilon = -0.5           # Regularization factor (keep negative!)
	grid_radvalue = 5             # Radius
	
	# Now create the grid. Note that the outputs of "skygridbuilder" are all 1D np.arrays
	ROI_ra_deg, ROI_dec_deg, ROI_blong_deg, ROI_blat_deg, ROI_rad_deg = skygridbuilder(grid_n,grid_epsilon,grid_radvalue)
	sourcenames = [J2000formatter(ROI_ra_deg[i],ROI_dec_deg[i]) for i in range(grid_n)]

	# The sky grid is associated to a .csv file, which stores in each line the coordinate of
	# one of the grid centers (both in RA/DEC and bLAT/bLONG), the radius of the circular ROI
	# centered there, and the average FD and LC rate there measured. Such file must be created
	# here, overwriting any pre-existing file, to avoid conflicts with remnants of older runs.
	#
	# First of all, define the names of the columns in the csv file. The row names are simply
	# the names of the sources (see above)
	gridfile = 'SkyGridDatabase.csv'

	# Create the pandas dataframe associated to the csv file and fill out its columns correctly
	df = pd.DataFrame(data = None,index = sourcenames, columns = colnames, dtype = np.float64)
	df.loc[:,'ROI_ra_deg']    = ROI_ra_deg
	df.loc[:,'ROI_dec_deg']   = ROI_dec_deg
	df.loc[:,'ROI_blat_deg']  = ROI_blat_deg
	df.loc[:,'ROI_blong_deg'] = ROI_blong_deg
	df.loc[:,'ROI_rad_deg']   = ROI_rad_deg
	df.loc[:,'Av_FD']         = np.zeros((grid_n,))  # Instantiate by default to 0
	df.loc[:,'Av_LC']         = np.zeros((grid_n,))

	# Write the csv file on disk
	df.to_csv(gridfile,sep=';',float_format='%5.15f')
	logging.warning(f"The full-sky-view grid was created and saved at the following path:")
	logging.warning(f"{gridfile}")
	logging.warning(f"")
else:
	# Read the grid from an external csv file. Note that if the "ifCompute" flag is set
	# to False, then this part of the script is executed even if the name of the file
	# is "FullSkyGrid".
	gridfile = gridtype + '.csv'
	df = pd.read_csv(gridfile,header=0,sep=';',index_col=0)

	# Extract the content of each column
	ROI_ra_deg    = np.array(df.loc[:,'ROI_ra_deg'],dtype=np.float64)
	ROI_dec_deg   = np.array(df.loc[:,'ROI_dec_deg'],dtype=np.float64)
	ROI_blat_deg  = np.array(df.loc[:,'ROI_blat_deg'],dtype=np.float64)
	ROI_blong_deg = np.array(df.loc[:,'ROI_blong_deg'],dtype=np.float64)
	ROI_rad_deg   = np.array(df.loc[:,'ROI_rad_deg'],dtype=np.float64)
	sourcenames   = df.index.to_list()
	grid_n = len(sourcenames)
	logging.warning(f"The custom sky grid was correctly loaded from the {gridfile} file")


# In debug mode, we want to overwrite the grid parameters and iterate on a custom 
# test grid
if ifdebug:
	# Fall in debug mode and set the grid to a list of known sources
	grid_n = 2
	ROI_ra_deg = [173.15, 173.15]
	ROI_dec_deg = [27.69, 27.69]
	ROI_blat_deg = [-7.51,-7.51]
	ROI_blong_deg = [176.5,176.5]
	ROI_rad_deg = [5,5]
	sourcenames = ['GRB_130427A_1','GRB_130427A_2']


# ==========================================================================================
#                             BACKGROUND RATES ANALYSIS
# ==========================================================================================
# Call the configuration script for a 1st level trigger analysis and retrieve from it
# an important information: whether the background rates should be computed on-the-fly
# for each ROI, or should be pre-determined. Note that here we use random values for
# several inputs (energy, time window, ROI center and extension) because we are only looking
# for the parameter named "ifLoadAverageRates", which is hard-coded in the config file.
logging.warning(f"Reading from the configuration file whether to compute or load the background rates in the 1st level trigger analysis...")
subprocess.run(['python3','utils_configcreator.py',f'{Emin}','-1','-1',
												   f'0','0','5',
												   '1','Scratch',f"{gridfile}"])

# Now load the configuration file and extract some relevant parameters
configfile = os.path.join(tempdir,f'configfile_trignum_1.json')
with open(configfile) as json_file:
	configdata = json.load(json_file)
	ifLoadAverageRates = configdata['ifLoadAverageRates']  # Load average rate from external source?
	SCdir = configdata['SCdir']                            # Where is the SC merged filed stored?
	imgdir_www = configdata['imgdir_www']                  # Where will we save the merged figure file?
	Emax = configdata['Emax'] / 1e3                        # Maximum energy for the analysis, in GeV
	if ifLoadAverageRates:
		# If needed, load the time window for the calculation of the background
		# (check configcreator to see what it is)
		bkg_tstart = configdata['bkg_tstart']  
		bkg_tstop = configdata['bkg_tstop']

logging.warning(f"Result: 'ifLoadAverageRates' is set to '{ifLoadAverageRates}'")

# If the background rates must be computed on-the-fly, then do nothing. But if they
# must be computed beforehand and then loaded in the script, we have to compute them NOW.
# This means iterating on every ROI available and calling the 1st level trigger analysis
# without saving the output images.
if ifCompute & ifLoadAverageRates:
	# Define some preliminary parameters
	ifplot = False        # While computing the rates, we do not need to produce any plot
	ifCalculatingBKG = True
	extensionsToBeCleaned = ['fits','reg','xml','list'] # Tempfiles with these exts will be cleared at each call

	# Cycle on each ROI
	logging.warning(f"Beginning the calculation of the background rates...")
	logging.warning(f"(ignore any interval apparently flagged for the 2nd level trigger analysis - we will not do that)")
	for i in range(grid_n):
		# Update log
		logging.warning(f"Checking ROI n. {i} / {grid_n-1} ({sourcenames[i]})")

		# Produce the configuration file necessary for the analysis
		subprocess.run(['python3','utils_configcreator.py',f'{Emin}',f'{bkg_tstart}',f'{bkg_tstop}',
											f'{ROI_ra_deg[i]}',f'{ROI_dec_deg[i]}',f'{ROI_rad_deg[i]}',
											'1',f'{sourcenames[i]}',f"{gridfile}"])

		# Call the 1st level trigger analysis to measure the FD and LC average rates
		subprocess.run(['python3','trigger_lv1.py',f'{ifplot}',f"{ifCalculatingBKG}"])

		# Remove the temporary files here produced to avoid conflicts 
		for ext in extensionsToBeCleaned:
			regexFilesToBeCleaned = os.path.join(tempdir,sourcenames[i],f'*.{ext}')
			allFilesToBeCleaned = glob.glob(regexFilesToBeCleaned)
			logging.info(f"Removing files with the signature (regex): {regexFilesToBeCleaned}")
			for fileToBeCleaned in allFilesToBeCleaned:
				os.remove(fileToBeCleaned)

	# Update log
	logging.warning(f"Background estimation completed!")
else:
	# Update log
	if ifLoadAverageRates:
		logging.warning(f"Background estimation is skipped: we will use the pre-computed values from the grid file...")
	else:
		logging.warning(f"Background estimation is skipped: we will calculate the background rates with the data...")

# Update the timing and the log
pytiming_t1 = time.perf_counter()
delta = pytiming_t1-pytiming_t0
logging.warning(f"")
logging.warning(f"Elapsed time for grid creation and background estimation (if done): {delta:.2f} s")
for _ in range(10):
	logging.warning(f"")

# ==========================================================================================
#                                   SKY SCANNER LOOP
# ==========================================================================================
# Now we can go with the actual scanner loop. First of all, define some useful parameter.
urlPHfiles = 'https://heasarc.gsfc.nasa.gov/FTP/fermi/data/lat/weekly/photon/'      # Web path of the weekly photon files
urlSCfiles = 'https://heasarc.gsfc.nasa.gov/FTP/fermi/data/lat/weekly/spacecraft/'  # Web path of the weekly spacecraft files
tablefile = os.path.join(tempdir,'GTIdict.npz')                 # Local path of the GTI table file
outputfile = os.path.join(tempdir,'outputfile.txt')             # Local path of the final output file
tempdir_www = '/eos/user/p/pmontigu/www/Subpages/FermiLAT/'     # Location where the file should be copied to be seen in the HTML
outputfile_www = os.path.join(tempdir_www,'outputfile.txt')     # Copied output file (in the HTML directory)
logfile_www = os.path.join(tempdir_www,'HEPA_debug.log')        # Copied log file (in the HTML directory)
outputimg_www = os.path.join(imgdir_www,'outputimg.pdf')        # Folder where the picture output file will be stored
sleeptime_minutes = 1                                           # How many minutes to sleep between two full-sky analyses?
minutes_to_seconds = 60                                         # How many seconds in a minute?
extensionsToBeCleaned = ['fits','reg','list'] 					# Temporary files with these extensions will be cleared at each call
ifexpmap = False                                                # True = final plot is a flux full-sky map; False = Count map
skymapsourcename = "SkyMap"                                     # Name of the subfolder where we put the picture and temp files of the sky map
breakFile = './*.done'                                          # If this file exists, the script closes after the current cycle
timethr = 1e4                                                   # Threshold for determining if a timestamp is MET or week

# Now let's go
logging.warning(f"")
logging.warning(f"Starting the cycle...")
doloop = True    # Cycle flag
ScanIndex = 0    # How many iterations we have done so far

while doloop:
	# Embed the full sky view in a try/except call to accept interruptions from CTRL+C commands
	# or other exceptions
	logging.warning(f"-"*75)
	try:
		# ==========================================================================================
		#                                 PRELIMINARY INFO
		# ==========================================================================================
		logging.warning(f"Starting the {ScanIndex}(th) round of the full-sky view")

		# Begin the timing
		pytiming_t1 = time.perf_counter()

		# Use wget to get any new weekly file and update the older ones, which may have
		# been modified in the meantime
		logging.warning(f"wget-ting new weekly photon and spacecraft files...")
		if not ifdebug:
			subprocess.run(['wget','-m','-P',dataPHdir,'-nH','--cut-dirs=6','-np','-e','robots=off','-N',urlPHfiles])
			subprocess.run(['wget','-m','-P',dataSCdir,'-nH','--cut-dirs=6','-np','-e','robots=off','-N',urlSCfiles])

		# Update the gtidict file to include the new start and stop of each week
		logging.warning(f"Updating the GTI tables...")
		subprocess.run(['python3','utils_GTIanalyzer.py'])

		# Determine the time extremes for the analysis, if the mode is "Live Monitor".
		# This is not necessary for the "Fixed Time" mode, since in that case the values
		# of both tstart and tstop were computed at the beginning of this script
		if scanmode == 'LM':
			# Open the gtidict file to retrieve the last week available in the data
			with np.load(tablefile) as file:
				weeknumbers = file['weeknumbers'] # These are all floating point np.arrays
				firstGTIs = file['firstGTIs']
				lastGTIs = file['lastGTIs']
			
			# Determine the first and last week to analyze
			tstop = np.max(weeknumbers)
			tstart = tstop - numweeks

		# Determine the type of time window
		if (tstart >= timethr) | (tstop >= timethr):
			timetype = "[MET]"
		else:
			timetype = "[weeks]"
		
		# ==========================================================================================
		#                                   GRID ITERATION
		# ==========================================================================================
		# Now iterate over every ROI in the grid. Each time we will call the entire HEPA
		# pipeline and retrieve the outputs.
		for i in range(grid_n):
			# Update log
			logging.warning(f"Checking ROI n. {i} / {grid_n-1} ({sourcenames[i]})")

			# Call the HEPA pipeline script, passing the correct parameters for the space, time and 
			# energy selection cuts. Note that we will need the creation of the config file only
			# in the first time that we perform a 1st level trigger analysis. Afterwards, instead,
			# we can just modify the pre-existing file and update some parameter.
			subprocess.run(['python3','HEPA_singlepoint.py',f"{Emin}",f"{tstart}",f"{tstop}",
								f"{ROI_ra_deg[i]}",f"{ROI_dec_deg[i]}",f"{ROI_rad_deg[i]}",
								f"{sourcenames[i]}",f"{gridfile}"])

			# At the end of the analysis, it is better to remove some files from
			# the temporary folder, to avoid cluttering the disk. This is also
			# useful to avoid using already-compiled xml files in the 2nd level
			# trigger analysis...
			logging.info(f"Clearing the temporary directory for this source from some clutter...")
			logging.info(f"")
			for ext in extensionsToBeCleaned:
				regexFilesToBeCleaned = os.path.join(tempdir,sourcenames[i],f'*.{ext}')
				allFilesToBeCleaned = glob.glob(regexFilesToBeCleaned)
				logging.info(f"Removing files with the signature (regex): {regexFilesToBeCleaned}")
				for fileToBeCleaned in allFilesToBeCleaned:
					if not ifdebug:
						os.remove(fileToBeCleaned)

			# Put some space in the log
			for _ in range(10):
				logging.info(f"")
		logging.warning(f"Grid analysis completed!")
		logging.warning(f"")

		# Now the scan has ended: we should remove the spacecraft file artificially created through
		# fmerge. This is necessary to make sure that the file is always created before the loop
		# and thus is always up-to-date.
		regexFilesToBeCleaned = os.path.join(SCdir,f'*.fits')
		allFilesToBeCleaned = glob.glob(regexFilesToBeCleaned)
		logging.info(f"Removing files with the signature (regex): {regexFilesToBeCleaned}")
		for fileToBeCleaned in allFilesToBeCleaned:
			if not ifdebug:
				os.remove(fileToBeCleaned)
		logging.info(f"")

		# ==========================================================================================
		#                                   OUTPUTS
		# ==========================================================================================
		# Now iterate over every source which passed the 1st level trigger and the gttsmap threshold
		# in the 2nd level analysis. For each of them, we load the output text file and merge it to
		# a global output.
		logging.warning(f"Merging the outputs from the grid...")
		filesContent = []
		filesToBeOpened = glob.glob(tempdir + f'/**/allflares_output.txt',recursive=True)
		with open(outputfile,'w',encoding="utf-8") as outfile:
			# Determine the current time
			time_now = time.localtime()
			time_readable = time.strftime("%H:%M:%S", time_now)
			date_now = datetime.date.today()

			# Open the global output and put some header
			outfile.write("*"*75+'\n')
			outfile.write("\t\t HEPA MONITOR OUTPUTS\n\n")
			outfile.write("*"*75+'\n')
			outfile.write(f"Current date and time: {date_now}, {time_readable}) \n\n")
			outfile.write(f"General parameters for the 1st level trigger analysis: \n\n")
			outfile.write(f"\tEnergy band: from {Emin} to {Emax} GeV \n")
			outfile.write(f"\tTime window: from {tstart} to {tstop} {timetype}\n\n")
			outfile.write("*"*75+'\n')
			outfile.write("\t\t LIST OF THE DETECTED FLARES \n\n")

			# Iterate over each file: open it and write it to the global output
			for file in filesToBeOpened:
				with open(file,'r') as infile:
					for line in infile.readlines():
						outfile.write(line)
				outfile.write('\n\n')

			outfile.write('-'*75)

		logging.warning(f"Text output file written correctly!")
		logging.warning(f"")

		# At the end, copy the global output file to the location where it can be accessed 
		# for html production
		if outputfile_www is not None:
			shutil.copy(outputfile,outputfile_www)
			logging.warning(f"Text output file copied correctly to the web folder!")
			logging.warning(f"")

		if logfile_www is not None:
			shutil.copy(logfile,logfile_www)
			logging.warning(f"Log file copied correctly to the web folder!")
			logging.warning(f"")	

		# Now produce the figure output by opening all the images available from all the sources
		# which passed the 1st level trigger (independently from the outcome of the 2nd level
		# trigger) and merging them in a single file. To determine the sources we just look
		# at their common signature: an output file called "allflares_output.txt".
		goodflarenames = []
		for el in filesToBeOpened:
			# Determine all the images corresponding to this source
			goodflarenames.append(el.split('/')[-2] + '/') # Take only the source name from the full path

		# Now iterate on the sources and merge the file
		if os.path.exists(outputimg_www):
			os.remove(outputimg_www)

		merger = PyPDF2.PdfMerger()
		for flare in goodflarenames:
			# Determine all the images corresponding to this source
			allimages = glob.glob(os.path.join(imgdir,flare,'*.pdf'))
			for pdf in allimages:
				# Merge the image
				merger.append(pdf)

		merger.write(outputimg_www)
		merger.close()

		# Update log
		logging.warning(f"Figure output files written correctly!")
		logging.warning(f"")

		# -----------------------------------------------------------------------------------------------
		# Finally, we should create a sky map showing the average background rate in each pixel
		# of the sky (with a FAVA-like analysis) and add marker over every 2nd-level triggered
		# flare (distinguishing the ones which passed the 2nd-level threshold)
		logging.warning(f"Now let's summarize the run in a single plot...")

		subprocess.run(['python3','utils_configcreator.py',f"{Emin}",f"{tstart}",f"{tstop}",'None','None','None',
																	'0',f"{skymapsourcename}",f"{gridfile}"])
		subprocess.run(['python3','utils_AllSkyFluxMap.py',f"{skymapsourcename}",f"{ifexpmap}"])

		# Update the timing and the log
		pytiming_t2 = time.perf_counter()
		delta = pytiming_t2-pytiming_t1

		logging.warning(f"")
		logging.warning(f"Elapsed time in this ROI check: {delta:.2f} s")
		for _ in range(10):
			logging.warning(f"")

		# Now, the end of the loop: if the mode is "Fixed Time", we have finished and we can just
		# exit the loop. Otherwise, we should increase the loop counter and start back from the 
		# beginning.
		if scanmode == 'LM':
			ScanIndex += 1
			logging.warning(f"Cycle ended, going to sleep...")
			time.sleep(sleeptime_minutes*minutes_to_seconds)
			logging.warning(f"Waking up again!")

			# --- TO DO: maybe add a clear of the logfile? Otherwise, change handler... ---
		elif scanmode == 'FT':
			logging.warning(f"Cycle ended, break!")
			doloop = False

		# Now an additional clause: add a "controlled escape" flag. This menas that the user
		# may force the script to end in a controlled way after an entire cycle, by simply
		# placing a ".done" (or something like that) file in the folder where this script is stored.
		if os.path.exists(breakFile):
			break

	# Manage some exceptions
	except KeyboardInterrupt:
		logging.warning(f"Ctrl+C command invoked!")
		logging.warning(f"Aborting the script...")
		break

logging.warning(f"")
logging.warning(f"Completed the execution of HEPA_skyscanner.py!")
logging.warning("*"*75)