# ==========================================================================================
#                                  PREMESSA (sintassi)
# ==========================================================================================
# Questo script prende in input i dati di tipo "photon" e "spacecraft" prodotti dal LAT
# e restituisce in output:
#    - I dati puliti con gtselect e gtmktime
#    - Una mappa di conteggi prodotta con gtbin
#    - Una mappa di esposizione prodotta con gtexpcube2 (dopo aver applicato gtltcube)
#
# Sintassi di call:
#   	python3 FAVAlike.py [Emin] [mode]
#
# dove Emin è l'energia minima da guardare e mode = "all" se si vogliono analizzare i dati di tutta la missione, oppure "wXXX" per analizzare la sola settimana XXX
#
# ==========================================================================================
#                                  PREAMBOLO (load moduli)
# ==========================================================================================
# Import dei moduli della Python Standard Library
import copy, gc                        # Per la gestione degli oggetti e della memoria
import glob, os, subprocess, sys       # Per agire sul sistema operativo
import inspect                         # Per guardare codice
import json                            # Per leggere i file JSON
import time                            # Per il timing degli script
import warnings                        # Per disattivare i warning

# Import dei moduli di calcolo scientifico
import h5py                            # Per l'apertura dei file H5
import lmfit                           # Per fit avanzati
import matplotlib as mpl               # Per gestire i plot
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.cm as pltmaps
import numpy as np                     # Per analisi con gli array
import pandas as pd                    # Per analisi con i dataframe
import PyPDF2                          # Per gestione dei file PDF
import scipy.io as io                  # Per strumenti ulteriori di analisi
import scipy.signal as sig
import scipy.ndimage as ndmg
import scipy.special as sp
import seaborn                         # Per gestire i plot più estetici
import uproot                          # Per l'apertura dei file ROOT
from scipy.special import factorial    # Funzione fattoriale
from scipy.stats import norm as spnorm # Funzione "inverso della gaussiana"
from scipy.optimize import minimize    # Funzione per Maximum Likelihood minimization

# Import dei moduli per l'astrofisica
import gt_apps                             # Per usare in python i fermitools base
import astropy.units as u                  # Unità di misura celesti
from GtApp import GtApp                    # Per usare in python qualsiasi fermitool
from gammapy.data import EventList         # Per aprire file .fits
from gammapy.maps import Map,WcsNDMap      # Per mostrare le mappe del cielo
from astropy.coordinates import SkyCoord   # Per manipolare sistemi di coordinate
from astropy.io import fits                # Per aprire i file fits
from astropy.stats import bayesian_blocks  # Per segmentazione con i Bayesian Blocks
from LATSourceModel import SourceList      # Per creare file xml con liste di sorgenti

# Import delle funzioni personali, localizzate nella cartella di root delle analisi
sys.path.append("/home/pmgunix/pmg-home/FermiLAT/Functions")
from MyFunctions import *
from AstroFunctions import *

# Set dei warning dei pacchetti
np.seterr(divide='ignore', invalid='ignore') # Per evitare print di errori in caso di divisioni 0/0
mpl.rcParams['figure.dpi'] = 300             # Per evitare che i plot inline siamo smearati
warnings.filterwarnings("ignore", category=DeprecationWarning)

# =========================================================================================
#                                      DEFINIZIONI                                      
# =========================================================================================
# Definisco le dimensioni dei font e delle figure e altre variabili generali
textfont = 20         # Fontsize per label e legend
dimfig = (12,7)       # Dimensioni delle figure (simile allo standard A4)
dimfigbig = (16,12)   # Dimensioni delle figure (più grande di A4, per massima leggibilità)
dimfiglong = (16,25)  # Dimensioni delle figure (formato lungo, per molti plot, tipo 11x2)
filetype = '.pdf'     # Formato di salvataggio delle immagini (jpeg per le presentazioni, pdf per la tesi)
filedpi = 520         # Risoluzione in caso di salvataggio di png/jpeg
nptsfit = 100000      # Numero di punti per il plot dei fit

# ==========================================================================================
#                                  DATI DALLO STATUSFILE
# ==========================================================================================
# Leggo il file di status
statusfile = '../statusfile.json'
with open(statusfile) as json_file:
	statusdata = json.load(json_file)

# Estraggo tutte le variabili di interesse dal file di status
datadir = statusdata['datadir']
imgdir = statusdata['imgdir']
tempdir = statusdata['tempdir']
dataSCdir = statusdata['dataSCdir']
dataPHdir = statusdata['dataPHdir']
catalog_file = statusdata['catalog_file_4FGL']

# Definisco il nome delle subfolder di imgdir e tempdir per questo quaderno
thisproject = '01_FAVA_tentatives'
pretempdir = '01a_FAVA'

# Correggo tempdir e imgdir per puntare già sulla sottocartella di questo script
imgdir = os.path.join(imgdir,thisproject,pretempdir)
tempdir = os.path.join(tempdir,thisproject,pretempdir)

# ==========================================================================================
#                                    PARAMETRI GENERALI
# ==========================================================================================
# Definisco l'intervallo di energie dove opereremo (in GeV) e altri parametri di utilità
# generale
try:
    Emin = float(sys.argv[1])
except:
    Emin = 10
    
Emax = 2000
zmax = 90                # Massimo valore di zenith per evitare la contaminazione dall'Earth Limb
chatter = 4              # Verbosità di tutti i processi
binsz = 0.5              # Lato dei pixel nelle mappe del cielo in °
nxpix = int(360/binsz)   # Numero di pixel in x (long) nelle SkyMap; 360 è la full sky view a 1° 
nypix = int(180/binsz)   # Numero di pixel in y (lat) nelle SkyMap; 180 è la full sky view a 1°

print(f"")
print(f"Emin: {Emin}")

# Definisco se sto lavorando su una settimana sola di dati ("wXXX") o su tutta
# la missione ("All")
try:
    mode = sys.argv[2]
    if mode == 'All':
        mode = 'all'
except:
    mode = "all"

print(f"")
print(f"Modalità di lavoro: {mode}")
print(f"")

# Definisco i nomi dei file da usare per l'analisi (photons e spacecraft)
PHfileloc = f"/home/pmgunix/pmg-home/FermiLAT/Data/LAT_weekly_photons/"
SCfileloc = f"/home/pmgunix/pmg-home/FermiLAT/Data/LAT_weekly_spacecraft/"

if mode == "all":
    wBEGIN = "009"
    wEND = "259"
    modename = f"w{wBEGIN}_w{wEND}"
    data_PH_in = f'@./Lists/PH_events_w{wBEGIN}_w{wEND}.list'
    data_SC = os.path.join(SCfileloc,f"lat_spacecraft_w{wBEGIN}_w{wEND}_p310_v001.fits")
else:
    modename = mode
    data_PH_in = os.path.join(PHfileloc,f"weekly/photon/lat_photon_weekly_{mode}_p305_v001.fits")
    data_SC = os.path.join(SCfileloc,f"weekly/spacecraft/lat_spacecraft_weekly_{mode}_p310_v001.fits")

print(f"File photons: {data_PH_in}")
print(f"File spacecraft: {data_SC}")
print(f"")

# Definisco i nomi dei file prodotti in output da ciascuno step dell'analisi
data_PH_after_gtselect = os.path.join(tempdir,f'PH_{mode}_after_gtselect_Emin_{Emin}.fits')
data_PH_after_gtmktime = os.path.join(tempdir,f'PH_{mode}_after_gtmktime_Emin_{Emin}.fits')
data_PH_after_gtbin = os.path.join(tempdir,f'PH_{mode}_after_gtbin_Emin_{Emin}.fits')
data_PH_after_gtltcube = os.path.join(tempdir,f'PH_{mode}_after_gtltcube_{Emin}.fits')
data_PH_after_gtexpcube2 = os.path.join(tempdir,f'PH_{mode}_after_gtexpcube2_Emin_{Emin}.fits')

# ==========================================================================================
#                        SELEZIONE DEGLI EVENTI (gtselect, gtmktime)
# ==========================================================================================
# Seleziono gli eventi raw acquisiti dal LAT
ifDoAnyways = False # Per forzare a entrare nell'if
if (not os.path.exists(data_PH_after_gtselect)) | ifDoAnyways:
    # Definisco i parametri da applicare con "gtselect"
    gt_apps.filter['evclass'] = 128      # P8R3_SOURCE class events
    gt_apps.filter['evtype'] = 3         # FRONT+BACK
    gt_apps.filter['ra'] = 'INDEF'       # No cut in RA, DEC or RAD aperture
    gt_apps.filter['dec'] = 'INDEF'
    gt_apps.filter['rad'] = 'INDEF'
    gt_apps.filter['emin'] = Emin*1e3    # Energy range (in MeV)
    gt_apps.filter['emax'] = Emax*1e3
    gt_apps.filter['zmax'] = zmax        # Zenith cut to avoid Earth limb contamination
    gt_apps.filter['tmin'] = 'INDEF'     # No time cut
    gt_apps.filter['tmax'] = 'INDEF'
    gt_apps.filter['chatter'] = chatter  # Verbosity
    gt_apps.filter['infile'] = data_PH_in
    gt_apps.filter['outfile'] = data_PH_after_gtselect
    
    # Chiamo gtselect
    print(f"Calling gtselect... \n")
    gt_apps.filter.run()
    print(f"")
else:
    print(f"Skipping gtselect... \n")

# Aggiusto i Good Time Intervals degli eventi selezionati per l'osservazione
ifDoAnyways = False # Per forzare a entrare nell'if
if (not os.path.exists(data_PH_after_gtmktime)) | ifDoAnyways:
    # Definisco i parametri da applicare con "gtmktime"
    gt_apps.maketime['scfile'] = data_SC
    gt_apps.maketime['filter'] = '(DATA_QUAL>0)&&(LAT_CONFIG==1)'   # Good data
    gt_apps.maketime['roicut'] = 'no'                               # No specific ROI
    gt_apps.maketime['chatter'] = chatter                           # Verbosity
    gt_apps.maketime['evfile'] = data_PH_after_gtselect
    gt_apps.maketime['outfile'] = data_PH_after_gtmktime
    
    # Chiamo gtmktime
    print(f"Calling gtmktime... \n")
    gt_apps.maketime.run()
    print(f"")
else:
    print(f"Skipping gtmktime... \n")

# ==========================================================================================
#                             MAPPA DI CONTEGGI (gtbin)
# ==========================================================================================
# Preparo i parametri per costruire una mappa di conteggi del cielo, senza correzione di esposizione,
# usando gtbin
ifDoAnyways = True # Per forzare a entrare nell'if
if (not os.path.exists(data_PH_after_gtbin)) | ifDoAnyways:
    gt_apps.evtbin['evfile'] = data_PH_after_gtmktime
    gt_apps.evtbin['outfile'] = data_PH_after_gtbin
    gt_apps.evtbin['scfile'] = data_SC
    gt_apps.evtbin['algorithm'] = 'CMAP'     # "CMAP": mappa x-y senza bin energetici
    gt_apps.evtbin['nxpix'] = nxpix          # Numero di pixel in x/long
    gt_apps.evtbin['nypix'] = nypix          # Numero di pixel in y/lat
    gt_apps.evtbin['binsz'] = binsz
    gt_apps.evtbin['coordsys'] = 'GAL'
    gt_apps.evtbin['xref'] = 0
    gt_apps.evtbin['yref'] = 0
    gt_apps.evtbin['axisrot'] = 0
    gt_apps.evtbin['proj'] = 'AIT'
    gt_apps.evtbin['chatter'] = chatter

    # Chiamo gtbin
    print(f"Calling gtbin... \n")
    gt_apps.evtbin.run()
    print(f"")
else:
    print(f"Skipping gtbin... \n")

# ==========================================================================================
#                             MAPPA DI ESPOSIZIONE (gtltcube, gtexpcube2)
# ==========================================================================================
# Creo la mappa di livetime del LAT
ifDoAnyways = False # Per forzare a entrare nell'if
if (not os.path.exists(data_PH_after_gtltcube)) | ifDoAnyways:
    # Definisco i parametri da applicare con "gtltcube".
    gt_apps.expCube['evfile'] = data_PH_after_gtmktime
    gt_apps.expCube['scfile'] = data_SC          # N.B. qui non ammette l'uso di una lista di file
    gt_apps.expCube['outfile'] = data_PH_after_gtltcube
    gt_apps.expCube['zmax'] = zmax
    gt_apps.expCube['dcostheta'] = 0.025
    gt_apps.expCube['binsz'] = binsz
    gt_apps.expCube['chatter'] = chatter  # Verbosity

    # Chiamo gtltcube
    print(f"Calling gtltcube... \n")
    gt_apps.expCube.run()
    print(f"")
else:
     print(f"Skipping gtltcube... \n")

# Prepare and call the exposure map creator
gtexpCube2= GtApp('gtexpcube2','Likelihood')

ifDoAnyways = False # Per forzare a entrare nell'if
if (not os.path.exists(data_PH_after_gtexpcube2)) | ifDoAnyways:
    gtexpCube2['infile'] = data_PH_after_gtltcube
    gtexpCube2['cmap'] = 'none'
    gtexpCube2['outfile'] = data_PH_after_gtexpcube2
    gtexpCube2['irfs'] = 'P8R3_SOURCE_V3'
    gtexpCube2['evtype'] = 'INDEF'
    gtexpCube2['nxpix'] = nxpix
    gtexpCube2['nypix'] = nypix
    gtexpCube2['binsz'] = binsz
    gtexpCube2['coordsys'] = 'GAL'
    gtexpCube2['xref'] = 0
    gtexpCube2['yref'] = 0
    gtexpCube2['axisrot'] = 0
    gtexpCube2['proj'] = 'AIT'
    gtexpCube2['ebinalg'] = 'LOG'
    gtexpCube2['emin'] = Emin*1e3    # Energy interval
    gtexpCube2['emax'] = Emax*1e3
    gtexpCube2['enumbins'] = 1
    gtexpCube2['chatter'] = chatter  # Verbosity

    # Chiamo gtexpcube2    
    print(f"Calling gtexpcube2... \n")
    gtexpCube2.run()
    print(f"")
else:
    print(f"Skipping gtexpcube2... \n")

print(f"All jobs done!")