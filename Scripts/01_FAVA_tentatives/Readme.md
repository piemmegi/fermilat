# Content of the folder

## Aims

These scripts were born in a first attempt to imitate the Fermi All-Sky Variability Analysis (FAVA). In the end, they become the core of what evolved in the HEPA pipeline. Nowadays, they are mostly legacy code - code that is used only to produce nice plots, but not to show anything actually interesting by itself.

| Script                        | Status     | Content |
|:-----------------------------:|:----------:|:-------:|
| `FAVAlike.py`                 | Deprecated | Python script to perform a FAVA-like analysis, i.e., a full-sky search for transient sources in a given energy band. |
| `01a_FAVAlike.ipynb`          | Deprecated | JP Notebook to visualize graphically the results produced by the `FAVAlike.py` script |
| `01b_LightCurves.ipynb`       | Deprecated | Deprecated version of the HEPA 1st level trigger (v. 1) |
| `01c_Timing.ipynb`            | Deprecated | Deprecated version of the HEPA 1st level trigger (v. 2) |
| `01d_SourceLikelihood.ipynb`  | Deprecated | Deprecated version of the HEPA 2nd level trigger: select a region in the sky, gttsmap, gtlike |
| `01e_Healpix.ipynb`           | Deprecated | Deprecated version of the method used in HEPA to create the full-sky tessellation, using a Healpix-based grid |
| `01f_grids.ipynb`             | Deprecated | Legacy version of the method used in HEPA to create the full-sky tessellation, using a Healpix-based grid |
| `01g_graphics.ipynb`          | In use     | Show-case of some of the HEPA features: Finite Difference and Pacman-clustering  |
| `01h_BayesianBlockTest.ipynb` | In use     | Beta tester for the application of the Bayesian Block algorithm |


