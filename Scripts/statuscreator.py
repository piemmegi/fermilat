'''
# ==========================================================================================
#                                          HEADER
# ==========================================================================================
The aim of this script is to produce a JSON file containing the coordinates for the folders
requrired to perform Fermi-LAT data analysis (images, temporary files, data files, etc.)

-------------------------------------------------------------------------------------------
Call syntax:
    python3 statuscreator.py

'''
# ==========================================================================================
#                                  PREAMBLE (module load)
# ==========================================================================================
import json, os

# ==========================================================================================
#                                  PARAMETERS DEFINITIONS
# ==========================================================================================
# Define the folder path containing the root Fermi-LAT folder
location = "pmgunix"
#location = "CERN_VM"
#location = "s3df"
if (location == "pmgunix"):
    # Working on pmgunix local foler
    mainfolder = '/home/pmgunix/pmg-home/FermiLAT'
    mainfolder_ImgAndTemp = mainfolder
elif (location == "CERN_VM"):
    # Working on a CERN VM
    mainfolder = '/eos/user/p/pmontigu/FermiLAT'
    mainfolder_ImgAndTemp = mainfolder
elif (location == "s3df"):
    # Working on s3df
    mainfolder = "/sdf/home/p/pietromg/fermi-user/FermiLAT" 
    mainfolder_ImgAndTemp = "/sdf/home/p/pietromg/fermi-user/FermiLAT"
    
# Build the paths to all the subfolders relevant for the analysis
datadir = os.path.join(mainfolder,'Data')
imgdir = os.path.join(mainfolder_ImgAndTemp,'Images')
tempdir = os.path.join(mainfolder_ImgAndTemp,'Tempfiles')

# Build the paths to the weekly photon and spacecraft files
dataSCdir = os.path.join(datadir,'LAT_weekly_spacecraft/weekly/spacecraft/')
dataPHdir = os.path.join(datadir,'LAT_weekly_photons/weekly/photon/')

# Build the paths to the catalog files
catalogdir = os.path.join(datadir,"Catalogs")

catalog_file_4FGL = os.path.join(datadir,catalogdir,f"gll_psc_v35.fit")
catalog_file_4FGL_xml = os.path.join(datadir,catalogdir,f"gll_psc_v32.xml")
catalog_file_4FGL_reg = os.path.join(datadir,catalogdir,f"gll_psc_v32.reg")
catalog_file_4FGL_regAssoc = os.path.join(datadir,catalogdir,f"gll_psc_v35_assoc.reg") # Mind the number!

catalog_file_3FHL = os.path.join(datadir,catalogdir,f"gll_psch_v13.fit")
catalog_file_3FHL_xml = os.path.join(datadir,catalogdir,f"gll_psch_v12.xml")
catalog_file_3FHL_reg = os.path.join(datadir,catalogdir,f"gll_psch_v12.reg")
catalog_file_3FHL_regAssoc = os.path.join(datadir,catalogdir,f"gll_psch_v12_assoc.reg")
catalog_templates_3FHL_extended_dir = os.path.join(datadir,catalogdir,'Extended_archive_v18')
catalog_templates_3FHL_extended_file = os.path.join(catalog_templates_3FHL_extended_dir,'LAT_extended_sources_v18.fits')

catalog_name_2FGES = "2FGES_table2"
catalog_file_2FGES_fits = os.path.join(datadir,catalogdir,f"{catalog_name_2FGES}.fits")

# ==========================================================================================
#                                  JSON FILE CREATION
# ==========================================================================================
# Creo la stringa da convertire in JSON
jsonstring = {"mainfolder": mainfolder,     # Root folder for data, scripts, functions, docs
              "mainfolder_ImgAndTemp": mainfolder_ImgAndTemp, # Root folder for images, tempfiles
              "datadir": datadir,           # Data folder
              "imgdir": imgdir,             # Picture folder
              "tempdir": tempdir,           # Temporary file folder generale dei file temporanei
              "dataSCdir": dataSCdir,       # Weekly spacecraft files subfolder
              "dataPHdir": dataPHdir,       # Weekly photon files subfolder
              "catalogdir": catalogdir,
              "catalog_file_4FGL": catalog_file_4FGL,                   # 4FGl-DR4 catalog [fits]
              "catalog_file_4FGL_xml": catalog_file_4FGL_xml,           # 4FGl-DR4 catalog [xml]
              "catalog_file_4FGL_reg": catalog_file_4FGL_reg,           # 4FGL-DR4 catalog [reg]
              "catalog_file_4FGL_regAssoc": catalog_file_4FGL_regAssoc, # 4FGL-DR4 catalog associations [reg]
              "catalog_file_3FHL": catalog_file_3FHL,                   # 3FHL catalog [fits]
              "catalog_file_3FHL_xml": catalog_file_3FHL_xml,           # 3FHL catalog [xml]
              "catalog_file_3FHL_reg": catalog_file_3FHL_reg,           # 3FHL catalog [reg]
              "catalog_file_3FHL_regAssoc": catalog_file_3FHL_regAssoc, # 3FHL catalog associations [reg]
              "catalog_templates_3FHL_extended_dir": catalog_templates_3FHL_extended_dir, # 3FHL catalog (extended sources dir)
              "catalog_templates_3FHL_extended_file": catalog_templates_3FHL_extended_file, # 3FHL catalog (extended sources file)
              "catalog_file_2FGES_fits": catalog_file_2FGES_fits}       # 2FGES catalog [fits]

# Converto la stringa in JSON
json_string = json.dumps(jsonstring,indent=4)

# Scrivo il file
filename = os.path.join('.','statusfile.json')
with open(filename, 'w') as outfile:
    outfile.write(json_string)
    print(f"Status file written: ")
    print(filename)
    print(f"")
