'''
# ==========================================================================================
#                                       HEADER
# ==========================================================================================
The aim of this script is to parse the data extracted by the 01a_DataExtractor script and
produce a map of the sky in a given rectangular sub-region. This is useful because, later on,
we will use pgwave on these count maps to find seeds for the likelihood analysis.

Note that this script creates a count map for each of the subregions defined in the 
SkyQuadrants.dat file.

-------------------------------------------------------------------------------------------
Call syntax:
  	python3 01b_CountMapCreator.py

'''
# ==========================================================================================
#                                 PREAMBLE (module load)
# ==========================================================================================
# Import of the Python Standard Library modules required for the analysis
import glob, os, subprocess, sys			# To act on the operative system
import json									# To read and write JSON files
import matplotlib as mpl					# For plotting
import matplotlib.pyplot as plt				# For plotting
import numpy as np							# For numerical analysis
import pandas as pd							# For reading and writing csv files
import warnings								# To deactivate the warnings
import time									# For script timing
import logging								# For logging purposes
import datetime								# For timestamp printing
import copy									# To copy objects

# Import of the astropy/gammapy/fermipy (& fermitools) modules necessary for astrophysics
import astropy.units as u					# Celestial units
from astropy.io import fits
from gammapy.maps import Map, WcsNDMap		# To work with sky maps
from astropy.coordinates import SkyCoord 	# To work with sky frames

# Import of personal functions from custom modules
sys.path.append("../../Functions")
from AstroFunctions import gtselect, gtmktime, gtbin_CMAP, gtltcube, gtexpcube2

# Set the warnings of a few functions
np.seterr(divide='ignore', invalid='ignore')	# To avoid 0/0 errors
mpl.rcParams['backend'] = 'agg'					# Non-interactive backend for plotting
warnings.filterwarnings("ignore", category=DeprecationWarning) # To avoid deprecation warnings

# =========================================================================================
#                                      COMMON DEFINITIONS                                      
# =========================================================================================
# Define the dimension for the fonts in the plots, the figures dimensions and so on
textfont = 20			# Fontsize for labels and legends
dimfig = (12,7)			# Figure dimensions (A4-like)
dimfigbig = (16,12)		# Figure dimensions (a bit larger than A4-like)
dimfiglong = (16,25)	# Figure dimensions (a bit larger and much longer than A4-like)
filetype = '.pdf'		# File format to save the pictures
filedpi = 520			# Resolution used when saving pictures in .png/.jpeg
nptsfit = 10000			# Number of points used in plotting the fits

# ==========================================================================================
#                                  STATUS FILE DATA
# ==========================================================================================
# Open the status file
statusfile = '../statusfile.json'
with open(statusfile) as json_file:
	statusdata = json.load(json_file)

# Extract all the relevant variables from the file
datadir = statusdata['datadir']
imgdir = statusdata['imgdir']
tempdir = statusdata['tempdir']
dataSCdir = statusdata['dataSCdir']
dataPHdir = statusdata['dataPHdir']
catalog_file_4FGL = statusdata['catalog_file_4FGL']
catalog_file_3FHL = statusdata['catalog_file_3FHL']
catalog_file_3FHL_reg = statusdata['catalog_file_3FHL_reg']
catalog_file_3FHL_regAssoc = statusdata['catalog_file_3FHL_regAssoc']

# Define the name for the subfolders (in imgdir and tempdir) where we will save
# the temporary images and files
pretempdir = '04_4FHL'
imgdir = os.path.join(imgdir,pretempdir)
tempdir = os.path.join(tempdir,pretempdir)

# Setting of the logging module
logfile = "debug.log"
logging.basicConfig(format='[%(asctime)s] %(message)s',level=logging.INFO,
					handlers=[logging.FileHandler(logfile,mode='w'),
					logging.StreamHandler()])
logging.info("*"*75)
logging.info(f"Beginning the execution of {__file__}")
logging.info(f"")

# ==========================================================================================
#                               CONFIGURATION PARAMETERS
# ==========================================================================================
# Open the configuration file made for the 0th level trigger and extract the variables
# necessary for the analysis
configfile = os.path.join(tempdir,f'configfile_trignum_0.json')
with open(configfile) as json_file:
	configdata = json.load(json_file)

# Extract all the relevant variables from the file
chatter        = configdata['chatter']     # All fermitools
gtmode         = configdata['gtmode']      # All fermitools
SCfilename     = configdata['SCfilename']  # File management

# Define the other parameters which will remain constant throughout the analysis and which
# regulate how gtbin works
CMAP_algorithm = 'CMAP'     # Parameter for gtbin
CMAP_binsz     = 0.1        # Binning step of the maps in x-y (x=long,y=lat)
CMAP_coordsys  = 'GAL'      # Frame of reference
CMAP_axisrot   = 0          # Rotation angle in the x-y plane
CMAP_proj      = 'CAR'      # Projection style
quadrantMarginAngle = 5     # Margin on the quadrant borders, in deg, to avoid border effects
                            #  (note: this is the total --> split it in two, one for each side)

# ==========================================================================================
#                                 TEMPORARY FILE NAMES
# ==========================================================================================
# Define the names of the .fits, .txt, .xml and .npz files which will be produced in the execution
subdir = "04_pgwave"
data_PH_after_gtmktime = os.path.join(tempdir,f"PH_after_gtmktime.fits")

# ==========================================================================================
#                                 QUADRANT STRUCTURE
# ==========================================================================================
# Open the file with the sky quadrants and parse its content
quadrantFile = "./SkyQuadrants.dat"
lat_min, lat_max, long_min, long_max = np.loadtxt(quadrantFile, skiprows = 1, unpack = True, dtype = np.int64, delimiter = "\t")
numQuadrants = np.shape(lat_min)[0]

# Compute the center and extension (in deg and number of pixels) of all the quadrants	
CMAP_xref = (long_max + long_min) / 2
CMAP_yref = (lat_max + lat_min) / 2

CMAP_xwidth = (long_max - long_min) + quadrantMarginAngle
CMAP_ywidth = (lat_max - lat_min) + quadrantMarginAngle

CMAP_nxpix = np.int64(CMAP_xwidth/CMAP_binsz)
CMAP_nypix = np.int64(CMAP_ywidth/CMAP_binsz)

# ==========================================================================================
#                                COUNT MAP PRODUCTION
# ==========================================================================================
# Iterate on all the quadrants to be analyzed
for i in range(numQuadrants):
	# Define the name of the output file for this step
	data_PH_after_gtbin = os.path.join(tempdir,subdir,f"PH_after_gtbin_{CMAP_proj}_{int(100*CMAP_binsz):02d}_Q{i:02d}.fits")

	# Call gtbin
	logging.info(f"Calling gtbin for quadrant n. {i}, with lat in [{lat_min[i]},{lat_max[i]}], long in [{long_min[i]},{long_max[i]}]")
	gtbin_CMAP(infile=data_PH_after_gtmktime,outfile=data_PH_after_gtbin,SCfile=SCfilename,
			   algorithm=CMAP_algorithm,nxpix=CMAP_nxpix[i],nypix=CMAP_nypix[i],binsz=CMAP_binsz,
			   coordsys=CMAP_coordsys,xref=CMAP_xref[i],yref=CMAP_yref[i],axisrot=CMAP_axisrot,proj=CMAP_proj,
			   chatter=chatter,gtmode=gtmode)
	logging.info(f"Done!")
	logging.info(f"")
	
# ==========================================================================================
# Final printouts
logging.info(f"")
logging.info(f"Completed the execution of {__file__}")
logging.info("*"*75)