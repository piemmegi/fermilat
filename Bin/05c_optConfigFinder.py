'''
# ==========================================================================================
#                                       HEADER
# ==========================================================================================
The aim of this script is to analyze the grid search performed on several pgwave2D parameters
configurations, to understand which is the best in terms of accuracy and efficiency.

-------------------------------------------------------------------------------------------
Call syntax:
  	python3 05c_optConfigFinder.py

'''
# ==========================================================================================
#                                 PREAMBLE (module load)
# ==========================================================================================
# Import of the Python Standard Library modules required for the analysis
import glob, os, subprocess, sys            # To act on the operative system
import json                                 # To read and write JSON files
import matplotlib as mpl                    # For plotting
import matplotlib.pyplot as plt             # For plotting
import numpy as np                          # For numerical analysis
import warnings                             # To deactivate the warnings
import time                                 # For script timing
import logging                              # For logging purposes
import datetime                             # For timestamp printing
import copy                                 # To copy objects

#import yaml                                 # For editing yaml files (necessary for fermipy)
#from fermipy.gtanalysis import GTAnalysis   # To run fermipy utilities
#from astropy.io import fits                 # To open fits files
#from gammapy.maps import WcsNDMap           # To plot maps

# Import of personal functions from custom modules
sys.path.append("../../Functions")
#from MyFunctions import fitter_linear
from AstroFunctions import angdist

# Set the warnings of a few functions
np.seterr(divide='ignore', invalid='ignore')    # To avoid 0/0 errors
mpl.rcParams['backend'] = 'agg'                 # Non-interactive backend for plotting
warnings.filterwarnings("ignore", category=DeprecationWarning) # To avoid deprecation warnings

# =========================================================================================
#                                      COMMON DEFINITIONS                                      
# =========================================================================================
# Define the dimension for the fonts in the plots, the figures dimensions and so on
textfont = 20           # Fontsize for labels and legends
dimfig = (12,7)         # Figure dimensions (A4-like)
dimfigbig = (16,12)     # Figure dimensions (a bit larger than A4-like)
dimfiglong = (16,25)    # Figure dimensions (a bit larger and much longer than A4-like)
filetype = '.pdf'       # File format to save the pictures
filedpi = 520           # Resolution used when saving pictures in .png/.jpeg
nptsfit = 10000         # Number of points used in plotting the fits

# ==========================================================================================
#                                  STATUS FILE DATA
# ==========================================================================================
# Open the status file
statusfile = '../statusfile.json'
with open(statusfile) as json_file:
    statusdata = json.load(json_file)

# Extract all the relevant variables from the file
datadir = statusdata['datadir']
imgdir = statusdata['imgdir']
tempdir = statusdata['tempdir']
dataSCdir = statusdata['dataSCdir']
dataPHdir = statusdata['dataPHdir']
catalog_file_4FGL = statusdata['catalog_file_4FGL']
catalog_file_3FHL = statusdata['catalog_file_3FHL']
catalog_file_3FHL_reg = statusdata['catalog_file_3FHL_reg']
catalog_file_3FHL_regAssoc = statusdata['catalog_file_3FHL_regAssoc']

# Define the name for the subfolders (in imgdir and tempdir) where we will save
# the temporary images and files
pretempdir = '04_4FHL'
imgdir = os.path.join(imgdir,pretempdir)
tempdir = os.path.join(tempdir,pretempdir)

# Setting of the logging module
logfile = "debug.log"
logging.basicConfig(format='[%(asctime)s] %(message)s',level=logging.INFO,
                    handlers=[logging.FileHandler(logfile,mode='w'),
                    logging.StreamHandler()])
logging.info("*"*75)
logging.info(f"Beginning the execution of {__file__}")
logging.info(f"")

# ==========================================================================================
#                                 INPUT PARSING
# ==========================================================================================
# Define what quadrants to include in the analysis
quadrantInds = [0]     #list(range(44)) #[4,5,6]
numQuadrants = len(quadrantInds)

# ==========================================================================================
#                               QUADRANT ITERATION
# ==========================================================================================
# Iterate on all the quadrants. The aim is to compute, for each one and for each hyperparameter
# configuration, the amount of correctly or wrongly associated sources (so we need the output
# from the fermipy simulation)
fermipy_simdir = '05_simCMAPs'
pgwave_simdir = '05_simPgwaveOut'
mindist_thr = 0.25     # Min dist in deg between a pg source and a real one, for the association

for i,quadrantInd in enumerate(quadrantInds):
    # Load the data from the fermipy simulation: extract the list of simulated sources (for this
    # quadrant)
    filename_fermipy = os.path.join(tempdir,fermipy_simdir,f'pgSIM_fermisim_Q{quadrantInd:02d}.npz')
    with np.load(filename_fermipy) as f:
        realSources_blat = f['all_blat']
        realSources_blong = f['all_blong']

    numRealSources = np.shape(realSources_blat)[0]

    # Iterate on all the configurations tested with pgwave
    filename_pgwave = os.path.join(tempdir, pgwave_simdir, f"Q{quadrantInd:02d}",'*')
    allfiles = glob.glob(filename_pgwave)

    all_scala = []
    all_otpix = []
    for j,file in enumerate(allfiles):
        # Parse the name of the file to compute the values of the parametrs. Remember that the
        # names are such as "pgSIM_scala_2.0_otpix_2.npz"
        all_scala.append ( float(file.split('_')[2]) )
        all_otpix.append ( float(file.split('_')[4].split('.')[0]) ) # Remove trailing ".npz"

        # Open the file and extract the sources coordinates
        with np.load(file) as f:
            pgSources_blat = f['pg_b']
            pgSources_blong = f['pg_l']

        # Associate the sources
        numPgSources = np.shape(pgSources_blat)[0]
        numRealMatched = 0
        numPgMatched = 0
        numRealUnmatched = 0
        indsPgMatching = []
        
        for k in range(numRealSources):
            # Compute the distance from this source to all the pgwave ones and see if the closest
            # is under threshold. If so, we have a match
            thisdist = angdist(realSources_blat[k],realSources_blong[k],pgSources_blat,pgSources_blong,ifdeg=True)
            if np.min(thisdist) <= mindist_thr:
                numRealMatched += 1
                numPgMatched += 1
                indsPgMatching.append(np.argmin(thisdist))
            else:
                numRealUnmatched += 1

        # Determine how many pgwave sources were never associated to the real ones
        numPgUnmatched = numPgSources - len(set(indsPgMatching))












all_matchedRealFlux = np.zeros((0,))  # To store the flux and counts of the matched sources
all_matchedPgCounts = np.zeros((0,))
all_numRealSources = 0                # To measure the pgwave2D accuracy
all_numPgSources = 0
all_numRealMatched = 0
all_numRealUnmatched = 0
all_numPgMatched = 0
all_numPgUnmatched = 0

#skipInds = list(range(16,28))
skipInds = list(range(0,16)) + list(range(28,44))
print(skipInds)


    # Check
    if j in skipInds:
        continue

    # ======================================================================================
    #                                 DATA EXTRACTION
    # ======================================================================================
    # Open the file created after the simulation and containing the simulated sources and the 
    # pgwave2D output
    logging.info("*"*50)
    logging.info(f"Looking at quadrant: {quadrantInd}")
    
    filename_pgwave = os.path.join(tempdir,simdir,f'pgSIM_pgrun_Q{quadrantInd:02d}.npz')

    try:
        

        
            pgSources_blat = f['pg_b']
            pgSources_blong = f['pg_l']
            pgSources_counts = f['pg_counts']
            
    except FileNotFoundError:
        logging.info(f"The output file for this quadrant was not found: skipping it!")
        continue
        
    

    # ======================================================================================
    #                                 SOURCE MATCHING
    # ======================================================================================
    # Iterate on all the real sources and match them
    mindist_thr = 0.1 # [deg]
    numRealMatched = 0
    numRealUnmatched = 0
    numPgMatched = 0
    indsRealMatching = []
    indsPgMatching = []

    

    # Extract the matched sources fluxes
    matchedRealFlux = realSources_flux[indsRealMatching]
    matchedPgCounts = pgSources_counts[indsPgMatching]

    # Save the outputs of this iteration step
    all_numRealSources += numRealSources
    all_numPgSources += numPgSources
    all_numRealMatched += numRealMatched
    all_numRealUnmatched += numRealUnmatched
    all_numPgMatched += numPgMatched
    all_numPgUnmatched += numPgUnmatched
    all_matchedRealFlux = np.hstack((all_matchedRealFlux,matchedRealFlux))
    all_matchedPgCounts = np.hstack((all_matchedPgCounts,matchedPgCounts))

    logging.info("Quadrant completed!")
    logging.info("")

logging.info("All quadrants done!")
logging.info("*"*50)

# Print some statistics. Note that we expect numRealMatched == numPgMatched
logging.info(f"Number of simulated sources: {all_numRealSources} ({all_numRealMatched} matched, {all_numRealUnmatched} unmatched)")
logging.info(f"Number of pgwave2D sources: {all_numPgSources} ({all_numPgMatched} matched, {all_numPgUnmatched} unmatched)")
logging.info(f"pgwave2D performance:")
logging.info(f"\t Fraction of real sources which are matched (TPR): {(100*all_numRealMatched / all_numRealSources):.3f} %")
logging.info(f"\t Fraction of real sources which are not matched (FNR): {(100*all_numRealUnmatched / all_numRealSources):.3f} %")
logging.info(f"\t Fraction of localized sources which are matched (???): {(100*all_numPgMatched / all_numPgSources):.3f} %")
logging.info(f"\t Fraction of localized sources which are unmatched (FPR): {(100*all_numPgUnmatched / all_numPgSources):.3f} %")


# ==========================================================================================
#                                 PGWAVE2D CALIBRATION
# ==========================================================================================
# Computing the correlation between the real fluxes and the pgwave counts
r = np.corrcoef(all_matchedRealFlux,all_matchedPgCounts)[0,1]
logging.info(f"Correlation between real flux and pgwave counts: {r:.3f}")
logging.info(f"")

# Fit the flux vs counts relationship with a straight line
all_matchedPgErrs = np.sqrt(all_matchedPgCounts)
result, xth, yth = fitter_linear(all_matchedRealFlux,all_matchedPgCounts,all_matchedPgErrs)

# Format the output
m,q = result.params['m'].value,result.params['q'].value
m_err, q_err = result.params['m'].stderr,result.params['q'].stderr
logging.info("Fitting: real flux = m*pgwave counts + q")
logging.info("Fit parameters:")
logging.info(f"\t m = {m:.2e} +/- {m_err:.2e} photons / counts cm2 s")
logging.info(f"\t q = {q:.2f} +/- {q_err:.2f} photons / cm2 s")
logging.info(f"")

# Plot the data and the relation
logging.info(f"Plotting...")
figname = os.path.join(imgdir,'05_fermipy',f'pgwave_fluxVScounts_allQs.pdf')

fig,ax = plt.subplots(figsize = dimfig)
ax.plot(all_matchedRealFlux,all_matchedPgCounts,color='mediumblue',linestyle='',marker='.',markersize=5,
            label = f'Data (all quadrants)')
ax.plot(xth,yth,color='red',linestyle='--',linewidth=1.5,label='Linear fit')
ax.legend(loc = 'upper left', fontsize = textfont, framealpha = 1)
ax.set_xlabel(f"Source flux [photons/cm2/s]",fontsize = textfont)
ax.set_ylabel(f"pgwave2D counts",fontsize = textfont)
ax.grid(linewidth=0.5)
ax.tick_params(labelsize = textfont)
    
fig.set_tight_layout('tight')
fig.savefig(figname)
plt.close(fig)
logging.info("Done!")

# ==========================================================================================
# Final printouts
logging.info(f"")
logging.info(f"Completed the execution of {__file__}")
logging.info("*"*75)