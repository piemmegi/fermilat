import requests
import sys

# Parse user input
try:
        MET = int(sys.argv[1])
except:
        MET = 756725683

print(f"User input: MET = {MET}")

# Submit url request
xtime_url = "https://heasarc.gsfc.nasa.gov/cgi-bin/Tools/xTime/xTime.pl"
args = dict(time_in_sf=f"{MET}",timesys_in="u",timesys_out="u",apply_clock_offset="yes")
content = requests.get(xtime_url, params=args).content

# Decode output and parse for wildcard
decoded_content = content.decode("utf-8")
pattern = """<td id="time_out_wf">*</td>\n"""
weekNumber = float(decoded_content.split('<td id="time_out_wf">')[1].split('</td>')[0])

print(f"... corresponding to week number {weekNumber}")
