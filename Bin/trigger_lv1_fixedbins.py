'''
# ==========================================================================================
#                                       HEADER
# ==========================================================================================
The aim of this script is to compute the LC of a source already processed by the 1st level
trigger analysis, with fixed time binning.

-------------------------------------------------------------------------------------------
Call syntax:
  	python3 trigger_lv1_fixedbins.py

'''
# ==========================================================================================
#                                 PREAMBLE (module load)
# ==========================================================================================
# Import of the Python Standard Library modules required for the analysis
import glob, os, subprocess, sys       # To act on the operative system
import json                            # To read and write JSON files
import matplotlib as mpl               # For plotting
import matplotlib.pyplot as plt        # For plotting
import numpy as np                     # For numerical analysis
import pandas as pd 				   # For reading and writing csv files
import warnings                        # To deactivate the warnings
import time                            # For script timing
import logging						   # For logging purposes

# Import of the astropy/gammapy/fermipy (& fermitools) modules necessary for astrophysics
from astropy.io import fits                # I/O of .fits files
from astropy.stats import bayesian_blocks  # Bayesian Block segmentation algorithm

# Import of personal functions from custom modules
sys.path.append("../../Functions")
from MyFunctions import wmean, truehisto1D, histoplotter1D
from AstroFunctions import nu_estimator, tot_intervals
from AstroFunctions import gtselect, gtmktime, gtbindef, gtbin, gtexposure

# Set the warnings of a few functions
np.seterr(divide='ignore', invalid='ignore') 					# To avoid 0/0 errors
mpl.rcParams['backend'] = 'agg'                                 # Non-interactive backend for plotting
warnings.filterwarnings("ignore", category=DeprecationWarning)  # To avoid deprecation warnings

# =========================================================================================
#                                      COMMON DEFINITIONS                                      
# =========================================================================================
# Define the dimension for the fonts in the plots, the figures dimensions and so on
textfont = 20         # Fontsize for labels and legends
dimfig = (12,7)       # Figure dimensions (A4-like)
dimfigbig = (16,12)   # Figure dimensions (a bit larger than A4-like)
dimfiglong = (16,25)  # Figure dimensions (a bit larger and much longer than A4-like)
filetype = '.pdf'     # File format to save the pictures
filedpi = 520         # Resolution used when saving pictures in .png/.jpeg
nptsfit = 10000       # Number of points used in plotting the fits

# ==========================================================================================
#                                  STATUS FILE DATA
# ==========================================================================================
# Open the status file
statusfile = '../statusfile.json'
with open(statusfile) as json_file:
	statusdata = json.load(json_file)

# Extract all the relevant variables from the file
datadir = statusdata['datadir']
imgdir = statusdata['imgdir']
tempdir = statusdata['tempdir']
dataSCdir = statusdata['dataSCdir']
dataPHdir = statusdata['dataPHdir']
catalog_file_4FGL = statusdata['catalog_file_4FGL']
catalog_file_3FHL = statusdata['catalog_file_3FHL']

# Define the name for the subfolders (in imgdir and tempdir) where we will save
# the temporary images and files
pretempdir = '02_HEPA'
imgdir = os.path.join(imgdir,pretempdir)
tempdir = os.path.join(tempdir,pretempdir)

# Setting of the logging module
logfile = "HEPA_debug.log"
logging.basicConfig(format='[%(asctime)s] %(message)s',level=logging.INFO,
					handlers=[logging.FileHandler(logfile,mode='a'),
				        	  logging.StreamHandler()])

logging.info("*"*75)
logging.info("Beginning execution of trigger_lv1.py ...")
logging.info(f"")

# ==========================================================================================
#                                 CONFIG FILE DATA
# ==========================================================================================
# Open the configuration file made for the 1st level trigger and extract the variables
# necessary for the analysis
configfile = os.path.join(tempdir,f'configfile_trignum_1.json')
with open(configfile) as json_file:
	configdata = json.load(json_file)

# Extract all the relevant variables from the file
wstart               = configdata['wstart']                # Coupled to tstart/tstop
wstop                = configdata['wstop']                 # Coupled to tstart/tstop
tstart               = configdata['tstart']                # gtselect
tstop                = configdata['tstop']                 # gtselect 
ROI_ra               = configdata['ROI_ra']                # gtselect
ROI_dec              = configdata['ROI_dec']               # gtselect
ROI_rad              = configdata['ROI_rad']               # gtselect
Emin                 = configdata['Emin']                  # gtselect
Emax                 = configdata['Emax']                  # gtselect
zmax                 = configdata['zmax']                  # gtselect
evclass              = configdata['evclass']               # gtselect
evtype               = configdata['evtype']                # gtselect
roicut               = configdata['roicut']                # gtmktime
SUNsepmin            = configdata['SUNsepmin']             # gtmktime
filtercond           = configdata['filtercond']            # gtmktime
bindef_bintype       = configdata['bindef_bintype']        # gtbindef
bindef_tbinalg       = configdata['bindef_tbinalg']        # gtbin
bindef_algorithm     = configdata['bindef_algorithm']      # gtbin
irfs                 = configdata['irfs']                  # gtexposure
srcmdl               = configdata['srcmdl']                # gtexposure
specin               = configdata['specin']                # gtexposure
chatter              = configdata['chatter']               # All fermitools
gtmode               = configdata['gtmode']                # All fermitools
FDnum                = configdata['FDnum']                 # 1st level trigger
FD_errorscale        = configdata['FD_errorscale']         # 1st level trigger
FD_errortype 		 = configdata['FD_errortype']		   # 1st level trigger
FD_signiftype 		 = configdata['FD_signiftype']		   # 1st level trigger
BayesOffset          = configdata['BayesOffset']           # 1st level trigger
start_p0             = configdata['start_p0']              # 1st level trigger
start_ncp            = configdata['start_ncp']             # 1st level trigger
timestep             = configdata['timestep']              # 1st level trigger
mergedist            = configdata['mergedist']             # 1st level trigger
trigger1_thrRAW      = configdata['trigger1_thrRAW']       # 1st level trigger
trigger1_thrLC       = configdata['trigger1_thrLC']        # 1st level trigger
ifLoadAverageRates   = configdata['ifLoadAverageRates']    # 1st level trigger
pathLoadAverageRates = configdata['pathLoadAverageRates']  # 1st level trigger
databasedtypes       = configdata['databasedtypes']        # 1st level trigger
sourcename           = configdata['sourcename']            # File management
PHlistname           = configdata['PHlistname']            # File management
SClistname           = configdata['SClistname']            # File management
SCfilename           = configdata['SCfilename']            # File management
imgsubdir            = configdata['imgsubdir']             # File management
imgdir_www           = configdata['imgdir_www']            # File management

# Correct the "databasedtypes" variable to switch from strings to dtypes...
for key in databasedtypes.keys():
	databasedtypes[key] = eval(databasedtypes[key])

# ==========================================================================================
#                                 TEMPORARY FILE NAMES
# ==========================================================================================
# Define the names of the .fits, .txt, .xml and .npz files which will be produced in the execution
data_PH_after_gtmktime  = os.path.join(tempdir,sourcename,f"PH_after_gtmktime.fits")
data_PH_after_gtbin     = os.path.join(tempdir,sourcename,f"PH_after_gtbin_fixedbins.fits")

# ==========================================================================================
#                                    LIGHT CURVE ANALYSIS
# ==========================================================================================
# Create the binning for the Light Curve using gtbindef and then the Light Curve using gtbin
d2s = 24*60*60  # Seconds in 1 day
dtime = 7*d2s   # Time step for the LC: 1 week seems good?

# Create the Light Curve
ifDoAnyways = False
if ifDoAnyways:
	logging.info(f"Calling gtbin and gtexposure...")
	gtbin(infile=data_PH_after_gtmktime,outfile=data_PH_after_gtbin,SCfile=SCfilename,
		  tstart=tstart,tstop=tstop,algorithm=bindef_algorithm,tbinalg='LIN',dtime=dtime,
		  chatter=chatter,gtmode=gtmode)

	# Adjust the Light Curve to account for the LAT exposure
	gtexposure(infile=data_PH_after_gtbin,SCfile=SCfilename,irfs=irfs,scrmdl=srcmdl,specin=specin,
			   chatter=chatter,gtmode=gtmode)
	logging.info(f"Done!")
	logging.info(f"")
else:
	logging.info(f"Skipping gtbin and gtexposure...")
	logging.info(f"")

# ==========================================================================================
#                                        DATA OPENING
# ==========================================================================================
# Open the Light Curve file and extract times, counts and exposures to compute the rate vs MET
with fits.open(data_PH_after_gtbin) as hdul:
    ind = 1
    LC_time = np.array(hdul[ind].data.field('TIME'),dtype=np.float64)
    LC_counts = np.array(hdul[ind].data.field('COUNTS'),dtype=np.float64)
    LC_exposure = np.array(hdul[ind].data.field('EXPOSURE'),dtype=np.float64)

LC_rate = LC_counts/LC_exposure
LC_rate_err = np.sqrt(LC_counts)/LC_exposure
LC_weights = 1/LC_rate_err**2

# Compute the average flux from the Light Curve
good_inds = (np.isnan(LC_weights) == False) & (np.isinf(LC_weights) == False)
LC_av_rate, _ = wmean(LC_rate[good_inds],LC_weights[good_inds])

logging.info(f"Average rate (LC, measured): {LC_av_rate:.2e} events * cm(-2) * s(-1)")	
logging.info(f"")

# ==========================================================================================
#                                        PLOTTING
# ==========================================================================================
# We want now to show in a single picture: the recomputed Light Curve and the trigger threshold.
# Do the plotting only if required
figname = os.path.join(imgsubdir,f'trig1b_LCwithBB_fixedbins' + filetype)

fig, ax = plt.subplots(figsize = dimfig)
ax.errorbar(LC_time,LC_rate,LC_rate_err,color='mediumblue',marker='.', markersize=5,
				linestyle='',label = 'Data')
ax.plot(LC_time,LC_time*0+LC_av_rate*trigger1_thrLC,color='red',linewidth=3,linestyle='--',label=f'{trigger1_thrLC:d} ' + r"$\cdot$ av. flux")
ax.set_xlabel('MET [s]',fontsize=textfont)
ax.set_ylabel(r'Flux [cm$^{-2}$ s$^{-1}$]',fontsize=textfont)
ax.set_yscale('log')
ax.set_title(sourcename,fontsize=1.5*textfont)
ax.legend(loc='upper left',fontsize=textfont,framealpha=0.75)
ax.grid(linewidth=0.5)
ax.tick_params(labelsize = textfont)
fig.set_tight_layout('tight')
fig.savefig(figname,dpi=filedpi)
plt.close(fig)

# ==========================================================================================
logging.info(f"Plotting done!")
logging.info(f"")
logging.warning(f"Completed the execution of {__file__}.py!")
logging.info("*"*75)