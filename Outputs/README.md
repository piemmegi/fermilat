Content of this folder, as of 2024.11.22

|:-----------------------------------------|:---------------------------:|
| File name                                | Content                     |
|:-----------------------------------------|:---------------------------:|
| 2024_10_31_flare_OP313                   | Output of the HEPA pipeline (1st + 2nd level trigger) used to analyze the flare of OP 313 in late October 2024 |
| 2024_11_22_flare_2sources                | Output of the HEPA pipeline (1st + 2nd level trigger) used to analyze the simultaneous flare of PG 1218+304 and B2 1215+30 in late November 2024 |
| VarSources3FHL_w9_w250_tempfiles         | Output of the full-sky HEPA pipeline in FT mode, run on mission weeks 9-250 on the 3FHL variable sources and PG 1553+113 (temporary files)
| VarSources3FHL_w9_w250_images            | Output of the full-sky HEPA pipeline in FT mode, run on mission weeks 9-250 on the 3FHL variable sources and PG 1553+113 (pictures)
|------------------------------------------|-----------------------------|