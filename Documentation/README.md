# Documentation

In this folder we report several useful guides on how to install and use the software necessary to do high energy gamma ray astrophysics analysis. At the moment of writing, we have available:
- [Fermitools](Documentation/Fermitools), a guide on how to install a `conda` environment with the Fermitools, Fermipy and the HEASOFT toolset.
- [s3df](Documentation/s3df), a guide on how to use the SLAC Computing Facility (S3DF).

Guide written and maintained by [P. Monti-Guarnieri](mailto:pietro.monti-guarnieri@phd.units.it). 

Last updated on: 24.05.2024