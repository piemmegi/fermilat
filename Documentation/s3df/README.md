# A short guide on how to use S3DF and the SLAC Computing Facility

Note: from now onwards, we assume to be writing a guide about a user with username "pietromg". Change all the code accordingly, if it must be used for another user.

Main references:
- [Best practices](https://confluence.slac.stanford.edu/display/SAS/Best+Practices+for+Using+the+SLAC+Batch+System) on how to use the Batch System, provided on Fermi Confluence.
- [S3DF Cheatsheet](https://confluence.slac.stanford.edu/display/SAS/Get+Started+on+S3DF+-+Cheat+Sheet), provided on Fermi Confluence.
- [Future data paths](https://confluence.slac.stanford.edu/pages/viewpage.action?pageId=473827825), after the full migration to the new syste,

Guide written and maintained by [P. Monti-Guarnieri](mailto:pietro.monti-guarnieri@phd.units.it). 

Last updated on: 2024.12.30

## How to access the SLAC Farm
First of all, ssh to the login node using the correct Message Authentication Code (MAC). In particular, we choose `-m hmac-sha1`, but the full list of available ones is given by `ssh -Q mac` and anyone working should be fine:
```bash
ssh -m hmac-sha1 pietromg@s3dflogin.slac.stanford.edu 
```

Then, insert the password of the SLAC account and log-in. At this point we are at the *bastion* node, meaning that we can't do actual calculations here. Instead, we should ssh to the interactive nodes:
```bash
ssh pietromg@fermi-devl # or even just: ssh fermi-devl
```

There are several folders available where one can write files:
- A shared space in weka, which is backed up. Here we have 30 GB of available memory and thus should be where our **code** goes:
```bash
cd /sdf/home/p/pietromg
```
- A space available on the Fermi group quota at SLAC. Here we have 1 TB of available memory. This is accessible either directly, or through a symlink placed in the home folder at `fermi-devl`:
```bash
cd /sdf/group/fermi/user/pietromg         # option 1, direct path
cd /sdf/home/p/pietromg && cd fermi-user  # option 2, symlink from user home on weka
```
- A space available in the `scratch` sector of the cluster. This is where temporary files should go, since it is frequently cleaned and not backed up. Here we have 100 GB of available memory:
```
cd /sdf/scratch/users/p/pietromg
```

## How to install and use Conda and the Fermitools
There is a shared set of `conda` environments, among which there are a few with the Fermitools already installed. They can be accessed by first sourcing a common file, and then by using conda to show the list of environments:
```bash
source /sdf/group/fermi/sw/conda/bin/activate fermitools-2.2.0
conda env list
conda activate <env_name>
```

There is also another source available for a common (shared) conda release. It can be accessed by sourcing a script and then using the usual conda commands:
```bash
source /sdf/group/fermi/sw/conda/bin/activate
conda activate
```

There is also the possibility of installing a custom conda environment in s3df. To do so, first of all, download the latest miniconda release and install it using folders created for the occasion. Replace the user name where needed. Note that the `/.../Software/conda` folder should not exist: it will be created by the installer in the regular procedure.
```bash
mkdir -p /sdf/scratch/users/p/pietromg/sfw_installer
mkdir -p /sdf/home/p/pietromg/fermi-user/Software
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O /sdf/scratch/users/p/pietromg/sfw_installer/Miniconda3-latest-Linux-x86_64.sh
bash /sdf/scratch/users/p/pietromg/sfw_installer/Miniconda3-latest-Linux-x86_64.sh -p /sdf/home/p/pietromg/fermi-user/Software/conda
```

Now accept the license and install as suggested. Then, exit and re-enter the ssh connection, to make sure that conda is correctly initialized. At this point, you can create a conda environment with the usual procedure and install whatever you want. Note that installing "root" will install pyroot even if ROOT is not technically installed in the s3df cluster. This is curious, but it works. Moreover, note that this release will conflict with the shared ones, if they are activated through the above reported commands.

## OnDemand & Jupyter
`OnDemand` is the service which allows to access all the files stored in the sd3f account. Access with your confluence credentials through the [main link](https://s3df.slac.stanford.edu/pun/sys/dashboard). After accessing, you can navigate to see and download all your files. Otherwise, you can also launch a JP Notebook/Lab instance. In order to do so, click `Interactive Apps` and then `Jupyter`. Then choose:
- **Jupyter Image**: Custom, from Conda Environment
- **Commands to instantiate Jupyter** (note that, in the S3DF cluster, after activating conda, che `echo $CONDA_PREFIX` command will show what path to actually put in the first of these lines):
```bash
export CONDA_PREFIX=/sdf/home/p/pietromg/fermi-user/Software/conda/envs/fermipmg
export PATH=${CONDA_PREFIX}/bin/:$PATH
source ${CONDA_PREFIX}/etc/profile.d/conda.sh
conda env list
conda activate fermipmg
```
- **Use JupyterLab instead of Jupyter Notebook?** Yes (tick)
- **Run on cluster type**: interactive, iana
- **Number of hours**: the minimum amount required for the work (usually 1-6)

Finally, click on "Launch" and wait a bit, then you can use it!

# Useful folders
## CalOnly data
```bash
/sdf/data/fermi/g/canda/CalOnly/*
```