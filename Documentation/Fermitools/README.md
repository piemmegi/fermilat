# Installation of the Fermitools & Fermipy
Guide written and maintained by [P. Monti-Guarnieri](mailto:pietro.monti-guarnieri@phd.units.it). 

Last updated on: 24.05.2024

---

Assuming conda is already installed, create a new environment:
```bash
conda install -c conda-forge nb_conda_kernels mamba
conda create -n fermipmg -c conda-forge python=3.9
conda activate fermipmg
```

Install fermitools, fermipy and some data analysis package:
```bash
mamba install -c conda-forge -c fermi fermitools
mamba install -c conda-forge -c fermi fermipy
mamba install -c conda-forge numpy=1.23
mamba install -c conda-forge h5py hdf5 ipython ipykernel jupyter jupyterlab
mamba install -c conda-forge pandas pypdf2 scikit-learn seaborn uproot lmfit
mamba install -c conda-forge jupyter_client jupyter_core traitlets ipython_genutils
mamba install -c conda-forge astropy=5.3.4
```

Install system-wide the XML tools
```bash
pip install LATSourceModel
```

Make an alias for the activation command of the environment:
```bash
nano ~/.bashrc
alias fermi="conda activate fermipmg"
source ~/.bashrc
```

# Installation of HEASOFT tools
It is not advised to install the pre-compiled binary, but it can be done. In this case, download the correct binary file from [here](https://heasarc.gsfc.nasa.gov/docs/software/lheasoft/download.html), choosing to install at least `futils` and `ftools`. Then unzip and untar the donwloaded file in an installation folder:
```bash
gzip -d heasoft-6.32.1rhel8.tar.gz
tar -xvf heasoft-6.32.1rhel8.tar
```

Install the required dependencies
```bash
sudo dnf -y install readline-devel
sudo dnf -y install ncurses-devel
sudo dnf -y install libXt-devel
```

Configure the software. Proceed only if the last line of `config.txt` reports "Finished".
```bash
cd heasoft-6.32.1/x86_64-pc-linux-gnu-libc2.28/BUILD_DIR/
./configure > config.txt
```

Finally, inizialize the environmental variable for HEASOFT.
```bash
nano ~/.bashrc
export HEADAS=/home/pmontigu/heasoft-6.32.1/x86_64-pc-linux-gnu-libc2.28
source $HEADAS/headas-init.sh
source ~/.bashrc
```